# CEC1702 Firmware

Firmware for communication with ChipWhisperer board using **SimpleSerial v.1_1**

### Implemented features

- basic commands:
    - c -callback of 16B message
    - k - set 16B key for AES256 encryption
    - x - reset

### Commands in detail

- c-command
- k-command
- e-command
- d-command
- x-command

### TODO

- [x] Solve how to send ACK to ChipWhisperer (ChW) board
- [ ] Implement AES encrypt / decrypt using HW functions properly
- [ ] Capturing traces for CPA/DPA of AES working
- [ ] Include *paillier* and implement encryption / decryption
- [ ] Include *bigi* and add commands to demonstrate *bigi* operations
- [ ] Implement RSA encrypt / decrypt using bigi
- [ ] Implement RSA encrypt / decrypt using HW functions
- [x] Refactor code from ChipWhisperer to make it readable
- [x]  Include Jupyter notebook demonstrating communication ChW-CEC1702