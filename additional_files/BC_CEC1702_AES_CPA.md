# CEC1702 Communication

## 1. ChipWhisperer Info
### ChipWhisperer-Lite (Capture + UFO), Includes SCAPACK-L1/SCAPACK-L2

This package uses a ChipWhisperer-Lite Capture board, along with a CW308 UFO target board. You'll need to attach the CW308 board to your ChipWhisperer-Lite Capture, and plug in your choosen target board. The suggested target for most tutorials is the `CW308_STM32F3` - you can tell this target by checking the part number on the chip, look for a `STM32F303`.

Here is an overview photo of the target plugged in:

<img src="img/cwcapture_ufo.jpg" alt="CW-Lite Plugged Into UFO" width=600>

See Section below on UFO Target Settings.

### UFO Target Settings

The UFO Board comes by default with working settings for most targets. A summary of the default jumper settings is included below for you, see the product documentation for more details.

<img src="img/cwufo_stm32f3.jpg" alt="UFO Board" width=500>

#### UFO Default Settings

* J1  - 3.3V (CW3.3V)
* J3  - HS2/OUT
* J14 - FILT/FILT_LP/FILT_HP
* J16 - No connection

* S1 -  ON

* 3.3V SRC - J1/CW
* VADJ SRC - 3.3V
* 1.2V, 1.8V, 2.5V, 3.3V, LDO SRC - J1/CW
* 5V SRC - J1/CW

## 2. Connecting to ChipWhisperer

### Import CW and default setup 


```python
import chipwhisperer as cw
print(".Import DONE")
scope = cw.scope()
print("..Scope set")
target = cw.target(scope, cw.targets.SimpleSerial) #cw.targets.SimpleSerial can be omitted
print("...Target set")
scope.default_setup()
print("....Scope setup done")
```

### Scope set up for successful CPA attack
The CEC1702 has no option to run off on an external clock. To make CPA attacks easier, a 12MHz PWM signal is generated on the CLKOUT pin. This can be used by putting a jumper on the lower HS1/IN pins on J3 and running the ChipWhisperer off of the ExtClk.

****
### ⚠ Ensure J3 on UFO Board routines CLKFB to HS1/IN ( the lowest jumper!) ⚠
****


```python
scope.gain.gain = 40
scope.gain.mode = "high"
scope.adc.samples = 400
scope.adc.offset = 0
scope.adc.basic_mode = "rising_edge"
scope.clock.clkgen_src = "extclk"
scope.clock.clkgen_mul = 8
scope.clock.clkgen_div = 2
scope.clock.adc_src = "clkgen_x1"
scope.trigger.triggers = "tio4"
scope.io.tio1 = "serial_rx"
scope.io.tio2 = "serial_tx"
scope.io.hs2 = None

ext_freq = scope.clock.freq_ctr
print("Input frequency of %d Hz" % ext_freq)

if ext_freq > 11850000 and ext_freq < 12150000:
    print("CLK frequency is in expected range. 👍")
else:
    print("WARNING: CLK frequency outside of expected range.")
```


```python
print("scope.clock.freq_ctr_src = ", scope.clock.freq_ctr_src)
print("scope.io.extclk_src = ", scope.io.extclk_src)
print("scope.clock.adc_freq = ", scope.clock.adc_freq)
print("\033[1m***************************************\033[0m")
print(f"\033[1mSample rate = ({(scope.clock.freq_ctr / 1000_000):2.4} MHz * {scope.clock.clkgen_mul}) / {scope.clock.clkgen_div} = {(scope.clock.adc_freq/ 1000_000):2.4} MHz\033[0m")
```

And that's it! Your ChipWhisperer is now setup and ready to attack a target. 

**NOTE: You'll need to disconnect the scope/target before connecting again, like you would in another notebook. This can be done with `scope.dis()` and `target.dis()`**

## 3. Capturing traces
****
### ⚠ Ensure Firmware is built in MikroC IDE and uploaded to CEC1702 via HW Programmer ⚠
****

Communication with targets, which is done through the `SimpleSerial target` object we got earlier, is grouped into two categories:

1. Raw serial via `target.read()`, `target.write()`, `target.flush()`, etc. 

1. SimpleSerial commands via `target.simpleserial_read()`, `target.simpleserial_write()`, `target.simpleserial_wait_ack()`, etc.

The firmware we uploaded uses the simpleserial protocol (https://wiki.newae.com/SimpleSerial).

### Example use of commands

**In software AES implemention** is only supported set key and encryption, **decryption is NOT implemneted.**

**In software implemention is only implemented AES128-ECB**

Hardware implemtation of AES supports set key, encryption and decryption.

Supported AES cipher macros: 
* **AES128__SW**
* **AES128__HW**



```python
from time import sleep
universal_bytes = [0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f]
cipher = bytearray("AES128__HW", 'utf-8')
key = bytearray(universal_bytes)
msg = bytearray(universal_bytes)

#set cipher as AES128-SW version
print("..Set cipher")
target.simpleserial_write('s', cipher)
target.simpleserial_wait_ack()
print("..Cipher set")

#set key for AES128
print("...Set key")
target.simpleserial_write('k', key)
target.simpleserial_wait_ack()
print ("...Key set")

#perform encryption
print ("...Start the encryption")
target.simpleserial_write('p', msg)
print ("...Read a result from CEC")
encypted_msg = target.simpleserial_read('r', 16, '\n', 1000, True)
print ("...Encryption performed: ", encypted_msg)


#perform decryption

print ("...Start the decryption")
target.simpleserial_write('c', encypted_msg)
print ("...Read a result from CEC")
decrypted_msg = target.simpleserial_read('r', 16, '\n', 1000, True) 
print ("...Decryption performed: ", decrypted_msg)

#print result

print ("...Evaluationg results")
print ("Result is:")
if msg != decrypted_msg:
    print("AES operations went wrong: MSG:%s  != DECRYPTED_MSG:%s", msg_str, decrypted_msg)
else :
    print ("AES succeeded!!!")
```

### Capture loop

The right choice of number of traces is very import.\
From previous experience it is observed that:
<pre>
   100 traces is very little.
 3 000 traces help to crack 10B of 16B key.
10 000 traces crack whole key.
</pre>

Maximum of 24400 samples per trace is recommended.\
Software implementation of AES128 is attacked. 


```python
input_key = "69b00b6969b00b6969b00b6969b00b69" #key in 16B hex representation
TRACES = 10000 # how many traces will be captured
scope.adc.samples = 24400 #The maximum number of samples is hardware-dependent: - cwlite: 24400 - cw1200: 96000
cipher = bytearray("AES128__HW", 'utf-8') 
target.simpleserial_write('s', cipher) #set cipher as AES128-SW version
target.simpleserial_wait_ack()
```


```python
from tqdm.notebook import tnrange, trange
import numpy as np
import time
from binascii import hexlify  # pro pouziti vypisu klice a dalsich 128-bit hodnot
ktp = cw.ktp.Basic()
traces = []
input_key = "69b00b6969b00b6969b00b6969b00b69"
key = bytearray.fromhex(input_key)

#nastavti klic pred loop
# target.simpleserial_write('k', key)
# target.simpleserial_wait_ack()

for i in tnrange(TRACES, desc='Capturing traces'):
    plaintext = ktp.next_text()  # manualni vytvoreni dalsiho plaintextu
    trace = cw.capture_trace(scope, target, plaintext, key)
    
    if trace is None:
        print("PROBLEM OCCURED")
        continue
    traces.append(trace)
    
#     target.flush()
#     scope.arm()
#     plaintext = ktp.next_text()  # manualni vytvoreni dalsiho plaintextu
#     target.simpleserial_write('p', plaintext)
#     timeout_of_capturing = scope.capture()
#     if timeout_of_capturing: 
#         print("Capture timed out.")
#         continue
    
#     trace = scope.get_last_trace(True)
#     if trace is None:
#         print("Trace is empty")
#         continue
#     traces.append(trace)
#     #ciphertext = target.simpleserial_read('r', 16, '\n', 1000, True)

trace_array = np.asarray([trace.wave for trace in traces])  # if you prefer to work with numpy array for number crunching
plaintext_array = np.asarray([trace.textin for trace in traces])
ciphertext_array = np.asarray([trace.textout for trace in traces])
known_key = np.asarray(trace.key)  # for fixed key, these keys are all the same
```

### Saving data into files for further use


```python
np.savetxt('CPA_AES_CEC_1702/key.txt', known_key, fmt='%02x', delimiter = ',', newline = ' ')
np.savetxt('CPA_AES_CEC_1702/plaintext.txt', plaintext_array, fmt='%02x', delimiter = ' ' )
np.savetxt('CPA_AES_CEC_1702/ciphertext.txt', ciphertext_array, fmt='%02x', delimiter = ' ' )
np.savetxt('CPA_AES_CEC_1702/AES_traces.txt', trace_array, fmt='%+1.10f', delimiter = ' ' )
```

Check print of files, if they are not empty


```bash
%%bash
printf "key.txt in hex:\n"
cat key.txt
printf "\n\nplaintext.txt in hex:\n"
head -n 2 plaintext.txt
printf "...\n\nciphertext.txt in hex:\n"
head -n 2 ciphertext.txt
printf "...\n\nAES_traces.txt in float with 10 decimal numbers:\n"
touch tmp
head -n 2 AES_traces.txt > tmp
cut -d' '  -f1 tmp
printf "...\n\n"
rm tmp
```

### Disconnect devices for use in other Notebooks


```python
scope.dis()
target.dis()
```
