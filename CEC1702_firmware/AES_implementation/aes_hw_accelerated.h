#ifndef AES_HW_ACCELERATED
#define AES_HW_ACCELERATED

uint8_t hw_aes_key_set (uint8_t* k);

uint8_t hw_aes_encrypt (uint8_t* plaintext);

uint8_t hw_aes_decrypt (uint8_t* ciphertext);

#endif