#include <stdio.h>
#include <stdint.h>

#include "../debug.h"
#include "../simpleserial/simpleserial.h"
#include "../device_setup/cec1702_setup.h"
/* AES-128 simple implementation template and testing */

/*
Author: Lukas Danek, daneklu2@fit.cvut.cz
Template: Jiri Bucek 2017
AES specification:
http://csrc.nist.gov/publications/fips/fips197/fips-197.pdf
*/

/* AES Constants */
#define KEY_LENGTH_DEF 4;
#define ROUND_CNT_DEF  10;

static const uint16_t KEY_LENGTH = 4;
static const uint16_t ROUND_CNT  = 10;
//mask for each byte of a word
static const uint32_t BYTE_MASK[4] = {0x00ffffff, 0xff00ffff, 0xffff00ff, 0xffffff00};
// forward sbox
static const uint8_t SBOX[256] = {
  //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
  0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
  0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
  0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
  0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
  0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
  0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
  0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
  0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
  0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
  0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
  0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
  0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
  0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
  0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
  0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
  0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 
}; 

static const uint8_t rCon[12] = {
        0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36,
};

/*AES global variables*/
static uint32_t expKey[44];

/* AES state type */
typedef uint32_t t_state[4];

uint32_t word(uint8_t a0, uint8_t a1, uint8_t a2, uint8_t a3) {
        return a0 | (uint32_t)a1 << 8 | (uint32_t)a2 << 16 | (uint32_t)a3 << 24;
}

uint8_t wbyte(uint32_t w, uint16_t pos) {
        return (w >> (pos * 8)) & 0xff;
}

// **************** AES  functions ****************
uint32_t subWord(uint32_t w) {
        return word(SBOX[wbyte(w, 0)], SBOX[wbyte(w, 1)], SBOX[wbyte(w, 2)], SBOX[wbyte(w, 3)]);
}

void subBytes(t_state s) {
        s[0] = subWord(s[0]);
        s[1] = subWord(s[1]);
        s[2] = subWord(s[2]);
        s[3] = subWord(s[3]);
}

void shiftRows(t_state s) {
        uint8_t row_byte[4];
        uint16_t i;
        for (i = 0; i < 3; ++i) {
                row_byte[0] = wbyte(s[0], i + 1);
                row_byte[1] = wbyte(s[1], i + 1);
                row_byte[2] = wbyte(s[2], i + 1);
                row_byte[3] = wbyte(s[3], i + 1);
                s[0] = (s[0] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 1) % 4]) << (8 * (i + 1)));
                s[1] = (s[1] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 2) % 4]) << (8 * (i + 1)));
                s[2] = (s[2] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 3) % 4]) << (8 * (i + 1)));
                s[3] = (s[3] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 0) % 4]) << (8 * (i + 1)));
        }
}

uint8_t xtime(uint8_t a) {
        if (a >> 7) {
                return ((a << 1) ^ 0x1b); 
        }
        return (a << 1);
}

// not mandatory - mix a single column
uint32_t mixColumn(uint32_t c) {
        uint8_t out_0, out_1, out_2, out_3;
        uint8_t byte_0 = wbyte(c, 0);
        uint8_t byte_1 = wbyte(c, 1);
        uint8_t byte_2 = wbyte(c, 2);
        uint8_t byte_3 = wbyte(c, 3);

        out_0 = xtime(byte_0) ^ byte_1 ^ xtime(byte_1) ^ byte_2 ^ byte_3;
        out_1 = byte_0 ^ xtime(byte_1) ^ byte_2 ^ xtime(byte_2) ^ byte_3;
        out_2 = byte_0 ^ byte_1 ^ xtime(byte_2) ^ byte_3 ^ xtime(byte_3);
        out_3 = byte_0 ^ xtime(byte_0) ^ byte_1 ^ byte_2 ^ xtime(byte_3);
        return word (out_0, out_1, out_2, out_3);
}


void mixColumns(t_state s) {
        s[0] = mixColumn(s[0]);
        s[1] = mixColumn(s[1]);
        s[2] = mixColumn(s[2]);
        s[3] = mixColumn(s[3]);
}

/*
* Key expansion from 128bits (4*32b)
* to 11 round keys (11*4*32b)
* each round key is 4*32b
*/
uint32_t rotateWord (uint32_t w) {
        uint8_t byte_0 = wbyte(w, 0);
        return ((w >> 8) | ((uint32_t)(byte_0) << 24)); 
}

void expandKey(uint8_t k[16], uint32_t ek[]) {
        //first round key is a key itself
        uint16_t i;
        uint32_t tmp;
        for (i = 0; i < KEY_LENGTH; ++i){
                ek[i] = word(k[(i * KEY_LENGTH) + 0], k[(i * KEY_LENGTH) + 1], k[(i * KEY_LENGTH) + 2], k[(i * KEY_LENGTH) + 3]);
        }

        for (i = KEY_LENGTH; i < 44; ++i) {
                tmp = ek[i - 1];
                if ((i % KEY_LENGTH) == 0 ) {
                        tmp = subWord (rotateWord(tmp)) ^ rCon[i / KEY_LENGTH];
                }
                ek[i] = ek[i - KEY_LENGTH] ^ tmp;
        }
}

/* Adding expanded round key (prepared before) */
void addRoundKey(t_state s, uint32_t ek[], uint16_t index) {
        s[0] = s[0] ^ ek[index + 0];
        s[1] = s[1] ^ ek[index + 1];
        s[2] = s[2] ^ ek[index + 2];
        s[3] = s[3] ^ ek[index + 3];
}

uint8_t sw_aes_encrypt (uint8_t* plaintext) {
        uint16_t i;
        uint8_t ciphertext[16];
        t_state state;

        for (i = 0; i < 4; ++i){
                state[i] = word(plaintext[(i << 2) + 0],  plaintext[(i << 2) + 1],  plaintext[(i << 2) + 2],  plaintext[(i << 2) + 3]);
        }

        trigger_high();

        //add key before first round
        addRoundKey(state, expKey, 0);

        for (i = 1; i < ROUND_CNT; ++i) {
                subBytes (state);
                shiftRows(state);
                mixColumns(state);
                addRoundKey(state, expKey, (i << 2));
        }

        //last round without mixColumns
        subBytes (state);
        shiftRows(state);
        addRoundKey(state, expKey, ROUND_CNT << 2);

        trigger_low();
        
        //make correct output
        for (i = 0; i < 16; i++) {
                if (i < 4) ciphertext[i] = wbyte(state[0], i % 4);
                else if (i < 8) ciphertext[i] = wbyte(state[1], i % 4);
                else if (i < 12) ciphertext[i] = wbyte(state[2], i % 4);
                else ciphertext[i] = wbyte(state[3], i % 4);
        }
        simpleserial_put('r', 16, ciphertext);
        return 0x00;
}

uint8_t sw_aes_key_set (uint8_t* k) {
        expandKey(k, expKey);
        return 0x00;
}

uint8_t sw_aes_decrypt (uint8_t* ciphertext) {
        ///TODO!
}