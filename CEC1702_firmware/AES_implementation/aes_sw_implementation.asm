_word:
;aes_sw_implementation.c,55 :: 		uint32_t word(uint8_t a0, uint8_t a1, uint8_t a2, uint8_t a3) {
; a3 start address is: 12 (R3)
; a2 start address is: 8 (R2)
; a1 start address is: 4 (R1)
; a0 start address is: 0 (R0)
SUB	SP, SP, #4
; a3 end address is: 12 (R3)
; a2 end address is: 8 (R2)
; a1 end address is: 4 (R1)
; a0 end address is: 0 (R0)
; a0 start address is: 0 (R0)
; a1 start address is: 4 (R1)
; a2 start address is: 8 (R2)
; a3 start address is: 12 (R3)
;aes_sw_implementation.c,56 :: 		return a0 | (uint32_t)a1 << 8 | (uint32_t)a2 << 16 | (uint32_t)a3 << 24;
UXTB	R4, R1
; a1 end address is: 4 (R1)
LSLS	R4, R4, #8
ORR	R5, R0, R4, LSL #0
; a0 end address is: 0 (R0)
UXTB	R4, R2
; a2 end address is: 8 (R2)
LSLS	R4, R4, #16
ORRS	R5, R4
UXTB	R4, R3
; a3 end address is: 12 (R3)
LSLS	R4, R4, #24
ORR	R4, R5, R4, LSL #0
MOV	R0, R4
;aes_sw_implementation.c,57 :: 		}
L_end_word:
ADD	SP, SP, #4
BX	LR
; end of _word
_wbyte:
;aes_sw_implementation.c,59 :: 		uint8_t wbyte(uint32_t w, uint16_t pos) {
; pos start address is: 4 (R1)
; w start address is: 0 (R0)
SUB	SP, SP, #4
; pos end address is: 4 (R1)
; w end address is: 0 (R0)
; w start address is: 0 (R0)
; pos start address is: 4 (R1)
;aes_sw_implementation.c,60 :: 		return (w >> (pos * 8)) & 0xff;
LSLS	R2, R1, #3
UXTH	R2, R2
; pos end address is: 4 (R1)
LSR	R2, R0, R2
; w end address is: 0 (R0)
AND	R2, R2, #255
UXTB	R0, R2
;aes_sw_implementation.c,61 :: 		}
L_end_wbyte:
ADD	SP, SP, #4
BX	LR
; end of _wbyte
_subWord:
;aes_sw_implementation.c,64 :: 		uint32_t subWord(uint32_t w) {
; w start address is: 0 (R0)
SUB	SP, SP, #16
STR	LR, [SP, #0]
MOV	R3, R0
; w end address is: 0 (R0)
; w start address is: 12 (R3)
;aes_sw_implementation.c,65 :: 		return word(SBOX[wbyte(w, 0)], SBOX[wbyte(w, 1)], SBOX[wbyte(w, 2)], SBOX[wbyte(w, 3)]);
MOVS	R1, #3
MOV	R0, R3
BL	_wbyte+0
MOVW	R1, #lo_addr(aes_sw_implementation_SBOX+0)
MOVT	R1, #hi_addr(aes_sw_implementation_SBOX+0)
ADDS	R1, R1, R0
LDRB	R1, [R1, #0]
STRB	R1, [SP, #12]
MOVS	R1, #2
MOV	R0, R3
BL	_wbyte+0
MOVW	R1, #lo_addr(aes_sw_implementation_SBOX+0)
MOVT	R1, #hi_addr(aes_sw_implementation_SBOX+0)
ADDS	R1, R1, R0
LDRB	R1, [R1, #0]
STRB	R1, [SP, #8]
MOVS	R1, #1
MOV	R0, R3
BL	_wbyte+0
MOVW	R1, #lo_addr(aes_sw_implementation_SBOX+0)
MOVT	R1, #hi_addr(aes_sw_implementation_SBOX+0)
ADDS	R1, R1, R0
LDRB	R1, [R1, #0]
STRB	R1, [SP, #4]
MOVS	R1, #0
MOV	R0, R3
; w end address is: 12 (R3)
BL	_wbyte+0
MOVW	R1, #lo_addr(aes_sw_implementation_SBOX+0)
MOVT	R1, #hi_addr(aes_sw_implementation_SBOX+0)
ADDS	R1, R1, R0
LDRB	R1, [R1, #0]
UXTB	R4, R1
LDRB	R3, [SP, #12]
LDRB	R2, [SP, #8]
LDRB	R1, [SP, #4]
UXTB	R0, R4
BL	_word+0
;aes_sw_implementation.c,66 :: 		}
L_end_subWord:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _subWord
_subBytes:
;aes_sw_implementation.c,68 :: 		void subBytes(t_state s) {
; s start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
MOV	R6, R0
; s end address is: 0 (R0)
; s start address is: 24 (R6)
;aes_sw_implementation.c,69 :: 		s[0] = subWord(s[0]);
MOV	R1, R6
STR	R1, [SP, #4]
LDR	R1, [R6, #0]
MOV	R0, R1
BL	_subWord+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,70 :: 		s[1] = subWord(s[1]);
ADDS	R1, R6, #4
STR	R1, [SP, #4]
ADDS	R1, R6, #4
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_subWord+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,71 :: 		s[2] = subWord(s[2]);
ADDW	R1, R6, #8
STR	R1, [SP, #4]
ADDW	R1, R6, #8
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_subWord+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,72 :: 		s[3] = subWord(s[3]);
ADDW	R1, R6, #12
STR	R1, [SP, #4]
ADDW	R1, R6, #12
; s end address is: 24 (R6)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_subWord+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,73 :: 		}
L_end_subBytes:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _subBytes
_shiftRows:
;aes_sw_implementation.c,75 :: 		void shiftRows(t_state s) {
; s start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
; s end address is: 0 (R0)
; s start address is: 0 (R0)
;aes_sw_implementation.c,78 :: 		for (i = 0; i < 3; ++i) {
; i start address is: 28 (R7)
MOVS	R7, #0
; s end address is: 0 (R0)
; i end address is: 28 (R7)
MOV	R6, R0
L_shiftRows0:
; i start address is: 28 (R7)
; s start address is: 24 (R6)
; s start address is: 24 (R6)
; s end address is: 24 (R6)
CMP	R7, #3
IT	CS
BCS	L_shiftRows1
; s end address is: 24 (R6)
;aes_sw_implementation.c,79 :: 		row_byte[0] = wbyte(s[0], i + 1);
; s start address is: 24 (R6)
ADD	R1, SP, #4
STR	R1, [SP, #8]
ADDS	R2, R7, #1
LDR	R1, [R6, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #8]
STRB	R0, [R1, #0]
;aes_sw_implementation.c,80 :: 		row_byte[1] = wbyte(s[1], i + 1);
ADD	R1, SP, #4
ADDS	R1, R1, #1
STR	R1, [SP, #8]
ADDS	R2, R7, #1
ADDS	R1, R6, #4
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #8]
STRB	R0, [R1, #0]
;aes_sw_implementation.c,81 :: 		row_byte[2] = wbyte(s[2], i + 1);
ADD	R1, SP, #4
ADDS	R1, R1, #2
STR	R1, [SP, #8]
ADDS	R2, R7, #1
ADDW	R1, R6, #8
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #8]
STRB	R0, [R1, #0]
;aes_sw_implementation.c,82 :: 		row_byte[3] = wbyte(s[3], i + 1);
ADD	R1, SP, #4
ADDS	R1, R1, #3
STR	R1, [SP, #8]
ADDS	R2, R7, #1
ADDW	R1, R6, #12
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #8]
STRB	R0, [R1, #0]
;aes_sw_implementation.c,83 :: 		s[0] = (s[0] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 1) % 4]) << (8 * (i + 1)));
LDR	R3, [R6, #0]
RSB	R1, R7, #2
UXTH	R1, R1
LSLS	R2, R1, #2
MOVW	R1, #lo_addr(aes_sw_implementation_BYTE_MASK+0)
MOVT	R1, #hi_addr(aes_sw_implementation_BYTE_MASK+0)
ADDS	R1, R1, R2
LDR	R1, [R1, #0]
AND	R4, R3, R1, LSL #0
ADDS	R3, R7, #1
UXTH	R3, R3
AND	R1, R3, #3
UXTH	R1, R1
ADD	R5, SP, #4
ADDS	R1, R5, R1
LDRB	R1, [R1, #0]
UXTB	R2, R1
LSLS	R1, R3, #3
UXTH	R1, R1
LSL	R1, R2, R1
ORR	R1, R4, R1, LSL #0
STR	R1, [R6, #0]
;aes_sw_implementation.c,84 :: 		s[1] = (s[1] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 2) % 4]) << (8 * (i + 1)));
ADDS	R4, R6, #4
ADDS	R1, R6, #4
LDR	R3, [R1, #0]
RSB	R1, R7, #2
UXTH	R1, R1
LSLS	R2, R1, #2
MOVW	R1, #lo_addr(aes_sw_implementation_BYTE_MASK+0)
MOVT	R1, #hi_addr(aes_sw_implementation_BYTE_MASK+0)
ADDS	R1, R1, R2
LDR	R1, [R1, #0]
ANDS	R3, R1
ADDS	R1, R7, #2
UXTH	R1, R1
AND	R1, R1, #3
UXTH	R1, R1
ADDS	R1, R5, R1
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDS	R1, R7, #1
UXTH	R1, R1
LSLS	R1, R1, #3
UXTH	R1, R1
LSL	R1, R2, R1
ORR	R1, R3, R1, LSL #0
STR	R1, [R4, #0]
;aes_sw_implementation.c,85 :: 		s[2] = (s[2] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 3) % 4]) << (8 * (i + 1)));
ADDW	R4, R6, #8
ADDW	R1, R6, #8
LDR	R3, [R1, #0]
RSB	R1, R7, #2
UXTH	R1, R1
LSLS	R2, R1, #2
MOVW	R1, #lo_addr(aes_sw_implementation_BYTE_MASK+0)
MOVT	R1, #hi_addr(aes_sw_implementation_BYTE_MASK+0)
ADDS	R1, R1, R2
LDR	R1, [R1, #0]
ANDS	R3, R1
ADDS	R1, R7, #3
UXTH	R1, R1
AND	R1, R1, #3
UXTH	R1, R1
ADDS	R1, R5, R1
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDS	R1, R7, #1
UXTH	R1, R1
LSLS	R1, R1, #3
UXTH	R1, R1
LSL	R1, R2, R1
ORR	R1, R3, R1, LSL #0
STR	R1, [R4, #0]
;aes_sw_implementation.c,86 :: 		s[3] = (s[3] & BYTE_MASK[2 - i]) | ((uint32_t)(row_byte[(i + 0) % 4]) << (8 * (i + 1)));
ADDW	R4, R6, #12
ADDW	R1, R6, #12
LDR	R3, [R1, #0]
RSB	R1, R7, #2
UXTH	R1, R1
LSLS	R2, R1, #2
MOVW	R1, #lo_addr(aes_sw_implementation_BYTE_MASK+0)
MOVT	R1, #hi_addr(aes_sw_implementation_BYTE_MASK+0)
ADDS	R1, R1, R2
LDR	R1, [R1, #0]
ANDS	R3, R1
AND	R1, R7, #3
UXTH	R1, R1
ADDS	R1, R5, R1
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDS	R1, R7, #1
UXTH	R1, R1
LSLS	R1, R1, #3
UXTH	R1, R1
LSL	R1, R2, R1
ORR	R1, R3, R1, LSL #0
STR	R1, [R4, #0]
;aes_sw_implementation.c,78 :: 		for (i = 0; i < 3; ++i) {
ADDS	R7, R7, #1
UXTH	R7, R7
;aes_sw_implementation.c,87 :: 		}
; s end address is: 24 (R6)
; i end address is: 28 (R7)
IT	AL
BAL	L_shiftRows0
L_shiftRows1:
;aes_sw_implementation.c,88 :: 		}
L_end_shiftRows:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _shiftRows
_xtime:
;aes_sw_implementation.c,90 :: 		uint8_t xtime(uint8_t a) {
; a start address is: 0 (R0)
SUB	SP, SP, #4
; a end address is: 0 (R0)
; a start address is: 0 (R0)
;aes_sw_implementation.c,91 :: 		if (a >> 7) {
LSRS	R1, R0, #7
UXTB	R1, R1
CMP	R1, #0
IT	EQ
BEQ	L_xtime3
;aes_sw_implementation.c,92 :: 		return ((a << 1) ^ 0x1b);
LSLS	R1, R0, #1
UXTH	R1, R1
; a end address is: 0 (R0)
EOR	R1, R1, #27
UXTB	R0, R1
IT	AL
BAL	L_end_xtime
;aes_sw_implementation.c,93 :: 		}
L_xtime3:
;aes_sw_implementation.c,94 :: 		return (a << 1);
; a start address is: 0 (R0)
LSLS	R1, R0, #1
; a end address is: 0 (R0)
UXTB	R0, R1
;aes_sw_implementation.c,95 :: 		}
L_end_xtime:
ADD	SP, SP, #4
BX	LR
; end of _xtime
_mixColumn:
;aes_sw_implementation.c,98 :: 		uint32_t mixColumn(uint32_t c) {
; c start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
MOV	R6, R0
; c end address is: 0 (R0)
; c start address is: 24 (R6)
;aes_sw_implementation.c,100 :: 		uint8_t byte_0 = wbyte(c, 0);
MOVS	R1, #0
MOV	R0, R6
BL	_wbyte+0
; byte_0 start address is: 12 (R3)
UXTB	R3, R0
;aes_sw_implementation.c,101 :: 		uint8_t byte_1 = wbyte(c, 1);
MOVS	R1, #1
MOV	R0, R6
BL	_wbyte+0
; byte_1 start address is: 16 (R4)
UXTB	R4, R0
;aes_sw_implementation.c,102 :: 		uint8_t byte_2 = wbyte(c, 2);
MOVS	R1, #2
MOV	R0, R6
BL	_wbyte+0
; byte_2 start address is: 20 (R5)
UXTB	R5, R0
;aes_sw_implementation.c,103 :: 		uint8_t byte_3 = wbyte(c, 3);
MOVS	R1, #3
MOV	R0, R6
; c end address is: 24 (R6)
BL	_wbyte+0
; byte_3 start address is: 8 (R2)
UXTB	R2, R0
;aes_sw_implementation.c,105 :: 		out_0 = xtime(byte_0) ^ byte_1 ^ xtime(byte_1) ^ byte_2 ^ byte_3;
UXTB	R0, R3
BL	_xtime+0
EOR	R1, R0, R4, LSL #0
STRB	R1, [SP, #4]
UXTB	R0, R4
BL	_xtime+0
LDRB	R1, [SP, #4]
EORS	R1, R0
UXTB	R1, R1
EORS	R1, R5
UXTB	R1, R1
EORS	R1, R2
; out_0 start address is: 24 (R6)
UXTB	R6, R1
;aes_sw_implementation.c,106 :: 		out_1 = byte_0 ^ xtime(byte_1) ^ byte_2 ^ xtime(byte_2) ^ byte_3;
UXTB	R0, R4
BL	_xtime+0
EOR	R1, R3, R0, LSL #0
UXTB	R1, R1
EORS	R1, R5
STRB	R1, [SP, #4]
UXTB	R0, R5
BL	_xtime+0
LDRB	R1, [SP, #4]
EORS	R1, R0
UXTB	R1, R1
EORS	R1, R2
; out_1 start address is: 28 (R7)
UXTB	R7, R1
;aes_sw_implementation.c,107 :: 		out_2 = byte_0 ^ byte_1 ^ xtime(byte_2) ^ byte_3 ^ xtime(byte_3);
EOR	R1, R3, R4, LSL #0
STRB	R1, [SP, #4]
UXTB	R0, R5
BL	_xtime+0
LDRB	R1, [SP, #4]
EORS	R1, R0
UXTB	R1, R1
EORS	R1, R2
STRB	R1, [SP, #4]
UXTB	R0, R2
BL	_xtime+0
LDRB	R1, [SP, #4]
EORS	R1, R0
; out_2 start address is: 32 (R8)
UXTB	R8, R1
;aes_sw_implementation.c,108 :: 		out_3 = byte_0 ^ xtime(byte_0) ^ byte_1 ^ byte_2 ^ xtime(byte_3);
UXTB	R0, R3
BL	_xtime+0
EOR	R1, R3, R0, LSL #0
UXTB	R1, R1
; byte_0 end address is: 12 (R3)
EORS	R1, R4
UXTB	R1, R1
; byte_1 end address is: 16 (R4)
EORS	R1, R5
; byte_2 end address is: 20 (R5)
STRB	R1, [SP, #4]
UXTB	R0, R2
; byte_3 end address is: 8 (R2)
BL	_xtime+0
LDRB	R1, [SP, #4]
EORS	R1, R0
;aes_sw_implementation.c,109 :: 		return word (out_0, out_1, out_2, out_3);
UXTB	R3, R1
UXTB	R2, R8
; out_2 end address is: 32 (R8)
UXTB	R1, R7
; out_1 end address is: 28 (R7)
UXTB	R0, R6
; out_0 end address is: 24 (R6)
BL	_word+0
;aes_sw_implementation.c,110 :: 		}
L_end_mixColumn:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _mixColumn
_mixColumns:
;aes_sw_implementation.c,113 :: 		void mixColumns(t_state s) {
; s start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
MOV	R9, R0
; s end address is: 0 (R0)
; s start address is: 36 (R9)
;aes_sw_implementation.c,114 :: 		s[0] = mixColumn(s[0]);
MOV	R1, R9
STR	R1, [SP, #4]
LDR	R1, [R9, #0]
MOV	R0, R1
BL	_mixColumn+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,115 :: 		s[1] = mixColumn(s[1]);
ADD	R1, R9, #4
STR	R1, [SP, #4]
ADD	R1, R9, #4
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_mixColumn+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,116 :: 		s[2] = mixColumn(s[2]);
ADD	R1, R9, #8
STR	R1, [SP, #4]
ADD	R1, R9, #8
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_mixColumn+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,117 :: 		s[3] = mixColumn(s[3]);
ADD	R1, R9, #12
STR	R1, [SP, #4]
ADD	R1, R9, #12
; s end address is: 36 (R9)
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_mixColumn+0
LDR	R1, [SP, #4]
STR	R0, [R1, #0]
;aes_sw_implementation.c,118 :: 		}
L_end_mixColumns:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _mixColumns
_rotateWord:
;aes_sw_implementation.c,125 :: 		uint32_t rotateWord (uint32_t w) {
; w start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R3, R0
; w end address is: 0 (R0)
; w start address is: 12 (R3)
;aes_sw_implementation.c,126 :: 		uint8_t byte_0 = wbyte(w, 0);
MOVS	R1, #0
MOV	R0, R3
BL	_wbyte+0
;aes_sw_implementation.c,127 :: 		return ((w >> 8) | ((uint32_t)(byte_0) << 24));
LSRS	R2, R3, #8
; w end address is: 12 (R3)
UXTB	R1, R0
LSLS	R1, R1, #24
ORR	R1, R2, R1, LSL #0
MOV	R0, R1
;aes_sw_implementation.c,128 :: 		}
L_end_rotateWord:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _rotateWord
_expandKey:
;aes_sw_implementation.c,130 :: 		void expandKey(uint8_t k[16], uint32_t ek[]) {
; ek start address is: 4 (R1)
; k start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ek end address is: 4 (R1)
; k end address is: 0 (R0)
; k start address is: 0 (R0)
; ek start address is: 4 (R1)
;aes_sw_implementation.c,134 :: 		for (i = 0; i < KEY_LENGTH; ++i){
; i start address is: 36 (R9)
MOVW	R9, #0
; k end address is: 0 (R0)
; ek end address is: 4 (R1)
; i end address is: 36 (R9)
MOV	R8, R0
MOV	R7, R1
L_expandKey4:
; i start address is: 36 (R9)
; k start address is: 32 (R8)
; ek start address is: 28 (R7)
; k start address is: 32 (R8)
; k end address is: 32 (R8)
CMP	R9, #4
IT	CS
BCS	L_expandKey5
; k end address is: 32 (R8)
;aes_sw_implementation.c,135 :: 		ek[i] = word(k[(i * KEY_LENGTH) + 0], k[(i * KEY_LENGTH) + 1], k[(i * KEY_LENGTH) + 2], k[(i * KEY_LENGTH) + 3]);
; k start address is: 32 (R8)
LSL	R6, R9, #2
ADDS	R2, R7, R6
STR	R2, [SP, #4]
UXTH	R2, R6
ADDS	R2, R2, #3
UXTH	R2, R2
ADD	R2, R8, R2, LSL #0
LDRB	R2, [R2, #0]
UXTB	R5, R2
UXTH	R2, R6
ADDS	R2, R2, #2
UXTH	R2, R2
ADD	R2, R8, R2, LSL #0
LDRB	R2, [R2, #0]
UXTB	R4, R2
UXTH	R2, R6
ADDS	R2, R2, #1
UXTH	R2, R2
ADD	R2, R8, R2, LSL #0
LDRB	R2, [R2, #0]
UXTB	R3, R2
UXTH	R2, R6
ADD	R2, R8, R2, LSL #0
LDRB	R2, [R2, #0]
UXTB	R1, R3
UXTB	R3, R5
UXTB	R0, R2
UXTB	R2, R4
BL	_word+0
LDR	R2, [SP, #4]
STR	R0, [R2, #0]
;aes_sw_implementation.c,134 :: 		for (i = 0; i < KEY_LENGTH; ++i){
ADD	R9, R9, #1
UXTH	R9, R9
;aes_sw_implementation.c,136 :: 		}
; k end address is: 32 (R8)
; i end address is: 36 (R9)
IT	AL
BAL	L_expandKey4
L_expandKey5:
;aes_sw_implementation.c,138 :: 		for (i = KEY_LENGTH; i < 44; ++i) {
; i start address is: 0 (R0)
MOVW	R0, #4
; ek end address is: 28 (R7)
; i end address is: 0 (R0)
MOV	R6, R7
UXTH	R7, R0
L_expandKey7:
; i start address is: 28 (R7)
; ek start address is: 24 (R6)
; ek start address is: 24 (R6)
; ek end address is: 24 (R6)
CMP	R7, #44
IT	CS
BCS	L_expandKey8
; ek end address is: 24 (R6)
;aes_sw_implementation.c,139 :: 		tmp = ek[i - 1];
; ek start address is: 24 (R6)
SUBS	R2, R7, #1
UXTH	R2, R2
LSLS	R2, R2, #2
ADDS	R2, R6, R2
LDR	R0, [R2, #0]
; tmp start address is: 0 (R0)
;aes_sw_implementation.c,140 :: 		if ((i % KEY_LENGTH) == 0 ) {
AND	R2, R7, #3
UXTH	R2, R2
CMP	R2, #0
IT	NE
BNE	L__expandKey26
;aes_sw_implementation.c,141 :: 		tmp = subWord (rotateWord(tmp)) ^ rCon[i / KEY_LENGTH];
; tmp end address is: 0 (R0)
BL	_rotateWord+0
BL	_subWord+0
LSRS	R3, R7, #2
UXTH	R3, R3
MOVW	R2, #lo_addr(aes_sw_implementation_rCon+0)
MOVT	R2, #hi_addr(aes_sw_implementation_rCon+0)
ADDS	R2, R2, R3
LDRB	R2, [R2, #0]
EORS	R0, R2
; tmp start address is: 0 (R0)
; tmp end address is: 0 (R0)
;aes_sw_implementation.c,142 :: 		}
IT	AL
BAL	L_expandKey10
L__expandKey26:
;aes_sw_implementation.c,140 :: 		if ((i % KEY_LENGTH) == 0 ) {
;aes_sw_implementation.c,142 :: 		}
L_expandKey10:
;aes_sw_implementation.c,143 :: 		ek[i] = ek[i - KEY_LENGTH] ^ tmp;
; tmp start address is: 0 (R0)
LSLS	R2, R7, #2
ADDS	R3, R6, R2
SUBS	R2, R7, #4
UXTH	R2, R2
LSLS	R2, R2, #2
ADDS	R2, R6, R2
LDR	R2, [R2, #0]
EORS	R2, R0
; tmp end address is: 0 (R0)
STR	R2, [R3, #0]
;aes_sw_implementation.c,138 :: 		for (i = KEY_LENGTH; i < 44; ++i) {
ADDS	R7, R7, #1
UXTH	R7, R7
;aes_sw_implementation.c,144 :: 		}
; ek end address is: 24 (R6)
; i end address is: 28 (R7)
IT	AL
BAL	L_expandKey7
L_expandKey8:
;aes_sw_implementation.c,145 :: 		}
L_end_expandKey:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _expandKey
_addRoundKey:
;aes_sw_implementation.c,148 :: 		void addRoundKey(t_state s, uint32_t ek[], uint16_t index) {
; index start address is: 8 (R2)
; ek start address is: 4 (R1)
; s start address is: 0 (R0)
SUB	SP, SP, #4
; index end address is: 8 (R2)
; ek end address is: 4 (R1)
; s end address is: 0 (R0)
; s start address is: 0 (R0)
; ek start address is: 4 (R1)
; index start address is: 8 (R2)
;aes_sw_implementation.c,149 :: 		s[0] = s[0] ^ ek[index + 0];
LDR	R4, [R0, #0]
LSLS	R3, R2, #2
ADDS	R3, R1, R3
LDR	R3, [R3, #0]
EOR	R3, R4, R3, LSL #0
STR	R3, [R0, #0]
;aes_sw_implementation.c,150 :: 		s[1] = s[1] ^ ek[index + 1];
ADDS	R5, R0, #4
ADDS	R3, R0, #4
LDR	R4, [R3, #0]
ADDS	R3, R2, #1
UXTH	R3, R3
LSLS	R3, R3, #2
ADDS	R3, R1, R3
LDR	R3, [R3, #0]
EOR	R3, R4, R3, LSL #0
STR	R3, [R5, #0]
;aes_sw_implementation.c,151 :: 		s[2] = s[2] ^ ek[index + 2];
ADDW	R5, R0, #8
ADDW	R3, R0, #8
LDR	R4, [R3, #0]
ADDS	R3, R2, #2
UXTH	R3, R3
LSLS	R3, R3, #2
ADDS	R3, R1, R3
LDR	R3, [R3, #0]
EOR	R3, R4, R3, LSL #0
STR	R3, [R5, #0]
;aes_sw_implementation.c,152 :: 		s[3] = s[3] ^ ek[index + 3];
ADDW	R5, R0, #12
ADDW	R3, R0, #12
; s end address is: 0 (R0)
LDR	R4, [R3, #0]
ADDS	R3, R2, #3
UXTH	R3, R3
; index end address is: 8 (R2)
LSLS	R3, R3, #2
ADDS	R3, R1, R3
; ek end address is: 4 (R1)
LDR	R3, [R3, #0]
EOR	R3, R4, R3, LSL #0
STR	R3, [R5, #0]
;aes_sw_implementation.c,153 :: 		}
L_end_addRoundKey:
ADD	SP, SP, #4
BX	LR
; end of _addRoundKey
_sw_aes_encrypt:
;aes_sw_implementation.c,155 :: 		uint8_t sw_aes_encrypt (uint8_t* plaintext) {
; plaintext start address is: 0 (R0)
SUB	SP, SP, #40
STR	LR, [SP, #0]
; plaintext end address is: 0 (R0)
; plaintext start address is: 0 (R0)
;aes_sw_implementation.c,160 :: 		for (i = 0; i < 4; ++i){
; i start address is: 28 (R7)
MOVS	R7, #0
; plaintext end address is: 0 (R0)
; i end address is: 28 (R7)
MOV	R6, R0
L_sw_aes_encrypt11:
; i start address is: 28 (R7)
; plaintext start address is: 24 (R6)
; plaintext start address is: 24 (R6)
; plaintext end address is: 24 (R6)
CMP	R7, #4
IT	CS
BCS	L_sw_aes_encrypt12
; plaintext end address is: 24 (R6)
;aes_sw_implementation.c,161 :: 		state[i] = word(plaintext[(i << 2) + 0],  plaintext[(i << 2) + 1],  plaintext[(i << 2) + 2],  plaintext[(i << 2) + 3]);
; plaintext start address is: 24 (R6)
ADD	R2, SP, #20
LSLS	R1, R7, #2
ADDS	R1, R2, R1
STR	R1, [SP, #36]
LSLS	R5, R7, #2
UXTH	R5, R5
ADDS	R1, R5, #3
UXTH	R1, R1
ADDS	R1, R6, R1
LDRB	R1, [R1, #0]
UXTB	R4, R1
ADDS	R1, R5, #2
UXTH	R1, R1
ADDS	R1, R6, R1
LDRB	R1, [R1, #0]
UXTB	R3, R1
ADDS	R1, R5, #1
UXTH	R1, R1
ADDS	R1, R6, R1
LDRB	R1, [R1, #0]
UXTB	R2, R1
ADDS	R1, R6, R5
LDRB	R1, [R1, #0]
UXTB	R0, R1
UXTB	R1, R2
UXTB	R2, R3
UXTB	R3, R4
BL	_word+0
LDR	R1, [SP, #36]
STR	R0, [R1, #0]
;aes_sw_implementation.c,160 :: 		for (i = 0; i < 4; ++i){
ADDS	R7, R7, #1
UXTH	R7, R7
;aes_sw_implementation.c,162 :: 		}
; plaintext end address is: 24 (R6)
; i end address is: 28 (R7)
IT	AL
BAL	L_sw_aes_encrypt11
L_sw_aes_encrypt12:
;aes_sw_implementation.c,164 :: 		trigger_high();
BL	_trigger_high+0
;aes_sw_implementation.c,167 :: 		addRoundKey(state, expKey, 0);
ADD	R1, SP, #20
MOVS	R2, #0
MOV	R0, R1
MOVW	R1, #lo_addr(aes_sw_implementation_expKey+0)
MOVT	R1, #hi_addr(aes_sw_implementation_expKey+0)
BL	_addRoundKey+0
;aes_sw_implementation.c,169 :: 		for (i = 1; i < ROUND_CNT; ++i) {
; i start address is: 40 (R10)
MOVW	R10, #1
; i end address is: 40 (R10)
L_sw_aes_encrypt14:
; i start address is: 40 (R10)
CMP	R10, #10
IT	CS
BCS	L_sw_aes_encrypt15
;aes_sw_implementation.c,170 :: 		subBytes (state);
ADD	R1, SP, #20
MOV	R0, R1
BL	_subBytes+0
;aes_sw_implementation.c,171 :: 		shiftRows(state);
ADD	R1, SP, #20
MOV	R0, R1
BL	_shiftRows+0
;aes_sw_implementation.c,172 :: 		mixColumns(state);
ADD	R1, SP, #20
MOV	R0, R1
BL	_mixColumns+0
;aes_sw_implementation.c,173 :: 		addRoundKey(state, expKey, (i << 2));
LSL	R2, R10, #2
ADD	R1, SP, #20
MOV	R0, R1
MOVW	R1, #lo_addr(aes_sw_implementation_expKey+0)
MOVT	R1, #hi_addr(aes_sw_implementation_expKey+0)
BL	_addRoundKey+0
;aes_sw_implementation.c,169 :: 		for (i = 1; i < ROUND_CNT; ++i) {
ADD	R10, R10, #1
UXTH	R10, R10
;aes_sw_implementation.c,174 :: 		}
; i end address is: 40 (R10)
IT	AL
BAL	L_sw_aes_encrypt14
L_sw_aes_encrypt15:
;aes_sw_implementation.c,177 :: 		subBytes (state);
ADD	R1, SP, #20
MOV	R0, R1
BL	_subBytes+0
;aes_sw_implementation.c,178 :: 		shiftRows(state);
ADD	R1, SP, #20
MOV	R0, R1
BL	_shiftRows+0
;aes_sw_implementation.c,179 :: 		addRoundKey(state, expKey, ROUND_CNT << 2);
ADD	R1, SP, #20
MOVW	R2, #40
MOV	R0, R1
MOVW	R1, #lo_addr(aes_sw_implementation_expKey+0)
MOVT	R1, #hi_addr(aes_sw_implementation_expKey+0)
BL	_addRoundKey+0
;aes_sw_implementation.c,181 :: 		trigger_low();
BL	_trigger_low+0
;aes_sw_implementation.c,184 :: 		for (i = 0; i < 16; i++) {
; i start address is: 12 (R3)
MOVS	R3, #0
; i end address is: 12 (R3)
L_sw_aes_encrypt17:
; i start address is: 12 (R3)
CMP	R3, #16
IT	CS
BCS	L_sw_aes_encrypt18
;aes_sw_implementation.c,185 :: 		if (i < 4) ciphertext[i] = wbyte(state[0], i % 4);
CMP	R3, #4
IT	CS
BCS	L_sw_aes_encrypt20
ADD	R1, SP, #4
ADDS	R1, R1, R3
STR	R1, [SP, #36]
AND	R2, R3, #3
ADD	R1, SP, #20
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #36]
STRB	R0, [R1, #0]
IT	AL
BAL	L_sw_aes_encrypt21
L_sw_aes_encrypt20:
;aes_sw_implementation.c,186 :: 		else if (i < 8) ciphertext[i] = wbyte(state[1], i % 4);
CMP	R3, #8
IT	CS
BCS	L_sw_aes_encrypt22
ADD	R1, SP, #4
ADDS	R1, R1, R3
STR	R1, [SP, #36]
AND	R2, R3, #3
ADD	R1, SP, #20
ADDS	R1, R1, #4
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #36]
STRB	R0, [R1, #0]
IT	AL
BAL	L_sw_aes_encrypt23
L_sw_aes_encrypt22:
;aes_sw_implementation.c,187 :: 		else if (i < 12) ciphertext[i] = wbyte(state[2], i % 4);
CMP	R3, #12
IT	CS
BCS	L_sw_aes_encrypt24
ADD	R1, SP, #4
ADDS	R1, R1, R3
STR	R1, [SP, #36]
AND	R2, R3, #3
ADD	R1, SP, #20
ADDS	R1, #8
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #36]
STRB	R0, [R1, #0]
IT	AL
BAL	L_sw_aes_encrypt25
L_sw_aes_encrypt24:
;aes_sw_implementation.c,188 :: 		else ciphertext[i] = wbyte(state[3], i % 4);
ADD	R1, SP, #4
ADDS	R1, R1, R3
STR	R1, [SP, #36]
AND	R2, R3, #3
ADD	R1, SP, #20
ADDS	R1, #12
LDR	R1, [R1, #0]
MOV	R0, R1
UXTH	R1, R2
BL	_wbyte+0
LDR	R1, [SP, #36]
STRB	R0, [R1, #0]
L_sw_aes_encrypt25:
L_sw_aes_encrypt23:
L_sw_aes_encrypt21:
;aes_sw_implementation.c,184 :: 		for (i = 0; i < 16; i++) {
ADDS	R3, R3, #1
UXTH	R3, R3
;aes_sw_implementation.c,189 :: 		}
; i end address is: 12 (R3)
IT	AL
BAL	L_sw_aes_encrypt17
L_sw_aes_encrypt18:
;aes_sw_implementation.c,190 :: 		simpleserial_put('r', 16, ciphertext);
ADD	R1, SP, #4
MOV	R2, R1
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;aes_sw_implementation.c,191 :: 		return 0x00;
MOVS	R0, #0
;aes_sw_implementation.c,192 :: 		}
L_end_sw_aes_encrypt:
LDR	LR, [SP, #0]
ADD	SP, SP, #40
BX	LR
; end of _sw_aes_encrypt
_sw_aes_key_set:
;aes_sw_implementation.c,194 :: 		uint8_t sw_aes_key_set (uint8_t* k) {
; k start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; k end address is: 0 (R0)
; k start address is: 0 (R0)
;aes_sw_implementation.c,195 :: 		expandKey(k, expKey);
MOVW	R1, #lo_addr(aes_sw_implementation_expKey+0)
MOVT	R1, #hi_addr(aes_sw_implementation_expKey+0)
; k end address is: 0 (R0)
BL	_expandKey+0
;aes_sw_implementation.c,196 :: 		return 0x00;
MOVS	R0, #0
;aes_sw_implementation.c,197 :: 		}
L_end_sw_aes_key_set:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _sw_aes_key_set
_sw_aes_decrypt:
;aes_sw_implementation.c,199 :: 		uint8_t sw_aes_decrypt (uint8_t* ciphertext) {
SUB	SP, SP, #4
;aes_sw_implementation.c,201 :: 		}
L_end_sw_aes_decrypt:
ADD	SP, SP, #4
BX	LR
; end of _sw_aes_decrypt
