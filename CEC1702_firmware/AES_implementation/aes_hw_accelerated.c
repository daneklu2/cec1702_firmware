#include "__Lib_AES_defines.h"
#include "../debug.h"
#include "../simpleserial/simpleserial.h"
#include "../device_setup/cec1702_setup.h"

static uint32_t key[4];
static uint32_t ptbuf[4] aligned(16);
static uint32_t outbuf[4] aligned(16);

uint8_t hw_aes_key_set (uint8_t* k)  {
        uint32_t aes_sts;
    memcpy(key, k, 16);

    while (aes_busy());

        if (aes_set_key(key, 0, AES_KEYLEN_128, 1) != AES_OK) {
                   return 0x01;
           }
    return 0x00;
}

uint8_t hw_aes_encrypt (uint8_t* plaintext) {
        uint32_t aes_sts;

        int i;
        if (!plaintext || !outbuf) {

                return 0x01;
        }


        memcpy(ptbuf, plaintext, 16);
        
        dbg_puts_labeled("aes_crypt()");
        trigger_high();

        if (aes_crypt(ptbuf, outbuf, 1, AES_MODE_ECB) != AES_OK) {
                        return 0x02;                
        }
        dbg_puts_labeled("aes_start");
        aes_start(0);
        dbg_puts_labeled("while()");
        while (!aes_done_status(&aes_sts));
        
        trigger_low();

        simpleserial_put('r', 16, outbuf);
        aes_stop();
        return 0x00;

}

uint8_t hw_aes_decrypt (uint8_t* ciphertext) {
        uint32_t aes_sts;

        int i;
        if (!ciphertext || !outbuf) {
           return 0x01;
        }


        memcpy(ptbuf, ciphertext, 16);

        trigger_high();

        if (aes_crypt(ptbuf, outbuf, 1, AES_MODE_DECRYPT | AES_MODE_ECB) != AES_OK) {
                        return 0x02;
        }

        aes_start(0);

        while (!aes_done_status(&aes_sts));

        trigger_low();

        simpleserial_put('r', 16, outbuf);
        aes_stop();
        return 0x00;
}