#line 1 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/AES_implementation/aes_hw_accelerated.c"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/aes_implementation/__lib_aes_defines.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"





typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int int32_t;
typedef signed long long int64_t;


typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long int uint32_t;
typedef unsigned long long uint64_t;


typedef signed char int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;
typedef signed long long int_least64_t;


typedef unsigned char uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;
typedef unsigned long long uint_least64_t;



typedef signed long int int_fast8_t;
typedef signed long int int_fast16_t;
typedef signed long int int_fast32_t;
typedef signed long long int_fast64_t;


typedef unsigned long int uint_fast8_t;
typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long long uint_fast64_t;


typedef signed long int intptr_t;
typedef unsigned long int uintptr_t;


typedef signed long long intmax_t;
typedef unsigned long long uintmax_t;
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/aes_implementation/../debug.h"




void dbg_putch (char c);

void dbg_puts (char * str);

void dbg_putch_labeled (char c);

void dbg_puts_labeled (char * str);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/aes_implementation/../simpleserial/simpleserial.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 14 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/aes_implementation/../simpleserial/simpleserial.h"
typedef struct ss_cmd
{
 char c;
 unsigned int len;
 uint8_t (*fp)(uint8_t*);
} ss_cmd;



void simpleserial_init(void);
#line 39 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/aes_implementation/../simpleserial/simpleserial.h"
int simpleserial_addcmd(char c, unsigned int len, uint8_t (*fp)(uint8_t*));







void simpleserial_get(void);




void simpleserial_put(char c, int size, uint8_t* output);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/aes_implementation/../device_setup/cec1702_setup.h"




void init_uart(void);

void putch(char c);

char getch(void);

void platform_init(void);

void trigger_setup(void);

void trigger_high(void);

void trigger_low(void);
#line 6 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/AES_implementation/aes_hw_accelerated.c"
static uint32_t key[4];
static uint32_t ptbuf[4] aligned(16);
static uint32_t outbuf[4] aligned(16);

uint8_t hw_aes_key_set (uint8_t* k) {
 uint32_t aes_sts;
 memcpy(key, k, 16);

 while (aes_busy());

 if (aes_set_key(key, 0,  (0ul) , 1) !=  (0) ) {
 return 0x01;
 }
 return 0x00;
}

uint8_t hw_aes_encrypt (uint8_t* plaintext) {
 uint32_t aes_sts;

 int i;
 if (!plaintext || !outbuf) {

 return 0x01;
 }


 memcpy(ptbuf, plaintext, 16);

 dbg_puts_labeled("aes_crypt()");
 trigger_high();

 if (aes_crypt(ptbuf, outbuf, 1,  (0ul) ) !=  (0) ) {
 return 0x02;
 }
 dbg_puts_labeled("aes_start");
 aes_start(0);
 dbg_puts_labeled("while()");
 while (!aes_done_status(&aes_sts));

 trigger_low();

 simpleserial_put('r', 16, outbuf);
 aes_stop();
 return 0x00;

}

uint8_t hw_aes_decrypt (uint8_t* ciphertext) {
 uint32_t aes_sts;

 int i;
 if (!ciphertext || !outbuf) {
 return 0x01;
 }


 memcpy(ptbuf, ciphertext, 16);

 trigger_high();

 if (aes_crypt(ptbuf, outbuf, 1,  (0x80ul)  |  (0ul) ) !=  (0) ) {
 return 0x02;
 }

 aes_start(0);

 while (!aes_done_status(&aes_sts));

 trigger_low();

 simpleserial_put('r', 16, outbuf);
 aes_stop();
 return 0x00;
}
