#ifndef AES_SW_IMPLEMENTATION
#define AES_SW_IMPLEMENTATION

uint8_t sw_aes_key_set (uint8_t* k);

uint8_t sw_aes_encrypt (uint8_t* plaintext);

uint8_t sw_aes_decrypt (uint8_t* ciphertext);

#endif