_hw_aes_key_set:
;aes_hw_accelerated.c,10 :: 		uint8_t hw_aes_key_set (uint8_t* k)  {
; k start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; k end address is: 0 (R0)
; k start address is: 0 (R0)
;aes_hw_accelerated.c,12 :: 		memcpy(key, k, 16);
MOVS	R2, #16
SXTH	R2, R2
MOV	R1, R0
; k end address is: 0 (R0)
MOVW	R0, #lo_addr(aes_hw_accelerated_key+0)
MOVT	R0, #hi_addr(aes_hw_accelerated_key+0)
BL	_memcpy+0
;aes_hw_accelerated.c,14 :: 		while (aes_busy());
L_hw_aes_key_set0:
BL	_aes_busy+0
CMP	R0, #0
IT	EQ
BEQ	L_hw_aes_key_set1
IT	AL
BAL	L_hw_aes_key_set0
L_hw_aes_key_set1:
;aes_hw_accelerated.c,16 :: 		if (aes_set_key(key, 0, AES_KEYLEN_128, 1) != AES_OK) {
MOVS	R3, #1
MOVS	R2, #0
MOVS	R1, #0
MOVW	R0, #lo_addr(aes_hw_accelerated_key+0)
MOVT	R0, #hi_addr(aes_hw_accelerated_key+0)
BL	_aes_set_key+0
CMP	R0, #0
IT	EQ
BEQ	L_hw_aes_key_set2
;aes_hw_accelerated.c,17 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_hw_aes_key_set
;aes_hw_accelerated.c,18 :: 		}
L_hw_aes_key_set2:
;aes_hw_accelerated.c,19 :: 		return 0x00;
MOVS	R0, #0
;aes_hw_accelerated.c,20 :: 		}
L_end_hw_aes_key_set:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _hw_aes_key_set
_hw_aes_encrypt:
;aes_hw_accelerated.c,22 :: 		uint8_t hw_aes_encrypt (uint8_t* plaintext) {
; plaintext start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; plaintext end address is: 0 (R0)
; plaintext start address is: 0 (R0)
;aes_hw_accelerated.c,26 :: 		if (!plaintext || !outbuf) {
CMP	R0, #0
IT	EQ
BEQ	L__hw_aes_encrypt17
MOVW	R1, #lo_addr(aes_hw_accelerated_outbuf+0)
MOVT	R1, #hi_addr(aes_hw_accelerated_outbuf+0)
CMP	R1, #0
IT	EQ
BEQ	L__hw_aes_encrypt16
IT	AL
BAL	L_hw_aes_encrypt5
; plaintext end address is: 0 (R0)
L__hw_aes_encrypt17:
L__hw_aes_encrypt16:
;aes_hw_accelerated.c,28 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_hw_aes_encrypt
;aes_hw_accelerated.c,29 :: 		}
L_hw_aes_encrypt5:
;aes_hw_accelerated.c,32 :: 		memcpy(ptbuf, plaintext, 16);
; plaintext start address is: 0 (R0)
MOVS	R2, #16
SXTH	R2, R2
MOV	R1, R0
; plaintext end address is: 0 (R0)
MOVW	R0, #lo_addr(aes_hw_accelerated_ptbuf+0)
MOVT	R0, #hi_addr(aes_hw_accelerated_ptbuf+0)
BL	_memcpy+0
;aes_hw_accelerated.c,34 :: 		dbg_puts_labeled("aes_crypt()");
MOVW	R1, #lo_addr(?lstr1_aes_hw_accelerated+0)
MOVT	R1, #hi_addr(?lstr1_aes_hw_accelerated+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;aes_hw_accelerated.c,35 :: 		trigger_high();
BL	_trigger_high+0
;aes_hw_accelerated.c,37 :: 		if (aes_crypt(ptbuf, outbuf, 1, AES_MODE_ECB) != AES_OK) {
MOVS	R3, #0
MOVS	R2, #1
MOVW	R1, #lo_addr(aes_hw_accelerated_outbuf+0)
MOVT	R1, #hi_addr(aes_hw_accelerated_outbuf+0)
MOVW	R0, #lo_addr(aes_hw_accelerated_ptbuf+0)
MOVT	R0, #hi_addr(aes_hw_accelerated_ptbuf+0)
BL	_aes_crypt+0
CMP	R0, #0
IT	EQ
BEQ	L_hw_aes_encrypt6
;aes_hw_accelerated.c,38 :: 		return 0x02;
MOVS	R0, #2
IT	AL
BAL	L_end_hw_aes_encrypt
;aes_hw_accelerated.c,39 :: 		}
L_hw_aes_encrypt6:
;aes_hw_accelerated.c,40 :: 		dbg_puts_labeled("aes_start");
MOVW	R1, #lo_addr(?lstr2_aes_hw_accelerated+0)
MOVT	R1, #hi_addr(?lstr2_aes_hw_accelerated+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;aes_hw_accelerated.c,41 :: 		aes_start(0);
MOVS	R0, #0
BL	_aes_start+0
;aes_hw_accelerated.c,42 :: 		dbg_puts_labeled("while()");
MOVW	R1, #lo_addr(?lstr3_aes_hw_accelerated+0)
MOVT	R1, #hi_addr(?lstr3_aes_hw_accelerated+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;aes_hw_accelerated.c,43 :: 		while (!aes_done_status(&aes_sts));
L_hw_aes_encrypt7:
ADD	R1, SP, #4
MOV	R0, R1
BL	_aes_done_status+0
CMP	R0, #0
IT	NE
BNE	L_hw_aes_encrypt8
IT	AL
BAL	L_hw_aes_encrypt7
L_hw_aes_encrypt8:
;aes_hw_accelerated.c,45 :: 		trigger_low();
BL	_trigger_low+0
;aes_hw_accelerated.c,47 :: 		simpleserial_put('r', 16, outbuf);
MOVW	R2, #lo_addr(aes_hw_accelerated_outbuf+0)
MOVT	R2, #hi_addr(aes_hw_accelerated_outbuf+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;aes_hw_accelerated.c,48 :: 		aes_stop();
BL	_aes_stop+0
;aes_hw_accelerated.c,49 :: 		return 0x00;
MOVS	R0, #0
;aes_hw_accelerated.c,51 :: 		}
L_end_hw_aes_encrypt:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _hw_aes_encrypt
_hw_aes_decrypt:
;aes_hw_accelerated.c,53 :: 		uint8_t hw_aes_decrypt (uint8_t* ciphertext) {
; ciphertext start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ciphertext end address is: 0 (R0)
; ciphertext start address is: 0 (R0)
;aes_hw_accelerated.c,57 :: 		if (!ciphertext || !outbuf) {
CMP	R0, #0
IT	EQ
BEQ	L__hw_aes_decrypt20
MOVW	R1, #lo_addr(aes_hw_accelerated_outbuf+0)
MOVT	R1, #hi_addr(aes_hw_accelerated_outbuf+0)
CMP	R1, #0
IT	EQ
BEQ	L__hw_aes_decrypt19
IT	AL
BAL	L_hw_aes_decrypt11
; ciphertext end address is: 0 (R0)
L__hw_aes_decrypt20:
L__hw_aes_decrypt19:
;aes_hw_accelerated.c,58 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_hw_aes_decrypt
;aes_hw_accelerated.c,59 :: 		}
L_hw_aes_decrypt11:
;aes_hw_accelerated.c,62 :: 		memcpy(ptbuf, ciphertext, 16);
; ciphertext start address is: 0 (R0)
MOVS	R2, #16
SXTH	R2, R2
MOV	R1, R0
; ciphertext end address is: 0 (R0)
MOVW	R0, #lo_addr(aes_hw_accelerated_ptbuf+0)
MOVT	R0, #hi_addr(aes_hw_accelerated_ptbuf+0)
BL	_memcpy+0
;aes_hw_accelerated.c,64 :: 		trigger_high();
BL	_trigger_high+0
;aes_hw_accelerated.c,66 :: 		if (aes_crypt(ptbuf, outbuf, 1, AES_MODE_DECRYPT | AES_MODE_ECB) != AES_OK) {
MOVS	R3, #128
MOVS	R2, #1
MOVW	R1, #lo_addr(aes_hw_accelerated_outbuf+0)
MOVT	R1, #hi_addr(aes_hw_accelerated_outbuf+0)
MOVW	R0, #lo_addr(aes_hw_accelerated_ptbuf+0)
MOVT	R0, #hi_addr(aes_hw_accelerated_ptbuf+0)
BL	_aes_crypt+0
CMP	R0, #0
IT	EQ
BEQ	L_hw_aes_decrypt12
;aes_hw_accelerated.c,67 :: 		return 0x02;
MOVS	R0, #2
IT	AL
BAL	L_end_hw_aes_decrypt
;aes_hw_accelerated.c,68 :: 		}
L_hw_aes_decrypt12:
;aes_hw_accelerated.c,70 :: 		aes_start(0);
MOVS	R0, #0
BL	_aes_start+0
;aes_hw_accelerated.c,72 :: 		while (!aes_done_status(&aes_sts));
L_hw_aes_decrypt13:
ADD	R1, SP, #4
MOV	R0, R1
BL	_aes_done_status+0
CMP	R0, #0
IT	NE
BNE	L_hw_aes_decrypt14
IT	AL
BAL	L_hw_aes_decrypt13
L_hw_aes_decrypt14:
;aes_hw_accelerated.c,74 :: 		trigger_low();
BL	_trigger_low+0
;aes_hw_accelerated.c,76 :: 		simpleserial_put('r', 16, outbuf);
MOVW	R2, #lo_addr(aes_hw_accelerated_outbuf+0)
MOVT	R2, #hi_addr(aes_hw_accelerated_outbuf+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;aes_hw_accelerated.c,77 :: 		aes_stop();
BL	_aes_stop+0
;aes_hw_accelerated.c,78 :: 		return 0x00;
MOVS	R0, #0
;aes_hw_accelerated.c,79 :: 		}
L_end_hw_aes_decrypt:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _hw_aes_decrypt
