_sw_rsa_init:
;rsa_sw_implementation.c,76 :: 		void sw_rsa_init (void) {
SUB	SP, SP, #4
;rsa_sw_implementation.c,78 :: 		plaintext_bigi.ary = plaintext_array;
MOVW	R1, #lo_addr(_plaintext_array+0)
MOVT	R1, #hi_addr(_plaintext_array+0)
MOVW	R0, #lo_addr(_plaintext_bigi+0)
MOVT	R0, #hi_addr(_plaintext_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,79 :: 		plaintext_bigi.wordlen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_plaintext_bigi+4)
MOVT	R0, #hi_addr(_plaintext_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,80 :: 		plaintext_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_plaintext_bigi+12)
MOVT	R0, #hi_addr(_plaintext_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,82 :: 		ciphertext_bigi.ary = ciphertext_array;
MOVW	R1, #lo_addr(_ciphertext_array+0)
MOVT	R1, #hi_addr(_ciphertext_array+0)
MOVW	R0, #lo_addr(_ciphertext_bigi+0)
MOVT	R0, #hi_addr(_ciphertext_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,83 :: 		ciphertext_bigi.wordlen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_ciphertext_bigi+4)
MOVT	R0, #hi_addr(_ciphertext_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,84 :: 		ciphertext_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_ciphertext_bigi+12)
MOVT	R0, #hi_addr(_ciphertext_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,86 :: 		p_bigi.ary = p_array;
MOVW	R1, #lo_addr(_p_array+0)
MOVT	R1, #hi_addr(_p_array+0)
MOVW	R0, #lo_addr(_p_bigi+0)
MOVT	R0, #hi_addr(_p_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,87 :: 		p_bigi.wordlen = (RSA_4096_WORDS >> 1);
MOVS	R1, #64
MOVW	R0, #lo_addr(_p_bigi+4)
MOVT	R0, #hi_addr(_p_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,88 :: 		p_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_p_bigi+12)
MOVT	R0, #hi_addr(_p_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,90 :: 		q_bigi.ary = q_array;
MOVW	R1, #lo_addr(_q_array+0)
MOVT	R1, #hi_addr(_q_array+0)
MOVW	R0, #lo_addr(_q_bigi+0)
MOVT	R0, #hi_addr(_q_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,91 :: 		q_bigi.wordlen = (RSA_4096_WORDS >> 1);
MOVS	R1, #64
MOVW	R0, #lo_addr(_q_bigi+4)
MOVT	R0, #hi_addr(_q_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,92 :: 		q_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_q_bigi+12)
MOVT	R0, #hi_addr(_q_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,94 :: 		n_bigi.ary = n_array;
MOVW	R1, #lo_addr(_n_array+0)
MOVT	R1, #hi_addr(_n_array+0)
MOVW	R0, #lo_addr(_n_bigi+0)
MOVT	R0, #hi_addr(_n_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,95 :: 		n_bigi.wordlen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_n_bigi+4)
MOVT	R0, #hi_addr(_n_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,96 :: 		n_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_n_bigi+12)
MOVT	R0, #hi_addr(_n_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,98 :: 		e_bigi.ary = e_array;
MOVW	R1, #lo_addr(_e_array+0)
MOVT	R1, #hi_addr(_e_array+0)
MOVW	R0, #lo_addr(_e_bigi+0)
MOVT	R0, #hi_addr(_e_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,99 :: 		e_bigi.wordlen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_e_bigi+4)
MOVT	R0, #hi_addr(_e_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,100 :: 		e_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_e_bigi+12)
MOVT	R0, #hi_addr(_e_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,102 :: 		d_bigi.ary = d_array;
MOVW	R1, #lo_addr(_d_array+0)
MOVT	R1, #hi_addr(_d_array+0)
MOVW	R0, #lo_addr(_d_bigi+0)
MOVT	R0, #hi_addr(_d_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,103 :: 		d_bigi.wordlen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_d_bigi+4)
MOVT	R0, #hi_addr(_d_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,104 :: 		d_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_d_bigi+12)
MOVT	R0, #hi_addr(_d_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,106 :: 		dp_bigi.ary = dp_array;
MOVW	R1, #lo_addr(_dp_array+0)
MOVT	R1, #hi_addr(_dp_array+0)
MOVW	R0, #lo_addr(_dp_bigi+0)
MOVT	R0, #hi_addr(_dp_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,107 :: 		dp_bigi.wordlen = (RSA_4096_WORDS >> 1);
MOVS	R1, #64
MOVW	R0, #lo_addr(_dp_bigi+4)
MOVT	R0, #hi_addr(_dp_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,108 :: 		dp_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_dp_bigi+12)
MOVT	R0, #hi_addr(_dp_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,110 :: 		dq_bigi.ary = dq_array;
MOVW	R1, #lo_addr(_dq_array+0)
MOVT	R1, #hi_addr(_dq_array+0)
MOVW	R0, #lo_addr(_dq_bigi+0)
MOVT	R0, #hi_addr(_dq_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,111 :: 		dq_bigi.wordlen = (RSA_4096_WORDS >> 1);
MOVS	R1, #64
MOVW	R0, #lo_addr(_dq_bigi+4)
MOVT	R0, #hi_addr(_dq_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,112 :: 		dq_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_dq_bigi+12)
MOVT	R0, #hi_addr(_dq_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,114 :: 		qi_bigi.ary = qi_array;
MOVW	R1, #lo_addr(_qi_array+0)
MOVT	R1, #hi_addr(_qi_array+0)
MOVW	R0, #lo_addr(_qi_bigi+0)
MOVT	R0, #hi_addr(_qi_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,115 :: 		qi_bigi.wordlen = (RSA_4096_WORDS >> 1);
MOVS	R1, #64
MOVW	R0, #lo_addr(_qi_bigi+4)
MOVT	R0, #hi_addr(_qi_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,116 :: 		qi_bigi.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_qi_bigi+12)
MOVT	R0, #hi_addr(_qi_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,118 :: 		for (i = 0; i < TMPARY_LEN; i++)
; i start address is: 12 (R3)
MOVS	R3, #0
; i end address is: 12 (R3)
L_sw_rsa_init0:
; i start address is: 12 (R3)
CMP	R3, #20
IT	CS
BCS	L_sw_rsa_init1
;rsa_sw_implementation.c,120 :: 		tmpary_bigi[i].ary      = &tmp_arys[i * (RSA_4096_WORDS + MULT_BUFFER_WORDLEN)];
LSLS	R1, R3, #4
MOVW	R0, #lo_addr(_tmpary_bigi+0)
MOVT	R0, #hi_addr(_tmpary_bigi+0)
ADDS	R2, R0, R1
MOVW	R0, #131
MULS	R0, R3, R0
UXTH	R0, R0
LSLS	R1, R0, #2
MOVW	R0, #lo_addr(_tmp_arys+0)
MOVT	R0, #hi_addr(_tmp_arys+0)
ADDS	R0, R0, R1
STR	R0, [R2, #0]
;rsa_sw_implementation.c,121 :: 		tmpary_bigi[i].wordlen  = RSA_4096_WORDS;
LSLS	R1, R3, #4
MOVW	R0, #lo_addr(_tmpary_bigi+0)
MOVT	R0, #hi_addr(_tmpary_bigi+0)
ADDS	R0, R0, R1
ADDS	R1, R0, #4
MOVS	R0, #128
STR	R0, [R1, #0]
;rsa_sw_implementation.c,122 :: 		tmpary_bigi[i].alloclen = RSA_4096_WORDS;
LSLS	R1, R3, #4
MOVW	R0, #lo_addr(_tmpary_bigi+0)
MOVT	R0, #hi_addr(_tmpary_bigi+0)
ADDS	R0, R0, R1
ADDW	R1, R0, #8
MOVS	R0, #128
STR	R0, [R1, #0]
;rsa_sw_implementation.c,123 :: 		tmpary_bigi[i].domain   = DOMAIN_NORMAL;
LSLS	R1, R3, #4
MOVW	R0, #lo_addr(_tmpary_bigi+0)
MOVT	R0, #hi_addr(_tmpary_bigi+0)
ADDS	R0, R0, R1
ADDW	R1, R0, #12
MOVS	R0, #0
STRB	R0, [R1, #0]
;rsa_sw_implementation.c,118 :: 		for (i = 0; i < TMPARY_LEN; i++)
ADDS	R3, R3, #1
UXTH	R3, R3
;rsa_sw_implementation.c,124 :: 		}
; i end address is: 12 (R3)
IT	AL
BAL	L_sw_rsa_init0
L_sw_rsa_init1:
;rsa_sw_implementation.c,125 :: 		tmp0_bigi.ary      = tmp0_array;
MOVW	R1, #lo_addr(_tmp0_array+0)
MOVT	R1, #hi_addr(_tmp0_array+0)
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,126 :: 		tmp0_bigi.wordlen  = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp0_bigi+4)
MOVT	R0, #hi_addr(_tmp0_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,127 :: 		tmp0_bigi.alloclen = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp0_bigi+8)
MOVT	R0, #hi_addr(_tmp0_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,128 :: 		tmp0_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp0_bigi+12)
MOVT	R0, #hi_addr(_tmp0_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,130 :: 		tmp1_bigi.ary      = tmp1_array;
MOVW	R1, #lo_addr(_tmp1_array+0)
MOVT	R1, #hi_addr(_tmp1_array+0)
MOVW	R0, #lo_addr(_tmp1_bigi+0)
MOVT	R0, #hi_addr(_tmp1_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,131 :: 		tmp1_bigi.wordlen  = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp1_bigi+4)
MOVT	R0, #hi_addr(_tmp1_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,132 :: 		tmp1_bigi.alloclen = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp1_bigi+8)
MOVT	R0, #hi_addr(_tmp1_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,133 :: 		tmp1_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp1_bigi+12)
MOVT	R0, #hi_addr(_tmp1_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,135 :: 		tmp2_bigi.ary      = tmp2_array;
MOVW	R1, #lo_addr(_tmp2_array+0)
MOVT	R1, #hi_addr(_tmp2_array+0)
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,136 :: 		tmp2_bigi.wordlen  = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp2_bigi+4)
MOVT	R0, #hi_addr(_tmp2_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,137 :: 		tmp2_bigi.alloclen = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp2_bigi+8)
MOVT	R0, #hi_addr(_tmp2_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,138 :: 		tmp2_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp2_bigi+12)
MOVT	R0, #hi_addr(_tmp2_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,140 :: 		tmp3_bigi.ary      = tmp3_array;
MOVW	R1, #lo_addr(_tmp3_array+0)
MOVT	R1, #hi_addr(_tmp3_array+0)
MOVW	R0, #lo_addr(_tmp3_bigi+0)
MOVT	R0, #hi_addr(_tmp3_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,141 :: 		tmp3_bigi.wordlen  = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp3_bigi+4)
MOVT	R0, #hi_addr(_tmp3_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,142 :: 		tmp3_bigi.alloclen = RSA_4096_WORDS >> 1;
MOVS	R1, #64
MOVW	R0, #lo_addr(_tmp3_bigi+8)
MOVT	R0, #hi_addr(_tmp3_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,143 :: 		tmp3_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp3_bigi+12)
MOVT	R0, #hi_addr(_tmp3_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,145 :: 		tmp00_bigi.ary      = tmp00_array;
MOVW	R1, #lo_addr(_tmp00_array+0)
MOVT	R1, #hi_addr(_tmp00_array+0)
MOVW	R0, #lo_addr(_tmp00_bigi+0)
MOVT	R0, #hi_addr(_tmp00_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,146 :: 		tmp00_bigi.wordlen  = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_tmp00_bigi+4)
MOVT	R0, #hi_addr(_tmp00_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,147 :: 		tmp00_bigi.alloclen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_tmp00_bigi+8)
MOVT	R0, #hi_addr(_tmp00_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,148 :: 		tmp00_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp00_bigi+12)
MOVT	R0, #hi_addr(_tmp00_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,150 :: 		tmp11_bigi.ary      = tmp11_array;
MOVW	R1, #lo_addr(_tmp11_array+0)
MOVT	R1, #hi_addr(_tmp11_array+0)
MOVW	R0, #lo_addr(_tmp11_bigi+0)
MOVT	R0, #hi_addr(_tmp11_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,151 :: 		tmp11_bigi.wordlen  = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_tmp11_bigi+4)
MOVT	R0, #hi_addr(_tmp11_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,152 :: 		tmp11_bigi.alloclen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_tmp11_bigi+8)
MOVT	R0, #hi_addr(_tmp11_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,153 :: 		tmp11_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp11_bigi+12)
MOVT	R0, #hi_addr(_tmp11_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,155 :: 		tmp22_bigi.ary      = tmp22_array;
MOVW	R1, #lo_addr(_tmp22_array+0)
MOVT	R1, #hi_addr(_tmp22_array+0)
MOVW	R0, #lo_addr(_tmp22_bigi+0)
MOVT	R0, #hi_addr(_tmp22_bigi+0)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,156 :: 		tmp22_bigi.wordlen  = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_tmp22_bigi+4)
MOVT	R0, #hi_addr(_tmp22_bigi+4)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,157 :: 		tmp22_bigi.alloclen = RSA_4096_WORDS;
MOVS	R1, #128
MOVW	R0, #lo_addr(_tmp22_bigi+8)
MOVT	R0, #hi_addr(_tmp22_bigi+8)
STR	R1, [R0, #0]
;rsa_sw_implementation.c,158 :: 		tmp22_bigi.domain   = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_tmp22_bigi+12)
MOVT	R0, #hi_addr(_tmp22_bigi+12)
STRB	R1, [R0, #0]
;rsa_sw_implementation.c,159 :: 		}
L_end_sw_rsa_init:
ADD	SP, SP, #4
BX	LR
; end of _sw_rsa_init
_sw_rsa_adjust_length:
;rsa_sw_implementation.c,161 :: 		void sw_rsa_adjust_length (index_t new_length) {
; new_length start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
MOV	R3, R0
; new_length end address is: 0 (R0)
; new_length start address is: 12 (R3)
;rsa_sw_implementation.c,163 :: 		dbg_puts_labeled ("IN ADJUST BIGI LENGTH");
MOVW	R1, #lo_addr(?lstr1_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr1_rsa_sw_implementation+0)
STR	R3, [SP, #4]
MOV	R0, R1
BL	_dbg_puts_labeled+0
LDR	R3, [SP, #4]
;rsa_sw_implementation.c,164 :: 		plaintext_bigi.wordlen = new_length;
MOVW	R1, #lo_addr(_plaintext_bigi+4)
MOVT	R1, #hi_addr(_plaintext_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,165 :: 		ciphertext_bigi.wordlen = new_length;
MOVW	R1, #lo_addr(_ciphertext_bigi+4)
MOVT	R1, #hi_addr(_ciphertext_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,167 :: 		p_bigi.wordlen   = (new_length >> 1);
LSRS	R2, R3, #1
MOVW	R1, #lo_addr(_p_bigi+4)
MOVT	R1, #hi_addr(_p_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,168 :: 		q_bigi.wordlen   = (new_length >> 1);
MOVW	R1, #lo_addr(_q_bigi+4)
MOVT	R1, #hi_addr(_q_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,169 :: 		n_bigi.wordlen   = new_length;
MOVW	R1, #lo_addr(_n_bigi+4)
MOVT	R1, #hi_addr(_n_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,170 :: 		e_bigi.wordlen   = new_length;
MOVW	R1, #lo_addr(_e_bigi+4)
MOVT	R1, #hi_addr(_e_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,171 :: 		d_bigi.wordlen   = new_length;
MOVW	R1, #lo_addr(_d_bigi+4)
MOVT	R1, #hi_addr(_d_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,173 :: 		dp_bigi.wordlen = (new_length >> 1);
MOVW	R1, #lo_addr(_dp_bigi+4)
MOVT	R1, #hi_addr(_dp_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,174 :: 		dq_bigi.wordlen = (new_length >> 1);
MOVW	R1, #lo_addr(_dq_bigi+4)
MOVT	R1, #hi_addr(_dq_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,175 :: 		qi_bigi.wordlen = (new_length >> 1);
MOVW	R1, #lo_addr(_qi_bigi+4)
MOVT	R1, #hi_addr(_qi_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,177 :: 		for (i = 0; i < TMPARY_LEN; i++)
; i start address is: 0 (R0)
MOVS	R0, #0
; new_length end address is: 12 (R3)
; i end address is: 0 (R0)
L_sw_rsa_adjust_length3:
; i start address is: 0 (R0)
; new_length start address is: 12 (R3)
CMP	R0, #20
IT	CS
BCS	L_sw_rsa_adjust_length4
;rsa_sw_implementation.c,179 :: 		tmpary_bigi[i].wordlen  = new_length;
LSLS	R2, R0, #4
MOVW	R1, #lo_addr(_tmpary_bigi+0)
MOVT	R1, #hi_addr(_tmpary_bigi+0)
ADDS	R1, R1, R2
ADDS	R1, R1, #4
STR	R3, [R1, #0]
;rsa_sw_implementation.c,177 :: 		for (i = 0; i < TMPARY_LEN; i++)
ADDS	R0, R0, #1
UXTH	R0, R0
;rsa_sw_implementation.c,180 :: 		}
; i end address is: 0 (R0)
IT	AL
BAL	L_sw_rsa_adjust_length3
L_sw_rsa_adjust_length4:
;rsa_sw_implementation.c,181 :: 		tmp0_bigi.wordlen  = (new_length >> 1);
LSRS	R2, R3, #1
MOVW	R1, #lo_addr(_tmp0_bigi+4)
MOVT	R1, #hi_addr(_tmp0_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,182 :: 		tmp1_bigi.wordlen  = (new_length >> 1);
MOVW	R1, #lo_addr(_tmp1_bigi+4)
MOVT	R1, #hi_addr(_tmp1_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,183 :: 		tmp2_bigi.wordlen  = (new_length >> 1);
MOVW	R1, #lo_addr(_tmp2_bigi+4)
MOVT	R1, #hi_addr(_tmp2_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,184 :: 		tmp3_bigi.wordlen  = (new_length >> 1);
MOVW	R1, #lo_addr(_tmp3_bigi+4)
MOVT	R1, #hi_addr(_tmp3_bigi+4)
STR	R2, [R1, #0]
;rsa_sw_implementation.c,186 :: 		tmp00_bigi.wordlen  = new_length;
MOVW	R1, #lo_addr(_tmp00_bigi+4)
MOVT	R1, #hi_addr(_tmp00_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,187 :: 		tmp11_bigi.wordlen  = new_length;
MOVW	R1, #lo_addr(_tmp11_bigi+4)
MOVT	R1, #hi_addr(_tmp11_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,188 :: 		tmp22_bigi.wordlen  = new_length;
MOVW	R1, #lo_addr(_tmp22_bigi+4)
MOVT	R1, #hi_addr(_tmp22_bigi+4)
STR	R3, [R1, #0]
;rsa_sw_implementation.c,190 :: 		actual_bigi_size = new_length;
MOVW	R1, #lo_addr(_actual_bigi_size+0)
MOVT	R1, #hi_addr(_actual_bigi_size+0)
STRH	R3, [R1, #0]
; new_length end address is: 12 (R3)
;rsa_sw_implementation.c,192 :: 		}
L_end_sw_rsa_adjust_length:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _sw_rsa_adjust_length
_sw_rsa_key_set:
;rsa_sw_implementation.c,194 :: 		uint8_t sw_rsa_key_set (uint8_t* k) {
; k start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; k end address is: 0 (R0)
; k start address is: 0 (R0)
;rsa_sw_implementation.c,196 :: 		if (key_parts_cnt >= key_parts) {
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CC
BCC	L_sw_rsa_key_set6
; k end address is: 0 (R0)
;rsa_sw_implementation.c,197 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_sw_rsa_key_set
;rsa_sw_implementation.c,198 :: 		}
L_sw_rsa_key_set6:
;rsa_sw_implementation.c,200 :: 		memcpy(key_byte_array + (key_parts_cnt * block_length_in_bytes), k, block_length_in_bytes);
; k start address is: 0 (R0)
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
LDRH	R3, [R1, #0]
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
LDRH	R1, [R1, #0]
MULS	R2, R1, R2
UXTH	R2, R2
MOVW	R1, #lo_addr(rsa_sw_implementation_key_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_key_byte_array+0)
ADDS	R1, R1, R2
STR	R1, [SP, #4]
SXTH	R2, R3
MOV	R1, R0
; k end address is: 0 (R0)
LDR	R0, [SP, #4]
BL	_memcpy+0
;rsa_sw_implementation.c,201 :: 		++key_parts_cnt;
MOVW	R3, #lo_addr(_key_parts_cnt+0)
MOVT	R3, #hi_addr(_key_parts_cnt+0)
LDRH	R1, [R3, #0]
ADDS	R2, R1, #1
UXTH	R2, R2
STRH	R2, [R3, #0]
;rsa_sw_implementation.c,203 :: 		if (key_parts_cnt < key_parts) {
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
LDRH	R1, [R1, #0]
CMP	R2, R1
IT	CS
BCS	L_sw_rsa_key_set7
;rsa_sw_implementation.c,204 :: 		return 0x00;
MOVS	R0, #0
IT	AL
BAL	L_end_sw_rsa_key_set
;rsa_sw_implementation.c,205 :: 		}
L_sw_rsa_key_set7:
;rsa_sw_implementation.c,209 :: 		if (actual_bigi_size != (operation_length_in_bites / 32)) {
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R2, R1, #5
UXTH	R2, R2
MOVW	R1, #lo_addr(_actual_bigi_size+0)
MOVT	R1, #hi_addr(_actual_bigi_size+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	EQ
BEQ	L_sw_rsa_key_set8
;rsa_sw_implementation.c,210 :: 		sw_rsa_adjust_length(operation_length_in_bites / 32);
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R1, R1, #5
UXTH	R1, R1
UXTH	R0, R1
BL	_sw_rsa_adjust_length+0
;rsa_sw_implementation.c,211 :: 		}
L_sw_rsa_key_set8:
;rsa_sw_implementation.c,212 :: 		rsa_block_size = operation_length_in_bites >> 3; //  for RSA4096 -> 4096 / 8 = 512B
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R2, R1, #3
UXTH	R2, R2
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,215 :: 		bigi_from_bytes(&p_bigi, key_byte_array, rsa_block_size >> 1); // for RSA 4096 > 512B / 2 = 256B
LSRS	R1, R2, #1
UXTH	R1, R1
UXTH	R2, R1
MOVW	R1, #lo_addr(rsa_sw_implementation_key_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_key_byte_array+0)
MOVW	R0, #lo_addr(_p_bigi+0)
MOVT	R0, #hi_addr(_p_bigi+0)
BL	_bigi_from_bytes+0
;rsa_sw_implementation.c,216 :: 		bigi_from_bytes(&q_bigi, key_byte_array + (rsa_block_size >> 1), rsa_block_size >> 1);
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
LDRH	R1, [R1, #0]
LSRS	R2, R1, #1
UXTH	R2, R2
MOVW	R1, #lo_addr(rsa_sw_implementation_key_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_key_byte_array+0)
ADDS	R1, R1, R2
MOVW	R0, #lo_addr(_q_bigi+0)
MOVT	R0, #hi_addr(_q_bigi+0)
BL	_bigi_from_bytes+0
;rsa_sw_implementation.c,217 :: 		bigi_from_bytes(&e_bigi, key_byte_array + rsa_block_size, rsa_block_size);
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
LDRH	R3, [R1, #0]
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(rsa_sw_implementation_key_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_key_byte_array+0)
ADDS	R1, R1, R2
MOV	R2, R3
MOVW	R0, #lo_addr(_e_bigi+0)
MOVT	R0, #hi_addr(_e_bigi+0)
BL	_bigi_from_bytes+0
;rsa_sw_implementation.c,219 :: 		dbg_puts_labeled ("P:"); bigi_print(&p_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr2_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr2_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_p_bigi+0)
MOVT	R0, #hi_addr(_p_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,220 :: 		dbg_puts_labeled ("Q:"); bigi_print(&q_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr3_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr3_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_q_bigi+0)
MOVT	R0, #hi_addr(_q_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,221 :: 		dbg_puts_labeled ("E:"); bigi_print(&e_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr4_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr4_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_e_bigi+0)
MOVT	R0, #hi_addr(_e_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,224 :: 		bigi_sub_one(&p_bigi,&tmp0_bigi);
MOVW	R1, #lo_addr(_tmp0_bigi+0)
MOVT	R1, #hi_addr(_tmp0_bigi+0)
MOVW	R0, #lo_addr(_p_bigi+0)
MOVT	R0, #hi_addr(_p_bigi+0)
BL	_bigi_sub_one+0
;rsa_sw_implementation.c,225 :: 		bigi_sub_one(&q_bigi,&tmp1_bigi);
MOVW	R1, #lo_addr(_tmp1_bigi+0)
MOVT	R1, #hi_addr(_tmp1_bigi+0)
MOVW	R0, #lo_addr(_q_bigi+0)
MOVT	R0, #hi_addr(_q_bigi+0)
BL	_bigi_sub_one+0
;rsa_sw_implementation.c,226 :: 		dbg_puts_labeled ("P - 1:"); bigi_print(&tmp0_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr5_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr5_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,227 :: 		dbg_puts_labeled ("Q - 1:"); bigi_print(&tmp1_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr6_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr6_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp1_bigi+0)
MOVT	R0, #hi_addr(_tmp1_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,230 :: 		bigi_mult_fit(&p_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &n_bigi);
MOVW	R1, #lo_addr(_n_bigi+0)
MOVT	R1, #hi_addr(_n_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_q_bigi+0)
MOVT	R1, #hi_addr(_q_bigi+0)
MOVW	R0, #lo_addr(_p_bigi+0)
MOVT	R0, #hi_addr(_p_bigi+0)
BL	_bigi_mult_fit+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,231 :: 		dbg_puts_labeled ("N:"); bigi_print(&n_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr7_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr7_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_n_bigi+0)
MOVT	R0, #hi_addr(_n_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,234 :: 		bigi_mult_fit(&tmp0_bigi, &tmp1_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp00_bigi);
MOVW	R1, #lo_addr(_tmp00_bigi+0)
MOVT	R1, #hi_addr(_tmp00_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_tmp1_bigi+0)
MOVT	R1, #hi_addr(_tmp1_bigi+0)
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
BL	_bigi_mult_fit+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,235 :: 		dbg_puts_labeled ("Phi:"); bigi_print(&tmp00_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr8_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr8_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp00_bigi+0)
MOVT	R0, #hi_addr(_tmp00_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,238 :: 		bigi_mod_inv(&e_bigi, &tmp00_bigi, &tmpary_bigi[0], TMPARY_LEN, &d_bigi);
MOVW	R1, #lo_addr(_d_bigi+0)
MOVT	R1, #hi_addr(_d_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_tmp00_bigi+0)
MOVT	R1, #hi_addr(_tmp00_bigi+0)
MOVW	R0, #lo_addr(_e_bigi+0)
MOVT	R0, #hi_addr(_e_bigi+0)
BL	_bigi_mod_inv+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,239 :: 		dbg_puts_labeled ("D:"); bigi_print(&d_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr9_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr9_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_d_bigi+0)
MOVT	R0, #hi_addr(_d_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,242 :: 		bigi_mod_red(&d_bigi, &tmp0_bigi, &tmpary_bigi[0], TMPARY_LEN, &dp_bigi);
MOVW	R1, #lo_addr(_dp_bigi+0)
MOVT	R1, #hi_addr(_dp_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_tmp0_bigi+0)
MOVT	R1, #hi_addr(_tmp0_bigi+0)
MOVW	R0, #lo_addr(_d_bigi+0)
MOVT	R0, #hi_addr(_d_bigi+0)
BL	_bigi_mod_red+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,243 :: 		dbg_puts_labeled ("Dp:"); bigi_print(&dp_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr10_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr10_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_dp_bigi+0)
MOVT	R0, #hi_addr(_dp_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,246 :: 		bigi_mod_red(&d_bigi, &tmp1_bigi, &tmpary_bigi[0], TMPARY_LEN, &dq_bigi);
MOVW	R1, #lo_addr(_dq_bigi+0)
MOVT	R1, #hi_addr(_dq_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_tmp1_bigi+0)
MOVT	R1, #hi_addr(_tmp1_bigi+0)
MOVW	R0, #lo_addr(_d_bigi+0)
MOVT	R0, #hi_addr(_d_bigi+0)
BL	_bigi_mod_red+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,247 :: 		dbg_puts_labeled ("Dq:"); bigi_print(&dq_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr11_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr11_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_dq_bigi+0)
MOVT	R0, #hi_addr(_dq_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,250 :: 		bigi_mod_inv(&q_bigi, &p_bigi, &tmpary_bigi[0], TMPARY_LEN, &qi_bigi);
MOVW	R1, #lo_addr(_qi_bigi+0)
MOVT	R1, #hi_addr(_qi_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_p_bigi+0)
MOVT	R1, #hi_addr(_p_bigi+0)
MOVW	R0, #lo_addr(_q_bigi+0)
MOVT	R0, #hi_addr(_q_bigi+0)
BL	_bigi_mod_inv+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,251 :: 		dbg_puts_labeled ("Qinv:"); bigi_print(&qi_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr12_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr12_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_qi_bigi+0)
MOVT	R0, #hi_addr(_qi_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,253 :: 		return 0x00;
MOVS	R0, #0
;rsa_sw_implementation.c,255 :: 		}
L_end_sw_rsa_key_set:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _sw_rsa_key_set
_sw_rsa_key_get:
;rsa_sw_implementation.c,257 :: 		uint8_t sw_rsa_key_get (uint8_t* k) {
SUB	SP, SP, #4
;rsa_sw_implementation.c,258 :: 		}
L_end_sw_rsa_key_get:
ADD	SP, SP, #4
BX	LR
; end of _sw_rsa_key_get
_sw_rsa_encrypt:
;rsa_sw_implementation.c,260 :: 		uint8_t sw_rsa_encrypt (uint8_t* plaintext) {
; plaintext start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; plaintext end address is: 0 (R0)
; plaintext start address is: 0 (R0)
;rsa_sw_implementation.c,262 :: 		if (encrytion_decryption_state != ENC) {
MOVW	R1, #lo_addr(_encrytion_decryption_state+0)
MOVT	R1, #hi_addr(_encrytion_decryption_state+0)
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_sw_rsa_encrypt9
;rsa_sw_implementation.c,263 :: 		dbg_puts_labeled ("CHANGED TO ENCRIPTION.");
MOVW	R1, #lo_addr(?lstr13_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr13_rsa_sw_implementation+0)
STR	R0, [SP, #4]
MOV	R0, R1
BL	_dbg_puts_labeled+0
LDR	R0, [SP, #4]
;rsa_sw_implementation.c,264 :: 		encrytion_decryption_state = ENC;
MOVS	R2, #0
MOVW	R1, #lo_addr(_encrytion_decryption_state+0)
MOVT	R1, #hi_addr(_encrytion_decryption_state+0)
STRB	R2, [R1, #0]
;rsa_sw_implementation.c,265 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,266 :: 		}
L_sw_rsa_encrypt9:
;rsa_sw_implementation.c,268 :: 		if (message_parts_cnt >= message_parts) {
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CC
BCC	L_sw_rsa_encrypt10
; plaintext end address is: 0 (R0)
;rsa_sw_implementation.c,269 :: 		dbg_puts_labeled ("MSG CNT HIGHER.");
MOVW	R1, #lo_addr(?lstr14_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr14_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;rsa_sw_implementation.c,270 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_sw_rsa_encrypt
;rsa_sw_implementation.c,271 :: 		}
L_sw_rsa_encrypt10:
;rsa_sw_implementation.c,273 :: 		if (message_parts_cnt < message_parts) {
; plaintext start address is: 0 (R0)
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CS
BCS	L_sw_rsa_encrypt11
;rsa_sw_implementation.c,274 :: 		memcpy(plaintext_byte_array + (message_parts_cnt * block_length_in_bytes), plaintext, block_length_in_bytes);
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
LDRH	R3, [R1, #0]
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
MULS	R2, R1, R2
UXTH	R2, R2
MOVW	R1, #lo_addr(rsa_sw_implementation_plaintext_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_plaintext_byte_array+0)
ADDS	R1, R1, R2
STR	R1, [SP, #4]
SXTH	R2, R3
MOV	R1, R0
; plaintext end address is: 0 (R0)
LDR	R0, [SP, #4]
BL	_memcpy+0
;rsa_sw_implementation.c,275 :: 		++message_parts_cnt;
MOVW	R2, #lo_addr(_message_parts_cnt+0)
MOVT	R2, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R2, #0]
ADDS	R1, R1, #1
STRH	R1, [R2, #0]
;rsa_sw_implementation.c,276 :: 		}
L_sw_rsa_encrypt11:
;rsa_sw_implementation.c,278 :: 		if (message_parts_cnt < message_parts) {
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CS
BCS	L_sw_rsa_encrypt12
;rsa_sw_implementation.c,279 :: 		return 0x00;
MOVS	R0, #0
IT	AL
BAL	L_end_sw_rsa_encrypt
;rsa_sw_implementation.c,280 :: 		}
L_sw_rsa_encrypt12:
;rsa_sw_implementation.c,282 :: 		if (actual_bigi_size != (operation_length_in_bites / 32)) {
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R2, R1, #5
UXTH	R2, R2
MOVW	R1, #lo_addr(_actual_bigi_size+0)
MOVT	R1, #hi_addr(_actual_bigi_size+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	EQ
BEQ	L_sw_rsa_encrypt13
;rsa_sw_implementation.c,283 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,284 :: 		dbg_puts_labeled ("ADJUST LENGHT");
MOVW	R1, #lo_addr(?lstr15_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr15_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;rsa_sw_implementation.c,285 :: 		sw_rsa_adjust_length(operation_length_in_bites / 32);
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R1, R1, #5
UXTH	R1, R1
UXTH	R0, R1
BL	_sw_rsa_adjust_length+0
;rsa_sw_implementation.c,286 :: 		}
L_sw_rsa_encrypt13:
;rsa_sw_implementation.c,288 :: 		if (key_parts_cnt != key_parts) {
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	EQ
BEQ	L_sw_rsa_encrypt14
;rsa_sw_implementation.c,289 :: 		dbg_puts_labeled ("KEY PARTS NOT MATCH");
MOVW	R1, #lo_addr(?lstr16_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr16_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;rsa_sw_implementation.c,290 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_sw_rsa_encrypt
;rsa_sw_implementation.c,291 :: 		}
L_sw_rsa_encrypt14:
;rsa_sw_implementation.c,295 :: 		bigi_from_bytes(&plaintext_bigi, plaintext_byte_array, rsa_block_size);
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
LDRH	R1, [R1, #0]
MOV	R2, R1
MOVW	R1, #lo_addr(rsa_sw_implementation_plaintext_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_plaintext_byte_array+0)
MOVW	R0, #lo_addr(_plaintext_bigi+0)
MOVT	R0, #hi_addr(_plaintext_bigi+0)
BL	_bigi_from_bytes+0
;rsa_sw_implementation.c,296 :: 		dbg_puts_labeled ("PLAINTEXT :"); bigi_print(&plaintext_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr17_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr17_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_plaintext_bigi+0)
MOVT	R0, #hi_addr(_plaintext_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,299 :: 		bigi_get_mont_params(&n_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp00_bigi, &mu_mult);
MOVW	R1, #lo_addr(_mu_mult+0)
MOVT	R1, #hi_addr(_mu_mult+0)
PUSH	(R1)
MOVW	R3, #lo_addr(_tmp00_bigi+0)
MOVT	R3, #hi_addr(_tmp00_bigi+0)
MOVS	R2, #20
MOVW	R1, #lo_addr(_tmpary_bigi+0)
MOVT	R1, #hi_addr(_tmpary_bigi+0)
MOVW	R0, #lo_addr(_n_bigi+0)
MOVT	R0, #hi_addr(_n_bigi+0)
BL	_bigi_get_mont_params+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,300 :: 		dbg_puts_labeled ("R:"); bigi_print(&tmp00_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr18_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr18_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp00_bigi+0)
MOVT	R0, #hi_addr(_tmp00_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,302 :: 		bigi_mod_exp_mont(&plaintext_bigi, &e_bigi, &n_bigi, &tmp00_bigi, mu_mult, &tmpary_bigi[0], TMPARY_LEN, &ciphertext_bigi);
MOVW	R4, #lo_addr(_ciphertext_bigi+0)
MOVT	R4, #hi_addr(_ciphertext_bigi+0)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_mu_mult+0)
MOVT	R1, #hi_addr(_mu_mult+0)
LDR	R1, [R1, #0]
PUSH	(R4)
PUSH	(R3)
PUSH	(R2)
PUSH	(R1)
MOVW	R3, #lo_addr(_tmp00_bigi+0)
MOVT	R3, #hi_addr(_tmp00_bigi+0)
MOVW	R2, #lo_addr(_n_bigi+0)
MOVT	R2, #hi_addr(_n_bigi+0)
MOVW	R1, #lo_addr(_e_bigi+0)
MOVT	R1, #hi_addr(_e_bigi+0)
MOVW	R0, #lo_addr(_plaintext_bigi+0)
MOVT	R0, #hi_addr(_plaintext_bigi+0)
BL	_bigi_mod_exp_mont+0
ADD	SP, SP, #16
;rsa_sw_implementation.c,303 :: 		dbg_puts_labeled ("CIPHERTEXT:"); bigi_print(&ciphertext_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr19_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr19_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_ciphertext_bigi+0)
MOVT	R0, #hi_addr(_ciphertext_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,314 :: 		bigi_to_bytes(ciphertext_byte_array, rsa_block_size, &ciphertext_bigi);
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
LDRH	R1, [R1, #0]
MOVW	R2, #lo_addr(_ciphertext_bigi+0)
MOVT	R2, #hi_addr(_ciphertext_bigi+0)
MOVW	R0, #lo_addr(rsa_sw_implementation_ciphertext_byte_array+0)
MOVT	R0, #hi_addr(rsa_sw_implementation_ciphertext_byte_array+0)
BL	_bigi_to_bytes+0
;rsa_sw_implementation.c,316 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,317 :: 		while (message_parts_cnt < message_parts) {
L_sw_rsa_encrypt15:
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CS
BCS	L_sw_rsa_encrypt16
;rsa_sw_implementation.c,318 :: 		simpleserial_put('r', block_length_in_bytes, ciphertext_byte_array + (message_parts_cnt * block_length_in_bytes));
MOVW	R3, #lo_addr(_block_length_in_bytes+0)
MOVT	R3, #hi_addr(_block_length_in_bytes+0)
LDRH	R2, [R3, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
MULS	R2, R1, R2
UXTH	R2, R2
MOVW	R1, #lo_addr(rsa_sw_implementation_ciphertext_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_ciphertext_byte_array+0)
ADDS	R2, R1, R2
MOV	R1, R3
LDRH	R1, [R1, #0]
MOVS	R0, #114
BL	_simpleserial_put+0
;rsa_sw_implementation.c,319 :: 		++message_parts_cnt;
MOVW	R2, #lo_addr(_message_parts_cnt+0)
MOVT	R2, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R2, #0]
ADDS	R1, R1, #1
STRH	R1, [R2, #0]
;rsa_sw_implementation.c,320 :: 		}
IT	AL
BAL	L_sw_rsa_encrypt15
L_sw_rsa_encrypt16:
;rsa_sw_implementation.c,322 :: 		return 0x00;
MOVS	R0, #0
;rsa_sw_implementation.c,324 :: 		}
L_end_sw_rsa_encrypt:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _sw_rsa_encrypt
_sw_rsa_decrypt:
;rsa_sw_implementation.c,326 :: 		uint8_t sw_rsa_decrypt (uint8_t* ciphertext) {
; ciphertext start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; ciphertext end address is: 0 (R0)
; ciphertext start address is: 0 (R0)
;rsa_sw_implementation.c,328 :: 		if (encrytion_decryption_state != DEC) {
MOVW	R1, #lo_addr(_encrytion_decryption_state+0)
MOVT	R1, #hi_addr(_encrytion_decryption_state+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	EQ
BEQ	L_sw_rsa_decrypt17
;rsa_sw_implementation.c,329 :: 		dbg_puts_labeled ("CHANGED TO DECRIPTION.");
MOVW	R1, #lo_addr(?lstr20_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr20_rsa_sw_implementation+0)
STR	R0, [SP, #4]
MOV	R0, R1
BL	_dbg_puts_labeled+0
LDR	R0, [SP, #4]
;rsa_sw_implementation.c,330 :: 		encrytion_decryption_state = DEC;
MOVS	R2, #1
MOVW	R1, #lo_addr(_encrytion_decryption_state+0)
MOVT	R1, #hi_addr(_encrytion_decryption_state+0)
STRB	R2, [R1, #0]
;rsa_sw_implementation.c,331 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,332 :: 		}
L_sw_rsa_decrypt17:
;rsa_sw_implementation.c,334 :: 		if (message_parts_cnt >= message_parts) {
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CC
BCC	L_sw_rsa_decrypt18
; ciphertext end address is: 0 (R0)
;rsa_sw_implementation.c,335 :: 		dbg_puts_labeled ("MSG CNT HIGHER.");
MOVW	R1, #lo_addr(?lstr21_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr21_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;rsa_sw_implementation.c,336 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_sw_rsa_decrypt
;rsa_sw_implementation.c,337 :: 		}
L_sw_rsa_decrypt18:
;rsa_sw_implementation.c,339 :: 		if (message_parts_cnt < message_parts) {
; ciphertext start address is: 0 (R0)
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CS
BCS	L_sw_rsa_decrypt19
;rsa_sw_implementation.c,340 :: 		memcpy(ciphertext_byte_array + (message_parts_cnt * block_length_in_bytes), ciphertext, block_length_in_bytes);
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
LDRH	R3, [R1, #0]
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
MULS	R2, R1, R2
UXTH	R2, R2
MOVW	R1, #lo_addr(rsa_sw_implementation_ciphertext_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_ciphertext_byte_array+0)
ADDS	R1, R1, R2
STR	R1, [SP, #4]
SXTH	R2, R3
MOV	R1, R0
; ciphertext end address is: 0 (R0)
LDR	R0, [SP, #4]
BL	_memcpy+0
;rsa_sw_implementation.c,341 :: 		++message_parts_cnt;
MOVW	R2, #lo_addr(_message_parts_cnt+0)
MOVT	R2, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R2, #0]
ADDS	R1, R1, #1
STRH	R1, [R2, #0]
;rsa_sw_implementation.c,342 :: 		}
L_sw_rsa_decrypt19:
;rsa_sw_implementation.c,344 :: 		if (message_parts_cnt < message_parts) {
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CS
BCS	L_sw_rsa_decrypt20
;rsa_sw_implementation.c,345 :: 		return 0x00;
MOVS	R0, #0
IT	AL
BAL	L_end_sw_rsa_decrypt
;rsa_sw_implementation.c,346 :: 		}
L_sw_rsa_decrypt20:
;rsa_sw_implementation.c,348 :: 		if (actual_bigi_size != (operation_length_in_bites / 32)) {
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R2, R1, #5
UXTH	R2, R2
MOVW	R1, #lo_addr(_actual_bigi_size+0)
MOVT	R1, #hi_addr(_actual_bigi_size+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	EQ
BEQ	L_sw_rsa_decrypt21
;rsa_sw_implementation.c,349 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,350 :: 		dbg_puts_labeled ("ADJUST LENGHT");
MOVW	R1, #lo_addr(?lstr22_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr22_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;rsa_sw_implementation.c,351 :: 		sw_rsa_adjust_length(operation_length_in_bites / 32);
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
LSRS	R1, R1, #5
UXTH	R1, R1
UXTH	R0, R1
BL	_sw_rsa_adjust_length+0
;rsa_sw_implementation.c,352 :: 		}
L_sw_rsa_decrypt21:
;rsa_sw_implementation.c,354 :: 		if (key_parts_cnt != key_parts) {
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	EQ
BEQ	L_sw_rsa_decrypt22
;rsa_sw_implementation.c,355 :: 		dbg_puts_labeled ("KEY PARTS NOT MATCH");
MOVW	R1, #lo_addr(?lstr23_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr23_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;rsa_sw_implementation.c,356 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_sw_rsa_decrypt
;rsa_sw_implementation.c,357 :: 		}
L_sw_rsa_decrypt22:
;rsa_sw_implementation.c,359 :: 		bigi_from_bytes(&ciphertext_bigi, ciphertext_byte_array, rsa_block_size);
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
LDRH	R1, [R1, #0]
MOV	R2, R1
MOVW	R1, #lo_addr(rsa_sw_implementation_ciphertext_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_ciphertext_byte_array+0)
MOVW	R0, #lo_addr(_ciphertext_bigi+0)
MOVT	R0, #hi_addr(_ciphertext_bigi+0)
BL	_bigi_from_bytes+0
;rsa_sw_implementation.c,360 :: 		dbg_puts_labeled ("CIPHERTEXT :"); bigi_print(&ciphertext_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr24_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr24_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_ciphertext_bigi+0)
MOVT	R0, #hi_addr(_ciphertext_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,385 :: 		bigi_mod_red(&ciphertext_bigi, &p_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp1_bigi);
MOVW	R1, #lo_addr(_tmp1_bigi+0)
MOVT	R1, #hi_addr(_tmp1_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_p_bigi+0)
MOVT	R1, #hi_addr(_p_bigi+0)
MOVW	R0, #lo_addr(_ciphertext_bigi+0)
MOVT	R0, #hi_addr(_ciphertext_bigi+0)
BL	_bigi_mod_red+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,386 :: 		dbg_puts_labeled ("Cp :"); bigi_print(&tmp1_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr25_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr25_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp1_bigi+0)
MOVT	R0, #hi_addr(_tmp1_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,388 :: 		bigi_mod_exp(&tmp1_bigi, &dp_bigi, &p_bigi, &tmp2_bigi, &tmp0_bigi);
MOVW	R1, #lo_addr(_tmp0_bigi+0)
MOVT	R1, #hi_addr(_tmp0_bigi+0)
PUSH	(R1)
MOVW	R3, #lo_addr(_tmp2_bigi+0)
MOVT	R3, #hi_addr(_tmp2_bigi+0)
MOVW	R2, #lo_addr(_p_bigi+0)
MOVT	R2, #hi_addr(_p_bigi+0)
MOVW	R1, #lo_addr(_dp_bigi+0)
MOVT	R1, #hi_addr(_dp_bigi+0)
MOVW	R0, #lo_addr(_tmp1_bigi+0)
MOVT	R0, #hi_addr(_tmp1_bigi+0)
BL	_bigi_mod_exp+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,389 :: 		dbg_puts_labeled ("M1 :"); bigi_print(&tmp0_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr26_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr26_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,393 :: 		bigi_mod_red(&ciphertext_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp2_bigi);
MOVW	R1, #lo_addr(_tmp2_bigi+0)
MOVT	R1, #hi_addr(_tmp2_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_q_bigi+0)
MOVT	R1, #hi_addr(_q_bigi+0)
MOVW	R0, #lo_addr(_ciphertext_bigi+0)
MOVT	R0, #hi_addr(_ciphertext_bigi+0)
BL	_bigi_mod_red+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,394 :: 		dbg_puts_labeled ("Cq :"); bigi_print(&tmp2_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr27_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr27_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,396 :: 		bigi_mod_exp(&tmp2_bigi, &dq_bigi, &q_bigi, &tmp3_bigi, &tmp1_bigi);
MOVW	R1, #lo_addr(_tmp1_bigi+0)
MOVT	R1, #hi_addr(_tmp1_bigi+0)
PUSH	(R1)
MOVW	R3, #lo_addr(_tmp3_bigi+0)
MOVT	R3, #hi_addr(_tmp3_bigi+0)
MOVW	R2, #lo_addr(_q_bigi+0)
MOVT	R2, #hi_addr(_q_bigi+0)
MOVW	R1, #lo_addr(_dq_bigi+0)
MOVT	R1, #hi_addr(_dq_bigi+0)
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_mod_exp+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,397 :: 		dbg_puts_labeled ("M2 :"); bigi_print(&tmp1_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr28_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr28_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp1_bigi+0)
MOVT	R0, #hi_addr(_tmp1_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,401 :: 		bigi_sub(&tmp0_bigi, &tmp1_bigi, &tmp2_bigi);
MOVW	R2, #lo_addr(_tmp2_bigi+0)
MOVT	R2, #hi_addr(_tmp2_bigi+0)
MOVW	R1, #lo_addr(_tmp1_bigi+0)
MOVT	R1, #hi_addr(_tmp1_bigi+0)
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
BL	_bigi_sub+0
;rsa_sw_implementation.c,402 :: 		bigi_shift_left(&tmp2_bigi, 96);
MOVS	R1, #96
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_shift_left+0
;rsa_sw_implementation.c,403 :: 		dbg_puts_labeled ("M1 - M2 << 96:"); bigi_print(&tmp2_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr29_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr29_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,404 :: 		bigi_shift_right(&tmp2_bigi, 96);
MOVS	R1, #96
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_shift_right+0
;rsa_sw_implementation.c,405 :: 		dbg_puts_labeled ("M1 - M2 >> 96:"); bigi_print(&tmp2_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr30_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr30_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,408 :: 		dbg_puts_labeled ("M1 - M2 :"); bigi_print(&tmp2_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr31_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr31_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,410 :: 		bigi_mod_mult (&tmp2_bigi, &qi_bigi, &p_bigi, &tmp0_bigi);
MOVW	R3, #lo_addr(_tmp0_bigi+0)
MOVT	R3, #hi_addr(_tmp0_bigi+0)
MOVW	R2, #lo_addr(_p_bigi+0)
MOVT	R2, #hi_addr(_p_bigi+0)
MOVW	R1, #lo_addr(_qi_bigi+0)
MOVT	R1, #hi_addr(_qi_bigi+0)
MOVW	R0, #lo_addr(_tmp2_bigi+0)
MOVT	R0, #hi_addr(_tmp2_bigi+0)
BL	_bigi_mod_mult+0
;rsa_sw_implementation.c,411 :: 		dbg_puts_labeled ("H :"); bigi_print(&tmp0_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr32_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr32_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,415 :: 		bigi_mult_fit(&tmp0_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp00_bigi);
MOVW	R1, #lo_addr(_tmp00_bigi+0)
MOVT	R1, #hi_addr(_tmp00_bigi+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary_bigi+0)
MOVT	R2, #hi_addr(_tmpary_bigi+0)
MOVW	R1, #lo_addr(_q_bigi+0)
MOVT	R1, #hi_addr(_q_bigi+0)
MOVW	R0, #lo_addr(_tmp0_bigi+0)
MOVT	R0, #hi_addr(_tmp0_bigi+0)
BL	_bigi_mult_fit+0
ADD	SP, SP, #4
;rsa_sw_implementation.c,416 :: 		dbg_puts_labeled ("H * Q :"); bigi_print(&tmp00_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr33_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr33_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_tmp00_bigi+0)
MOVT	R0, #hi_addr(_tmp00_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,418 :: 		bigi_add(&tmp1_bigi, &tmp00_bigi, &plaintext_bigi);
MOVW	R2, #lo_addr(_plaintext_bigi+0)
MOVT	R2, #hi_addr(_plaintext_bigi+0)
MOVW	R1, #lo_addr(_tmp00_bigi+0)
MOVT	R1, #hi_addr(_tmp00_bigi+0)
MOVW	R0, #lo_addr(_tmp1_bigi+0)
MOVT	R0, #hi_addr(_tmp1_bigi+0)
BL	_bigi_add+0
;rsa_sw_implementation.c,419 :: 		dbg_puts_labeled ("PLAINTEXT :"); bigi_print(&plaintext_bigi); dbg_putch('\n');
MOVW	R1, #lo_addr(?lstr34_rsa_sw_implementation+0)
MOVT	R1, #hi_addr(?lstr34_rsa_sw_implementation+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
MOVW	R0, #lo_addr(_plaintext_bigi+0)
MOVT	R0, #hi_addr(_plaintext_bigi+0)
BL	_bigi_print+0
MOVS	R0, #10
BL	_dbg_putch+0
;rsa_sw_implementation.c,423 :: 		bigi_to_bytes(plaintext_byte_array, rsa_block_size, &plaintext_bigi);
MOVW	R1, #lo_addr(_rsa_block_size+0)
MOVT	R1, #hi_addr(_rsa_block_size+0)
LDRH	R1, [R1, #0]
MOVW	R2, #lo_addr(_plaintext_bigi+0)
MOVT	R2, #hi_addr(_plaintext_bigi+0)
MOVW	R0, #lo_addr(rsa_sw_implementation_plaintext_byte_array+0)
MOVT	R0, #hi_addr(rsa_sw_implementation_plaintext_byte_array+0)
BL	_bigi_to_bytes+0
;rsa_sw_implementation.c,425 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;rsa_sw_implementation.c,426 :: 		while (message_parts_cnt < message_parts) {
L_sw_rsa_decrypt23:
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
LDRH	R2, [R1, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
CMP	R1, R2
IT	CS
BCS	L_sw_rsa_decrypt24
;rsa_sw_implementation.c,427 :: 		simpleserial_put('r', block_length_in_bytes, plaintext_byte_array + (message_parts_cnt * block_length_in_bytes));
MOVW	R3, #lo_addr(_block_length_in_bytes+0)
MOVT	R3, #hi_addr(_block_length_in_bytes+0)
LDRH	R2, [R3, #0]
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R1, #0]
MULS	R2, R1, R2
UXTH	R2, R2
MOVW	R1, #lo_addr(rsa_sw_implementation_plaintext_byte_array+0)
MOVT	R1, #hi_addr(rsa_sw_implementation_plaintext_byte_array+0)
ADDS	R2, R1, R2
MOV	R1, R3
LDRH	R1, [R1, #0]
MOVS	R0, #114
BL	_simpleserial_put+0
;rsa_sw_implementation.c,428 :: 		++message_parts_cnt;
MOVW	R2, #lo_addr(_message_parts_cnt+0)
MOVT	R2, #hi_addr(_message_parts_cnt+0)
LDRH	R1, [R2, #0]
ADDS	R1, R1, #1
STRH	R1, [R2, #0]
;rsa_sw_implementation.c,429 :: 		}
IT	AL
BAL	L_sw_rsa_decrypt23
L_sw_rsa_decrypt24:
;rsa_sw_implementation.c,431 :: 		return 0x00;
MOVS	R0, #0
;rsa_sw_implementation.c,432 :: 		}
L_end_sw_rsa_decrypt:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _sw_rsa_decrypt
