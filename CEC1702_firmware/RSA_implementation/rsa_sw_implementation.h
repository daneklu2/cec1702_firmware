#ifndef RSA_SW_IMPLEMENTATION
#define RSA_SW_IMPLEMENTATION

#include <stdint.h>

void sw_rsa_init (void);

uint8_t sw_rsa_key_set (uint8_t* k);

uint8_t sw_rsa_key_get (uint8_t* k);

uint8_t sw_rsa_encrypt (uint8_t* plaintext);

uint8_t sw_rsa_decrypt (uint8_t* ciphertext);

#endif