#ifndef BIGI_IMPL_H
#define BIGI_IMPL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "bigi.h"

/* MAX_VAL must be double word type due to comparisons with numbers larger than machine word */
#if MACHINE_WORD_BITLEN == 8
    #define MAX_VAL         ((dword_t)0xFF)
#elif MACHINE_WORD_BITLEN == 16
    #define MAX_VAL         ((dword_t)0xFFFF)
#elif (MACHINE_WORD_BITLEN == 32) || (MACHINE_WORD_BITLEN == 64)
    #define MAX_VAL         ((dword_t)0xFFFFFFFF)
#else
    #error MSG_WRONG_MW_BITLEN
#endif

/**
 *  @brief        Black magic
 *
 *  @param        Unsigned integer
 *  @return       TBD
 *
 */
byte_t nlz(word_t x);

static const word_t MultiplyDeBruijnBitPosition[32] =
{
     0,  1, 28,  2, 29, 14, 24,  3, 30, 22, 20, 15, 25, 17,  4,  8,
    31, 27, 13, 23, 21, 19, 16,  7, 26, 12, 18,  6, 11,  5, 10,  9
};

#ifdef __cplusplus
}
#endif

#endif