#include "bigi_communication.h"
#include "bigi.h"
#include "bigi_io.h"
#include "../../simpleserial/simpleserial.h"
#include "../../debug.h"

#ifdef DEMO

#define EXAMPLE_BITLEN 128
#define EXAMPLE_TMPARY_LEN 20
#define BUFFLEN ((EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN + 3) * HEXCHARS_PER_MW)
word_t demo_tmp_arys[EXAMPLE_TMPARY_LEN * (EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN)];
bigi_t tmpary[EXAMPLE_TMPARY_LEN];
word_t  a_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  b_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  p_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  q_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  n_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  x_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  r_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t ar_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t br_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t mu_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  y_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
word_t  z_ary[EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN];
char * demo128_sp = "0000000000000000C6288119B3E3C199";
char * demo128_sq = "0000000000000000DDAC808AAA1DB0FF";
char * demo128_sa = "4e8839e2876bcb217511f129c68265a6";
char * demo128_sb = "a892170d2e9809870923f82e150ac166";
char * demo128_sn = "ab967e2983a0ce46edd7c01d4d4c0767";
char buff[BUFFLEN];
char output[32];
word_t demo_mu_mult;
index_t i;
bigi_t a, b, r, ar, br, mu, p, q, n, x, y, z;

static uint8_t INIT_STATE = 0;

uint8_t demo128_init () {
        a.ary = a_ary;
    a.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    a.domain = DOMAIN_NORMAL;
    b.ary = b_ary;
    b.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    b.domain = DOMAIN_NORMAL;
    p.ary = p_ary;
    p.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    p.domain = DOMAIN_NORMAL;
    q.ary = q_ary;
    q.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    q.domain = DOMAIN_NORMAL;
    n.ary = n_ary;
    n.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    n.domain = DOMAIN_NORMAL;
    r.ary = r_ary;
    r.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    r.domain = DOMAIN_NORMAL;
    ar.ary = ar_ary;
    ar.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    ar.domain = DOMAIN_MONTGOMERY;
    br.ary = br_ary;
    br.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    br.domain = DOMAIN_MONTGOMERY;
    mu.ary = mu_ary;
    mu.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    mu.domain = DOMAIN_NORMAL;
    x.ary = x_ary;
    x.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    x.domain = DOMAIN_NORMAL;
    y.ary = y_ary;
    y.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    y.domain = DOMAIN_NORMAL;
    z.ary = z_ary;
    z.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
    z.domain = DOMAIN_NORMAL;

    for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
    {
        tmpary[i].ary      = &demo_tmp_arys[i * (EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN)];
        tmpary[i].wordlen  = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
        tmpary[i].alloclen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
        tmpary[i].domain   = DOMAIN_NORMAL;
    }

    /* load from hex */
    bigi_from_hex(demo128_sa, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &a);    
    bigi_from_hex(demo128_sb, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &b);    
    bigi_from_hex(demo128_sp, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &p);    
    bigi_from_hex(demo128_sq, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &q);    
    bigi_from_hex(demo128_sn, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &n); 
    INIT_STATE = 0x01;
        return 0x00;
}



uint8_t demo128_bigi_add (uint8_t* params) {
        if (0x01 != INIT_STATE) {
                if (0x00 != demo128_init()) {
                         return 0x01;
                }
        }
        bigi_add(&a, &b, &x);
        bigi_to_bytes(output, 16, &x);

        dbg_puts_labeled ("A:");
        bigi_print(&a);
        dbg_putch('\n');
        dbg_puts_labeled ("B:");
        bigi_print(&b);
        dbg_putch('\n');
        dbg_puts_labeled ("X:");
        bigi_print(&x);
        dbg_putch('\n');
    
        simpleserial_put('r', 16, output);
        return 0x00;


}
uint8_t demo128_bigi_mod_exp (uint8_t* params) {
        if (0x01 != INIT_STATE) {
                if (0x00 != demo128_init()) {
                         return 0x01;
                }
        }
        bigi_mod_exp(&a, &b, &n, &y, &x);
        bigi_to_bytes(output, 16, &x);

        dbg_puts_labeled ("A:");
        bigi_print(&a);
        dbg_putch('\n');
        dbg_puts_labeled ("B:");
        bigi_print(&b);
        dbg_putch('\n');
        dbg_puts_labeled ("N:");
        bigi_print(&n);
        dbg_putch('\n');
        dbg_puts_labeled ("X:");
        bigi_print(&x);
        dbg_putch('\n');
    
        simpleserial_put('r', 16, output);
        return 0x00;
}

uint8_t demo128_bigi_gcd (uint8_t* params) {
        if (0x01 != INIT_STATE) {
                if (0x00 != demo128_init()) {
                         return 0x01;
                }
        }
        bigi_gcd(&a, &b, &tmpary[0], EXAMPLE_TMPARY_LEN, &x);
        bigi_to_bytes(output, 16, &x);

        dbg_puts_labeled ("A:");
        bigi_print(&a);
        dbg_putch('\n');
        dbg_puts_labeled ("B:");
        bigi_print(&b);
        dbg_putch('\n');
        dbg_puts_labeled ("X:");
        bigi_print(&x);
        dbg_putch('\n');
    
        simpleserial_put('r', 16, output);
        return 0x00;
        return 0x00;
}

uint8_t demo128_bigi_mod_exp_mont (uint8_t* params) {
        if (0x01 != INIT_STATE) {
                if (0x00 != demo128_init()) {
                         return 0x01;
                }
        }
        bigi_get_mont_params(&n, &tmpary[0], EXAMPLE_TMPARY_LEN, &r, &demo_mu_mult);
        dbg_puts_labeled ("R:");
        bigi_print(&r);
        dbg_putch('\n');
        bigi_mod_exp_mont(&a, &b, &n, &r, demo_mu_mult, &tmpary[0], EXAMPLE_TMPARY_LEN, &x);

        bigi_to_bytes(output, 16, &x);

        dbg_puts_labeled ("A:");
        bigi_print(&a);
        dbg_putch('\n');
        dbg_puts_labeled ("B:");
        bigi_print(&b);
        dbg_putch('\n');
        dbg_puts_labeled ("X:");
        bigi_print(&x);
        dbg_putch('\n');
    
        simpleserial_put('r', 16, output);
        return 0x00;
        return 0x00;
}
#endif   //DEMO