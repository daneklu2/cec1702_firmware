#ifndef BIGI_H
#define BIGI_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#if defined(DEBUG) || defined (TEST)
    #define DBGT
    #ifndef __MIKROC_PRO_FOR_ARM__
        #include <stdio.h> /* sprintf; in MikroC sprintf must be chosen in the list of libs */
    #endif
#endif



/* *****************************************************************************
 *
 *  TYPES & DEFINITIONS
 *                                                                            */

/* -----------------------------------------------------------------------------
 *  General types & definitions
 *                                                                            */
#define BITS_IN_BYTE 8
#define MULT_BUFFER_WORDLEN 3
#define MSG_WRONG_MW_BITLEN "Unsupported machine word bit-length."
typedef uint8_t byte_t;

/* -----------------------------------------------------------------------------
 *  Machine word bit-/byte-length, type
 *  (customizable, not necessarily by platform)
 *                                                                            */
#ifndef MACHINE_WORD_BITLEN
    #define MACHINE_WORD_BITLEN 32
#endif
#define MACHINE_WORD_BYTELEN ((MACHINE_WORD_BITLEN)/(BITS_IN_BYTE))

#if MACHINE_WORD_BITLEN ==      8
    typedef uint8_t             word_t;
    typedef uint16_t            dword_t;
    typedef int16_t             dsgn_t;

    typedef dword_t             index_t;    /* 16 bits */
    #define MAX_BIGI_BITLEN     32768       /* indexable with 16 bits */
    #define MAX_BIGI_BYTELEN    (MAX_BIGI_BITLEN / BITS_IN_BYTE)

#elif MACHINE_WORD_BITLEN ==    16
    typedef uint16_t            word_t;
    typedef uint32_t            dword_t;
    typedef int32_t             dsgn_t;

    typedef word_t              index_t;    /* 16 bits */
    #define MAX_BIGI_BITLEN     32768       /* indexable with 16 bits */
    #define MAX_BIGI_BYTELEN    (MAX_BIGI_BITLEN / BITS_IN_BYTE)

#elif (MACHINE_WORD_BITLEN ==   32) || (MACHINE_WORD_BITLEN == 64) /* use max 64-bit types */
    typedef uint32_t            word_t;
    typedef uint64_t            dword_t;
    typedef int64_t             dsgn_t;

    typedef word_t              index_t;    /* 32 bits */
    #define MAX_BIGI_BITLEN     2147483648  /* indexable with 32 bits */
    #define MAX_BIGI_BYTELEN    (MAX_BIGI_BITLEN / BITS_IN_BYTE)

#else
    #error MSG_WRONG_MW_BITLEN
#endif

/* -----------------------------------------------------------------------------
 *  bigi struct
 *                                                                            */
typedef enum
{
    DOMAIN_NORMAL       = 0,
    DOMAIN_MONTGOMERY   = 1
} domain_t;

typedef struct bigi_t
{
    word_t * ary;       /* pointer to the array of (wordlen + MULT_BUFFER_WORDLEN) machine words */
    index_t wordlen;    /* length of bigi number (in machine words) */
    index_t alloclen;   /* length of allocated memory (in machine words, excl MULT_BUFFER_WORDLEN) */
                        /* intended ONLY for temporary variables !! */
                        /* TODO: consider const */
    domain_t domain;    /* Montgomery domain flag */
} bigi_t;

/* -----------------------------------------------------------------------------
 *  Comparison enum
 *                                                                            */
typedef enum
{
    CMP_EQUAL       = 0,
    CMP_NEQ         = 1,
    CMP_LOWER       = 2,
    CMP_GREATER     = 3,
    CMP_UNDEFINED   = 11
} cmp_t;

#ifdef DBGT
    /* -------------------------------------------------------------------------
     *  bigi error codes in DEBUG and TEST mode
     *                                                                        */
    typedef enum
    {
        /* general error codes */
        OK                      = 0,
        ERR_UNDEFINED           = 1,
        ERR_NULLPTR             = 2,
        ERR_OUT_OF_RANGE        = 3,
        ERR_BUFFER_OVERFLOW     = 4,
        ERR_NOT_ENOUHGH_MEMORY  = 5,
        ERR_INVALID_PARAMS      = 6,
        ERR_HW                  = 7,
        /* bigi-specific error codes */
        BIGI_WRONG_SIZE         = 11,
        BIGI_OVERFLOW           = 12,
        BIGI_WRONG_DOMAIN       = 13,
        BIGI_DIV_BY_ZERO        = 14,
        BIGI_NOT_COPRIME        = 15
    } BigiStatus;

    static const char STR_OK[] = "OK";
    static const char STR_ERR_UNDEFINED[] = "ERR_UNDEFINED";
    static const char STR_ERR_NULLPTR[] = "ERR_NULLPTR";
    static const char STR_ERR_OUT_OF_RANGE[] = "ERR_OUT_OF_RANGE";
    static const char STR_ERR_BUFFER_OVERFLOW[] = "ERR_BUFFER_OVERFLOW";
    static const char STR_ERR_NOT_ENOUHGH_MEMORY[] = "ERR_NOT_ENOUHGH_MEMORY";
    static const char STR_ERR_INVALID_PARAMS[] = "ERR_INVALID_PARAMS";
    static const char STR_ERR_HW[] = "ERR_HW";

    static const char STR_BIGI_WRONG_SIZE[] = "BIGI_WRONG_SIZE";
    static const char STR_BIGI_OVERFLOW[] = "BIGI_OVERFLOW";
    static const char STR_BIGI_WRONG_DOMAIN[] = "BIGI_WRONG_DOMAIN";
    static const char STR_BIGI_DIV_BY_ZERO[] = "BIGI_DIV_BY_ZERO";
    static const char STR_BIGI_NOT_COPRIME[] = "BIGI_NOT_COPRIME";

    static const char STR_UNKNOWN_CODE[] = "!!! Unknown error code to be printed !!!";

    const char * bigi_status_str(BigiStatus bigist);

    /* -------------------------------------------------------------------------
     *  Bigi Safe Call: ON
     *                                                                        */
    #define BIGI_STATUS BigiStatus
    #define BIGI_ST_VAR bigist
    #define DECL_BIGI_STATUS BIGI_STATUS BIGI_ST_VAR = ERR_UNDEFINED;

    #define BIGI_CALL(func) {BIGI_ST_VAR = func; if (BIGI_ST_VAR != OK) return BIGI_ST_VAR;}
    #define BIGI_RETURN(func) {return func;}
#else
    /* -------------------------------------------------------------------------
     *  No bigi error codes in production
     *                                                                        */
    #define OK
    #define ERR_UNDEFINED
    #define ERR_NULLPTR
    #define ERR_OUT_OF_RANGE
    #define ERR_BUFFER_OVERFLOW
    #define ERR_NOT_ENOUHGH_MEMORY
    #define ERR_INVALID_PARAMS
    #define ERR_HW

    #define BIGI_WRONG_SIZE
    #define BIGI_OVERFLOW
    #define BIGI_WRONG_DOMAIN
    #define BIGI_DIV_BY_ZERO
    #define BIGI_NOT_COPRIME

    /* -------------------------------------------------------------------------
     *  Bigi Safe Call: OFF
     *                                                                        */
    #define BIGI_STATUS void
    #define DECL_BIGI_STATUS

    #define BIGI_CALL(func) {func;}
    #define BIGI_RETURN(func) {func; return;}
#endif

/* -----------------------------------------------------------------------------
 *  Minimum required amount of temporary values
 *                                                                            */
#define GCD__MIN_TMPARY_LEN 2       /* we need 2 temporary values */
#define MULT_FIT__MIN_TMPARY_LEN 2  /* we need 2 temporary values */
#define DIV__MIN_TMPARY_LEN 4       /* we need 4 temporary values */
#define BARRETT__MIN_TMPARY_LEN (6 + (MULT_FIT__MIN_TMPARY_LEN))
#define MOD_INV__MIN_TMPARY_LEN (8 + (DIV__MIN_TMPARY_LEN))
                                    /* we need 8 + DIV temporary values */
#define MODEXP_MONT__MIN_TMPARY_LEN (3 + (MOD_MULT_MONT__MIN_TMPARY_LEN))
                                    /* we need 8 + max(DIV, MOD_INV) = 8 + MOD_INV temporary values (MOD_INV uses DIV) */
#define MOD_MULT_MONT__MIN_TMPARY_LEN 2
#define FROM_MONT__MIN_TMPARY_LEN (1 + (MOD_MULT_MONT__MIN_TMPARY_LEN))
#define MONT_PARAMS__MIN_TMPARY_LEN (4 + (MOD_INV__MIN_TMPARY_LEN))



/* *****************************************************************************
 *
 *  bigi Functions
 *                                                                            */

/* -----------------------------------------------------------------------------
 *  Init, manipulate, compare, etc.
 *                                                                            */

/**
 *  @brief        Initialize bigi_t with zeroes
 *
 *  @param[in]    bigi_t to be initialized
 *
 */
BIGI_STATUS bigi_set_zero(bigi_t *const A);

/**
 *  @brief        Copy bigi_t
 *
 *  @param[out]   bigi_t destination
 *  @param[in]    bigi_t source
 *
 */
BIGI_STATUS bigi_copy(bigi_t *const OUT,
                      const bigi_t *const IN);

/**
 *  @brief        Convert bigi_t from an array of machine words to an array of bytes
 *
 *  @param[out]   Pointer to the array of bytes (BIGI_MEMORY_BYTELEN bytes long)
 *  @param[in]    bigi_t to be converted
 *
 */
BIGI_STATUS bigi_to_bytes(byte_t *const out,
                          const index_t outlen,
                          const bigi_t *const IN);

/**
 *  @brief        Convert an array of bytes to bigi_t
 *
 *  @param[out]   bigi_t to be loaded
 *  @param[in]    Pointer to the array of bytes (BIGI_MEMORY_BYTELEN bytes long)
 *
 */
BIGI_STATUS bigi_from_bytes(bigi_t *const OUT,
                            const byte_t *const in,
                            const index_t inlen);

/**
 *  @brief        Returns the value of a bit at given position within bigi_t
 *
 *  @param[in]    bigi_t
 *  @param[in]    Position of desired bit, must be within [0..BIGI_BITLEN-1]
 *  @return       {0,1}
 */
BIGI_STATUS bigi_get_bit(const bigi_t *const A,
                         const index_t index,
                         word_t *const res);

/**
 *  @brief        Compare two bigi_t's ( A == B )
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @return       0 when not equal, 1 if equal
 *
 */
BIGI_STATUS bigi_cmp(const bigi_t *const A,
                     const bigi_t *const B,
                     cmp_t *const res);

/**
 *  @brief        Check if bigi_t is zero ( A == 0 )
 *
 *  @param[in]    bigi_t to be compared
 *  @return       0 if A != 0, 1 if A == 0
 *
 */
BIGI_STATUS bigi_is_zero(const bigi_t *const A,
                         cmp_t *const res);

/**
 *  @brief        Check if bigi_t is one ( A == 1 )
 *
 *  @param[in]    bigi_t to be compared
 *  @return       BIGI_STATUS
 *
 */
BIGI_STATUS bigi_is_one(const bigi_t *const A,
                        cmp_t *const res);

/**
 *  @brief        Shorten a temporary value
 *
 *  @param[in]    bigi_t to be shortened
 *  @param[in]    requested wordlen
 *  @return       BIGI_STATUS
 *
 */
BIGI_STATUS bigi_tmp_safe_shorten(bigi_t *const TMP,
                                  const index_t new_wordlen);

/* -----------------------------------------------------------------------------
 *  Arithmetics
 *                                                                            */

/**
 *  @brief        Left shift
 *
 *  @param[in]    bigi_t to be shifted
 *  @param[in]    Amount of bits to be shifted by
 *
 */
BIGI_STATUS bigi_shift_left(bigi_t *const A,
                            const index_t count);

/**
 *  @brief        Right shift
 *
 *  @param[in]    bigi_t to be shifted
 *  @param[in]    Amount of bits to be shifted by
 *
 */
BIGI_STATUS bigi_shift_right(bigi_t *const A,
                             const index_t count);

/**
 *  @brief        Bitwise logical AND
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_and(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES);

/**
 *  @brief        Bitwise logical OR
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_xor(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES);

/**
 *  @brief        Addition
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_add(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES);

/**
 *  @brief        Increment by 1
 *
 *  @param[in]    bigi_t to be incremented
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_add_one(const bigi_t *const A,
                         bigi_t *const RES);

/**
 *  @brief        Subtraction
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_sub(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES);

/**
 *  @brief        Decrement by 1
 *
 *  @param[in]    bigi_t to be decremented
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_sub_one(const bigi_t *const A,
                         bigi_t *const RES);

/**
 *  @brief        Modular reduction A % MOD
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mod_red(const bigi_t *const A,
                         const bigi_t *const MOD,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const RES);

/**
 *  @brief        Barrett's modular reduction A % MOD
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[in]    bigi_t MU (Barrett's pre-computed constant mu)
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mod_red_barrett(const bigi_t *const X,
                                 const bigi_t *const MOD,
                                 const bigi_t *const MU,
                                 bigi_t *const TMPARY,
                                 const index_t tmparylen,
                                 bigi_t *const RES);

/**
 *  @brief        Multiplication by a machine word
 *
 *  @param[in]    bigi_t A
 *  @param[in]    machine word B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mult_word(const bigi_t *const A,
                           const word_t B,
                           bigi_t *const TMP,
                           bigi_t *const RES);

/**
 *  @brief        Multiplication where the result fits into bigi_t
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mult_fit(const bigi_t *const A,
                          const bigi_t *const B,
                          bigi_t *const TMPARY,
                          const index_t tmparylen,
                          bigi_t *const RES);

/**
 *  @brief        Modular multiplication
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mod_mult(const bigi_t *const A,
                          const bigi_t *const B,
                          const bigi_t *const MOD,
                          bigi_t *const RES);

/**
 *  @brief        Modular exponentiation
 *
 *  @param[in]    bigi_t A (base)
 *  @param[in]    bigi_t B (exponent)
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mod_exp(const bigi_t *const A,
                         const bigi_t *const B,
                         const bigi_t *const MOD,
                         bigi_t *const TMP,
                         bigi_t *const RES);

/**
 *  @brief        Modular inverse
 *
 *  @param[in]    bigi_t A to be inverted
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[out]   Result
 *  @return       1 if the inverse exist, 0 if the inverse does not exist
 *
 */
BIGI_STATUS bigi_mod_inv(const bigi_t *const A,
                         const bigi_t *const MOD,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const RES);

/**
 *  @brief        Integer division
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Remainder, if NULL pointer is provided, only quotient is computed
 *  @param[out]   Quotient
 *
 */
BIGI_STATUS bigi_div(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const TMPARY,
                     const index_t tmparylen,
                     bigi_t *const QUO,
                     bigi_t *const REM);

/**
 *  @brief        Greatest Common Divisor
 *
 *  @param[in]    bigi_t A
 *  @param[in]    bigi_t B
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_gcd(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const TMPARY,
                     const index_t tmparylen,
                     bigi_t *const RES);

/* -----------------------------------------------------------------------------
 *  Montgomery domain
 *                                                                            */

/**
 *  @brief        Get Montgomery domain parameters (R, mu)
 *
 *  @param[in]    bigi_t MOD
 *  @param[out]   bigi_t R
 *  @param[out]   word_t mu
 *
 */
BIGI_STATUS bigi_get_mont_params(const bigi_t *const MOD,
                                 bigi_t *const TMPARY,
                                 const index_t tmparylen,
                                 bigi_t *const R,
                                 word_t *const mu);

/**
 *  @brief        Convert number to Montgomery domain
 *
 *  @param[in]    bigi_t A (number to be converted)
 *  @param[in]    bigi_t R (Montgomery domain parameter)
 *  @param[in]    bigi_t MOD
 *  @param[out]   bigi_t AR
 *
 */
BIGI_STATUS bigi_to_mont_domain(const bigi_t *const A,
                                const bigi_t *const MOD,
                                const bigi_t *const R,
                                bigi_t *const AR);

/**
 *  @brief        Convert number from Montgomery domain
 *
 *  @param[in]    bigi_t AR (number to be converted)
 *  @param[in]    bigi_t R (Montgomery domain parameter)
 *  @param[in]    bigi_t MOD
 *  @param[out]   bigi_t A
 *
 */
BIGI_STATUS bigi_from_mont_domain(const bigi_t *const AR,
                                  const bigi_t *const MOD,
                                  const bigi_t *const R,
                                  const word_t mu,
                                  bigi_t *const TMPARY,
                                  const index_t tmparylen,
                                  bigi_t *const A);

/**
 *  @brief        Modular multiplication in Mongomery domain
 *
 *  @param[in]    bigi_t A (in Montgomery domain)
 *  @param[in]    bigi_t B (in Montgomery domain)
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[in]    machine word MU (Montgomery domain parameter)
 *  @param[out]   Result (in Montgomery domain)
 *
 */
BIGI_STATUS bigi_mod_mult_mont(const bigi_t *const AR,
                               const bigi_t *const BR,
                               const bigi_t *const MOD,
                               const word_t mu,
                               bigi_t *const TMPARY,
                               const index_t tmparylen,
                               bigi_t *const RES);

/**
 *  @brief        Modular exponentiation that internally uses Mongomery domain multiplication
 *
 *  @param[in]    bigi_t A (base)
 *  @param[in]    bigi_t B (exponent)
 *  @param[in]    bigi_t MOD (modulus)
 *  @param[out]   Result
 *
 */
BIGI_STATUS bigi_mod_exp_mont(const bigi_t *const A,
                              const bigi_t *const B,
                              const bigi_t *const MOD,
                              const bigi_t *const R,
                              const word_t mu,
                              bigi_t *const TMPARY,
                              const index_t tmparylen,
                              bigi_t *const RES);

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef BIGI_H */
