#line 1 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
#line 1 "c:/mikroc pro for arm/include/stdint.h"





typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int int32_t;
typedef signed long long int64_t;


typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long int uint32_t;
typedef unsigned long long uint64_t;


typedef signed char int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;
typedef signed long long int_least64_t;


typedef unsigned char uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;
typedef unsigned long long uint_least64_t;



typedef signed long int int_fast8_t;
typedef signed long int int_fast16_t;
typedef signed long int int_fast32_t;
typedef signed long long int_fast64_t;


typedef unsigned long int uint_fast8_t;
typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long long uint_fast64_t;


typedef signed long int intptr_t;
typedef unsigned long int uintptr_t;


typedef signed long long intmax_t;
typedef unsigned long long uintmax_t;
#line 1 "c:/mikroc pro for arm/include/stddef.h"



typedef long ptrdiff_t;


 typedef unsigned long size_t;

typedef unsigned long wchar_t;
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 30 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef uint8_t byte_t;
#line 60 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 typedef uint32_t word_t;
 typedef uint64_t dword_t;
 typedef int64_t dsgn_t;

 typedef word_t index_t;
#line 75 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef enum
{
 DOMAIN_NORMAL = 0,
 DOMAIN_MONTGOMERY = 1
} domain_t;

typedef struct bigi_t
{
 word_t * ary;
 index_t wordlen;
 index_t alloclen;


 domain_t domain;
} bigi_t;
#line 94 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef enum
{
 CMP_EQUAL = 0,
 CMP_NEQ = 1,
 CMP_LOWER = 2,
 CMP_GREATER = 3,
 CMP_UNDEFINED = 11
} cmp_t;
#line 215 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_set_zero(bigi_t *const A);
#line 224 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_copy(bigi_t *const OUT,
 const bigi_t *const IN);
#line 234 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_to_bytes(byte_t *const out,
 const index_t outlen,
 const bigi_t *const IN);
#line 245 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_from_bytes(bigi_t *const OUT,
 const byte_t *const in,
 const index_t inlen);
#line 256 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_get_bit(const bigi_t *const A,
 const index_t index,
 word_t *const res);
#line 268 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_cmp(const bigi_t *const A,
 const bigi_t *const B,
 cmp_t *const res);
#line 279 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_is_zero(const bigi_t *const A,
 cmp_t *const res);
#line 289 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_is_one(const bigi_t *const A,
 cmp_t *const res);
#line 300 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_tmp_safe_shorten(bigi_t *const TMP,
 const index_t new_wordlen);
#line 314 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_shift_left(bigi_t *const A,
 const index_t count);
#line 324 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_shift_right(bigi_t *const A,
 const index_t count);
#line 335 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_and(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 347 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_xor(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 359 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_add(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 370 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_add_one(const bigi_t *const A,
 bigi_t *const RES);
#line 381 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_sub(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 392 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_sub_one(const bigi_t *const A,
 bigi_t *const RES);
#line 403 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_red(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 418 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_red_barrett(const bigi_t *const X,
 const bigi_t *const MOD,
 const bigi_t *const MU,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 433 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mult_word(const bigi_t *const A,
 const word_t B,
 bigi_t *const TMP,
 bigi_t *const RES);
#line 446 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mult_fit(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 461 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_mult(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const RES);
#line 475 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_exp(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const TMP,
 bigi_t *const RES);
#line 490 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_inv(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 505 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_div(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const QUO,
 bigi_t *const REM);
#line 520 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_gcd(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 538 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_get_mont_params(const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const R,
 word_t *const mu);
#line 553 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_to_mont_domain(const bigi_t *const A,
 const bigi_t *const MOD,
 const bigi_t *const R,
 bigi_t *const AR);
#line 567 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_from_mont_domain(const bigi_t *const AR,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const A);
#line 585 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_mult_mont(const bigi_t *const AR,
 const bigi_t *const BR,
 const bigi_t *const MOD,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 602 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_exp_mont(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_impl.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 28 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_impl.h"
byte_t nlz(word_t x);

static const word_t MultiplyDeBruijnBitPosition[32] =
{
 0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
 31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
};
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 22 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_to_hex(const bigi_t *const A,
 char *const str,
 const index_t strlen);
#line 34 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_from_hex(const char *const str,
 const index_t strlen,
 bigi_t *const A);
#line 44 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_print(const bigi_t *const A);
#line 53 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_from_serial(const bigi_t *const A);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/../../debug.h"




void dbg_putch (char c);

void dbg_puts (char * str);

void dbg_putch_labeled (char c);

void dbg_puts_labeled (char * str);
#line 73 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 void  bigi_set_zero(bigi_t *const A)
{
 index_t i;
#line 82 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < A->wordlen +  3 ; i++)
 A->ary[i] = 0;

 return  ;
}

 void  bigi_copy(bigi_t *const OUT,
 const bigi_t *const IN)
{
 index_t i;
#line 100 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < IN->wordlen +  3 ; i++)
 OUT->ary[i] = IN->ary[i];
 for (i = IN->wordlen +  3 ; i < OUT->wordlen +  3 ; i++)
 OUT->ary[i] = 0u;
 OUT->domain = IN->domain;

 return  ;
}

 void  bigi_to_bytes(byte_t *const out,
 const index_t outlen,
 const bigi_t *const IN)
{
 index_t i;
 word_t tmp;
#line 126 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < IN->wordlen; i++) {
#line 132 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 tmp = IN->ary[IN->wordlen - i - 1];
 out[i *  (( 32 )/( 8 ))  + 0] = (byte_t)((tmp >> 24) & 0xFF);
 out[i *  (( 32 )/( 8 ))  + 1] = (byte_t)((tmp >> 16) & 0xFF);
 out[i *  (( 32 )/( 8 ))  + 2] = (byte_t)((tmp >> 8) & 0xFF);
 out[i *  (( 32 )/( 8 ))  + 3] = (byte_t)((tmp >> 0) & 0xFF);
#line 140 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 }
 return  ;
}

 void  bigi_from_bytes(bigi_t *const OUT,
 const byte_t *const in,
 const index_t inlen) {
 index_t i;
#line 156 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < OUT->wordlen; i++) {


 OUT->ary[OUT->wordlen - i - 1] = (((word_t)(in[i *  (( 32 )/( 8 ))  ])) << 24) |
 (((word_t)(in[i *  (( 32 )/( 8 ))  + 1])) << 16) |
 (((word_t)(in[i *  (( 32 )/( 8 ))  + 2])) << 8) |
 (((word_t)(in[i *  (( 32 )/( 8 ))  + 3])) << 0) ;
 }
 for (i = OUT->wordlen; i < OUT->wordlen +  3 ; i++)
 OUT->ary[i] = 0u;
 OUT->domain = DOMAIN_NORMAL;

 return  ;
}

 void  bigi_get_bit(const bigi_t *const A,
 const index_t index,
 word_t *const res)
{
#line 182 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 *res = ((A->ary[index/ 32 ] >> (index %  32 ))) & 1;

 return  ;
}

 void  bigi_cmp(const bigi_t *const A,
 const bigi_t *const B,
 cmp_t *const res)
{
 int32_t i;
#line 206 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = A->wordlen +  3  - 1; i >= 0; i--)
 {
 if (A->ary[i] == B->ary[i])
 continue;
 else if (A->ary[i] > B->ary[i])
 {
 *res = CMP_GREATER;
 return  ;
 }
 else
 {
 *res = CMP_LOWER;
 return  ;
 }
 }
 *res = CMP_EQUAL;

 return  ;
}

 void  bigi_is_zero(const bigi_t *const A,
 cmp_t *const res)
{
 index_t i;
#line 236 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < A->wordlen; i++)
 {
 if (A->ary[i] != 0u)
 {
 *res = CMP_NEQ;
 return  ;
 }
 }
 if (A->domain != DOMAIN_NORMAL)
 {
 for (i = A->wordlen; i < A->wordlen +  3 ; i++)
 {
 if (A->ary[i] != 0u)
 {
 *res = CMP_NEQ;
 return  ;
 }
 }
 }
 *res = CMP_EQUAL;

 return  ;
}

 void  bigi_is_one(const bigi_t *const A,
 cmp_t *const res)
{
 index_t i;
#line 272 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 if (A->ary[0] != 1u)
 {
 *res = CMP_NEQ;
 return  ;
 }

 for (i = 1; i < A->wordlen; i++)
 {
 if (A->ary[i] != 0)
 {
 *res = CMP_NEQ;
 return  ;
 }
 }
 *res = CMP_EQUAL;

 return  ;
}

 void  bigi_tmp_safe_shorten(bigi_t *const TMP,
 const index_t new_wordlen)
{
 index_t i;
#line 304 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = new_wordlen; i < TMP->wordlen +  3 ; i++)
 {
 if (TMP->ary[i] != 0u)
 return  ;
 }

 TMP->wordlen = new_wordlen;

 return  ;
}
#line 320 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 void  bigi_shift_left(bigi_t *const A,
 const index_t count)
{
 index_t i, bigs, smalls;
#line 332 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 if (count == 0)
 return  ;


 if (count >= (A->wordlen +  3 ) *  32 )
 {
  {bigi_set_zero(A); return;} 
 }


 bigs = count /  32 ;
 if (bigs > 0)
 {

 for (i = A->wordlen +  3  - 1; i >= bigs; i--)
 {
 A->ary[i] = A->ary[i-bigs];
 }
 for (i = 0; i < bigs; i++)
 {
 A->ary[i] = 0u;
 }
 }


 smalls = count %  32 ;
 if (smalls == 0)
 return  ;

 for (i = A->wordlen +  3  - 1; i > bigs ; i--)
 {
 A->ary[i] <<= smalls;
 A->ary[i] |= (A->ary[i-1] >> ( 32  - smalls));
 }
 A->ary[bigs] <<= smalls;

 return  ;
}

 void  bigi_shift_right(bigi_t *const A,
 const index_t count)
{
 index_t i, bigs, smalls;
#line 382 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 if (count == 0)
 return  ;


 if (count >= (A->wordlen +  3 ) *  32 )
  {bigi_set_zero(A); return;} 


 bigs = count /  32 ;
 if (bigs > 0)
 {

 for (i = 0; i < A->wordlen +  3  - bigs; i++)
 {
 A->ary[i] = A->ary[i+bigs];
 }
 for (i = A->wordlen +  3  - bigs; i < A->wordlen +  3 ; i++)
 {
 A->ary[i] = 0u;
 }
 }


 smalls = count %  32 ;
 if (smalls == 0)
 return  ;

 for (i = 0; i < A->wordlen +  3  - bigs - 1; i++)
 {
 A->ary[i] >>= smalls;
 A->ary[i] |= (A->ary[i+1] << ( 32  - smalls));
 }
 A->ary[A->wordlen +  3  - bigs - 1] >>= smalls;

 return  ;
}

 void  bigi_and(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES)
{
 index_t i;
#line 432 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < A->wordlen +  3 ; i++)
 RES->ary[i] = A->ary[i] & B->ary[i];
 for (i = A->wordlen +  3 ; i < RES->wordlen +  3 ; i++)
 RES->ary[i] = 0u;

 return  ;
}

 void  bigi_xor(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES)
{
 index_t i;
#line 453 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < A->wordlen +  3 ; i++)
 RES->ary[i] = A->ary[i] ^ B->ary[i];
 for (i = A->wordlen +  3 ; i < RES->wordlen +  3 ; i++)
 RES->ary[i] = 0u;

 return  ;
}

 void  bigi_add(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES)
{
 dword_t tmp;
 byte_t carry = 0u;
 index_t i;
 const bigi_t * shorter, * longer;
#line 481 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 shorter = (A->wordlen < B->wordlen) ? A : B;
 longer = (shorter == A) ? B : A;

 for (i = 0; i < shorter->wordlen +  3 ; i++)
 {
 tmp = (dword_t)(longer->ary[i]) + shorter->ary[i] + carry;
 carry = (tmp >  ((dword_t)0xFFFFFFFF) );
#line 490 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 tmp = tmp &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i] = (word_t)(tmp);
 }
 for ( ; i < longer->wordlen +  3 ; i++)
 {
 tmp = (dword_t)(longer->ary[i]) + carry;
 carry = (tmp >  ((dword_t)0xFFFFFFFF) );
#line 499 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 tmp = tmp &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i] = (word_t)(tmp);
 }

 for ( ; i < RES->wordlen +  3 ; i++)
 RES->ary[i] = 0u;

 if (carry)
 {
 if (RES->wordlen == longer->wordlen)
 return  ;
 else
 RES->ary[longer->wordlen +  3 ] = carry;
 }

 return  ;
}

 void  bigi_add_one(const bigi_t *const A,
 bigi_t *const RES)
{
 dword_t tmp;
 byte_t carry = 1u;
 index_t i;
#line 532 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; (carry > 0) && (i < A->wordlen +  3 ); i++)
 {
 tmp = (dword_t)(A->ary[i]) + carry;
 carry = (tmp >  ((dword_t)0xFFFFFFFF) );
#line 538 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 tmp = tmp &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i] = (word_t)(tmp);
 }
 for (; i < A->wordlen +  3 ; i++)
 RES->ary[i] = A->ary[i];

 for ( ; i < RES->wordlen +  3 ; i++)
 RES->ary[i] = 0u;

 if (carry)
 {
 if (RES->wordlen == A->wordlen)
 return  ;
 else
 RES->ary[A->wordlen +  3 ] = carry;
 }

 return  ;
}

 void  bigi_sub(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES)
{
 dword_t r;
 word_t borrow = 0u;
 index_t i;
#line 573 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < A->wordlen +  3 ; i++)
 {
 r = (dword_t)(A->ary[i]) +  ((dword_t)0xFFFFFFFF)  + 1 - B->ary[i] - borrow;
 borrow = (r <=  ((dword_t)0xFFFFFFFF) );
#line 579 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 r = r &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i] = (word_t)(r);
 }

 for ( ; i < RES->wordlen +  3 ; i++)
 RES->ary[i] = 0u;

 if (borrow)
 return  ;

 return  ;
}

 void  bigi_sub_one(const bigi_t *const A,
 bigi_t *const RES)
{
 dword_t r;
 word_t borrow = 1u;
 index_t i;
#line 607 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; (borrow > 0) && (i < A->wordlen +  3 ); i++)
 {
 r = (dword_t)(A->ary[i]) +  ((dword_t)0xFFFFFFFF)  + 1 - borrow;
 borrow = (r <=  ((dword_t)0xFFFFFFFF) );
#line 613 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 r = r &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i] = (word_t)(r);
 }
 for (; i < A->wordlen +  3 ; i++)
 RES->ary[i] = A->ary[i];

 for ( ; i < RES->wordlen +  3 ; i++)
 RES->ary[i] = 0u;

 if (borrow)
 return  ;

 return  ;
}

 void  bigi_mod_red(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{
#line 644 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 TMPARY[0].domain = DOMAIN_NORMAL;
 TMPARY[0].wordlen = A->wordlen;

  {bigi_div(A, MOD, &TMPARY[1], tmparylen - 1, &TMPARY[0], RES); return;} 
}

 void  bigi_mod_red_barrett(const bigi_t *const A,
 const bigi_t *const MOD,
 const bigi_t *const MU,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{
 bigi_t * Q1 =  ((void *)0) ;
 bigi_t * Q2 =  ((void *)0) ;
 bigi_t * Q3 =  ((void *)0) ;
 bigi_t * Bk =  ((void *)0) ;
 bigi_t * R1 =  ((void *)0) ;
 bigi_t * R2;
 int32_t i;
 index_t k;
 cmp_t cmp;
  
#line 691 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  (6 + ( 2 )) ; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = A->wordlen;
 }
 RES->domain = DOMAIN_NORMAL;

 Q1 = &TMPARY[0];
 Q2 = &TMPARY[1];
 Q3 = &TMPARY[2];
 Bk = &TMPARY[3];
 R1 = &TMPARY[4];
 R2 = &TMPARY[5];

  {bigi_set_zero(Q1);} 
  {bigi_set_zero(Q2);} 
  {bigi_set_zero(Q3);} 
  {bigi_set_zero(Bk);} 


 k = 0;
 for (i = MOD->wordlen +  3  - 1; i >= 0; i--)
 {
 if (MOD->ary[i] != 0u)
 {
 k = i + 1;
 break;
 }
 }
 Bk->ary[MOD->wordlen +  3  - 1 - k] = 1u;

  {bigi_copy(Q1, A);} 
  {bigi_shift_left(Q1, 32  * (k-1));} 

  {bigi_mult_fit(Q1, MU, &TMPARY[6], tmparylen - 6, Q2);} 
  {bigi_copy(Q3, Q2);} 
  {bigi_shift_left(Q2, 32  * (k+1));} 


  {bigi_copy(R1, A);} 
 for (i = R1->wordlen +  3  - 1; i >= 0; i--)
 {

 if (i > k)
 R1->ary[i] = 0u;
 }

  {bigi_mult_fit(Q3, MOD, &TMPARY[6], tmparylen - 6, R2);} 
 for (i = R2->wordlen +  3  - 1; i >= 0; i--)
 {

 if (i > k)
 R2->ary[i] = 0u;
 }

  {bigi_cmp(R2, R1, &cmp);} 
 if (cmp == CMP_GREATER)

 R1->ary[R1->wordlen +  3  - 1 - (k+2)] = 1u;

  {bigi_sub(R1, R2, RES);} 

 return  ;
}

 void  bigi_mult_word(const bigi_t *const A,
 const word_t B,
 bigi_t *const TMP,
 bigi_t *const RES)
{
 dword_t k, t, tmp_loc;
 int32_t i, m = 0;
  
#line 772 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 TMP->domain = A->domain;
 RES->domain = A->domain;

 for (i = A->wordlen +  3  - 1; i >= 0; i--)
 {
 if (A->ary[i] != 0u)
 {
 m = i + 1;
 break;
 }
 }
#line 789 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
  {bigi_copy(TMP, A);} 
  {bigi_set_zero(RES);} 

 for (k = 0, i = 0; i < m; i++)
 {
 t = (dword_t)(TMP->ary[i]);
 t *= (dword_t)(B);
 t += (dword_t)(RES->ary[i]);
 t += k;
#line 800 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 tmp_loc = t &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i] = (word_t)(tmp_loc);
 k = t >>  32 ;
 }
 RES->ary[m] = (word_t)(k);

 return  ;
}

 void  bigi_mult_fit(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{
 dword_t k, t, tmp;
 int32_t i;
 index_t j, m = 0, n = 0;
 bigi_t * TMPA =  ((void *)0) ;
 bigi_t * TMPB =  ((void *)0) ;
  
#line 840 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  2 ; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = A->wordlen;
 }
 RES->domain = DOMAIN_NORMAL;

 TMPA = &TMPARY[0];
 TMPB = &TMPARY[1];



 for (i = A->wordlen +  3  - 1; i >= 0; i--)
 {
 if (A->ary[i] != 0u)
 {
 m = i + 1;
 break;
 }
 }

 for (i = B->wordlen +  3  - 1; i >= 0; i--)
 {
 if (B->ary[i] != 0u)
 {
 n = i + 1;
 break;
 }
 }
#line 875 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
  {bigi_copy(TMPA, A);} 
  {bigi_copy(TMPB, B);} 

  {bigi_set_zero(RES);} 

 for (j = 0; j < n; j++)
 {
 for (k = 0, i = 0; i < m; i++)
 {
 t = (dword_t)(TMPA->ary[i]);
 t *= (dword_t)(TMPB->ary[j]);
 t += (dword_t)(RES->ary[i + j]);
 t += k;


 tmp = t &  ((dword_t)0xFFFFFFFF) ;
 RES->ary[i + j] = (word_t)(tmp);
 k = t >>  32 ;
 }
 RES->ary[j + m] = (word_t)(k);
 }

 return  ;
}

 void  bigi_mod_mult(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const RES)
{
 int32_t i;
 cmp_t cmp = CMP_UNDEFINED;
 word_t readbit = 0u;
  
#line 926 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 RES->domain = DOMAIN_NORMAL;
  {bigi_set_zero(RES);} 

 for (i = B->wordlen *  32  - 1; i >= 0; i--)
 {

  {bigi_shift_left(RES, 1);} 


  {bigi_cmp(RES, MOD, &cmp);} 
 if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
 {
  {bigi_sub(RES, MOD, RES);} 
 }

  {bigi_cmp(RES, MOD, &cmp);} 
 if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
 {
  {bigi_sub(RES, MOD, RES);} 
 }


  {bigi_get_bit(B, i, &readbit);} 
 if (readbit)
 {
  {bigi_add(A, RES, RES);} 
 }


  {bigi_cmp(RES, MOD, &cmp);} 
 if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
 {
  {bigi_sub(RES, MOD, RES);} 
 }

  {bigi_cmp(RES, MOD, &cmp);} 
 if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
 {
  {bigi_sub(RES, MOD, RES);} 
 }
 }

 return  ;
}

 void  bigi_mod_exp(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const TMP,
 bigi_t *const RES)
{
 int32_t i;
 word_t readbit = 0u;
  
#line 998 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 TMP->domain = DOMAIN_NORMAL;
 RES->domain = DOMAIN_NORMAL;

  {bigi_set_zero(RES);} 
 RES->ary[0] = 1u;

 for (i = B->wordlen *  32  - 1; i >= 0; i--)
 {

  {bigi_mod_mult(RES, RES, MOD, TMP);} 


  {bigi_get_bit(B, i, &readbit);} 
 if (readbit)
 {
  {bigi_mod_mult(A, TMP, MOD, RES);} 
 }
 else
 {
  {bigi_copy(RES, TMP);} 
 }
 }
 return  ;
}

 void  bigi_mod_inv(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{
 int32_t i;
 cmp_t cmpx, cmpy;
 bigi_t * T =  ((void *)0) ;
 bigi_t * NT =  ((void *)0) ;
 bigi_t * R =  ((void *)0) ;
 bigi_t * NR =  ((void *)0) ;
 bigi_t * Q =  ((void *)0) ;
 bigi_t * TMP =  ((void *)0) ;
 bigi_t * TMP1 =  ((void *)0) ;
 bigi_t * TMP2 =  ((void *)0) ;
  
#line 1061 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  (8 + ( 4 )) ; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = A->wordlen;
 }
 RES->domain = DOMAIN_NORMAL;

 T = &TMPARY[0];
 NT = &TMPARY[1];
 R = &TMPARY[2];
 NR = &TMPARY[3];
 Q = &TMPARY[4];
 TMP = &TMPARY[5];
 TMP1 = &TMPARY[6];
 TMP2 = &TMPARY[7];

  {bigi_set_zero(T);} 
  {bigi_set_zero(NT);} 
  {bigi_set_zero(Q);} 
  {bigi_set_zero(TMP1);} 
  {bigi_copy(R, MOD);} 



  {bigi_copy(NR,A);} 

 NT->ary[0] = 1u;

  {bigi_is_zero(NR, &cmpx);} 
  {bigi_is_one(R, &cmpy);} 

 while ((cmpx == CMP_NEQ) && (cmpy != CMP_EQUAL))
 {
  {bigi_div(R, NR, &TMPARY[8], tmparylen - 8, Q, TMP1);} 
  {bigi_copy(TMP, NT);} 
  {bigi_mod_mult(Q, NT, MOD, TMP2);} 

  {bigi_cmp(T, TMP2, &cmpx);} 
 if (cmpx == CMP_GREATER)
 {
  {bigi_sub(T, TMP2, NT);} 
 }
 else
 {
  {bigi_sub(TMP2, T, NT);} 
  {bigi_sub(MOD, NT, NT);} 
 }

  {bigi_copy(T, TMP);} 
  {bigi_copy(TMP, NR);} 
  {bigi_mod_mult(Q, NR, MOD, TMP2);} 
  {bigi_copy(NR, TMP1);} 
  {bigi_copy(R, TMP);} 

  {bigi_is_zero(NR, &cmpx);} 
  {bigi_is_one(R, &cmpy);} 
 }

  {bigi_is_one(R, &cmpy);} 
 if (cmpy == CMP_NEQ) return  ;

  {bigi_copy(RES, T);} 

 return  ;
}

 void  bigi_div(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const QUO,
 bigi_t *const REM)
{
 const dword_t b1 = (dword_t)(1) <<  32 ;
 bigi_t * U =  ((void *)0) ;
 bigi_t * V =  ((void *)0) ;
 bigi_t * UN =  ((void *)0) ;
 bigi_t * VN =  ((void *)0) ;
 dword_t qhat;
 dword_t rhat, tmp1_big, tmp2_big;
 dword_t p,tmp;
 int64_t t, k;
 byte_t s;
 int32_t i, j;
 int32_t m = 0, n = 0;

 word_t tmp2;
  
#line 1172 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  4 ; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = A->wordlen;
 }
 if (REM !=  ((void *)0) )
 REM->domain = DOMAIN_NORMAL;
 QUO->domain = DOMAIN_NORMAL;

 U = &TMPARY[0];
 UN = &TMPARY[1];
 V = &TMPARY[2];
 VN = &TMPARY[3];
 V->wordlen = B->wordlen;
 VN->wordlen = B->wordlen;

  {bigi_copy(U, A);} 
  {bigi_copy(V, B);} 
#line 1193 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
  {bigi_set_zero(UN);} 
  {bigi_set_zero(VN);} 
  {bigi_set_zero(QUO);} 
 if (REM !=  ((void *)0) )
 {
  {bigi_set_zero(REM);} 
 }

 for (i = U->wordlen +  3  - 1; i >= 0; i--)
 {
 if (U->ary[i] != 0u)
 {
 m = i + 1;
 break;
 }
 }
 for (i = V->wordlen +  3  - 1; i >= 0; i--)
 {
 if (V->ary[i] != 0u)
 {
 n = i + 1;
 break;
 }
 }


 if (m < n)
 {
 if (REM !=  ((void *)0) )
 {
  {bigi_copy(REM, A);} 
 }
 return  ;
 }

 if (n == 0)
 return  ;


 if (n == 1)
 {
 for (k = 0, j = m - 1; j >= 0; j--)
 {
 tmp = ((k <<  32 ) + U->ary[j])/ V->ary[0];
 QUO->ary[j] = (word_t)(tmp);
 k = ((k <<  32 ) + U->ary[j])- QUO->ary[j] * V->ary[0];
 }

 if (REM !=  ((void *)0) )
 REM->ary[0] = (word_t)(k);

 return  ;
 }
#line 1250 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 s = nlz(V->ary[n-1]);

 for (i = n - 1; i > 0; i--)
 {

 VN->ary[i] = V->ary[i] << s;
 tmp2 = (dword_t)(V->ary[i-1]) >> ( 32  - s);
 VN->ary[i] |= tmp2;
 }
 VN->ary[0] = V->ary[0] << s;

 UN->ary[m] = (dword_t)(U->ary[m-1]) >> ( 32  - s);
 for (i = m - 1; i > 0; i--)
 {

 UN->ary[i] = U->ary[i] << s;
 tmp = (dword_t)(U->ary[i-1]) >> ( 32  - s);
 UN->ary[i] |= (word_t)(tmp);
 }
 UN->ary[0] = U->ary[0] << s;

 for (j = m - n; j >= 0; j--)
 {

 qhat = (dword_t)(UN->ary[j+n]);
 qhat <<=  32 ;
 qhat += (dword_t)(UN->ary[j+n-1]);
 qhat /= VN->ary[n-1];


 rhat = (dword_t)(UN->ary[j+n]);
 rhat <<=  32 ;
 rhat += (dword_t)(UN->ary[j+n-1]);
 rhat -= qhat * (dword_t)(VN->ary[n-1]);


 again:
 tmp1_big = qhat;
 tmp1_big *= (dword_t)(VN->ary[n-2]);
 tmp2_big = (rhat <<  32 );
 tmp2_big += UN->ary[j+n-2];
#line 1293 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 if ((qhat >= b1) || (tmp1_big > tmp2_big))
 {
 qhat--;
 rhat += (dword_t)(VN->ary[n-1]);
 if (rhat < b1)
 goto again;
 }

 for (k = 0, i = 0; i < n; i++)
 {
 p = qhat * VN->ary[i];
 t = (dword_t)(UN->ary[i+j]) - k - (p &  ((dword_t)0xFFFFFFFF) );
 UN->ary[i+j] = (word_t)(t);
 k = (p >>  32 ) - (t >>  32 );
 }
 t = (dword_t)(UN->ary[j+n]) - k;
 UN->ary[j+n] = (word_t)(t);
 QUO->ary[j] = (word_t)(qhat);

 if (t < 0)
 {
 QUO->ary[j]--;
 for (k = 0, i = 0; i < n; i++)
 {
 t = (dword_t)(UN->ary[i+j]);
 t += (dword_t)(VN->ary[i]);
 t += k;
 UN->ary[i+j] = t;
 k = t >>  32 ;
 }
 UN->ary[j+n] = UN->ary[j+n] + k;
 }
 }

 if (REM !=  ((void *)0) )
 {
 for (i = 0; i < n-1; i++)
 {
 tmp = UN->ary[i] >> s;
 REM->ary[i] = (word_t)(tmp);
 tmp = (dword_t)(UN->ary[i+1]) << ( 32  - s);
 REM->ary[i] |= (word_t)(tmp);
 }
 REM->ary[n-1] = UN->ary[n-1] >> s;
 }

 return  ;
}

 void  bigi_gcd(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{
 bigi_t * A_big =  ((void *)0) ;
 bigi_t * B_big =  ((void *)0) ;
 index_t shift;
 word_t a1, b1, i, index_a, index_b;
 cmp_t cmp = CMP_UNDEFINED;
  
#line 1373 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i < tmparylen; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = A->wordlen;
 }
 RES->domain = DOMAIN_NORMAL;

 A_big = &TMPARY[0];
 B_big = &TMPARY[1];


  {bigi_is_zero(A, &cmp);} 
 if (cmp == CMP_EQUAL)
 {
  {bigi_copy(RES, B);} 
 return  ;
 }
  {bigi_is_zero(B, &cmp);} 
 if (cmp == CMP_EQUAL)
 {
  {bigi_copy(RES, A);} 
 return  ;
 }

  {bigi_copy(A_big, A);} 
  {bigi_copy(B_big, B);} 


 for (shift = 0; shift < A_big->wordlen +  3 ; shift++)
 {
 if ((A_big->ary[shift] != 0u) || (B_big->ary[shift] != 0u))
 {

 index_a = 0;
 index_b = 0;
 i = 0;
 a1 = A_big->ary[shift];
 b1 = B_big->ary[shift];



 index_a = MultiplyDeBruijnBitPosition[((word_t)((a1 & -a1) * 0x077CB531U)) >> 27];
 index_b = MultiplyDeBruijnBitPosition[((word_t)((b1 & -b1) * 0x077CB531U)) >> 27];

 if (A_big->ary[shift] == 0)
 i = index_b;
 else if (B_big->ary[shift] == 0)
 i = index_a;
 else if (index_a < index_b)
 i = index_a;
 else
 i = index_b;

 shift = shift *  32  + i;
 break;
 }
 }

  {bigi_shift_right(A_big, shift);} 
  {bigi_shift_right(B_big, shift);} 

  {bigi_get_bit(A_big, 0, &i);} 

 while (i == 0)
 {
  {bigi_shift_right(A_big, 1);} 
  {bigi_get_bit(A_big, 0, &i);} 
 }

 do
 {
  {bigi_get_bit(B_big, 0, &i);} 

 while (i == 0)
 {
  {bigi_shift_right(B_big, 1);} 
  {bigi_get_bit(B_big, 0, &i);} 
 }

  {bigi_cmp(A_big, B_big, &cmp);} 

 if (cmp == CMP_GREATER)
 {

 A_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
 B_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
 A_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
 }
  {bigi_sub(B_big, A_big, B_big);} 

  {bigi_is_zero(B_big, &cmp);} 
 } while (cmp == CMP_NEQ);

  {bigi_shift_left(A_big, shift);} 
  {bigi_copy(RES, A_big);} 

 return  ;
}
#line 1478 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 void  bigi_get_mont_params(const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const R,
 word_t *const mu)
{



 int32_t i;
 bigi_t * POW2 =  ((void *)0) ;
 bigi_t * X =  ((void *)0) ;
 bigi_t * Y =  ((void *)0) ;
 bigi_t * Z =  ((void *)0) ;
  
#line 1509 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  (4 + ( (8 + ( 4 )) )) ; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = MOD->wordlen;
 }

 POW2 = &TMPARY[0];
 X = &TMPARY[1];
 Y = &TMPARY[2];
 Z = &TMPARY[3];


  {bigi_set_zero(POW2);} 
 i = MOD->wordlen - 1;
 while ((i >= 0) && (MOD->ary[i] == 0u))
 {
 i--;
 }
 if (i >= 0)
 {
 POW2->ary[i+1] = 1u;
 }
 else
 {
 return  ;
 }
  {bigi_mod_red(POW2, MOD, &TMPARY[4], tmparylen - 4, R);} 


  {bigi_set_zero(X);} 
 X->ary[1] = 1;
  {bigi_set_zero(Y);} 
 Y->ary[0] = MOD->ary[0];

  {bigi_mod_inv(Y, X, &TMPARY[4], tmparylen - 4, POW2);} 

  {bigi_set_zero(Y);} 
 Y->ary[0] = (word_t)( ((dword_t)0xFFFFFFFF) );

  {bigi_mod_mult(Y, POW2, X, Z);} 
 *mu = Z->ary[0];

 return  ;
}

 void  bigi_to_mont_domain(const bigi_t *const A,
 const bigi_t *const MOD,
 const bigi_t *const R,
 bigi_t *const AR)
{
  
#line 1575 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
  {bigi_mod_mult(A, R, MOD, AR);} 

 AR->domain = DOMAIN_MONTGOMERY;

 return  ;
}

 void  bigi_from_mont_domain(const bigi_t *const AR,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const A)
{



 index_t i;
 bigi_t * TMP =  ((void *)0) ;
  
#line 1615 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  (1 + ( 2 )) ; i++)
 {
 TMPARY[i].domain = DOMAIN_MONTGOMERY;
 TMPARY[i].wordlen = AR->wordlen;
 }
 TMP = &TMPARY[0];

  {bigi_set_zero(TMP);} 
 TMP->ary[0] = 1u;

  {bigi_mod_mult_mont(TMP, AR, MOD, mu, &TMPARY[1], tmparylen - 1, A);} 

 A->domain = DOMAIN_NORMAL;

 return  ;
}

 void  bigi_mod_mult_mont(const bigi_t *const AR,
 const bigi_t *const BR,
 const bigi_t *const MOD,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{



 dword_t q;
 index_t i;
 int32_t mlen;
 bigi_t * TMPM =  ((void *)0) ;
 bigi_t * TMPW =  ((void *)0) ;
 cmp_t cmp = CMP_UNDEFINED;
  
#line 1670 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  2 ; i++)
 {
 TMPARY[i].domain = DOMAIN_MONTGOMERY;
 TMPARY[i].wordlen = AR->wordlen;
 }

 TMPM = &TMPARY[0];
 TMPW = &TMPARY[1];

  {bigi_set_zero(RES);} 
#line 1685 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (mlen = MOD->wordlen - 1; mlen >= 0 ; mlen--)
 if (MOD->ary[mlen]) break;

 if ((mlen == 0) && (MOD->ary[0] == 0u))
 return  ;

 for (i = 0; i <= mlen; i++)
 {
  {bigi_mult_word(BR, AR->ary[i], TMPW, TMPM);} 
  {bigi_add(TMPM, RES, RES);} 

 q = (dword_t)(mu) * RES->ary[0];
 q &=  ((dword_t)0xFFFFFFFF) ;

  {bigi_mult_word(MOD, (word_t)(q), TMPW, TMPM);} 
  {bigi_add(TMPM, RES, RES);} 
  {bigi_shift_right(RES, 32 );} 
 }

  {bigi_cmp(RES, MOD, &cmp);} 

 if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
 {
  {bigi_sub(RES, MOD, RES);} 
 }

 RES->domain = DOMAIN_MONTGOMERY;

 return  ;
}

 void  bigi_mod_exp_mont(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES)
{
 index_t i;
 word_t readbit = 0u;
 bigi_t * A_R =  ((void *)0) ;
 bigi_t * B_R =  ((void *)0) ;
 bigi_t * TMP =  ((void *)0) ;
  
#line 1757 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 for (i = 0; i <  (3 + ( 2 )) ; i++)
 {
 TMPARY[i].domain = DOMAIN_NORMAL;
 TMPARY[i].wordlen = MOD->wordlen;
 }

 A_R = &TMPARY[0];
 B_R = &TMPARY[1];
 TMP = &TMPARY[2];

  {bigi_set_zero(RES);} 
  {bigi_set_zero(TMP);} 


  {bigi_to_mont_domain(A, MOD, R, A_R);} 
  {bigi_copy(B_R, R);} 

 A_R->domain = DOMAIN_MONTGOMERY;
 B_R->domain = DOMAIN_MONTGOMERY;
 RES->domain = DOMAIN_MONTGOMERY;
 TMP->domain = DOMAIN_MONTGOMERY;

 for (i = 0; i < B->wordlen *  32 ; i++)
 {
  {bigi_get_bit(B, i, &readbit);} 
 if (readbit)
 {
  {bigi_mod_mult_mont(A_R, B_R, MOD, mu, &TMPARY[3], tmparylen - 3, TMP);} 
  {bigi_copy(B_R, TMP);} 
 }

  {bigi_mod_mult_mont(A_R, A_R, MOD, mu, &TMPARY[3], tmparylen - 3, TMP);} 
  {bigi_copy(A_R, TMP);} 
 }

  {bigi_set_zero(A_R);} 
 A_R->ary[0] = 1u;

  {bigi_mod_mult_mont(A_R, B_R, MOD, mu, &TMPARY[3], tmparylen - 3, RES);} 

 RES->domain = DOMAIN_NORMAL;

 return  ;
}
#line 1808 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
byte_t nlz(word_t x)
{
 byte_t n;

 if (x == 0) return( 32 );

 n = 0;










 if (x <= 0x0000FFFF) {n = n +16; x = x <<16;}
 if (x <= 0x00FFFFFF) {n = n + 8; x = x << 8;}
 if (x <= 0x0FFFFFFF) {n = n + 4; x = x << 4;}
 if (x <= 0x3FFFFFFF) {n = n + 2; x = x << 2;}
 if (x <= 0x7FFFFFFF) {n = n + 1;}
#line 1834 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi.c"
 return n;
}
