#ifndef BIGI_IO_IMPL_H
#define BIGI_IO_IMPL_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "bigi.h"

/* function for magic table */
dsgn_t hexdec(const char * hex);

#ifdef __cplusplus
}
#endif

#endif