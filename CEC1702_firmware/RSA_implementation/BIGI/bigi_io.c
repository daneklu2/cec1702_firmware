#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include "bigi.h"
#include "bigi_io.h"
#include "bigi_impl.h"
#include "bigi_io_impl.h"

BIGI_STATUS bigi_to_hex(const bigi_t *const A,
                        char *const str,
                        const index_t strlen)
{
    index_t i, mult_buffer_in_use;

    if ((A == NULL) || (str == NULL))
        return ERR_NULLPTR;
    if (A->domain != DOMAIN_NORMAL) /* wish: add support for import in Mont domain */
        return BIGI_WRONG_DOMAIN;
    if (strlen < (A->wordlen * HEXCHARS_PER_MW + 1))
        return ERR_BUFFER_OVERFLOW;

    /* mult_buffer_in_use = (A->domain == DOMAIN_NORMAL) ? 0 : MULT_BUFFER_WORDLEN; */
    mult_buffer_in_use = MULT_BUFFER_WORDLEN;
    /* TODO: if there is something in mult buffer, print it! */

    for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
    {
        sprintf(str + i * HEXCHARS_PER_MW, "%08x", A->ary[A->wordlen + mult_buffer_in_use - i - 1]);
    }

    return OK;
}

BIGI_STATUS bigi_from_hex(const char *const str,
                          const index_t strlen,
                          bigi_t *const A)
{
    index_t i,j;
    dsgn_t ret;

    if ((A == NULL) || (str == NULL))
        return ERR_NULLPTR;
    if (A->domain != DOMAIN_NORMAL)
        return BIGI_WRONG_DOMAIN;
    if ((A->wordlen * HEXCHARS_PER_MW) != strlen)
        return ERR_BUFFER_OVERFLOW;

    for (i = 0, j = A->wordlen - 1;
         i < strlen;
         i += HEXCHARS_PER_MW, j--)
    {
        ret = hexdec(&str[i]);
        if (ret < 0)
            return ERR_INVALID_PARAMS;
        A->ary[j] = (word_t)ret;
    }
    for (j = A->wordlen; j < A->wordlen + MULT_BUFFER_WORDLEN; j++)
        A->ary[j] = 0u;

    return OK;
}

BIGI_STATUS bigi_print(const bigi_t *const A)
{
    index_t i, mult_buffer_in_use;
#ifdef __MIKROC_PRO_FOR_ARM__
    char buff[10]; /* TODO: remove literal constants; 1 mw = 32 bits = 4 bytes = 8 hex chars */
#endif

    if (A == NULL)
        return ERR_NULLPTR;

    /* mult_buffer_in_use = (A->domain == DOMAIN_NORMAL) ? 0 : MULT_BUFFER_WORDLEN; */
    mult_buffer_in_use = MULT_BUFFER_WORDLEN;
    /* TODO: if there is something in mult buffer, print it! */

#ifdef __MIKROC_PRO_FOR_ARM__
    for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
    {
        sprintf(buff, "%04x", (A->ary[A->wordlen + mult_buffer_in_use - i - 1] >> 16) & 0x0000ffff);
        UART0_Write_Text(buff);
        sprintf(buff, "%04x", (A->ary[A->wordlen + mult_buffer_in_use - i - 1] >>  0) & 0x0000ffff);
        UART0_Write_Text(buff);
    }
#else
    for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
        printf("%08x", A->ary[A->wordlen + mult_buffer_in_use - i - 1]);
#endif

    return OK;
}

#ifdef __MIKROC_PRO_FOR_ARM__
BIGI_STATUS bigi_from_serial(const bigi_t *const A)
{
    volatile char buff[HEXCHARS_PER_MW];
    volatile index_t i, j;
    dsgn_t ret;

    if (A == NULL)
        return ERR_NULLPTR;
    if (A->domain != DOMAIN_NORMAL)
        return BIGI_WRONG_DOMAIN;

    for (i = 0;
         i < A->wordlen;
         i++)
    {
        for (j = 0; j < HEXCHARS_PER_MW; j++)
        {
            while (!UART0_Data_Ready());
            buff[j] = UART0_Read();
        }
        ret = hexdec(&buff[0]);
        if (ret < 0)
            return ERR_INVALID_PARAMS;
        A->ary[A->wordlen - 1 - i] = (word_t)ret;
    }
    for (i = A->wordlen; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        A->ary[i] = 0u;

    return OK;
}
#endif

/*
Magic table from
https://stackoverflow.com/questions/10324/convert-a-hexadecimal-string-to-an-integer-efficiently-in-c/11068850
*/
const dsgn_t hextable[] =
{
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1, 0,1,2,3,4,5,6,7,8,9,-1,-1,-1,-1,-1,-1,-1,10,11,12,13,14,15,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
    -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
};

/* TODO: add error codes for invalid characters */
dsgn_t hexdec(const char * hex)
{
    dsgn_t ret = 0;
    index_t i;
    
    for (i = 0; i < HEXCHARS_PER_MW; i++)
        ret = (ret << BITS_PER_HEXCHAR) | hextable[(byte_t)*(hex++)];
    
    return ret;
}