_bigi_to_hex:
;bigi_io.c,11 :: 		const index_t strlen)
; strlen start address is: 8 (R2)
; str start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #20
STR	LR, [SP, #0]
; strlen end address is: 8 (R2)
; str end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; str start address is: 4 (R1)
; strlen start address is: 8 (R2)
;bigi_io.c,15 :: 		if ((A == NULL) || (str == NULL))
CMP	R0, #0
IT	EQ
BEQ	L__bigi_to_hex43
CMP	R1, #0
IT	EQ
BEQ	L__bigi_to_hex42
IT	AL
BAL	L_bigi_to_hex2
; A end address is: 0 (R0)
; strlen end address is: 8 (R2)
; str end address is: 4 (R1)
L__bigi_to_hex43:
L__bigi_to_hex42:
;bigi_io.c,16 :: 		return ERR_NULLPTR;
IT	AL
BAL	L_end_bigi_to_hex
L_bigi_to_hex2:
;bigi_io.c,17 :: 		if (A->domain != DOMAIN_NORMAL) /* wish: add support for import in Mont domain */
; str start address is: 4 (R1)
; strlen start address is: 8 (R2)
; A start address is: 0 (R0)
ADDW	R3, R0, #12
LDRB	R3, [R3, #0]
CMP	R3, #0
IT	EQ
BEQ	L_bigi_to_hex3
; A end address is: 0 (R0)
; strlen end address is: 8 (R2)
; str end address is: 4 (R1)
;bigi_io.c,18 :: 		return BIGI_WRONG_DOMAIN;
IT	AL
BAL	L_end_bigi_to_hex
L_bigi_to_hex3:
;bigi_io.c,19 :: 		if (strlen < (A->wordlen * HEXCHARS_PER_MW + 1))
; str start address is: 4 (R1)
; strlen start address is: 8 (R2)
; A start address is: 0 (R0)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
LSLS	R3, R3, #3
ADDS	R3, R3, #1
CMP	R2, R3
IT	CS
BCS	L_bigi_to_hex4
; A end address is: 0 (R0)
; strlen end address is: 8 (R2)
; str end address is: 4 (R1)
;bigi_io.c,20 :: 		return ERR_BUFFER_OVERFLOW;
IT	AL
BAL	L_end_bigi_to_hex
L_bigi_to_hex4:
;bigi_io.c,23 :: 		mult_buffer_in_use = MULT_BUFFER_WORDLEN;
; str start address is: 4 (R1)
; mult_buffer_in_use start address is: 8 (R2)
; A start address is: 0 (R0)
MOVS	R2, #3
;bigi_io.c,26 :: 		for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
; i start address is: 24 (R6)
MOVS	R6, #0
; A end address is: 0 (R0)
; mult_buffer_in_use end address is: 8 (R2)
; i end address is: 24 (R6)
STR	R2, [SP, #4]
MOV	R2, R0
LDR	R0, [SP, #4]
L_bigi_to_hex5:
; i start address is: 24 (R6)
; mult_buffer_in_use start address is: 0 (R0)
; str start address is: 4 (R1)
; str end address is: 4 (R1)
; A start address is: 8 (R2)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, R0
CMP	R6, R3
IT	CS
BCS	L_bigi_to_hex6
; str end address is: 4 (R1)
;bigi_io.c,28 :: 		sprintf(str + i * HEXCHARS_PER_MW, "%08x", A->ary[A->wordlen + mult_buffer_in_use - i - 1]);
; str start address is: 4 (R1)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, R0
SUB	R3, R3, R6
SUBS	R3, R3, #1
LDR	R4, [R2, #0]
LSLS	R3, R3, #2
ADDS	R3, R4, R3
LDR	R3, [R3, #0]
MOV	R5, R3
MOVW	R4, #lo_addr(?lstr_1_bigi_io+0)
MOVT	R4, #hi_addr(?lstr_1_bigi_io+0)
LSLS	R3, R6, #3
ADDS	R3, R1, R3
STR	R2, [SP, #4]
STR	R1, [SP, #8]
STR	R0, [SP, #12]
STR	R6, [SP, #16]
PUSH	(R5)
PUSH	(R4)
PUSH	(R3)
BL	_sprintf+0
ADD	SP, SP, #12
LDR	R6, [SP, #16]
LDR	R0, [SP, #12]
LDR	R1, [SP, #8]
LDR	R2, [SP, #4]
;bigi_io.c,26 :: 		for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
ADDS	R6, R6, #1
;bigi_io.c,29 :: 		}
; mult_buffer_in_use end address is: 0 (R0)
; str end address is: 4 (R1)
; A end address is: 8 (R2)
; i end address is: 24 (R6)
IT	AL
BAL	L_bigi_to_hex5
L_bigi_to_hex6:
;bigi_io.c,31 :: 		return OK;
;bigi_io.c,32 :: 		}
L_end_bigi_to_hex:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _bigi_to_hex
_bigi_from_hex:
;bigi_io.c,36 :: 		bigi_t *const A)
; A start address is: 8 (R2)
; strlen start address is: 4 (R1)
; str start address is: 0 (R0)
SUB	SP, SP, #20
STR	LR, [SP, #0]
STR	R2, [SP, #4]
MOV	R2, R1
MOV	R1, R0
LDR	R0, [SP, #4]
; A end address is: 8 (R2)
; strlen end address is: 4 (R1)
; str end address is: 0 (R0)
; str start address is: 4 (R1)
; strlen start address is: 8 (R2)
; A start address is: 0 (R0)
;bigi_io.c,41 :: 		if ((A == NULL) || (str == NULL))
CMP	R0, #0
IT	EQ
BEQ	L__bigi_from_hex46
CMP	R1, #0
IT	EQ
BEQ	L__bigi_from_hex45
IT	AL
BAL	L_bigi_from_hex10
; str end address is: 4 (R1)
; strlen end address is: 8 (R2)
; A end address is: 0 (R0)
L__bigi_from_hex46:
L__bigi_from_hex45:
;bigi_io.c,42 :: 		return ERR_NULLPTR;
IT	AL
BAL	L_end_bigi_from_hex
L_bigi_from_hex10:
;bigi_io.c,43 :: 		if (A->domain != DOMAIN_NORMAL)
; A start address is: 0 (R0)
; strlen start address is: 8 (R2)
; str start address is: 4 (R1)
ADDW	R3, R0, #12
LDRB	R3, [R3, #0]
CMP	R3, #0
IT	EQ
BEQ	L_bigi_from_hex11
; str end address is: 4 (R1)
; strlen end address is: 8 (R2)
; A end address is: 0 (R0)
;bigi_io.c,44 :: 		return BIGI_WRONG_DOMAIN;
IT	AL
BAL	L_end_bigi_from_hex
L_bigi_from_hex11:
;bigi_io.c,45 :: 		if ((A->wordlen * HEXCHARS_PER_MW) != strlen)
; A start address is: 0 (R0)
; strlen start address is: 8 (R2)
; str start address is: 4 (R1)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
LSLS	R3, R3, #3
CMP	R3, R2
IT	EQ
BEQ	L_bigi_from_hex12
; str end address is: 4 (R1)
; strlen end address is: 8 (R2)
; A end address is: 0 (R0)
;bigi_io.c,46 :: 		return ERR_BUFFER_OVERFLOW;
IT	AL
BAL	L_end_bigi_from_hex
L_bigi_from_hex12:
;bigi_io.c,48 :: 		for (i = 0, j = A->wordlen - 1;
; i start address is: 28 (R7)
; A start address is: 0 (R0)
; strlen start address is: 8 (R2)
; str start address is: 4 (R1)
MOVS	R7, #0
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
SUBS	R3, R3, #1
; j start address is: 16 (R4)
MOV	R4, R3
; str end address is: 4 (R1)
; strlen end address is: 8 (R2)
; A end address is: 0 (R0)
; j end address is: 16 (R4)
; i end address is: 28 (R7)
MOV	R6, R1
MOV	R5, R2
MOV	R2, R4
MOV	R8, R0
L_bigi_from_hex13:
;bigi_io.c,49 :: 		i < strlen;
; j start address is: 8 (R2)
; str start address is: 24 (R6)
; i start address is: 28 (R7)
; A start address is: 32 (R8)
; strlen start address is: 20 (R5)
; str start address is: 24 (R6)
; str end address is: 24 (R6)
CMP	R7, R5
IT	CS
BCS	L_bigi_from_hex14
; str end address is: 24 (R6)
;bigi_io.c,52 :: 		ret = hexdec(&str[i]);
; str start address is: 24 (R6)
ADDS	R3, R6, R7
STR	R6, [SP, #4]
STR	R5, [SP, #8]
STR	R7, [SP, #12]
STR	R2, [SP, #16]
MOV	R0, R3
BL	_hexdec+0
LDR	R2, [SP, #16]
LDR	R7, [SP, #12]
LDR	R5, [SP, #8]
LDR	R6, [SP, #4]
; ret start address is: 36 (R9)
MOV	R9, R0
MOV	R10, R1
;bigi_io.c,53 :: 		if (ret < 0)
SUBS	R3, R0, #0
SBCS	R3, R1, #0
IT	GE
BGE	L_bigi_from_hex16
; A end address is: 32 (R8)
; strlen end address is: 20 (R5)
; str end address is: 24 (R6)
; ret end address is: 36 (R9)
; i end address is: 28 (R7)
; j end address is: 8 (R2)
;bigi_io.c,54 :: 		return ERR_INVALID_PARAMS;
IT	AL
BAL	L_end_bigi_from_hex
L_bigi_from_hex16:
;bigi_io.c,55 :: 		A->ary[j] = (word_t)ret;
; j start address is: 8 (R2)
; i start address is: 28 (R7)
; ret start address is: 36 (R9)
; str start address is: 24 (R6)
; strlen start address is: 20 (R5)
; A start address is: 32 (R8)
LDR	R4, [R8, #0]
LSLS	R3, R2, #2
ADDS	R3, R4, R3
STR	R9, [R3, #0]
; ret end address is: 36 (R9)
;bigi_io.c,50 :: 		i += HEXCHARS_PER_MW, j--)
ADDS	R7, #8
SUBS	R2, R2, #1
;bigi_io.c,56 :: 		}
; strlen end address is: 20 (R5)
; str end address is: 24 (R6)
; i end address is: 28 (R7)
; j end address is: 8 (R2)
IT	AL
BAL	L_bigi_from_hex13
L_bigi_from_hex14:
;bigi_io.c,57 :: 		for (j = A->wordlen; j < A->wordlen + MULT_BUFFER_WORDLEN; j++)
ADD	R3, R8, #4
LDR	R0, [R3, #0]
; j start address is: 0 (R0)
; A end address is: 32 (R8)
; j end address is: 0 (R0)
MOV	R1, R8
L_bigi_from_hex17:
; j start address is: 0 (R0)
; A start address is: 4 (R1)
ADDS	R3, R1, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R0, R3
IT	CS
BCS	L_bigi_from_hex18
;bigi_io.c,58 :: 		A->ary[j] = 0u;
LDR	R4, [R1, #0]
LSLS	R3, R0, #2
ADDS	R4, R4, R3
MOVS	R3, #0
STR	R3, [R4, #0]
;bigi_io.c,57 :: 		for (j = A->wordlen; j < A->wordlen + MULT_BUFFER_WORDLEN; j++)
ADDS	R0, R0, #1
;bigi_io.c,58 :: 		A->ary[j] = 0u;
; A end address is: 4 (R1)
; j end address is: 0 (R0)
IT	AL
BAL	L_bigi_from_hex17
L_bigi_from_hex18:
;bigi_io.c,60 :: 		return OK;
;bigi_io.c,61 :: 		}
L_end_bigi_from_hex:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _bigi_from_hex
_bigi_print:
;bigi_io.c,63 :: 		BIGI_STATUS bigi_print(const bigi_t *const A)
; A start address is: 0 (R0)
SUB	SP, SP, #28
STR	LR, [SP, #0]
; A end address is: 0 (R0)
; A start address is: 0 (R0)
;bigi_io.c,70 :: 		if (A == NULL)
CMP	R0, #0
IT	NE
BNE	L_bigi_print20
; A end address is: 0 (R0)
;bigi_io.c,71 :: 		return ERR_NULLPTR;
IT	AL
BAL	L_end_bigi_print
L_bigi_print20:
;bigi_io.c,74 :: 		mult_buffer_in_use = MULT_BUFFER_WORDLEN;
; A start address is: 0 (R0)
MOVS	R1, #3
STR	R1, [SP, #12]
;bigi_io.c,78 :: 		for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
MOVS	R1, #0
STR	R1, [SP, #8]
; A end address is: 0 (R0)
L_bigi_print21:
; A start address is: 0 (R0)
ADDS	R1, R0, #4
LDR	R2, [R1, #0]
LDR	R1, [SP, #12]
ADDS	R2, R2, R1
LDR	R1, [SP, #8]
CMP	R1, R2
IT	CS
BCS	L_bigi_print22
;bigi_io.c,80 :: 		sprintf(buff, "%04x", (A->ary[A->wordlen + mult_buffer_in_use - i - 1] >> 16) & 0x0000ffff);
ADDS	R1, R0, #4
LDR	R2, [R1, #0]
LDR	R1, [SP, #12]
ADDS	R2, R2, R1
LDR	R1, [SP, #8]
SUB	R1, R2, R1
SUBS	R1, R1, #1
LDR	R2, [R0, #0]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R1, [R1, #0]
LSRS	R2, R1, #16
MOVW	R1, #65535
AND	R3, R2, R1, LSL #0
MOVW	R2, #lo_addr(?lstr_2_bigi_io+0)
MOVT	R2, #hi_addr(?lstr_2_bigi_io+0)
ADD	R1, SP, #16
STR	R0, [SP, #4]
PUSH	(R3)
PUSH	(R2)
PUSH	(R1)
BL	_sprintf+0
ADD	SP, SP, #12
;bigi_io.c,81 :: 		UART0_Write_Text(buff);
ADD	R1, SP, #16
MOV	R0, R1
BL	_UART0_Write_Text+0
LDR	R0, [SP, #4]
;bigi_io.c,82 :: 		sprintf(buff, "%04x", (A->ary[A->wordlen + mult_buffer_in_use - i - 1] >>  0) & 0x0000ffff);
ADDS	R1, R0, #4
LDR	R2, [R1, #0]
LDR	R1, [SP, #12]
ADDS	R2, R2, R1
LDR	R1, [SP, #8]
SUB	R1, R2, R1
SUBS	R1, R1, #1
LDR	R2, [R0, #0]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
LDR	R2, [R1, #0]
MOVW	R1, #65535
AND	R3, R2, R1, LSL #0
MOVW	R2, #lo_addr(?lstr_3_bigi_io+0)
MOVT	R2, #hi_addr(?lstr_3_bigi_io+0)
ADD	R1, SP, #16
STR	R0, [SP, #4]
PUSH	(R3)
PUSH	(R2)
PUSH	(R1)
BL	_sprintf+0
ADD	SP, SP, #12
;bigi_io.c,83 :: 		UART0_Write_Text(buff);
ADD	R1, SP, #16
MOV	R0, R1
BL	_UART0_Write_Text+0
LDR	R0, [SP, #4]
;bigi_io.c,78 :: 		for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
LDR	R1, [SP, #8]
ADDS	R1, R1, #1
STR	R1, [SP, #8]
;bigi_io.c,84 :: 		}
; A end address is: 0 (R0)
IT	AL
BAL	L_bigi_print21
L_bigi_print22:
;bigi_io.c,90 :: 		return OK;
;bigi_io.c,91 :: 		}
L_end_bigi_print:
LDR	LR, [SP, #0]
ADD	SP, SP, #28
BX	LR
; end of _bigi_print
_bigi_from_serial:
;bigi_io.c,94 :: 		BIGI_STATUS bigi_from_serial(const bigi_t *const A)
; A start address is: 0 (R0)
SUB	SP, SP, #20
STR	LR, [SP, #0]
; A end address is: 0 (R0)
; A start address is: 0 (R0)
;bigi_io.c,100 :: 		if (A == NULL)
CMP	R0, #0
IT	NE
BNE	L_bigi_from_serial24
; A end address is: 0 (R0)
;bigi_io.c,101 :: 		return ERR_NULLPTR;
IT	AL
BAL	L_end_bigi_from_serial
L_bigi_from_serial24:
;bigi_io.c,102 :: 		if (A->domain != DOMAIN_NORMAL)
; A start address is: 0 (R0)
ADDW	R1, R0, #12
LDRB	R1, [R1, #0]
CMP	R1, #0
IT	EQ
BEQ	L_bigi_from_serial25
; A end address is: 0 (R0)
;bigi_io.c,103 :: 		return BIGI_WRONG_DOMAIN;
IT	AL
BAL	L_end_bigi_from_serial
L_bigi_from_serial25:
;bigi_io.c,105 :: 		for (i = 0;
; i start address is: 12 (R3)
; A start address is: 0 (R0)
MOVS	R3, #0
; A end address is: 0 (R0)
; i end address is: 12 (R3)
L_bigi_from_serial26:
;bigi_io.c,106 :: 		i < A->wordlen;
; i start address is: 12 (R3)
; A start address is: 0 (R0)
ADDS	R1, R0, #4
LDR	R1, [R1, #0]
CMP	R3, R1
IT	CS
BCS	L_bigi_from_serial27
;bigi_io.c,109 :: 		for (j = 0; j < HEXCHARS_PER_MW; j++)
; j start address is: 8 (R2)
MOVS	R2, #0
; j end address is: 8 (R2)
; A end address is: 0 (R0)
; i end address is: 12 (R3)
MOV	R8, R0
L_bigi_from_serial29:
; j start address is: 8 (R2)
; A start address is: 32 (R8)
; i start address is: 12 (R3)
CMP	R2, #8
IT	CS
BCS	L_bigi_from_serial30
; A end address is: 32 (R8)
; j end address is: 8 (R2)
; i end address is: 12 (R3)
;bigi_io.c,111 :: 		while (!UART0_Data_Ready());
L_bigi_from_serial32:
; i start address is: 12 (R3)
; A start address is: 32 (R8)
; j start address is: 8 (R2)
BL	_UART0_Data_Ready+0
CMP	R0, #0
IT	NE
BNE	L_bigi_from_serial33
IT	AL
BAL	L_bigi_from_serial32
L_bigi_from_serial33:
;bigi_io.c,112 :: 		buff[j] = UART0_Read();
ADD	R1, SP, #8
ADDS	R1, R1, R2
STR	R1, [SP, #16]
BL	_UART0_Read+0
LDR	R1, [SP, #16]
STRB	R0, [R1, #0]
;bigi_io.c,109 :: 		for (j = 0; j < HEXCHARS_PER_MW; j++)
ADDS	R1, R2, #1
MOV	R2, R1
;bigi_io.c,113 :: 		}
; j end address is: 8 (R2)
IT	AL
BAL	L_bigi_from_serial29
L_bigi_from_serial30:
;bigi_io.c,114 :: 		ret = hexdec(&buff[0]);
ADD	R1, SP, #8
STR	R3, [SP, #4]
MOV	R0, R1
BL	_hexdec+0
LDR	R3, [SP, #4]
; ret start address is: 16 (R4)
MOV	R4, R0
MOV	R5, R1
;bigi_io.c,115 :: 		if (ret < 0)
SUBS	R1, R0, #0
SBCS	R1, R1, #0
IT	GE
BGE	L_bigi_from_serial34
; A end address is: 32 (R8)
; ret end address is: 16 (R4)
; i end address is: 12 (R3)
;bigi_io.c,116 :: 		return ERR_INVALID_PARAMS;
IT	AL
BAL	L_end_bigi_from_serial
L_bigi_from_serial34:
;bigi_io.c,117 :: 		A->ary[A->wordlen - 1 - i] = (word_t)ret;
; i start address is: 12 (R3)
; ret start address is: 16 (R4)
; A start address is: 32 (R8)
ADD	R1, R8, #4
LDR	R1, [R1, #0]
SUBS	R1, R1, #1
SUB	R1, R1, R3
LDR	R2, [R8, #0]
LSLS	R1, R1, #2
ADDS	R1, R2, R1
STR	R4, [R1, #0]
; ret end address is: 16 (R4)
;bigi_io.c,107 :: 		i++)
ADDS	R1, R3, #1
MOV	R3, R1
;bigi_io.c,118 :: 		}
MOV	R0, R8
; A end address is: 32 (R8)
; i end address is: 12 (R3)
IT	AL
BAL	L_bigi_from_serial26
L_bigi_from_serial27:
;bigi_io.c,119 :: 		for (i = A->wordlen; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
; A start address is: 0 (R0)
ADDS	R1, R0, #4
LDR	R3, [R1, #0]
; i start address is: 12 (R3)
; A end address is: 0 (R0)
; i end address is: 12 (R3)
L_bigi_from_serial35:
; i start address is: 12 (R3)
; A start address is: 0 (R0)
ADDS	R1, R0, #4
LDR	R1, [R1, #0]
ADDS	R1, R1, #3
CMP	R3, R1
IT	CS
BCS	L_bigi_from_serial36
;bigi_io.c,120 :: 		A->ary[i] = 0u;
LDR	R2, [R0, #0]
LSLS	R1, R3, #2
ADDS	R2, R2, R1
MOVS	R1, #0
STR	R1, [R2, #0]
;bigi_io.c,119 :: 		for (i = A->wordlen; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R1, R3, #1
MOV	R3, R1
;bigi_io.c,120 :: 		A->ary[i] = 0u;
; A end address is: 0 (R0)
; i end address is: 12 (R3)
IT	AL
BAL	L_bigi_from_serial35
L_bigi_from_serial36:
;bigi_io.c,122 :: 		return OK;
;bigi_io.c,123 :: 		}
L_end_bigi_from_serial:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _bigi_from_serial
_hexdec:
;bigi_io.c,146 :: 		dsgn_t hexdec(const char * hex)
; hex start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R1, R0
; hex end address is: 0 (R0)
; hex start address is: 4 (R1)
;bigi_io.c,148 :: 		dsgn_t ret = 0;
; ret start address is: 24 (R6)
MOV	R6, #0
MOVS	R7, #0
;bigi_io.c,151 :: 		for (i = 0; i < HEXCHARS_PER_MW; i++)
; i start address is: 0 (R0)
MOVS	R0, #0
; hex end address is: 4 (R1)
; ret end address is: 24 (R6)
; i end address is: 0 (R0)
MOV	R5, R1
L_hexdec38:
; i start address is: 0 (R0)
; ret start address is: 24 (R6)
; hex start address is: 20 (R5)
CMP	R0, #8
IT	CS
BCS	L_hexdec39
;bigi_io.c,152 :: 		ret = (ret << BITS_PER_HEXCHAR) | hextable[(byte_t)*(hex++)];
LSLS	R4, R7, #4
LSRS	R3, R6, #28
ORRS	R4, R3
LSLS	R3, R6, #4
; ret end address is: 24 (R6)
LDRB	R1, [R5, #0]
LSLS	R2, R1, #3
MOVW	R1, #lo_addr(_hextable+0)
MOVT	R1, #hi_addr(_hextable+0)
ADDS	R1, R1, R2
LDRD	R1, R2, [R1, #0]
ORR	R1, R3, R1, LSL #0
ORR	R2, R4, R2, LSL #0
; ret start address is: 24 (R6)
MOV	R6, R1
MOV	R7, R2
ADDS	R5, R5, #1
;bigi_io.c,151 :: 		for (i = 0; i < HEXCHARS_PER_MW; i++)
ADDS	R0, R0, #1
;bigi_io.c,152 :: 		ret = (ret << BITS_PER_HEXCHAR) | hextable[(byte_t)*(hex++)];
; hex end address is: 20 (R5)
; i end address is: 0 (R0)
IT	AL
BAL	L_hexdec38
L_hexdec39:
;bigi_io.c,154 :: 		return ret;
MOV	R0, R6
MOV	R1, R7
; ret end address is: 24 (R6)
;bigi_io.c,155 :: 		}
L_end_hexdec:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _hexdec
