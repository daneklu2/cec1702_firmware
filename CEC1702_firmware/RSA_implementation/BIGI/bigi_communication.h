#ifndef BIGI_COMMUNICATION
#define BIGI_COMMUNICATION

#include <stdint.h>

#define DEMO
#ifdef DEMO

uint8_t demo128_bigi_add (uint8_t* pt);

uint8_t demo128_bigi_mod_exp (uint8_t* pt);

uint8_t demo128_bigi_gcd (uint8_t* pt);

uint8_t demo128_bigi_mod_exp_mont (uint8_t* pt);

#endif


#endif //BIGI_COMMUNICATION