#ifndef BIGI_IO_H
#define BIGI_IO_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "bigi.h"

#define HEXCHARS_PER_MW ((MACHINE_WORD_BYTELEN) * 2)
#define BITS_PER_HEXCHAR 4

/**
 *  @brief        Convert bigi_t to a hex string
 *
 *  @param[in]    bigi_t A converted
 *  @param[out]   Pointer to an output string (char buffer)
 *  @param[out]   Length of the char buffer excl. '\0', i.e., the buffer must be at least one char longer
 *
 */
BIGI_STATUS bigi_to_hex(const bigi_t *const A,
                        char *const str,
                        const index_t strlen);

/**
 *  @brief        Load bigi_t from a hex string
 *
 *  @param[in]    Pointer to the input string (char buffer)
 *  @param[in]    Length of the char buffer excl. '\0', i.e., the buffer must be at least one char longer
 *  @param[out]   bigi_t A to be loaded
 *
 */
BIGI_STATUS bigi_from_hex(const char *const str,
                          const index_t strlen,
                          bigi_t *const A);

/**
 *  @brief        Print bigi_t in hex format
 *
 *  @param[in]    bigi_t A to be printed
 *
 */
BIGI_STATUS bigi_print(const bigi_t *const A);

#ifdef __MIKROC_PRO_FOR_ARM__
/**
 *  @brief        Read bigi_t from serial link (in hex format)
 *
 *  @param[out]   bigi_t A to be loaded
 *
 */
BIGI_STATUS bigi_from_serial(const bigi_t *const A);
#endif /* #ifdef __MIKROC_PRO_FOR_ARM__ */

#ifdef __cplusplus
}
#endif

#endif