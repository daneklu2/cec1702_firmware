#include <stdint.h>
#include <stddef.h>
#include "bigi.h"
#include "bigi_impl.h"
#include "bigi_io.h"

#include "../../debug.h"

/* TODO:
 *
 * replace unsigned integral types (int**_t), if possible
 * MISRA: e.g., replace literal constants
 * check that param_in_1 != param_in_2 (in particular for pointers)
 * consider machine endianity
 * proper support for different lengths
 * Barrett reduction
 * support for different mw bitlengths
 * check if any volatile variables are needed
 * DOCS !!
 *      IMPORTANT NOTE: all temp variables in TMPARY MUST have the same wordlen !!!
 * DBGT .. split to sth like SAFE_MODE and perform all the checks
 *
 * */

#ifdef DEBUG

const char * bigi_status_str(BigiStatus bigist)
{
    switch (bigist)
    {
        case OK:
            return STR_OK;

        case ERR_BUFFER_OVERFLOW:
            return STR_ERR_BUFFER_OVERFLOW;
        case ERR_NOT_ENOUHGH_MEMORY:
            return STR_ERR_NOT_ENOUHGH_MEMORY;
        case ERR_NULLPTR:
            return STR_ERR_NULLPTR;
        case ERR_OUT_OF_RANGE:
            return STR_ERR_OUT_OF_RANGE;
        case ERR_UNDEFINED:
            return STR_ERR_UNDEFINED;
        case ERR_INVALID_PARAMS:
            return STR_ERR_INVALID_PARAMS;
        case ERR_HW:
            return STR_ERR_HW;

        case BIGI_DIV_BY_ZERO:
            return STR_BIGI_DIV_BY_ZERO;
        case BIGI_NOT_COPRIME:
            return STR_BIGI_NOT_COPRIME;
        case BIGI_OVERFLOW:
            return STR_BIGI_OVERFLOW;
        case BIGI_WRONG_DOMAIN:
            return STR_BIGI_WRONG_DOMAIN;
        case BIGI_WRONG_SIZE:
            return STR_BIGI_WRONG_SIZE;

        default:
            return STR_UNKNOWN_CODE;
    }
}

#endif



/* -----------------------------------------------------------------------------
 *  Init, manipulate, compare, etc.
 *                                                                            */

BIGI_STATUS bigi_set_zero(bigi_t *const A)
{
    index_t i;

#ifdef DBGT
    if (A == NULL)
        return ERR_NULLPTR;
#endif

    for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        A->ary[i] = 0;

    return OK;
}

BIGI_STATUS bigi_copy(bigi_t *const OUT,
                      const bigi_t *const IN)
{
    index_t i;

#ifdef DBGT
    if ((IN == NULL) || (OUT == NULL))
        return ERR_NULLPTR;
    if (OUT->wordlen < IN->wordlen)
        return BIGI_OVERFLOW;
#endif

    for (i = 0; i < IN->wordlen + MULT_BUFFER_WORDLEN; i++)
        OUT->ary[i] = IN->ary[i];
    for (i = IN->wordlen + MULT_BUFFER_WORDLEN; i < OUT->wordlen + MULT_BUFFER_WORDLEN; i++)
        OUT->ary[i] = 0u;
    OUT->domain = IN->domain;

    return OK;
}

BIGI_STATUS bigi_to_bytes(byte_t *const out,
                          const index_t outlen,
                          const bigi_t *const IN)
{
    index_t i;
    word_t tmp;

#ifdef DBGT
    if ((IN == NULL) || (out == NULL))
        return ERR_NULLPTR;
    if (outlen != IN->wordlen * MACHINE_WORD_BYTELEN)
        return BIGI_WRONG_SIZE;
    if (IN->domain != DOMAIN_NORMAL)
        return BIGI_WRONG_DOMAIN;
    /* TODO: this only works with little endian machine architecture!! */
#endif

    for (i = 0; i < IN->wordlen; i++) {
        /*
        *(word_t*)&out[i * MACHINE_WORD_BYTELEN] = IN->ary[IN->wordlen - i - 1];
         */
        /*IN->ary[IN->wordlen - i - 1]; zachovat, v bigi jsou LittleEndian slova*/

        tmp = IN->ary[IN->wordlen - i - 1];
        out[i * MACHINE_WORD_BYTELEN + 0] = (byte_t)((tmp >> 24) & 0xFF);
        out[i * MACHINE_WORD_BYTELEN + 1] = (byte_t)((tmp >> 16) & 0xFF);
        out[i * MACHINE_WORD_BYTELEN + 2] = (byte_t)((tmp >> 8) & 0xFF);
        out[i * MACHINE_WORD_BYTELEN + 3] = (byte_t)((tmp >> 0) & 0xFF);
        /*
        *
        */
    }
    return OK;
}

BIGI_STATUS bigi_from_bytes(bigi_t *const OUT,
                            const byte_t *const in,
                            const index_t inlen) {
    index_t i;

#ifdef DBGT
    if ((in == NULL) || (OUT == NULL))
        return ERR_NULLPTR;
    if (OUT->wordlen * MACHINE_WORD_BYTELEN != inlen)
        return BIGI_WRONG_SIZE;
#endif

    for (i = 0; i < OUT->wordlen; i++) {
        /*OUT->ary[OUT->wordlen - i - 1] = *(word_t*)&in[i * MACHINE_WORD_BYTELEN];*/

        OUT->ary[OUT->wordlen - i - 1] =  (((word_t)(in[i * MACHINE_WORD_BYTELEN    ])) << 24) |
                                          (((word_t)(in[i * MACHINE_WORD_BYTELEN + 1])) << 16) |
                                          (((word_t)(in[i * MACHINE_WORD_BYTELEN + 2])) <<  8) |
                                          (((word_t)(in[i * MACHINE_WORD_BYTELEN + 3])) <<  0) ;
    }
    for (i = OUT->wordlen; i < OUT->wordlen + MULT_BUFFER_WORDLEN; i++)
        OUT->ary[i] = 0u;
    OUT->domain = DOMAIN_NORMAL;

    return OK;
}

BIGI_STATUS bigi_get_bit(const bigi_t *const A,
                         const index_t index,
                         word_t *const res)
{
#ifdef DBGT
    if ((A == NULL) || (res == NULL))
        return ERR_NULLPTR;
    if (index >= ((A->wordlen + MULT_BUFFER_WORDLEN) * MACHINE_WORD_BITLEN))
        return ERR_OUT_OF_RANGE;
#endif

    *res = ((A->ary[index/MACHINE_WORD_BITLEN] >> (index % MACHINE_WORD_BITLEN))) & 1;

    return OK;
}

BIGI_STATUS bigi_cmp(const bigi_t *const A,
                     const bigi_t *const B,
                     cmp_t *const res)
{
    int32_t i;

#ifdef DBGT
    if ((A == NULL) || (B == NULL) || (res == NULL))
        return ERR_NULLPTR;
    if (A->wordlen != B->wordlen) /* should work for different lengths */
        return BIGI_WRONG_SIZE;
    /* TODO: domain check needed? */
    /*
    if (A->domain != B->domain)
        return BIGI_WRONG_DOMAIN;
        * */
#endif

    /* TODO: don't check buffer in case of normal domain */
    for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (A->ary[i] == B->ary[i])
            continue;
        else if (A->ary[i] > B->ary[i])
        {
            *res = CMP_GREATER;
            return OK;
        }
        else
        {
            *res = CMP_LOWER;
            return OK;
        }
    }
    *res = CMP_EQUAL;

    return OK;
}

BIGI_STATUS bigi_is_zero(const bigi_t *const A,
                         cmp_t *const res)
{
    index_t i;

#ifdef DBGT
    if ((A == NULL) || (res == NULL))
        return ERR_NULLPTR;
#endif

    for (i = 0; i < A->wordlen; i++)
    {
        if (A->ary[i] != 0u)
        {
            *res = CMP_NEQ;
            return OK;
        }
    }
    if (A->domain != DOMAIN_NORMAL)
    {
        for (i = A->wordlen; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        {
            if (A->ary[i] != 0u)
            {
                *res = CMP_NEQ;
                return OK;
            }
        }
    }
    *res = CMP_EQUAL;

    return OK;
}

BIGI_STATUS bigi_is_one(const bigi_t *const A,
                        cmp_t *const res)
{
    index_t i;

#ifdef DBGT
    if ((A == NULL) || (res == NULL))
        return ERR_NULLPTR;
    if (A->domain != DOMAIN_NORMAL)
        return BIGI_WRONG_DOMAIN;
#endif

    if (A->ary[0] != 1u)
    {
        *res = CMP_NEQ;
        return OK;
    }

    for (i = 1; i < A->wordlen; i++)
    {
        if (A->ary[i] != 0)
        {
            *res = CMP_NEQ;
            return OK;
        }
    }
    *res = CMP_EQUAL;

    return OK;
}

BIGI_STATUS bigi_tmp_safe_shorten(bigi_t *const TMP,
                                  const index_t new_wordlen)
{
    index_t i;

#ifdef DBGT
    if (TMP == NULL)
        return ERR_NULLPTR;
    if ((new_wordlen == 0) ||
        (new_wordlen > TMP->wordlen))
        return ERR_INVALID_PARAMS;
#endif

    for (i = new_wordlen; i < TMP->wordlen + MULT_BUFFER_WORDLEN; i++)
    {
        if (TMP->ary[i] != 0u)
            return BIGI_OVERFLOW;
    }

    TMP->wordlen = new_wordlen;

    return OK;
}


/* -----------------------------------------------------------------------------
 *  Arithmetics
 *                                                                            */

BIGI_STATUS bigi_shift_left(bigi_t *const A,
                            const index_t count)
{
    index_t i, bigs, smalls;

#ifdef DBGT
    if (A == NULL)
        return ERR_NULLPTR;
    /* TODO: check overflow */
#endif

    /** no shift */
    if (count == 0)
        return OK;

    /** looong shift */
    if (count >= (A->wordlen + MULT_BUFFER_WORDLEN) * MACHINE_WORD_BITLEN)
    {
        BIGI_RETURN(bigi_set_zero(A))
    }

    /** copy large distance */
    bigs = count / MACHINE_WORD_BITLEN;
    if (bigs > 0)
    {
        /* i is dest index */
        for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= bigs; i--)
        {
            A->ary[i] = A->ary[i-bigs];
        }
        for (i = 0; i < bigs; i++)
        {
            A->ary[i] = 0u;
        }
    }

    /** shift local distance */
    smalls = count % MACHINE_WORD_BITLEN;
    if (smalls == 0)
        return OK;

    for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i > bigs ; i--)
    {                                                                           /* 1011        . 0100 << 3 */
        A->ary[i] <<= smalls;                                                   /* 1000        . 0100 */
        A->ary[i] |= (A->ary[i-1] >> (MACHINE_WORD_BITLEN - smalls));           /* 1000 | 0010 . 0100 */
    }                                                                           /* 1010        . 0100 */
    A->ary[bigs] <<= smalls;

    return OK;
}

BIGI_STATUS bigi_shift_right(bigi_t *const A,
                             const index_t count)
{
    index_t i, bigs, smalls;

#ifdef DBGT
    if (A == NULL)
        return ERR_NULLPTR;
#endif

    /** no shift */
    if (count == 0)
        return OK;

    /** looong shift */
    if (count >= (A->wordlen + MULT_BUFFER_WORDLEN) * MACHINE_WORD_BITLEN)
        BIGI_RETURN(bigi_set_zero(A))

    /** copy large distance */
    bigs = count / MACHINE_WORD_BITLEN;
    if (bigs > 0)
    {
        /* i is dest index */
        for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN - bigs; i++)
        {
            A->ary[i] = A->ary[i+bigs];
        }
        for (i = A->wordlen + MULT_BUFFER_WORDLEN - bigs; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        {
            A->ary[i] = 0u;
        }
    }

    /** shift local distance */
    smalls = count % MACHINE_WORD_BITLEN;
    if (smalls == 0)
        return OK;

    for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN - bigs - 1; i++)
    {
        A->ary[i] >>= smalls;
        A->ary[i] |= (A->ary[i+1] << (MACHINE_WORD_BITLEN - smalls));
    }
    A->ary[A->wordlen + MULT_BUFFER_WORDLEN - bigs - 1] >>= smalls;

    return OK;
}

BIGI_STATUS bigi_and(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES)
{
    index_t i;

#ifdef DBGT
    if ((A == NULL) || (B == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != B->wordlen) || (A->wordlen > RES->wordlen))
        return BIGI_WRONG_SIZE;
#endif

    for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = A->ary[i] & B->ary[i];
    for (i = A->wordlen + MULT_BUFFER_WORDLEN; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = 0u;

    return OK;
}

BIGI_STATUS bigi_xor(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES)
{
    index_t i;

#ifdef DBGT
    if ((A == NULL) || (B == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != B->wordlen) || (A->wordlen > RES->wordlen))
        return BIGI_WRONG_SIZE;
#endif

    for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = A->ary[i] ^ B->ary[i];
    for (i = A->wordlen + MULT_BUFFER_WORDLEN; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = 0u;

    return OK;
}

BIGI_STATUS bigi_add(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES)
{
    dword_t tmp;
    byte_t carry = 0u;
    index_t i;
    const bigi_t * shorter, * longer;

#ifdef DBGT
    /* it can be that A == B */
    if ((A == NULL) ||
        (B == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen > RES->wordlen) ||
        (B->wordlen > RES->wordlen))
        return BIGI_WRONG_SIZE;
#endif

    shorter = (A->wordlen < B->wordlen) ? A : B;
    longer = (shorter == A) ? B : A;

    for (i = 0; i < shorter->wordlen + MULT_BUFFER_WORDLEN; i++)
    {
        tmp = (dword_t)(longer->ary[i]) + shorter->ary[i] + carry;
        carry = (tmp > MAX_VAL); /* TODO: check if this is MISRA-OK, or use ternary: (...) ? 1u : 0u; */
        /* IMPORTANT! Never merge the two following lines into single one (i.e., RES->ary[i] = (word_t)(tmp & MAX_VAL);),
         * since MikroC compiler is a piece of shit! And compiles it incorrectly! */
        tmp = tmp & MAX_VAL;
        RES->ary[i] = (word_t)(tmp);
    }
    for (/* i = shorter->wordlen + MULT_BUFFER_WORDLEN */; i < longer->wordlen + MULT_BUFFER_WORDLEN; i++)
    {
        tmp = (dword_t)(longer->ary[i]) + carry;
        carry = (tmp > MAX_VAL); /* TODO: check if this is MISRA-OK, or use ternary: (...) ? 1u : 0u; */
        /* IMPORTANT! Never merge the two following lines into single one (i.e., RES->ary[i] = (word_t)(tmp & MAX_VAL);),
         * since MikroC compiler is a piece of shit! And compiles it incorrectly! */
        tmp = tmp & MAX_VAL;
        RES->ary[i] = (word_t)(tmp);
    }

    for (/* i = longer->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = 0u;

    if (carry)
    {
        if (RES->wordlen == longer->wordlen)
            return BIGI_OVERFLOW;
        else
            RES->ary[longer->wordlen + MULT_BUFFER_WORDLEN] = carry;
    }

    return OK;
}

BIGI_STATUS bigi_add_one(const bigi_t *const A,
                         bigi_t *const RES)
{
    dword_t tmp;
    byte_t carry = 1u;
    index_t i;

#ifdef DBGT
    /* it can be that A == RES */
    if ((A == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    if (A->wordlen > RES->wordlen)
        return BIGI_WRONG_SIZE;
#endif

    for (i = 0; (carry > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
    {
        tmp = (dword_t)(A->ary[i]) + carry;
        carry = (tmp > MAX_VAL);
        /* IMPORTANT! Never merge the two following lines into single one (i.e., RES->ary[i] = (word_t)(tmp & MAX_VAL);),
         * since MikroC compiler is a piece of shit! And compiles it incorrectly! */
        tmp = tmp & MAX_VAL;
        RES->ary[i] = (word_t)(tmp);
    }
    for (; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = A->ary[i];

    for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = 0u;

    if (carry)
    {
        if (RES->wordlen == A->wordlen)
            return BIGI_OVERFLOW;
        else
            RES->ary[A->wordlen + MULT_BUFFER_WORDLEN] = carry;
    }

    return OK;
}

BIGI_STATUS bigi_sub(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const RES)
{
    dword_t r;
    word_t borrow = 0u;
    index_t i;

#ifdef DBGT
    if ((A == NULL) || (B == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != B->wordlen) || (A->wordlen > RES->wordlen))
        return BIGI_WRONG_SIZE;
#endif

    for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
    {
        r = (dword_t)(A->ary[i]) + MAX_VAL + 1 - B->ary[i] - borrow;
        borrow = (r <= MAX_VAL);
        /* IMPORTANT! Never merge the two following lines into single one (i.e., RES->ary[i] = (word_t)(tmp & MAX_VAL);),
         * since MikroC compiler is a piece of shit! And compiles it incorrectly! */
        r = r & MAX_VAL;
        RES->ary[i] = (word_t)(r);
    }

    for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = 0u;

    if (borrow)
        return BIGI_OVERFLOW;

    return OK;
}

BIGI_STATUS bigi_sub_one(const bigi_t *const A,
                         bigi_t *const RES)
{
    dword_t r;
    word_t borrow = 1u;
    index_t i;

#ifdef DBGT
    /* seems to work for A == RES */
    if ((A == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    if (A->wordlen > RES->wordlen)
        return BIGI_WRONG_SIZE;
#endif

    for (i = 0; (borrow > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
    {
        r = (dword_t)(A->ary[i]) + MAX_VAL + 1 - borrow;
        borrow = (r <= MAX_VAL);
        /* IMPORTANT! Never merge the two following lines into single one (i.e., RES->ary[i] = (word_t)(tmp & MAX_VAL);),
         * since MikroC compiler is a piece of shit! And compiles it incorrectly! */
        r = r & MAX_VAL;
        RES->ary[i] = (word_t)(r);
    }
    for (; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = A->ary[i];

    for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
        RES->ary[i] = 0u;

    if (borrow)
        return BIGI_OVERFLOW;

    return OK;
}

BIGI_STATUS bigi_mod_red(const bigi_t *const A,
                         const bigi_t *const MOD,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const RES)
{
    /* TODO: do not use */
#ifdef DBGT
    if (tmparylen < DIV__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if (TMPARY == NULL)
        return ERR_NULLPTR;
    if (TMPARY[0].alloclen < A->wordlen)
        return BIGI_WRONG_SIZE;
#endif

    TMPARY[0].domain = DOMAIN_NORMAL;
    TMPARY[0].wordlen = A->wordlen;

    BIGI_RETURN(bigi_div(A, MOD, &TMPARY[1], tmparylen - 1, &TMPARY[0], RES))
}

BIGI_STATUS bigi_mod_red_barrett(const bigi_t *const A,
                                 const bigi_t *const MOD,
                                 const bigi_t *const MU,
                                 bigi_t *const TMPARY,
                                 const index_t tmparylen,
                                 bigi_t *const RES)
{
    bigi_t * Q1 = NULL;
    bigi_t * Q2 = NULL;
    bigi_t * Q3 = NULL;
    bigi_t * Bk = NULL;
    bigi_t * R1 = NULL;
    bigi_t * R2;
    int32_t i;
    index_t k;
    cmp_t cmp;
    DECL_BIGI_STATUS

/* look for minimum tmparylen at BARRETT__MIN_TMPARY_LEN */

#ifdef DBGT
    if (tmparylen < BARRETT__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((A == NULL) ||
        (MOD == NULL) ||
        (MU == NULL) ||
        (TMPARY == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != MOD->wordlen) || /* TODO: A->wordlen < MOD->wordlen */
        (MOD->wordlen != MU->wordlen) ||
        (MU->wordlen != RES->wordlen))
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < A->wordlen)
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (MOD->domain != DOMAIN_NORMAL) ||
        (MU->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    for (i = 0; i < BARRETT__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = A->wordlen;
    }
    RES->domain = DOMAIN_NORMAL;

    Q1 = &TMPARY[0];
    Q2 = &TMPARY[1];
    Q3 = &TMPARY[2];
    Bk = &TMPARY[3];
    R1 = &TMPARY[4];
    R2 = &TMPARY[5];

    BIGI_CALL(bigi_set_zero(Q1))
    BIGI_CALL(bigi_set_zero(Q2))
    BIGI_CALL(bigi_set_zero(Q3))
    BIGI_CALL(bigi_set_zero(Bk))

    /* determine k */
    k = 0;
    for (i = MOD->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (MOD->ary[i] != 0u)
        {
            k = i + 1;
            break;
        }
    }
    Bk->ary[MOD->wordlen + MULT_BUFFER_WORDLEN - 1 - k] = 1u;

    BIGI_CALL(bigi_copy(Q1, A))
    BIGI_CALL(bigi_shift_left(Q1, MACHINE_WORD_BITLEN * (k-1)))
    /* TODO: stretch something as much as possible */
    BIGI_CALL(bigi_mult_fit(Q1, MU, &TMPARY[6], tmparylen - 6, Q2))
    BIGI_CALL(bigi_copy(Q3, Q2))
    BIGI_CALL(bigi_shift_left(Q2, MACHINE_WORD_BITLEN * (k+1)))

    /* R1, R2 */
    BIGI_CALL(bigi_copy(R1, A))
    for (i = R1->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        /* TODO: check indexes, was: if (i < k + 1) R1[i] = 0u; */
        if (i > k)
            R1->ary[i] = 0u;
    }

    BIGI_CALL(bigi_mult_fit(Q3, MOD, &TMPARY[6], tmparylen - 6, R2))
    for (i = R2->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        /* TODO: check indexes, was: if (i < k + 1) R2[i] = 0u; */
        if (i > k)
            R2->ary[i] = 0u;
    }

    BIGI_CALL(bigi_cmp(R2, R1, &cmp))
    if (cmp == CMP_GREATER)
        /* TODO: check index, was: R1[k+2] = 1u; */
        R1->ary[R1->wordlen + MULT_BUFFER_WORDLEN - 1 - (k+2)] = 1u;

    BIGI_CALL(bigi_sub(R1, R2, RES))

    return OK;
}

BIGI_STATUS bigi_mult_word(const bigi_t *const A,
                           const word_t B,
                           bigi_t *const TMP,
                           bigi_t *const RES)
{
    dword_t k, t, tmp_loc;
    int32_t i, m = 0;
    DECL_BIGI_STATUS

#ifdef DBGT
    if ((A == NULL) || (TMP == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != TMP->wordlen) || (RES->wordlen < A->wordlen))
        return BIGI_WRONG_SIZE;
#endif

    TMP->domain = A->domain;
    RES->domain = A->domain;

    for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (A->ary[i] != 0u)
        {
            m = i + 1;
            break;
        }
    }
    /* TODO: find out a better way of checking overflow ... */
    /* this fails in some cases (in old bigi-machineword endianity):
    if (m + 1 > RES->wordlen + mult_buffer_in_use)
        return BIGI_OVERFLOW;
        * */

    BIGI_CALL(bigi_copy(TMP, A))
    BIGI_CALL(bigi_set_zero(RES))

    for (k = 0, i = 0; i < m; i++)
    {
        t  = (dword_t)(TMP->ary[i]);
        t *= (dword_t)(B);
        t += (dword_t)(RES->ary[i]);
        t += k;
        /* IMPORTANT! Never merge the two following lines into single one (i.e., RES->ary[i] = (word_t)(tmp_loc & MAX_VAL);),
         * since MikroC compiler is a piece of shit! And compiles it incorrectly! */
        tmp_loc = t & MAX_VAL;   /* (I.e., t & 0xFFFF). */
        RES->ary[i] = (word_t)(tmp_loc);
        k = t >> MACHINE_WORD_BITLEN;
    }
    RES->ary[m] = (word_t)(k);

    return OK;
}

BIGI_STATUS bigi_mult_fit(const bigi_t *const A, /* TODO: check lengths properly */
                          const bigi_t *const B,
                          bigi_t *const TMPARY,
                          const index_t tmparylen,
                          bigi_t *const RES)
{
    dword_t k, t, tmp;
    int32_t i;
    index_t j, m = 0, n = 0;
    bigi_t * TMPA = NULL;
    bigi_t * TMPB = NULL;
    DECL_BIGI_STATUS

/* look for minimum tmparylen at MULT_FIT__MIN_TMPARY_LEN */

#ifdef DBGT
    if (tmparylen < MULT_FIT__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((A == NULL) || (B == NULL) || (TMPARY == NULL) || (RES == NULL))
        return ERR_NULLPTR;
    /* adequate RES length will be resolved later in more detail; find (#) marker */
    if (A->wordlen != B->wordlen)
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < A->wordlen)
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) || (B->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    /* TODO FIXME it seems that if B = 0, then it segfaults (possibly for A = 0 as well) */

    for (i = 0; i < MULT_FIT__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = A->wordlen;
    }
    RES->domain = DOMAIN_NORMAL;

    TMPA = &TMPARY[0];
    TMPB = &TMPARY[1];

    /* Knuth's Algorithm M from [Knuth Vol. 2 Third edition (1998)] Hacker's Delight */
    /*Lukas komentar - hleda zacatek cisla (0000110 -> 110) neboli prvni nenulovy znak*/
    for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (A->ary[i] != 0u)
        {
            m = i + 1;
            break;
        }
    }

    for (i = B->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (B->ary[i] != 0u)
        {
            n = i + 1;
            break;
        }
    }
    /* (#) */
#ifdef DBGT
    if (m + n > RES->wordlen)
        return BIGI_OVERFLOW;
#endif

    BIGI_CALL(bigi_copy(TMPA, A))
    BIGI_CALL(bigi_copy(TMPB, B))

    BIGI_CALL(bigi_set_zero(RES))

    for (j = 0; j < n; j++)
    {
        for (k = 0, i = 0; i < m; i++)
        {
            t  = (dword_t)(TMPA->ary[i]);
            t *= (dword_t)(TMPB->ary[j]);
            t += (dword_t)(RES->ary[i + j]);
            t += k;


            tmp = t & MAX_VAL;   /* (I.e., t & 0xFFFF). */
            RES->ary[i + j] = (word_t)(tmp);
            k = t >> MACHINE_WORD_BITLEN;
        }
        RES->ary[j + m] = (word_t)(k);
    }

    return OK;
}

BIGI_STATUS bigi_mod_mult(const bigi_t *const A,
                          const bigi_t *const B,
                          const bigi_t *const MOD,
                          bigi_t *const RES)
{
    int32_t i;
    cmp_t cmp = CMP_UNDEFINED;
    word_t readbit = 0u;
    DECL_BIGI_STATUS

#ifdef DBGT
    if ((A == NULL) ||
        (B == NULL) ||
        (MOD == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen > MOD->wordlen) ||
        (B->wordlen > MOD->wordlen) ||
        (MOD->wordlen != RES->wordlen))
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (B->domain != DOMAIN_NORMAL) ||
        (MOD->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    RES->domain = DOMAIN_NORMAL;
    BIGI_CALL(bigi_set_zero(RES))

    for (i = B->wordlen * MACHINE_WORD_BITLEN - 1; i >= 0; i--)
    {
        /* double ... */
        BIGI_CALL(bigi_shift_left(RES, 1))

        /** 2x reduce */
        BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
        if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
        {
            BIGI_CALL(bigi_sub(RES, MOD, RES))
        }

        BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
        if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
        {
            BIGI_CALL(bigi_sub(RES, MOD, RES))
        }

        /* ... and add */
        BIGI_CALL(bigi_get_bit(B, i, &readbit))
        if (readbit)
        {
            BIGI_CALL(bigi_add(A, RES, RES))
        }

        /** 2x reduce */
        BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
        if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
        {
            BIGI_CALL(bigi_sub(RES, MOD, RES))
        }

        BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
        if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
        {
            BIGI_CALL(bigi_sub(RES, MOD, RES))
        }
    }

    return OK;
}

BIGI_STATUS bigi_mod_exp(const bigi_t *const A,
                         const bigi_t *const B,
                         const bigi_t *const MOD,
                         bigi_t *const TMP,
                         bigi_t *const RES)
{
    int32_t i;
    word_t readbit = 0u;
    DECL_BIGI_STATUS

#ifdef DBGT
    if ((A == NULL) ||
        (B == NULL) ||
        (MOD == NULL) ||
        (TMP == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen > MOD->wordlen) ||
        (MOD->wordlen != RES->wordlen) ||
        (RES->wordlen != TMP->wordlen))
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (B->domain != DOMAIN_NORMAL) ||
        (MOD->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    TMP->domain = DOMAIN_NORMAL;
    RES->domain = DOMAIN_NORMAL;

    BIGI_CALL(bigi_set_zero(RES))
    RES->ary[0] = 1u;

    for (i = B->wordlen * MACHINE_WORD_BITLEN - 1; i >= 0; i--)
    {
        /* square ... */
        BIGI_CALL(bigi_mod_mult(RES, RES, MOD, TMP))

        /* ... and multiply */
        BIGI_CALL(bigi_get_bit(B, i, &readbit))
        if (readbit)
        {
            BIGI_CALL(bigi_mod_mult(A, TMP, MOD, RES))
        }
        else
        {
            BIGI_CALL(bigi_copy(RES, TMP))
        }
    }
    return OK;
}

BIGI_STATUS bigi_mod_inv(const bigi_t *const A,
                         const bigi_t *const MOD,
                         bigi_t *const TMPARY,
                         const index_t tmparylen,
                         bigi_t *const RES)
{
    int32_t i;
    cmp_t cmpx, cmpy;
    bigi_t * T  = NULL;
    bigi_t * NT = NULL;
    bigi_t * R  = NULL;
    bigi_t * NR = NULL;
    bigi_t * Q  = NULL;
    bigi_t * TMP  = NULL;
    bigi_t * TMP1 = NULL;
    bigi_t * TMP2 = NULL;
    DECL_BIGI_STATUS

/* look for minimum tmparylen at MOD_INV__MIN_TMPARY_LEN */

#ifdef DBGT
    if (tmparylen < MOD_INV__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((A == NULL) ||
        (MOD == NULL) ||
        (TMPARY == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != MOD->wordlen) ||
        (MOD->wordlen != RES->wordlen))
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < A->wordlen)
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (MOD->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    for (i = 0; i < MOD_INV__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = A->wordlen;
    }
    RES->domain = DOMAIN_NORMAL;

    T =     &TMPARY[0];
    NT =    &TMPARY[1];
    R =     &TMPARY[2];
    NR =    &TMPARY[3];
    Q =     &TMPARY[4];
    TMP =   &TMPARY[5];
    TMP1 =  &TMPARY[6];
    TMP2 =  &TMPARY[7];

    BIGI_CALL(bigi_set_zero(T))
    BIGI_CALL(bigi_set_zero(NT))
    BIGI_CALL(bigi_set_zero(Q))
    BIGI_CALL(bigi_set_zero(TMP1))
    BIGI_CALL(bigi_copy(R, MOD))

    /* https://rosettacode.org/wiki/Modular_inverse#C */

    BIGI_CALL(bigi_copy(NR,A))

    NT->ary[0] = 1u;

    BIGI_CALL(bigi_is_zero(NR, &cmpx))
    BIGI_CALL(bigi_is_one(R, &cmpy))

    while ((cmpx == CMP_NEQ) && (cmpy != CMP_EQUAL))
    {
        BIGI_CALL(bigi_div(R, NR, &TMPARY[8], tmparylen - 8, Q, TMP1))
        BIGI_CALL(bigi_copy(TMP, NT))
        BIGI_CALL(bigi_mod_mult(Q, NT, MOD, TMP2))

        BIGI_CALL(bigi_cmp(T, TMP2, &cmpx))
        if (cmpx == CMP_GREATER)
        {
            BIGI_CALL(bigi_sub(T, TMP2, NT))
        }
        else
        {
            BIGI_CALL(bigi_sub(TMP2, T, NT))
            BIGI_CALL(bigi_sub(MOD, NT, NT))
        }

        BIGI_CALL(bigi_copy(T, TMP))
        BIGI_CALL(bigi_copy(TMP, NR))
        BIGI_CALL(bigi_mod_mult(Q, NR, MOD, TMP2))
        BIGI_CALL(bigi_copy(NR, TMP1))
        BIGI_CALL(bigi_copy(R, TMP))

        BIGI_CALL(bigi_is_zero(NR, &cmpx))
        BIGI_CALL(bigi_is_one(R, &cmpy))
    }

    BIGI_CALL(bigi_is_one(R, &cmpy))
    if (cmpy == CMP_NEQ) return BIGI_NOT_COPRIME;

    BIGI_CALL(bigi_copy(RES, T))

    return OK;
}

BIGI_STATUS bigi_div(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const TMPARY,
                     const index_t tmparylen,
                     bigi_t *const QUO,
                     bigi_t *const REM)
{
    const dword_t b1 = (dword_t)(1) << MACHINE_WORD_BITLEN; /* Number base (2**MACHINE_WORD_BITLEN). */
    bigi_t * U = NULL;
    bigi_t * V = NULL;
    bigi_t * UN = NULL;
    bigi_t * VN = NULL;
    dword_t qhat;                       /* Estimated quotient digit. */
    dword_t rhat, tmp1_big, tmp2_big;   /* A remainder. */
    dword_t p,tmp;                      /* Product of two digits. */
    int64_t t, k;
    byte_t s;
    int32_t i, j;
    int32_t m = 0, n = 0;
    /* word_t tmp1, tmp2; */
    word_t tmp2;
    DECL_BIGI_STATUS

/* look for minimum tmparylen at DIV__MIN_TMPARY_LEN */

#ifdef DBGT
    if (tmparylen < DIV__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((A == NULL) ||
        (B == NULL) ||
        (TMPARY == NULL) ||
        (QUO == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != QUO->wordlen) ||
        (A->wordlen < B->wordlen))
        return BIGI_WRONG_SIZE; /* TODO: variable lengths */
    if ((REM != NULL) && (REM->wordlen != B->wordlen))
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < A->wordlen)
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (B->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    for (i = 0; i < DIV__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = A->wordlen;
    }
    if (REM != NULL)
        REM->domain = DOMAIN_NORMAL;
    QUO->domain = DOMAIN_NORMAL;

    U   = &TMPARY[0];
    UN  = &TMPARY[1];
    V   = &TMPARY[2];
    VN  = &TMPARY[3];
    V->wordlen  = B->wordlen;
    VN->wordlen = B->wordlen;

    BIGI_CALL(bigi_copy(U, A))
    BIGI_CALL(bigi_copy(V, B))

    /* Algorithm 14.20 from Handbook of applied cryptography
     * Implementation of Knuth's Algorithm D from http://www.hackersdelight.org/hdcodetxt/divmnu.c.txt (edited) */
    BIGI_CALL(bigi_set_zero(UN))
    BIGI_CALL(bigi_set_zero(VN))
    BIGI_CALL(bigi_set_zero(QUO))
    if (REM != NULL)
    {
        BIGI_CALL(bigi_set_zero(REM))
    }

    for (i = U->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (U->ary[i] != 0u)
        {
            m = i + 1;
            break;
        }
    }
    for (i = V->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
    {
        if (V->ary[i] != 0u)
        {
            n = i + 1;
            break;
        }
    }

    /* obviously A < B */
    if (m < n)
    {
        if (REM != NULL)
        {
            BIGI_CALL(bigi_copy(REM, A))
        }
        return OK;
    }
    /* B == 0 */
    if (n == 0)
        return BIGI_DIV_BY_ZERO;

    /* single word division */
    if (n == 1)
    {
        for (k = 0, j = m - 1; j >= 0; j--)
        {
            tmp =  ((k << MACHINE_WORD_BITLEN) + U->ary[j])/ V->ary[0];
            QUO->ary[j] = (word_t)(tmp);
            k = ((k << MACHINE_WORD_BITLEN) + U->ary[j])- QUO->ary[j] * V->ary[0];
        }

        if (REM != NULL)
            REM->ary[0] = (word_t)(k);

        return OK;
    }

    /* Normalize by shifting V left just enough so that its high-order
    bit is on, and shift U left the same amount. We may have to append a
    high-order digit on the dividend; we do that unconditionally. */
    s = nlz(V->ary[n-1]); /* 0 <= s <= 31. */

    for (i = n - 1; i > 0; i--)
    {
        /* VN[i] = (V[i] << s) | ((dword_t)(V[i-1]) >> (MACHINE_WORD_BITLEN-s)); */
        VN->ary[i] = V->ary[i] << s;
        tmp2 = (dword_t)(V->ary[i-1]) >> (MACHINE_WORD_BITLEN - s);
        VN->ary[i] |= tmp2;
    }
    VN->ary[0] = V->ary[0] << s;

    UN->ary[m] = (dword_t)(U->ary[m-1]) >> (MACHINE_WORD_BITLEN - s);
    for (i = m - 1; i > 0; i--)
    {
        /* UN[i] = (U[i] << s) | ((dword_t)(U[i-1]) >> (MACHINE_WORD_BITLEN-s)); */
        UN->ary[i] = U->ary[i] << s;
        tmp = (dword_t)(U->ary[i-1]) >> (MACHINE_WORD_BITLEN - s);
        UN->ary[i] |= (word_t)(tmp);
    }
    UN->ary[0] = U->ary[0] << s;

    for (j = m - n; j >= 0; j--)
    {
        /* qhat = (UN[j+n]*b1 + UN[j+n-1])/VN[n-1]; */
        qhat  = (dword_t)(UN->ary[j+n]);
        qhat <<= MACHINE_WORD_BITLEN;
        qhat += (dword_t)(UN->ary[j+n-1]);
        qhat /= VN->ary[n-1];

        /* rhat = (UN[j+n]*b1 + UN[j+n-1]) - qhat*VN[n-1]; */
        rhat  = (dword_t)(UN->ary[j+n]);
        rhat <<= MACHINE_WORD_BITLEN;
        rhat += (dword_t)(UN->ary[j+n-1]);
        rhat -= qhat * (dword_t)(VN->ary[n-1]);

        /* TODO: REMOVE GOTO */
        again:
        tmp1_big = qhat;
        tmp1_big *= (dword_t)(VN->ary[n-2]);
        tmp2_big = (rhat << MACHINE_WORD_BITLEN);
        tmp2_big += UN->ary[j+n-2];
        /* if (qhat >= b1 || qhat*VN[n-2] > b1*rhat + UN[j+n-2])
        { */
        if ((qhat >= b1) || (tmp1_big > tmp2_big))
        {
            qhat--;
            rhat += (dword_t)(VN->ary[n-1]);
            if (rhat < b1)
                goto again;
        }

        for (k = 0, i = 0; i < n; i++)
        {
            p = qhat * VN->ary[i];
            t = (dword_t)(UN->ary[i+j]) - k - (p & MAX_VAL);
            UN->ary[i+j] = (word_t)(t);
            k = (p >> MACHINE_WORD_BITLEN) - (t >> MACHINE_WORD_BITLEN);
        }
        t = (dword_t)(UN->ary[j+n]) - k;
        UN->ary[j+n] = (word_t)(t);
        QUO->ary[j] = (word_t)(qhat);

        if (t < 0)
        {
            QUO->ary[j]--;
            for (k = 0, i = 0; i < n; i++)
            {
                t = (dword_t)(UN->ary[i+j]);
                t += (dword_t)(VN->ary[i]);
                t += k;
                UN->ary[i+j] = t;
                k = t >> MACHINE_WORD_BITLEN;
            }
            UN->ary[j+n] = UN->ary[j+n] + k;
        }
    } /* End j. */

    if (REM != NULL)
    {
        for (i = 0; i < n-1; i++)
        {
            tmp = UN->ary[i] >> s;
            REM->ary[i] = (word_t)(tmp);
            tmp = (dword_t)(UN->ary[i+1]) << (MACHINE_WORD_BITLEN - s);
            REM->ary[i] |= (word_t)(tmp);
        }
        REM->ary[n-1] = UN->ary[n-1] >> s;
    }

    return OK;
}

BIGI_STATUS bigi_gcd(const bigi_t *const A,
                     const bigi_t *const B,
                     bigi_t *const TMPARY,
                     const index_t tmparylen,
                     bigi_t *const RES)
{
    bigi_t * A_big = NULL;
    bigi_t * B_big = NULL;
    index_t shift;
    word_t a1, b1, i, index_a, index_b;
    cmp_t cmp = CMP_UNDEFINED;
    DECL_BIGI_STATUS

#ifdef DBGT
    if ((A == NULL) ||
        (B == NULL) ||
        (TMPARY == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen != B->wordlen) ||
        (B->wordlen != RES->wordlen))
        return BIGI_WRONG_SIZE; /* TODO: variable lengths */
    if (TMPARY[0].alloclen < A->wordlen)
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (B->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
    if (tmparylen < GCD__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
#endif

    for (i = 0; i < tmparylen; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = A->wordlen;
    }
    RES->domain = DOMAIN_NORMAL;

    A_big = &TMPARY[0];
    B_big = &TMPARY[1];

    /* special case: A == 0 or B == 0 */
    BIGI_CALL(bigi_is_zero(A, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_copy(RES, B))
        return OK;
    }
    BIGI_CALL(bigi_is_zero(B, &cmp))
    if (cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_copy(RES, A))
        return OK;
    }

    BIGI_CALL(bigi_copy(A_big, A))
    BIGI_CALL(bigi_copy(B_big, B))

    /* Binary GCD (Stein's algorithm) from https://en.wikipedia.org/wiki/Binary_GCD_algorithm */
    for (shift = 0; shift < A_big->wordlen + MULT_BUFFER_WORDLEN; shift++)
    {
        if ((A_big->ary[shift] != 0u) || (B_big->ary[shift] != 0u))
        {
            /* find index in word */
            index_a = 0;
            index_b = 0;
            i = 0;
            a1 = A_big->ary[shift];
            b1 = B_big->ary[shift];
            /* little hack taken from http://graphics.stanford.edu/~seander/bithacks.html */
            /* TODO: check out how this works for different mw bit-lengths */
            /* TODO: check MikroC bug (word_t)(a1 & b1), applies? */
            index_a = MultiplyDeBruijnBitPosition[((word_t)((a1 & -a1) * 0x077CB531U)) >> 27];
            index_b = MultiplyDeBruijnBitPosition[((word_t)((b1 & -b1) * 0x077CB531U)) >> 27];

            if (A_big->ary[shift] == 0)
                i = index_b;
            else if (B_big->ary[shift] == 0)
                i = index_a;
            else if (index_a < index_b)
                i = index_a;
            else
                i = index_b;

            shift = shift * MACHINE_WORD_BITLEN + i;
            break;
        }
    }

    BIGI_CALL(bigi_shift_right(A_big, shift))
    BIGI_CALL(bigi_shift_right(B_big, shift))

    BIGI_CALL(bigi_get_bit(A_big, 0, &i))

    while (i == 0) /* TODO: this could be more effective for long sequences of zeroes */
    {
        BIGI_CALL(bigi_shift_right(A_big, 1))
        BIGI_CALL(bigi_get_bit(A_big, 0, &i))
    }

    do
    {
        BIGI_CALL(bigi_get_bit(B_big, 0, &i))

        while (i == 0) /* TODO: same applies here */
        {
            BIGI_CALL(bigi_shift_right(B_big, 1))
            BIGI_CALL(bigi_get_bit(B_big, 0, &i))
        }

        BIGI_CALL(bigi_cmp(A_big, B_big, &cmp))

        if (cmp == CMP_GREATER)
        {
            /* swap A_big <-> B_big */
            A_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
            B_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
            A_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
        }
        BIGI_CALL(bigi_sub(B_big, A_big, B_big)) /* TODO: check if this works properly */

        BIGI_CALL(bigi_is_zero(B_big, &cmp))
    } while (cmp == CMP_NEQ);

    BIGI_CALL(bigi_shift_left(A_big, shift))
    BIGI_CALL(bigi_copy(RES, A_big))

    return OK;
}



/* -----------------------------------------------------------------------------
 *  Montgomery domain
 *                                                                            */

BIGI_STATUS bigi_get_mont_params(const bigi_t *const MOD,
                                 bigi_t *const TMPARY,
                                 const index_t tmparylen,
                                 bigi_t *const R,
                                 word_t *const mu)
{

/* look for minimum tmparylen at MONT_PARAMS__MIN_TMPARY_LEN */

    int32_t i;
    bigi_t * POW2 = NULL;
    bigi_t * X = NULL;
    bigi_t * Y = NULL;
    bigi_t * Z = NULL;
    DECL_BIGI_STATUS

#ifdef DBGT
    if (tmparylen < MONT_PARAMS__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((MOD == NULL) ||
        (R == NULL) ||
        (mu == NULL))
        return ERR_NULLPTR;
    if (MOD->wordlen != R->wordlen)
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < MOD->wordlen)
        return BIGI_WRONG_SIZE;
    if ((MOD->domain != DOMAIN_NORMAL) || (R->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    for (i = 0; i < MONT_PARAMS__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = MOD->wordlen;
    }

    POW2    = &TMPARY[0];
    X       = &TMPARY[1];
    Y       = &TMPARY[2];
    Z       = &TMPARY[3];

    /* compute R */
    BIGI_CALL(bigi_set_zero(POW2))
    i = MOD->wordlen - 1;
    while ((i >= 0) && (MOD->ary[i] == 0u))
    {
        i--;
    }
    if (i >= 0)
    {
        POW2->ary[i+1] = 1u;
    }
    else
    {
        return ERR_INVALID_PARAMS; /* MOD == 0 */
    }
    BIGI_CALL(bigi_mod_red(POW2, MOD, &TMPARY[4], tmparylen - 4, R))

    /* compute mu */
    BIGI_CALL(bigi_set_zero(X))
    X->ary[1] = 1;
    BIGI_CALL(bigi_set_zero(Y))
    Y->ary[0] = MOD->ary[0];

    BIGI_CALL(bigi_mod_inv(Y, X, &TMPARY[4], tmparylen - 4, POW2))

    BIGI_CALL(bigi_set_zero(Y))
    Y->ary[0] = (word_t)(MAX_VAL);

    BIGI_CALL(bigi_mod_mult(Y, POW2, X, Z))
    *mu = Z->ary[0];

    return OK;
}

BIGI_STATUS bigi_to_mont_domain(const bigi_t *const A,
                                const bigi_t *const MOD,
                                const bigi_t *const R,
                                bigi_t *const AR)
{
    DECL_BIGI_STATUS

#ifdef DBGT
    if ((A == NULL) ||
        (R == NULL) ||
        (MOD == NULL) ||
        (AR == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen > MOD->wordlen) ||
        (MOD->wordlen != R->wordlen) ||
        (R->wordlen != AR->wordlen))
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) || (R->domain != DOMAIN_NORMAL) || (MOD->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    BIGI_CALL(bigi_mod_mult(A, R, MOD, AR))

    AR->domain = DOMAIN_MONTGOMERY;

    return OK;
}

BIGI_STATUS bigi_from_mont_domain(const bigi_t *const AR,
                                  const bigi_t *const MOD,
                                  const bigi_t *const R,
                                  const word_t mu,
                                  bigi_t *const TMPARY,
                                  const index_t tmparylen,
                                  bigi_t *const A)
{

/* look for minimum tmparylen at FROM_MONT__MIN_TMPARY_LEN */

    index_t i;
    bigi_t * TMP = NULL;
    DECL_BIGI_STATUS

#ifdef DBGT
    if (tmparylen < FROM_MONT__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((AR == NULL) ||
        (R == NULL) ||
        (MOD == NULL) ||
        (A == NULL))
        return ERR_NULLPTR;
    if ((AR->wordlen != R->wordlen) ||
        (R->wordlen != MOD->wordlen) ||
        (MOD->wordlen != A->wordlen))
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < AR->wordlen)
        return BIGI_WRONG_SIZE;
    if ((AR->domain != DOMAIN_MONTGOMERY) || (R->domain != DOMAIN_NORMAL) || (MOD->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    for (i = 0; i < FROM_MONT__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_MONTGOMERY;
        TMPARY[i].wordlen = AR->wordlen;
    }
    TMP = &TMPARY[0];

    BIGI_CALL(bigi_set_zero(TMP))
    TMP->ary[0] = 1u;

    BIGI_CALL(bigi_mod_mult_mont(TMP, AR, MOD, mu, &TMPARY[1], tmparylen - 1, A))

    A->domain = DOMAIN_NORMAL;

    return OK;
}

BIGI_STATUS bigi_mod_mult_mont(const bigi_t *const AR,
                               const bigi_t *const BR,
                               const bigi_t *const MOD,
                               const word_t mu,
                               bigi_t *const TMPARY,
                               const index_t tmparylen,
                               bigi_t *const RES)
{

/* look for minimum tmparylen at MOD_MULT_MONT__MIN_TMPARY_LEN */

    dword_t q;
    index_t i;
    int32_t mlen;
    bigi_t * TMPM = NULL;
    bigi_t * TMPW = NULL;
    cmp_t cmp = CMP_UNDEFINED;
    DECL_BIGI_STATUS

#ifdef DBGT
    if (tmparylen < MOD_MULT_MONT__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
    if ((AR == NULL) ||
        (BR == NULL) ||
        (MOD == NULL) ||
        (TMPARY == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((AR->wordlen != BR->wordlen) ||
        (BR->wordlen != MOD->wordlen) ||
        (MOD->wordlen != RES->wordlen))
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < AR->wordlen)
        return BIGI_WRONG_SIZE;
    if ((AR->domain != DOMAIN_MONTGOMERY) || (BR->domain != DOMAIN_MONTGOMERY) || (MOD->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
#endif

    for (i = 0; i < MOD_MULT_MONT__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_MONTGOMERY;
        TMPARY[i].wordlen = AR->wordlen;
    }

    TMPM = &TMPARY[0];
    TMPW = &TMPARY[1];

    BIGI_CALL(bigi_set_zero(RES))
    /* Was: for (i = 0; i < AR->wordlen; i++)
     * But, there shhould be the true wordlen of modulus, i.e., without leading zeroes.
     * TODO: write a method for true length
     * */

    for (mlen = MOD->wordlen - 1; mlen >= 0 ; mlen--)
        if (MOD->ary[mlen]) break;
    /* if MOD == 0 */
    if ((mlen == 0) && (MOD->ary[0] == 0u))
        return BIGI_DIV_BY_ZERO;

    for (i = 0; i <= mlen; i++)
    {
        BIGI_CALL(bigi_mult_word(BR, AR->ary[i], TMPW, TMPM))
        BIGI_CALL(bigi_add(TMPM, RES, RES))

        q  = (dword_t)(mu) * RES->ary[0];
        q &= MAX_VAL;

        BIGI_CALL(bigi_mult_word(MOD, (word_t)(q), TMPW, TMPM))
        BIGI_CALL(bigi_add(TMPM, RES, RES))
        BIGI_CALL(bigi_shift_right(RES, MACHINE_WORD_BITLEN))   /* TODO: use circular buffer instead & omit this operation */
    }

    BIGI_CALL(bigi_cmp(RES, MOD, &cmp))

    if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
    {
        BIGI_CALL(bigi_sub(RES, MOD, RES))
    }

    RES->domain = DOMAIN_MONTGOMERY;

    return OK;
}

BIGI_STATUS bigi_mod_exp_mont(const bigi_t *const A,
                              const bigi_t *const B,
                              const bigi_t *const MOD,
                              const bigi_t *const R,
                              const word_t mu,
                              bigi_t *const TMPARY,
                              const index_t tmparylen,
                              bigi_t *const RES)
{
    index_t i;
    word_t readbit = 0u;
    bigi_t * A_R = NULL;
    bigi_t * B_R = NULL;
    bigi_t * TMP = NULL;
    DECL_BIGI_STATUS

/* look for minimum tmparylen at MODEXP_MONT__MIN_TMPARY_LEN */

#ifdef DBGT
    if ((A == NULL) ||
        (B == NULL) ||
        (MOD == NULL) ||
        (R == NULL) ||
        (TMPARY == NULL) ||
        (RES == NULL))
        return ERR_NULLPTR;
    if ((A->wordlen > MOD->wordlen) ||   /* TODO: allow shorter inputs for A, do not check B at all */
        (MOD->wordlen != R->wordlen) ||
        (R->wordlen != RES->wordlen))
        return BIGI_WRONG_SIZE;
    if (TMPARY[0].alloclen < MOD->wordlen)
        return BIGI_WRONG_SIZE;
    if ((A->domain != DOMAIN_NORMAL) ||
        (B->domain != DOMAIN_NORMAL) ||
        (MOD->domain != DOMAIN_NORMAL) ||
        (R->domain != DOMAIN_NORMAL))
        return BIGI_WRONG_DOMAIN;
    if (tmparylen < MODEXP_MONT__MIN_TMPARY_LEN)
        return ERR_INVALID_PARAMS;
#endif

    for (i = 0; i < MODEXP_MONT__MIN_TMPARY_LEN; i++)
    {
        TMPARY[i].domain = DOMAIN_NORMAL;
        TMPARY[i].wordlen = MOD->wordlen;
    }

    A_R  = &TMPARY[0];
    B_R  = &TMPARY[1];
    TMP  = &TMPARY[2];

    BIGI_CALL(bigi_set_zero(RES))
    BIGI_CALL(bigi_set_zero(TMP))

    /** convert A to Mont domain **/
    BIGI_CALL(bigi_to_mont_domain(A, MOD, R, A_R))
    BIGI_CALL(bigi_copy(B_R, R))

    A_R->domain = DOMAIN_MONTGOMERY;
    B_R->domain = DOMAIN_MONTGOMERY;
    RES->domain = DOMAIN_MONTGOMERY;
    TMP->domain = DOMAIN_MONTGOMERY; /* TODO: needed? */

    for (i = 0; i < B->wordlen * MACHINE_WORD_BITLEN; i++)
    {
        BIGI_CALL(bigi_get_bit(B, i, &readbit))
        if (readbit)
        {
            BIGI_CALL(bigi_mod_mult_mont(A_R, B_R, MOD, mu, &TMPARY[3], tmparylen - 3, TMP))
            BIGI_CALL(bigi_copy(B_R, TMP))
        }

        BIGI_CALL(bigi_mod_mult_mont(A_R, A_R, MOD, mu, &TMPARY[3], tmparylen - 3, TMP))
        BIGI_CALL(bigi_copy(A_R, TMP))
    }

    BIGI_CALL(bigi_set_zero(A_R))
    A_R->ary[0] = 1u;

    BIGI_CALL(bigi_mod_mult_mont(A_R, B_R, MOD, mu, &TMPARY[3], tmparylen - 3, RES))

    RES->domain = DOMAIN_NORMAL;

    return OK;
}



/* -----------------------------------------------------------------------------
 *  Misc
 *                                                                            */

byte_t nlz(word_t x)
{
    byte_t n;

    if (x == 0) return(MACHINE_WORD_BITLEN);

    n = 0;
#if MACHINE_WORD_BITLEN == 8
    if (x <= 0x0F)    {n = n + 4; x = x << 4;}
    if (x <= 0x3F)    {n = n + 2; x = x << 2;}
    if (x <= 0x7F)    {n = n + 1;}
#elif MACHINE_WORD_BITLEN == 16
    if (x <= 0x00FF)    {n = n + 8; x = x << 8;}
    if (x <= 0x0FFF)    {n = n + 4; x = x << 4;}
    if (x <= 0x3FFF)    {n = n + 2; x = x << 2;}
    if (x <= 0x7FFF)    {n = n + 1;}
#elif MACHINE_WORD_BITLEN == 32
    if (x <= 0x0000FFFF)    {n = n +16; x = x <<16;}
    if (x <= 0x00FFFFFF)    {n = n + 8; x = x << 8;}
    if (x <= 0x0FFFFFFF)    {n = n + 4; x = x << 4;}
    if (x <= 0x3FFFFFFF)    {n = n + 2; x = x << 2;}
    if (x <= 0x7FFFFFFF)    {n = n + 1;}
#else
    #error MSG_WRONG_MW_BITLEN
#endif

    return n;
}