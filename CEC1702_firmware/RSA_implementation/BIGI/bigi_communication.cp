#line 1 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi_communication.c"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_communication.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"





typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int int32_t;
typedef signed long long int64_t;


typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long int uint32_t;
typedef unsigned long long uint64_t;


typedef signed char int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;
typedef signed long long int_least64_t;


typedef unsigned char uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;
typedef unsigned long long uint_least64_t;



typedef signed long int int_fast8_t;
typedef signed long int int_fast16_t;
typedef signed long int int_fast32_t;
typedef signed long long int_fast64_t;


typedef unsigned long int uint_fast8_t;
typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long long uint_fast64_t;


typedef signed long int intptr_t;
typedef unsigned long int uintptr_t;


typedef signed long long intmax_t;
typedef unsigned long long uintmax_t;
#line 9 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_communication.h"
uint8_t demo128_bigi_add (uint8_t* pt);

uint8_t demo128_bigi_mod_exp (uint8_t* pt);

uint8_t demo128_bigi_gcd (uint8_t* pt);

uint8_t demo128_bigi_mod_exp_mont (uint8_t* pt);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 30 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef uint8_t byte_t;
#line 60 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 typedef uint32_t word_t;
 typedef uint64_t dword_t;
 typedef int64_t dsgn_t;

 typedef word_t index_t;
#line 75 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef enum
{
 DOMAIN_NORMAL = 0,
 DOMAIN_MONTGOMERY = 1
} domain_t;

typedef struct bigi_t
{
 word_t * ary;
 index_t wordlen;
 index_t alloclen;


 domain_t domain;
} bigi_t;
#line 94 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef enum
{
 CMP_EQUAL = 0,
 CMP_NEQ = 1,
 CMP_LOWER = 2,
 CMP_GREATER = 3,
 CMP_UNDEFINED = 11
} cmp_t;
#line 215 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_set_zero(bigi_t *const A);
#line 224 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_copy(bigi_t *const OUT,
 const bigi_t *const IN);
#line 234 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_to_bytes(byte_t *const out,
 const index_t outlen,
 const bigi_t *const IN);
#line 245 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_from_bytes(bigi_t *const OUT,
 const byte_t *const in,
 const index_t inlen);
#line 256 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_get_bit(const bigi_t *const A,
 const index_t index,
 word_t *const res);
#line 268 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_cmp(const bigi_t *const A,
 const bigi_t *const B,
 cmp_t *const res);
#line 279 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_is_zero(const bigi_t *const A,
 cmp_t *const res);
#line 289 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_is_one(const bigi_t *const A,
 cmp_t *const res);
#line 300 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_tmp_safe_shorten(bigi_t *const TMP,
 const index_t new_wordlen);
#line 314 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_shift_left(bigi_t *const A,
 const index_t count);
#line 324 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_shift_right(bigi_t *const A,
 const index_t count);
#line 335 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_and(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 347 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_xor(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 359 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_add(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 370 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_add_one(const bigi_t *const A,
 bigi_t *const RES);
#line 381 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_sub(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 392 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_sub_one(const bigi_t *const A,
 bigi_t *const RES);
#line 403 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_red(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 418 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_red_barrett(const bigi_t *const X,
 const bigi_t *const MOD,
 const bigi_t *const MU,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 433 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mult_word(const bigi_t *const A,
 const word_t B,
 bigi_t *const TMP,
 bigi_t *const RES);
#line 446 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mult_fit(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 461 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_mult(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const RES);
#line 475 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_exp(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const TMP,
 bigi_t *const RES);
#line 490 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_inv(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 505 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_div(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const QUO,
 bigi_t *const REM);
#line 520 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_gcd(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 538 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_get_mont_params(const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const R,
 word_t *const mu);
#line 553 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_to_mont_domain(const bigi_t *const A,
 const bigi_t *const MOD,
 const bigi_t *const R,
 bigi_t *const AR);
#line 567 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_from_mont_domain(const bigi_t *const AR,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const A);
#line 585 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_mult_mont(const bigi_t *const AR,
 const bigi_t *const BR,
 const bigi_t *const MOD,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 602 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_exp_mont(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 22 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_to_hex(const bigi_t *const A,
 char *const str,
 const index_t strlen);
#line 34 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_from_hex(const char *const str,
 const index_t strlen,
 bigi_t *const A);
#line 44 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_print(const bigi_t *const A);
#line 53 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_from_serial(const bigi_t *const A);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/../../simpleserial/simpleserial.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 14 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/../../simpleserial/simpleserial.h"
typedef struct ss_cmd
{
 char c;
 unsigned int len;
 uint8_t (*fp)(uint8_t*);
} ss_cmd;



void simpleserial_init(void);
#line 39 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/../../simpleserial/simpleserial.h"
int simpleserial_addcmd(char c, unsigned int len, uint8_t (*fp)(uint8_t*));







void simpleserial_get(void);




void simpleserial_put(char c, int size, uint8_t* output);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/../../debug.h"




void dbg_putch (char c);

void dbg_puts (char * str);

void dbg_putch_labeled (char c);

void dbg_puts_labeled (char * str);
#line 12 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi_communication.c"
word_t demo_tmp_arys[ 20  * ( 128  /  32  +  3 )];
bigi_t tmpary[ 20 ];
word_t a_ary[ 128  /  32  +  3 ];
word_t b_ary[ 128  /  32  +  3 ];
word_t p_ary[ 128  /  32  +  3 ];
word_t q_ary[ 128  /  32  +  3 ];
word_t n_ary[ 128  /  32  +  3 ];
word_t x_ary[ 128  /  32  +  3 ];
word_t r_ary[ 128  /  32  +  3 ];
word_t ar_ary[ 128  /  32  +  3 ];
word_t br_ary[ 128  /  32  +  3 ];
word_t mu_ary[ 128  /  32  +  3 ];
word_t y_ary[ 128  /  32  +  3 ];
word_t z_ary[ 128  /  32  +  3 ];
char * demo128_sp = "0000000000000000C6288119B3E3C199";
char * demo128_sq = "0000000000000000DDAC808AAA1DB0FF";
char * demo128_sa = "4e8839e2876bcb217511f129c68265a6";
char * demo128_sb = "a892170d2e9809870923f82e150ac166";
char * demo128_sn = "ab967e2983a0ce46edd7c01d4d4c0767";
char buff[ (( 128  / 32  + 3  + 3) * (( (( 32 )/( 8 )) ) * 2) ) ];
char output[32];
word_t demo_mu_mult;
index_t i;
bigi_t a, b, r, ar, br, mu, p, q, n, x, y, z;

static uint8_t INIT_STATE = 0;

uint8_t demo128_init () {
 a.ary = a_ary;
 a.wordlen =  128  /  32 ;
 a.domain = DOMAIN_NORMAL;
 b.ary = b_ary;
 b.wordlen =  128  /  32 ;
 b.domain = DOMAIN_NORMAL;
 p.ary = p_ary;
 p.wordlen =  128  /  32 ;
 p.domain = DOMAIN_NORMAL;
 q.ary = q_ary;
 q.wordlen =  128  /  32 ;
 q.domain = DOMAIN_NORMAL;
 n.ary = n_ary;
 n.wordlen =  128  /  32 ;
 n.domain = DOMAIN_NORMAL;
 r.ary = r_ary;
 r.wordlen =  128  /  32 ;
 r.domain = DOMAIN_NORMAL;
 ar.ary = ar_ary;
 ar.wordlen =  128  /  32 ;
 ar.domain = DOMAIN_MONTGOMERY;
 br.ary = br_ary;
 br.wordlen =  128  /  32 ;
 br.domain = DOMAIN_MONTGOMERY;
 mu.ary = mu_ary;
 mu.wordlen =  128  /  32 ;
 mu.domain = DOMAIN_NORMAL;
 x.ary = x_ary;
 x.wordlen =  128  /  32 ;
 x.domain = DOMAIN_NORMAL;
 y.ary = y_ary;
 y.wordlen =  128  /  32 ;
 y.domain = DOMAIN_NORMAL;
 z.ary = z_ary;
 z.wordlen =  128  /  32 ;
 z.domain = DOMAIN_NORMAL;

 for (i = 0; i <  20 ; i++)
 {
 tmpary[i].ary = &demo_tmp_arys[i * ( 128  /  32  +  3 )];
 tmpary[i].wordlen =  128  /  32 ;
 tmpary[i].alloclen =  128  /  32 ;
 tmpary[i].domain = DOMAIN_NORMAL;
 }


 bigi_from_hex(demo128_sa,  (( (( 32 )/( 8 )) ) * 2)  *  128  /  32 , &a);
 bigi_from_hex(demo128_sb,  (( (( 32 )/( 8 )) ) * 2)  *  128  /  32 , &b);
 bigi_from_hex(demo128_sp,  (( (( 32 )/( 8 )) ) * 2)  *  128  /  32 , &p);
 bigi_from_hex(demo128_sq,  (( (( 32 )/( 8 )) ) * 2)  *  128  /  32 , &q);
 bigi_from_hex(demo128_sn,  (( (( 32 )/( 8 )) ) * 2)  *  128  /  32 , &n);
 INIT_STATE = 0x01;
 return 0x00;
}



uint8_t demo128_bigi_add (uint8_t* params) {
 if (0x01 != INIT_STATE) {
 if (0x00 != demo128_init()) {
 return 0x01;
 }
 }
 bigi_add(&a, &b, &x);
 bigi_to_bytes(output, 16, &x);

 dbg_puts_labeled ("A:");
 bigi_print(&a);
 dbg_putch('\n');
 dbg_puts_labeled ("B:");
 bigi_print(&b);
 dbg_putch('\n');
 dbg_puts_labeled ("X:");
 bigi_print(&x);
 dbg_putch('\n');

 simpleserial_put('r', 16, output);
 return 0x00;


}
uint8_t demo128_bigi_mod_exp (uint8_t* params) {
 if (0x01 != INIT_STATE) {
 if (0x00 != demo128_init()) {
 return 0x01;
 }
 }
 bigi_mod_exp(&a, &b, &n, &y, &x);
 bigi_to_bytes(output, 16, &x);

 dbg_puts_labeled ("A:");
 bigi_print(&a);
 dbg_putch('\n');
 dbg_puts_labeled ("B:");
 bigi_print(&b);
 dbg_putch('\n');
 dbg_puts_labeled ("N:");
 bigi_print(&n);
 dbg_putch('\n');
 dbg_puts_labeled ("X:");
 bigi_print(&x);
 dbg_putch('\n');

 simpleserial_put('r', 16, output);
 return 0x00;
}

uint8_t demo128_bigi_gcd (uint8_t* params) {
 if (0x01 != INIT_STATE) {
 if (0x00 != demo128_init()) {
 return 0x01;
 }
 }
 bigi_gcd(&a, &b, &tmpary[0],  20 , &x);
 bigi_to_bytes(output, 16, &x);

 dbg_puts_labeled ("A:");
 bigi_print(&a);
 dbg_putch('\n');
 dbg_puts_labeled ("B:");
 bigi_print(&b);
 dbg_putch('\n');
 dbg_puts_labeled ("X:");
 bigi_print(&x);
 dbg_putch('\n');

 simpleserial_put('r', 16, output);
 return 0x00;
 return 0x00;
}

uint8_t demo128_bigi_mod_exp_mont (uint8_t* params) {
 if (0x01 != INIT_STATE) {
 if (0x00 != demo128_init()) {
 return 0x01;
 }
 }
 bigi_get_mont_params(&n, &tmpary[0],  20 , &r, &demo_mu_mult);
 dbg_puts_labeled ("R:");
 bigi_print(&r);
 dbg_putch('\n');
 bigi_mod_exp_mont(&a, &b, &n, &r, demo_mu_mult, &tmpary[0],  20 , &x);

 bigi_to_bytes(output, 16, &x);

 dbg_puts_labeled ("A:");
 bigi_print(&a);
 dbg_putch('\n');
 dbg_puts_labeled ("B:");
 bigi_print(&b);
 dbg_putch('\n');
 dbg_puts_labeled ("X:");
 bigi_print(&x);
 dbg_putch('\n');

 simpleserial_put('r', 16, output);
 return 0x00;
 return 0x00;
}
