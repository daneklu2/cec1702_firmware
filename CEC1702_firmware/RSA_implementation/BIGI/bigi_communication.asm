_demo128_init:
;bigi_communication.c,39 :: 		uint8_t demo128_init () {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;bigi_communication.c,40 :: 		a.ary = a_ary;
MOVW	R1, #lo_addr(_a_ary+0)
MOVT	R1, #hi_addr(_a_ary+0)
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
STR	R1, [R0, #0]
;bigi_communication.c,41 :: 		a.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_a+4)
MOVT	R0, #hi_addr(_a+4)
STR	R1, [R0, #0]
;bigi_communication.c,42 :: 		a.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_a+12)
MOVT	R0, #hi_addr(_a+12)
STRB	R1, [R0, #0]
;bigi_communication.c,43 :: 		b.ary = b_ary;
MOVW	R1, #lo_addr(_b_ary+0)
MOVT	R1, #hi_addr(_b_ary+0)
MOVW	R0, #lo_addr(_b+0)
MOVT	R0, #hi_addr(_b+0)
STR	R1, [R0, #0]
;bigi_communication.c,44 :: 		b.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_b+4)
MOVT	R0, #hi_addr(_b+4)
STR	R1, [R0, #0]
;bigi_communication.c,45 :: 		b.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_b+12)
MOVT	R0, #hi_addr(_b+12)
STRB	R1, [R0, #0]
;bigi_communication.c,46 :: 		p.ary = p_ary;
MOVW	R1, #lo_addr(_p_ary+0)
MOVT	R1, #hi_addr(_p_ary+0)
MOVW	R0, #lo_addr(_p+0)
MOVT	R0, #hi_addr(_p+0)
STR	R1, [R0, #0]
;bigi_communication.c,47 :: 		p.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_p+4)
MOVT	R0, #hi_addr(_p+4)
STR	R1, [R0, #0]
;bigi_communication.c,48 :: 		p.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_p+12)
MOVT	R0, #hi_addr(_p+12)
STRB	R1, [R0, #0]
;bigi_communication.c,49 :: 		q.ary = q_ary;
MOVW	R1, #lo_addr(_q_ary+0)
MOVT	R1, #hi_addr(_q_ary+0)
MOVW	R0, #lo_addr(_q+0)
MOVT	R0, #hi_addr(_q+0)
STR	R1, [R0, #0]
;bigi_communication.c,50 :: 		q.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_q+4)
MOVT	R0, #hi_addr(_q+4)
STR	R1, [R0, #0]
;bigi_communication.c,51 :: 		q.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_q+12)
MOVT	R0, #hi_addr(_q+12)
STRB	R1, [R0, #0]
;bigi_communication.c,52 :: 		n.ary = n_ary;
MOVW	R1, #lo_addr(_n_ary+0)
MOVT	R1, #hi_addr(_n_ary+0)
MOVW	R0, #lo_addr(_n+0)
MOVT	R0, #hi_addr(_n+0)
STR	R1, [R0, #0]
;bigi_communication.c,53 :: 		n.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_n+4)
MOVT	R0, #hi_addr(_n+4)
STR	R1, [R0, #0]
;bigi_communication.c,54 :: 		n.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_n+12)
MOVT	R0, #hi_addr(_n+12)
STRB	R1, [R0, #0]
;bigi_communication.c,55 :: 		r.ary = r_ary;
MOVW	R1, #lo_addr(_r_ary+0)
MOVT	R1, #hi_addr(_r_ary+0)
MOVW	R0, #lo_addr(_r+0)
MOVT	R0, #hi_addr(_r+0)
STR	R1, [R0, #0]
;bigi_communication.c,56 :: 		r.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_r+4)
MOVT	R0, #hi_addr(_r+4)
STR	R1, [R0, #0]
;bigi_communication.c,57 :: 		r.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_r+12)
MOVT	R0, #hi_addr(_r+12)
STRB	R1, [R0, #0]
;bigi_communication.c,58 :: 		ar.ary = ar_ary;
MOVW	R1, #lo_addr(_ar_ary+0)
MOVT	R1, #hi_addr(_ar_ary+0)
MOVW	R0, #lo_addr(_ar+0)
MOVT	R0, #hi_addr(_ar+0)
STR	R1, [R0, #0]
;bigi_communication.c,59 :: 		ar.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_ar+4)
MOVT	R0, #hi_addr(_ar+4)
STR	R1, [R0, #0]
;bigi_communication.c,60 :: 		ar.domain = DOMAIN_MONTGOMERY;
MOVS	R1, #1
MOVW	R0, #lo_addr(_ar+12)
MOVT	R0, #hi_addr(_ar+12)
STRB	R1, [R0, #0]
;bigi_communication.c,61 :: 		br.ary = br_ary;
MOVW	R1, #lo_addr(_br_ary+0)
MOVT	R1, #hi_addr(_br_ary+0)
MOVW	R0, #lo_addr(_br+0)
MOVT	R0, #hi_addr(_br+0)
STR	R1, [R0, #0]
;bigi_communication.c,62 :: 		br.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_br+4)
MOVT	R0, #hi_addr(_br+4)
STR	R1, [R0, #0]
;bigi_communication.c,63 :: 		br.domain = DOMAIN_MONTGOMERY;
MOVS	R1, #1
MOVW	R0, #lo_addr(_br+12)
MOVT	R0, #hi_addr(_br+12)
STRB	R1, [R0, #0]
;bigi_communication.c,64 :: 		mu.ary = mu_ary;
MOVW	R1, #lo_addr(_mu_ary+0)
MOVT	R1, #hi_addr(_mu_ary+0)
MOVW	R0, #lo_addr(_mu+0)
MOVT	R0, #hi_addr(_mu+0)
STR	R1, [R0, #0]
;bigi_communication.c,65 :: 		mu.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_mu+4)
MOVT	R0, #hi_addr(_mu+4)
STR	R1, [R0, #0]
;bigi_communication.c,66 :: 		mu.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_mu+12)
MOVT	R0, #hi_addr(_mu+12)
STRB	R1, [R0, #0]
;bigi_communication.c,67 :: 		x.ary = x_ary;
MOVW	R1, #lo_addr(_x_ary+0)
MOVT	R1, #hi_addr(_x_ary+0)
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
STR	R1, [R0, #0]
;bigi_communication.c,68 :: 		x.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_x+4)
MOVT	R0, #hi_addr(_x+4)
STR	R1, [R0, #0]
;bigi_communication.c,69 :: 		x.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_x+12)
MOVT	R0, #hi_addr(_x+12)
STRB	R1, [R0, #0]
;bigi_communication.c,70 :: 		y.ary = y_ary;
MOVW	R1, #lo_addr(_y_ary+0)
MOVT	R1, #hi_addr(_y_ary+0)
MOVW	R0, #lo_addr(_y+0)
MOVT	R0, #hi_addr(_y+0)
STR	R1, [R0, #0]
;bigi_communication.c,71 :: 		y.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_y+4)
MOVT	R0, #hi_addr(_y+4)
STR	R1, [R0, #0]
;bigi_communication.c,72 :: 		y.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_y+12)
MOVT	R0, #hi_addr(_y+12)
STRB	R1, [R0, #0]
;bigi_communication.c,73 :: 		z.ary = z_ary;
MOVW	R1, #lo_addr(_z_ary+0)
MOVT	R1, #hi_addr(_z_ary+0)
MOVW	R0, #lo_addr(_z+0)
MOVT	R0, #hi_addr(_z+0)
STR	R1, [R0, #0]
;bigi_communication.c,74 :: 		z.wordlen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOVW	R1, #4
MOVW	R0, #lo_addr(_z+4)
MOVT	R0, #hi_addr(_z+4)
STR	R1, [R0, #0]
;bigi_communication.c,75 :: 		z.domain = DOMAIN_NORMAL;
MOVS	R1, #0
MOVW	R0, #lo_addr(_z+12)
MOVT	R0, #hi_addr(_z+12)
STRB	R1, [R0, #0]
;bigi_communication.c,77 :: 		for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
MOVS	R1, #0
MOVW	R0, #lo_addr(_i+0)
MOVT	R0, #hi_addr(_i+0)
STR	R1, [R0, #0]
L_demo128_init0:
MOVW	R0, #lo_addr(_i+0)
MOVT	R0, #hi_addr(_i+0)
LDR	R0, [R0, #0]
CMP	R0, #20
IT	CS
BCS	L_demo128_init1
;bigi_communication.c,79 :: 		tmpary[i].ary      = &demo_tmp_arys[i * (EXAMPLE_BITLEN / MACHINE_WORD_BITLEN + MULT_BUFFER_WORDLEN)];
MOVW	R3, #lo_addr(_i+0)
MOVT	R3, #hi_addr(_i+0)
LDR	R0, [R3, #0]
LSLS	R1, R0, #4
MOVW	R0, #lo_addr(_tmpary+0)
MOVT	R0, #hi_addr(_tmpary+0)
ADDS	R2, R0, R1
MOV	R0, R3
LDR	R1, [R0, #0]
MOVW	R0, #7
MULS	R0, R1, R0
LSLS	R1, R0, #2
MOVW	R0, #lo_addr(_demo_tmp_arys+0)
MOVT	R0, #hi_addr(_demo_tmp_arys+0)
ADDS	R0, R0, R1
STR	R0, [R2, #0]
;bigi_communication.c,80 :: 		tmpary[i].wordlen  = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOV	R0, R3
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
MOVW	R0, #lo_addr(_tmpary+0)
MOVT	R0, #hi_addr(_tmpary+0)
ADDS	R0, R0, R1
ADDS	R1, R0, #4
MOVW	R0, #4
STR	R0, [R1, #0]
;bigi_communication.c,81 :: 		tmpary[i].alloclen = EXAMPLE_BITLEN / MACHINE_WORD_BITLEN;
MOV	R0, R3
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
MOVW	R0, #lo_addr(_tmpary+0)
MOVT	R0, #hi_addr(_tmpary+0)
ADDS	R0, R0, R1
ADDW	R1, R0, #8
MOVW	R0, #4
STR	R0, [R1, #0]
;bigi_communication.c,82 :: 		tmpary[i].domain   = DOMAIN_NORMAL;
MOV	R0, R3
LDR	R0, [R0, #0]
LSLS	R1, R0, #4
MOVW	R0, #lo_addr(_tmpary+0)
MOVT	R0, #hi_addr(_tmpary+0)
ADDS	R0, R0, R1
ADDW	R1, R0, #12
MOVS	R0, #0
STRB	R0, [R1, #0]
;bigi_communication.c,77 :: 		for (i = 0; i < EXAMPLE_TMPARY_LEN; i++)
MOV	R0, R3
LDR	R0, [R0, #0]
ADDS	R0, R0, #1
STR	R0, [R3, #0]
;bigi_communication.c,83 :: 		}
IT	AL
BAL	L_demo128_init0
L_demo128_init1:
;bigi_communication.c,86 :: 		bigi_from_hex(demo128_sa, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &a);
MOVW	R0, #lo_addr(_demo128_sa+0)
MOVT	R0, #hi_addr(_demo128_sa+0)
LDR	R0, [R0, #0]
MOVW	R2, #lo_addr(_a+0)
MOVT	R2, #hi_addr(_a+0)
MOVW	R1, #32
BL	_bigi_from_hex+0
;bigi_communication.c,87 :: 		bigi_from_hex(demo128_sb, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &b);
MOVW	R0, #lo_addr(_demo128_sb+0)
MOVT	R0, #hi_addr(_demo128_sb+0)
LDR	R0, [R0, #0]
MOVW	R2, #lo_addr(_b+0)
MOVT	R2, #hi_addr(_b+0)
MOVW	R1, #32
BL	_bigi_from_hex+0
;bigi_communication.c,88 :: 		bigi_from_hex(demo128_sp, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &p);
MOVW	R0, #lo_addr(_demo128_sp+0)
MOVT	R0, #hi_addr(_demo128_sp+0)
LDR	R0, [R0, #0]
MOVW	R2, #lo_addr(_p+0)
MOVT	R2, #hi_addr(_p+0)
MOVW	R1, #32
BL	_bigi_from_hex+0
;bigi_communication.c,89 :: 		bigi_from_hex(demo128_sq, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &q);
MOVW	R0, #lo_addr(_demo128_sq+0)
MOVT	R0, #hi_addr(_demo128_sq+0)
LDR	R0, [R0, #0]
MOVW	R2, #lo_addr(_q+0)
MOVT	R2, #hi_addr(_q+0)
MOVW	R1, #32
BL	_bigi_from_hex+0
;bigi_communication.c,90 :: 		bigi_from_hex(demo128_sn, HEXCHARS_PER_MW * EXAMPLE_BITLEN / MACHINE_WORD_BITLEN, &n);
MOVW	R0, #lo_addr(_demo128_sn+0)
MOVT	R0, #hi_addr(_demo128_sn+0)
LDR	R0, [R0, #0]
MOVW	R2, #lo_addr(_n+0)
MOVT	R2, #hi_addr(_n+0)
MOVW	R1, #32
BL	_bigi_from_hex+0
;bigi_communication.c,91 :: 		INIT_STATE = 0x01;
MOVS	R1, #1
MOVW	R0, #lo_addr(bigi_communication_INIT_STATE+0)
MOVT	R0, #hi_addr(bigi_communication_INIT_STATE+0)
STRB	R1, [R0, #0]
;bigi_communication.c,92 :: 		return 0x00;
MOVS	R0, #0
;bigi_communication.c,93 :: 		}
L_end_demo128_init:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _demo128_init
_demo128_bigi_add:
;bigi_communication.c,97 :: 		uint8_t demo128_bigi_add (uint8_t* params) {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;bigi_communication.c,98 :: 		if (0x01 != INIT_STATE) {
MOVW	R1, #lo_addr(bigi_communication_INIT_STATE+0)
MOVT	R1, #hi_addr(bigi_communication_INIT_STATE+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	EQ
BEQ	L_demo128_bigi_add3
;bigi_communication.c,99 :: 		if (0x00 != demo128_init()) {
BL	_demo128_init+0
CMP	R0, #0
IT	EQ
BEQ	L_demo128_bigi_add4
;bigi_communication.c,100 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_demo128_bigi_add
;bigi_communication.c,101 :: 		}
L_demo128_bigi_add4:
;bigi_communication.c,102 :: 		}
L_demo128_bigi_add3:
;bigi_communication.c,103 :: 		bigi_add(&a, &b, &x);
MOVW	R2, #lo_addr(_x+0)
MOVT	R2, #hi_addr(_x+0)
MOVW	R1, #lo_addr(_b+0)
MOVT	R1, #hi_addr(_b+0)
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_add+0
;bigi_communication.c,104 :: 		bigi_to_bytes(output, 16, &x);
MOVW	R2, #lo_addr(_x+0)
MOVT	R2, #hi_addr(_x+0)
MOVS	R1, #16
MOVW	R0, #lo_addr(_output+0)
MOVT	R0, #hi_addr(_output+0)
BL	_bigi_to_bytes+0
;bigi_communication.c,106 :: 		dbg_puts_labeled ("A:");
MOVW	R1, #lo_addr(?lstr6_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr6_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,107 :: 		bigi_print(&a);
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_print+0
;bigi_communication.c,108 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,109 :: 		dbg_puts_labeled ("B:");
MOVW	R1, #lo_addr(?lstr7_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr7_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,110 :: 		bigi_print(&b);
MOVW	R0, #lo_addr(_b+0)
MOVT	R0, #hi_addr(_b+0)
BL	_bigi_print+0
;bigi_communication.c,111 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,112 :: 		dbg_puts_labeled ("X:");
MOVW	R1, #lo_addr(?lstr8_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr8_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,113 :: 		bigi_print(&x);
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
BL	_bigi_print+0
;bigi_communication.c,114 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,116 :: 		simpleserial_put('r', 16, output);
MOVW	R2, #lo_addr(_output+0)
MOVT	R2, #hi_addr(_output+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;bigi_communication.c,117 :: 		return 0x00;
MOVS	R0, #0
;bigi_communication.c,120 :: 		}
L_end_demo128_bigi_add:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _demo128_bigi_add
_demo128_bigi_mod_exp:
;bigi_communication.c,121 :: 		uint8_t demo128_bigi_mod_exp (uint8_t* params) {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;bigi_communication.c,122 :: 		if (0x01 != INIT_STATE) {
MOVW	R1, #lo_addr(bigi_communication_INIT_STATE+0)
MOVT	R1, #hi_addr(bigi_communication_INIT_STATE+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	EQ
BEQ	L_demo128_bigi_mod_exp5
;bigi_communication.c,123 :: 		if (0x00 != demo128_init()) {
BL	_demo128_init+0
CMP	R0, #0
IT	EQ
BEQ	L_demo128_bigi_mod_exp6
;bigi_communication.c,124 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_demo128_bigi_mod_exp
;bigi_communication.c,125 :: 		}
L_demo128_bigi_mod_exp6:
;bigi_communication.c,126 :: 		}
L_demo128_bigi_mod_exp5:
;bigi_communication.c,127 :: 		bigi_mod_exp(&a, &b, &n, &y, &x);
MOVW	R1, #lo_addr(_x+0)
MOVT	R1, #hi_addr(_x+0)
PUSH	(R1)
MOVW	R3, #lo_addr(_y+0)
MOVT	R3, #hi_addr(_y+0)
MOVW	R2, #lo_addr(_n+0)
MOVT	R2, #hi_addr(_n+0)
MOVW	R1, #lo_addr(_b+0)
MOVT	R1, #hi_addr(_b+0)
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_mod_exp+0
ADD	SP, SP, #4
;bigi_communication.c,128 :: 		bigi_to_bytes(output, 16, &x);
MOVW	R2, #lo_addr(_x+0)
MOVT	R2, #hi_addr(_x+0)
MOVS	R1, #16
MOVW	R0, #lo_addr(_output+0)
MOVT	R0, #hi_addr(_output+0)
BL	_bigi_to_bytes+0
;bigi_communication.c,130 :: 		dbg_puts_labeled ("A:");
MOVW	R1, #lo_addr(?lstr9_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr9_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,131 :: 		bigi_print(&a);
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_print+0
;bigi_communication.c,132 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,133 :: 		dbg_puts_labeled ("B:");
MOVW	R1, #lo_addr(?lstr10_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr10_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,134 :: 		bigi_print(&b);
MOVW	R0, #lo_addr(_b+0)
MOVT	R0, #hi_addr(_b+0)
BL	_bigi_print+0
;bigi_communication.c,135 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,136 :: 		dbg_puts_labeled ("N:");
MOVW	R1, #lo_addr(?lstr11_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr11_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,137 :: 		bigi_print(&n);
MOVW	R0, #lo_addr(_n+0)
MOVT	R0, #hi_addr(_n+0)
BL	_bigi_print+0
;bigi_communication.c,138 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,139 :: 		dbg_puts_labeled ("X:");
MOVW	R1, #lo_addr(?lstr12_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr12_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,140 :: 		bigi_print(&x);
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
BL	_bigi_print+0
;bigi_communication.c,141 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,143 :: 		simpleserial_put('r', 16, output);
MOVW	R2, #lo_addr(_output+0)
MOVT	R2, #hi_addr(_output+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;bigi_communication.c,144 :: 		return 0x00;
MOVS	R0, #0
;bigi_communication.c,145 :: 		}
L_end_demo128_bigi_mod_exp:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _demo128_bigi_mod_exp
_demo128_bigi_gcd:
;bigi_communication.c,147 :: 		uint8_t demo128_bigi_gcd (uint8_t* params) {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;bigi_communication.c,148 :: 		if (0x01 != INIT_STATE) {
MOVW	R1, #lo_addr(bigi_communication_INIT_STATE+0)
MOVT	R1, #hi_addr(bigi_communication_INIT_STATE+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	EQ
BEQ	L_demo128_bigi_gcd7
;bigi_communication.c,149 :: 		if (0x00 != demo128_init()) {
BL	_demo128_init+0
CMP	R0, #0
IT	EQ
BEQ	L_demo128_bigi_gcd8
;bigi_communication.c,150 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_demo128_bigi_gcd
;bigi_communication.c,151 :: 		}
L_demo128_bigi_gcd8:
;bigi_communication.c,152 :: 		}
L_demo128_bigi_gcd7:
;bigi_communication.c,153 :: 		bigi_gcd(&a, &b, &tmpary[0], EXAMPLE_TMPARY_LEN, &x);
MOVW	R1, #lo_addr(_x+0)
MOVT	R1, #hi_addr(_x+0)
PUSH	(R1)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary+0)
MOVT	R2, #hi_addr(_tmpary+0)
MOVW	R1, #lo_addr(_b+0)
MOVT	R1, #hi_addr(_b+0)
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_gcd+0
ADD	SP, SP, #4
;bigi_communication.c,154 :: 		bigi_to_bytes(output, 16, &x);
MOVW	R2, #lo_addr(_x+0)
MOVT	R2, #hi_addr(_x+0)
MOVS	R1, #16
MOVW	R0, #lo_addr(_output+0)
MOVT	R0, #hi_addr(_output+0)
BL	_bigi_to_bytes+0
;bigi_communication.c,156 :: 		dbg_puts_labeled ("A:");
MOVW	R1, #lo_addr(?lstr13_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr13_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,157 :: 		bigi_print(&a);
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_print+0
;bigi_communication.c,158 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,159 :: 		dbg_puts_labeled ("B:");
MOVW	R1, #lo_addr(?lstr14_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr14_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,160 :: 		bigi_print(&b);
MOVW	R0, #lo_addr(_b+0)
MOVT	R0, #hi_addr(_b+0)
BL	_bigi_print+0
;bigi_communication.c,161 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,162 :: 		dbg_puts_labeled ("X:");
MOVW	R1, #lo_addr(?lstr15_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr15_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,163 :: 		bigi_print(&x);
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
BL	_bigi_print+0
;bigi_communication.c,164 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,166 :: 		simpleserial_put('r', 16, output);
MOVW	R2, #lo_addr(_output+0)
MOVT	R2, #hi_addr(_output+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;bigi_communication.c,167 :: 		return 0x00;
MOVS	R0, #0
;bigi_communication.c,169 :: 		}
L_end_demo128_bigi_gcd:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _demo128_bigi_gcd
_demo128_bigi_mod_exp_mont:
;bigi_communication.c,171 :: 		uint8_t demo128_bigi_mod_exp_mont (uint8_t* params) {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;bigi_communication.c,172 :: 		if (0x01 != INIT_STATE) {
MOVW	R1, #lo_addr(bigi_communication_INIT_STATE+0)
MOVT	R1, #hi_addr(bigi_communication_INIT_STATE+0)
LDRB	R1, [R1, #0]
CMP	R1, #1
IT	EQ
BEQ	L_demo128_bigi_mod_exp_mont9
;bigi_communication.c,173 :: 		if (0x00 != demo128_init()) {
BL	_demo128_init+0
CMP	R0, #0
IT	EQ
BEQ	L_demo128_bigi_mod_exp_mont10
;bigi_communication.c,174 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_demo128_bigi_mod_exp_mont
;bigi_communication.c,175 :: 		}
L_demo128_bigi_mod_exp_mont10:
;bigi_communication.c,176 :: 		}
L_demo128_bigi_mod_exp_mont9:
;bigi_communication.c,177 :: 		bigi_get_mont_params(&n, &tmpary[0], EXAMPLE_TMPARY_LEN, &r, &demo_mu_mult);
MOVW	R1, #lo_addr(_demo_mu_mult+0)
MOVT	R1, #hi_addr(_demo_mu_mult+0)
PUSH	(R1)
MOVW	R3, #lo_addr(_r+0)
MOVT	R3, #hi_addr(_r+0)
MOVS	R2, #20
MOVW	R1, #lo_addr(_tmpary+0)
MOVT	R1, #hi_addr(_tmpary+0)
MOVW	R0, #lo_addr(_n+0)
MOVT	R0, #hi_addr(_n+0)
BL	_bigi_get_mont_params+0
ADD	SP, SP, #4
;bigi_communication.c,178 :: 		dbg_puts_labeled ("R:");
MOVW	R1, #lo_addr(?lstr16_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr16_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,179 :: 		bigi_print(&r);
MOVW	R0, #lo_addr(_r+0)
MOVT	R0, #hi_addr(_r+0)
BL	_bigi_print+0
;bigi_communication.c,180 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,181 :: 		bigi_mod_exp_mont(&a, &b, &n, &r, demo_mu_mult, &tmpary[0], EXAMPLE_TMPARY_LEN, &x);
MOVW	R4, #lo_addr(_x+0)
MOVT	R4, #hi_addr(_x+0)
MOVS	R3, #20
MOVW	R2, #lo_addr(_tmpary+0)
MOVT	R2, #hi_addr(_tmpary+0)
MOVW	R1, #lo_addr(_demo_mu_mult+0)
MOVT	R1, #hi_addr(_demo_mu_mult+0)
LDR	R1, [R1, #0]
PUSH	(R4)
PUSH	(R3)
PUSH	(R2)
PUSH	(R1)
MOVW	R3, #lo_addr(_r+0)
MOVT	R3, #hi_addr(_r+0)
MOVW	R2, #lo_addr(_n+0)
MOVT	R2, #hi_addr(_n+0)
MOVW	R1, #lo_addr(_b+0)
MOVT	R1, #hi_addr(_b+0)
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_mod_exp_mont+0
ADD	SP, SP, #16
;bigi_communication.c,183 :: 		bigi_to_bytes(output, 16, &x);
MOVW	R2, #lo_addr(_x+0)
MOVT	R2, #hi_addr(_x+0)
MOVS	R1, #16
MOVW	R0, #lo_addr(_output+0)
MOVT	R0, #hi_addr(_output+0)
BL	_bigi_to_bytes+0
;bigi_communication.c,185 :: 		dbg_puts_labeled ("A:");
MOVW	R1, #lo_addr(?lstr17_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr17_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,186 :: 		bigi_print(&a);
MOVW	R0, #lo_addr(_a+0)
MOVT	R0, #hi_addr(_a+0)
BL	_bigi_print+0
;bigi_communication.c,187 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,188 :: 		dbg_puts_labeled ("B:");
MOVW	R1, #lo_addr(?lstr18_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr18_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,189 :: 		bigi_print(&b);
MOVW	R0, #lo_addr(_b+0)
MOVT	R0, #hi_addr(_b+0)
BL	_bigi_print+0
;bigi_communication.c,190 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,191 :: 		dbg_puts_labeled ("X:");
MOVW	R1, #lo_addr(?lstr19_bigi_communication+0)
MOVT	R1, #hi_addr(?lstr19_bigi_communication+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;bigi_communication.c,192 :: 		bigi_print(&x);
MOVW	R0, #lo_addr(_x+0)
MOVT	R0, #hi_addr(_x+0)
BL	_bigi_print+0
;bigi_communication.c,193 :: 		dbg_putch('\n');
MOVS	R0, #10
BL	_dbg_putch+0
;bigi_communication.c,195 :: 		simpleserial_put('r', 16, output);
MOVW	R2, #lo_addr(_output+0)
MOVT	R2, #hi_addr(_output+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;bigi_communication.c,196 :: 		return 0x00;
MOVS	R0, #0
;bigi_communication.c,198 :: 		}
L_end_demo128_bigi_mod_exp_mont:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _demo128_bigi_mod_exp_mont
bigi_communication____?ag:
SUB	SP, SP, #4
L_end_bigi_communication___?ag:
ADD	SP, SP, #4
BX	LR
; end of bigi_communication____?ag
