#line 1 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi_io.c"
#line 1 "c:/mikroc pro for arm/include/stdio.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"





typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int int32_t;
typedef signed long long int64_t;


typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long int uint32_t;
typedef unsigned long long uint64_t;


typedef signed char int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;
typedef signed long long int_least64_t;


typedef unsigned char uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;
typedef unsigned long long uint_least64_t;



typedef signed long int int_fast8_t;
typedef signed long int int_fast16_t;
typedef signed long int int_fast32_t;
typedef signed long long int_fast64_t;


typedef unsigned long int uint_fast8_t;
typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long long uint_fast64_t;


typedef signed long int intptr_t;
typedef unsigned long int uintptr_t;


typedef signed long long intmax_t;
typedef unsigned long long uintmax_t;
#line 1 "c:/mikroc pro for arm/include/stddef.h"



typedef long ptrdiff_t;


 typedef unsigned long size_t;

typedef unsigned long wchar_t;
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 30 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef uint8_t byte_t;
#line 60 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 typedef uint32_t word_t;
 typedef uint64_t dword_t;
 typedef int64_t dsgn_t;

 typedef word_t index_t;
#line 75 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef enum
{
 DOMAIN_NORMAL = 0,
 DOMAIN_MONTGOMERY = 1
} domain_t;

typedef struct bigi_t
{
 word_t * ary;
 index_t wordlen;
 index_t alloclen;


 domain_t domain;
} bigi_t;
#line 94 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
typedef enum
{
 CMP_EQUAL = 0,
 CMP_NEQ = 1,
 CMP_LOWER = 2,
 CMP_GREATER = 3,
 CMP_UNDEFINED = 11
} cmp_t;
#line 215 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_set_zero(bigi_t *const A);
#line 224 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_copy(bigi_t *const OUT,
 const bigi_t *const IN);
#line 234 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_to_bytes(byte_t *const out,
 const index_t outlen,
 const bigi_t *const IN);
#line 245 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_from_bytes(bigi_t *const OUT,
 const byte_t *const in,
 const index_t inlen);
#line 256 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_get_bit(const bigi_t *const A,
 const index_t index,
 word_t *const res);
#line 268 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_cmp(const bigi_t *const A,
 const bigi_t *const B,
 cmp_t *const res);
#line 279 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_is_zero(const bigi_t *const A,
 cmp_t *const res);
#line 289 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_is_one(const bigi_t *const A,
 cmp_t *const res);
#line 300 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_tmp_safe_shorten(bigi_t *const TMP,
 const index_t new_wordlen);
#line 314 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_shift_left(bigi_t *const A,
 const index_t count);
#line 324 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_shift_right(bigi_t *const A,
 const index_t count);
#line 335 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_and(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 347 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_xor(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 359 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_add(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 370 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_add_one(const bigi_t *const A,
 bigi_t *const RES);
#line 381 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_sub(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const RES);
#line 392 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_sub_one(const bigi_t *const A,
 bigi_t *const RES);
#line 403 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_red(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 418 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_red_barrett(const bigi_t *const X,
 const bigi_t *const MOD,
 const bigi_t *const MU,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 433 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mult_word(const bigi_t *const A,
 const word_t B,
 bigi_t *const TMP,
 bigi_t *const RES);
#line 446 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mult_fit(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 461 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_mult(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const RES);
#line 475 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_exp(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 bigi_t *const TMP,
 bigi_t *const RES);
#line 490 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_inv(const bigi_t *const A,
 const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 505 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_div(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const QUO,
 bigi_t *const REM);
#line 520 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_gcd(const bigi_t *const A,
 const bigi_t *const B,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 538 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_get_mont_params(const bigi_t *const MOD,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const R,
 word_t *const mu);
#line 553 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_to_mont_domain(const bigi_t *const A,
 const bigi_t *const MOD,
 const bigi_t *const R,
 bigi_t *const AR);
#line 567 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_from_mont_domain(const bigi_t *const AR,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const A);
#line 585 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_mult_mont(const bigi_t *const AR,
 const bigi_t *const BR,
 const bigi_t *const MOD,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 602 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
 void  bigi_mod_exp_mont(const bigi_t *const A,
 const bigi_t *const B,
 const bigi_t *const MOD,
 const bigi_t *const R,
 const word_t mu,
 bigi_t *const TMPARY,
 const index_t tmparylen,
 bigi_t *const RES);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 22 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_to_hex(const bigi_t *const A,
 char *const str,
 const index_t strlen);
#line 34 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_from_hex(const char *const str,
 const index_t strlen,
 bigi_t *const A);
#line 44 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_print(const bigi_t *const A);
#line 53 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io.h"
 void  bigi_from_serial(const bigi_t *const A);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_impl.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 28 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_impl.h"
byte_t nlz(word_t x);

static const word_t MultiplyDeBruijnBitPosition[32] =
{
 0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
 31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
};
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io_impl.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi.h"
#line 12 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/rsa_implementation/bigi/bigi_io_impl.h"
dsgn_t hexdec(const char * hex);
#line 9 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi_io.c"
 void  bigi_to_hex(const bigi_t *const A,
 char *const str,
 const index_t strlen)
{
 index_t i, mult_buffer_in_use;

 if ((A ==  ((void *)0) ) || (str ==  ((void *)0) ))
 return  ;
 if (A->domain != DOMAIN_NORMAL)
 return  ;
 if (strlen < (A->wordlen *  (( (( 32 )/( 8 )) ) * 2)  + 1))
 return  ;


 mult_buffer_in_use =  3 ;


 for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
 {
 sprintf(str + i *  (( (( 32 )/( 8 )) ) * 2) , "%08x", A->ary[A->wordlen + mult_buffer_in_use - i - 1]);
 }

 return  ;
}

 void  bigi_from_hex(const char *const str,
 const index_t strlen,
 bigi_t *const A)
{
 index_t i,j;
 dsgn_t ret;

 if ((A ==  ((void *)0) ) || (str ==  ((void *)0) ))
 return  ;
 if (A->domain != DOMAIN_NORMAL)
 return  ;
 if ((A->wordlen *  (( (( 32 )/( 8 )) ) * 2) ) != strlen)
 return  ;

 for (i = 0, j = A->wordlen - 1;
 i < strlen;
 i +=  (( (( 32 )/( 8 )) ) * 2) , j--)
 {
 ret = hexdec(&str[i]);
 if (ret < 0)
 return  ;
 A->ary[j] = (word_t)ret;
 }
 for (j = A->wordlen; j < A->wordlen +  3 ; j++)
 A->ary[j] = 0u;

 return  ;
}

 void  bigi_print(const bigi_t *const A)
{
 index_t i, mult_buffer_in_use;

 char buff[10];


 if (A ==  ((void *)0) )
 return  ;


 mult_buffer_in_use =  3 ;



 for (i = 0; i < A->wordlen + mult_buffer_in_use; i++)
 {
 sprintf(buff, "%04x", (A->ary[A->wordlen + mult_buffer_in_use - i - 1] >> 16) & 0x0000ffff);
 UART0_Write_Text(buff);
 sprintf(buff, "%04x", (A->ary[A->wordlen + mult_buffer_in_use - i - 1] >> 0) & 0x0000ffff);
 UART0_Write_Text(buff);
 }
#line 90 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi_io.c"
 return  ;
}


 void  bigi_from_serial(const bigi_t *const A)
{
 volatile char buff[ (( (( 32 )/( 8 )) ) * 2) ];
 volatile index_t i, j;
 dsgn_t ret;

 if (A ==  ((void *)0) )
 return  ;
 if (A->domain != DOMAIN_NORMAL)
 return  ;

 for (i = 0;
 i < A->wordlen;
 i++)
 {
 for (j = 0; j <  (( (( 32 )/( 8 )) ) * 2) ; j++)
 {
 while (!UART0_Data_Ready());
 buff[j] = UART0_Read();
 }
 ret = hexdec(&buff[0]);
 if (ret < 0)
 return  ;
 A->ary[A->wordlen - 1 - i] = (word_t)ret;
 }
 for (i = A->wordlen; i < A->wordlen +  3 ; i++)
 A->ary[i] = 0u;

 return  ;
}
#line 130 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/RSA_implementation/BIGI/bigi_io.c"
const dsgn_t hextable[] =
{
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1, 0,1,2,3,4,5,6,7,8,9,-1,-1,-1,-1,-1,-1,-1,10,11,12,13,14,15,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,10,11,12,13,14,15,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,
 -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1
};


dsgn_t hexdec(const char * hex)
{
 dsgn_t ret = 0;
 index_t i;

 for (i = 0; i <  (( (( 32 )/( 8 )) ) * 2) ; i++)
 ret = (ret <<  4 ) | hextable[(byte_t)*(hex++)];

 return ret;
}
