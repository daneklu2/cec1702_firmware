_bigi_set_zero:
;bigi.c,73 :: 		BIGI_STATUS bigi_set_zero(bigi_t *const A)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; A end address is: 0 (R0)
; A start address is: 0 (R0)
;bigi.c,82 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 12 (R3)
MOVS	R3, #0
; A end address is: 0 (R0)
; i end address is: 12 (R3)
L_bigi_set_zero0:
; i start address is: 12 (R3)
; A start address is: 0 (R0)
ADDS	R1, R0, #4
LDR	R1, [R1, #0]
ADDS	R1, R1, #3
CMP	R3, R1
IT	CS
BCS	L_bigi_set_zero1
;bigi.c,83 :: 		A->ary[i] = 0;
LDR	R2, [R0, #0]
LSLS	R1, R3, #2
ADDS	R2, R2, R1
MOVS	R1, #0
STR	R1, [R2, #0]
;bigi.c,82 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R3, R3, #1
;bigi.c,83 :: 		A->ary[i] = 0;
; A end address is: 0 (R0)
; i end address is: 12 (R3)
IT	AL
BAL	L_bigi_set_zero0
L_bigi_set_zero1:
;bigi.c,85 :: 		return OK;
;bigi.c,86 :: 		}
L_end_bigi_set_zero:
ADD	SP, SP, #4
BX	LR
; end of _bigi_set_zero
_bigi_copy:
;bigi.c,89 :: 		const bigi_t *const IN)
; IN start address is: 4 (R1)
; OUT start address is: 0 (R0)
SUB	SP, SP, #4
; IN end address is: 4 (R1)
; OUT end address is: 0 (R0)
; OUT start address is: 0 (R0)
; IN start address is: 4 (R1)
;bigi.c,100 :: 		for (i = 0; i < IN->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 20 (R5)
MOVS	R5, #0
; OUT end address is: 0 (R0)
; IN end address is: 4 (R1)
; i end address is: 20 (R5)
STR	R1, [SP, #0]
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_copy3:
; i start address is: 20 (R5)
; IN start address is: 0 (R0)
; OUT start address is: 4 (R1)
ADDS	R2, R0, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R5, R2
IT	CS
BCS	L_bigi_copy4
;bigi.c,101 :: 		OUT->ary[i] = IN->ary[i];
LDR	R2, [R1, #0]
LSLS	R4, R5, #2
ADDS	R3, R2, R4
LDR	R2, [R0, #0]
ADDS	R2, R2, R4
LDR	R2, [R2, #0]
STR	R2, [R3, #0]
;bigi.c,100 :: 		for (i = 0; i < IN->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R5, R5, #1
;bigi.c,101 :: 		OUT->ary[i] = IN->ary[i];
; i end address is: 20 (R5)
IT	AL
BAL	L_bigi_copy3
L_bigi_copy4:
;bigi.c,102 :: 		for (i = IN->wordlen + MULT_BUFFER_WORDLEN; i < OUT->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R2, R0, #4
LDR	R2, [R2, #0]
ADDS	R4, R2, #3
; i start address is: 16 (R4)
; OUT end address is: 4 (R1)
; IN end address is: 0 (R0)
; i end address is: 16 (R4)
L_bigi_copy6:
; i start address is: 16 (R4)
; OUT start address is: 4 (R1)
; IN start address is: 0 (R0)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R4, R2
IT	CS
BCS	L_bigi_copy7
;bigi.c,103 :: 		OUT->ary[i] = 0u;
LDR	R3, [R1, #0]
LSLS	R2, R4, #2
ADDS	R3, R3, R2
MOVS	R2, #0
STR	R2, [R3, #0]
;bigi.c,102 :: 		for (i = IN->wordlen + MULT_BUFFER_WORDLEN; i < OUT->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R4, R4, #1
;bigi.c,103 :: 		OUT->ary[i] = 0u;
; i end address is: 16 (R4)
IT	AL
BAL	L_bigi_copy6
L_bigi_copy7:
;bigi.c,104 :: 		OUT->domain = IN->domain;
ADDW	R3, R1, #12
; OUT end address is: 4 (R1)
ADDW	R2, R0, #12
; IN end address is: 0 (R0)
LDRB	R2, [R2, #0]
STRB	R2, [R3, #0]
;bigi.c,106 :: 		return OK;
;bigi.c,107 :: 		}
L_end_bigi_copy:
ADD	SP, SP, #4
BX	LR
; end of _bigi_copy
_bigi_to_bytes:
;bigi.c,111 :: 		const bigi_t *const IN)
; i start address is: 8 (R2)
; IN start address is: 8 (R2)
; out start address is: 0 (R0)
SUB	SP, SP, #4
;bigi.c,126 :: 		for (i = 0; i < IN->wordlen; i++) {
;bigi.c,111 :: 		const bigi_t *const IN)
;bigi.c,126 :: 		for (i = 0; i < IN->wordlen; i++) {
;bigi.c,111 :: 		const bigi_t *const IN)
MOV	R1, R2
; i end address is: 8 (R2)
; IN end address is: 8 (R2)
; out end address is: 0 (R0)
; out start address is: 0 (R0)
; IN start address is: 4 (R1)
;bigi.c,126 :: 		for (i = 0; i < IN->wordlen; i++) {
; i start address is: 8 (R2)
MOVS	R2, #0
; out end address is: 0 (R0)
; IN end address is: 4 (R1)
; i end address is: 8 (R2)
STR	R1, [SP, #0]
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_to_bytes9:
; i start address is: 8 (R2)
; out start address is: 4 (R1)
; IN start address is: 0 (R0)
; out start address is: 4 (R1)
; out end address is: 4 (R1)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
CMP	R2, R3
IT	CS
BCS	L_bigi_to_bytes10
; out end address is: 4 (R1)
;bigi.c,132 :: 		tmp = IN->ary[IN->wordlen - i - 1];
; out start address is: 4 (R1)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
SUB	R3, R3, R2
SUBS	R3, R3, #1
LDR	R4, [R0, #0]
LSLS	R3, R3, #2
ADDS	R3, R4, R3
LDR	R5, [R3, #0]
; tmp start address is: 24 (R6)
MOV	R6, R5
;bigi.c,133 :: 		out[i * MACHINE_WORD_BYTELEN + 0] = (byte_t)((tmp >> 24) & 0xFF);
LSLS	R3, R2, #2
ADDS	R4, R1, R3
LSRS	R3, R5, #24
AND	R3, R3, #255
UXTB	R3, R3
STRB	R3, [R4, #0]
;bigi.c,134 :: 		out[i * MACHINE_WORD_BYTELEN + 1] = (byte_t)((tmp >> 16) & 0xFF);
LSLS	R3, R2, #2
ADDS	R3, R3, #1
ADDS	R4, R1, R3
LSRS	R3, R6, #16
AND	R3, R3, #255
UXTB	R3, R3
STRB	R3, [R4, #0]
;bigi.c,135 :: 		out[i * MACHINE_WORD_BYTELEN + 2] = (byte_t)((tmp >> 8) & 0xFF);
LSLS	R3, R2, #2
ADDS	R3, R3, #2
ADDS	R4, R1, R3
LSRS	R3, R6, #8
AND	R3, R3, #255
UXTB	R3, R3
STRB	R3, [R4, #0]
;bigi.c,136 :: 		out[i * MACHINE_WORD_BYTELEN + 3] = (byte_t)((tmp >> 0) & 0xFF);
LSLS	R3, R2, #2
ADDS	R3, R3, #3
ADDS	R4, R1, R3
AND	R3, R6, #255
; tmp end address is: 24 (R6)
UXTB	R3, R3
STRB	R3, [R4, #0]
;bigi.c,126 :: 		for (i = 0; i < IN->wordlen; i++) {
ADDS	R2, R2, #1
;bigi.c,140 :: 		}
; IN end address is: 0 (R0)
; out end address is: 4 (R1)
; i end address is: 8 (R2)
IT	AL
BAL	L_bigi_to_bytes9
L_bigi_to_bytes10:
;bigi.c,141 :: 		return OK;
;bigi.c,142 :: 		}
L_end_bigi_to_bytes:
ADD	SP, SP, #4
BX	LR
; end of _bigi_to_bytes
_bigi_from_bytes:
;bigi.c,146 :: 		const index_t inlen) {
; i start address is: 8 (R2)
; in start address is: 4 (R1)
; OUT start address is: 0 (R0)
SUB	SP, SP, #4
;bigi.c,156 :: 		for (i = 0; i < OUT->wordlen; i++) {
;bigi.c,146 :: 		const index_t inlen) {
;bigi.c,156 :: 		for (i = 0; i < OUT->wordlen; i++) {
;bigi.c,146 :: 		const index_t inlen) {
; i end address is: 8 (R2)
; in end address is: 4 (R1)
; OUT end address is: 0 (R0)
; OUT start address is: 0 (R0)
; in start address is: 4 (R1)
;bigi.c,156 :: 		for (i = 0; i < OUT->wordlen; i++) {
; i start address is: 8 (R2)
MOVS	R2, #0
; i end address is: 8 (R2)
; OUT end address is: 0 (R0)
L_bigi_from_bytes12:
; i start address is: 8 (R2)
; in start address is: 4 (R1)
; in end address is: 4 (R1)
; OUT start address is: 0 (R0)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
CMP	R2, R3
IT	CS
BCS	L_bigi_from_bytes13
; in end address is: 4 (R1)
;bigi.c,159 :: 		OUT->ary[OUT->wordlen - i - 1] =  (((word_t)(in[i * MACHINE_WORD_BYTELEN    ])) << 24) |
; in start address is: 4 (R1)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
SUB	R3, R3, R2
SUBS	R3, R3, #1
LDR	R4, [R0, #0]
LSLS	R3, R3, #2
ADDS	R6, R4, R3
LSLS	R5, R2, #2
ADDS	R3, R1, R5
LDRB	R3, [R3, #0]
LSLS	R4, R3, #24
;bigi.c,160 :: 		(((word_t)(in[i * MACHINE_WORD_BYTELEN + 1])) << 16) |
ADDS	R3, R5, #1
ADDS	R3, R1, R3
LDRB	R3, [R3, #0]
LSLS	R3, R3, #16
ORRS	R4, R3
;bigi.c,161 :: 		(((word_t)(in[i * MACHINE_WORD_BYTELEN + 2])) <<  8) |
ADDS	R3, R5, #2
ADDS	R3, R1, R3
LDRB	R3, [R3, #0]
LSLS	R3, R3, #8
ORRS	R4, R3
;bigi.c,162 :: 		(((word_t)(in[i * MACHINE_WORD_BYTELEN + 3])) <<  0) ;
ADDS	R3, R5, #3
ADDS	R3, R1, R3
LDRB	R3, [R3, #0]
ORR	R3, R4, R3, LSL #0
STR	R3, [R6, #0]
;bigi.c,156 :: 		for (i = 0; i < OUT->wordlen; i++) {
ADDS	R2, R2, #1
;bigi.c,163 :: 		}
; in end address is: 4 (R1)
; i end address is: 8 (R2)
IT	AL
BAL	L_bigi_from_bytes12
L_bigi_from_bytes13:
;bigi.c,164 :: 		for (i = OUT->wordlen; i < OUT->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R3, R0, #4
LDR	R1, [R3, #0]
; i start address is: 4 (R1)
; OUT end address is: 0 (R0)
; i end address is: 4 (R1)
L_bigi_from_bytes15:
; i start address is: 4 (R1)
; OUT start address is: 0 (R0)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R1, R3
IT	CS
BCS	L_bigi_from_bytes16
;bigi.c,165 :: 		OUT->ary[i] = 0u;
LDR	R4, [R0, #0]
LSLS	R3, R1, #2
ADDS	R4, R4, R3
MOVS	R3, #0
STR	R3, [R4, #0]
;bigi.c,164 :: 		for (i = OUT->wordlen; i < OUT->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R1, R1, #1
;bigi.c,165 :: 		OUT->ary[i] = 0u;
; i end address is: 4 (R1)
IT	AL
BAL	L_bigi_from_bytes15
L_bigi_from_bytes16:
;bigi.c,166 :: 		OUT->domain = DOMAIN_NORMAL;
ADDW	R4, R0, #12
; OUT end address is: 0 (R0)
MOVS	R3, #0
STRB	R3, [R4, #0]
;bigi.c,168 :: 		return OK;
;bigi.c,169 :: 		}
L_end_bigi_from_bytes:
ADD	SP, SP, #4
BX	LR
; end of _bigi_from_bytes
_bigi_get_bit:
;bigi.c,173 :: 		word_t *const res)
; res start address is: 8 (R2)
; index start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; res end address is: 8 (R2)
; index end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; index start address is: 4 (R1)
; res start address is: 8 (R2)
;bigi.c,182 :: 		*res = ((A->ary[index/MACHINE_WORD_BITLEN] >> (index % MACHINE_WORD_BITLEN))) & 1;
LSRS	R3, R1, #5
LDR	R4, [R0, #0]
; A end address is: 0 (R0)
LSLS	R3, R3, #2
ADDS	R3, R4, R3
LDR	R4, [R3, #0]
AND	R3, R1, #31
; index end address is: 4 (R1)
LSR	R3, R4, R3
AND	R3, R3, #1
STR	R3, [R2, #0]
; res end address is: 8 (R2)
;bigi.c,184 :: 		return OK;
;bigi.c,185 :: 		}
L_end_bigi_get_bit:
ADD	SP, SP, #4
BX	LR
; end of _bigi_get_bit
_bigi_cmp:
;bigi.c,189 :: 		cmp_t *const res)
; res start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
STR	R2, [SP, #0]
MOV	R2, R0
LDR	R0, [SP, #0]
; res end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 8 (R2)
; B start address is: 4 (R1)
; res start address is: 0 (R0)
;bigi.c,206 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R6, R3, #3
SUBS	R6, R6, #1
; i start address is: 24 (R6)
; res end address is: 0 (R0)
; i end address is: 24 (R6)
L_bigi_cmp18:
; i start address is: 24 (R6)
; res start address is: 0 (R0)
; B start address is: 4 (R1)
; B end address is: 4 (R1)
; A start address is: 8 (R2)
; A end address is: 8 (R2)
CMP	R6, #0
IT	LT
BLT	L_bigi_cmp19
; B end address is: 4 (R1)
; A end address is: 8 (R2)
;bigi.c,208 :: 		if (A->ary[i] == B->ary[i])
; A start address is: 8 (R2)
; B start address is: 4 (R1)
LDR	R3, [R2, #0]
LSLS	R5, R6, #2
ADDS	R3, R3, R5
LDR	R4, [R3, #0]
LDR	R3, [R1, #0]
ADDS	R3, R3, R5
LDR	R3, [R3, #0]
CMP	R4, R3
IT	NE
BNE	L_bigi_cmp21
;bigi.c,209 :: 		continue;
IT	AL
BAL	L_bigi_cmp20
L_bigi_cmp21:
;bigi.c,210 :: 		else if (A->ary[i] > B->ary[i])
LDR	R3, [R2, #0]
; A end address is: 8 (R2)
LSLS	R5, R6, #2
; i end address is: 24 (R6)
ADDS	R3, R3, R5
LDR	R4, [R3, #0]
LDR	R3, [R1, #0]
; B end address is: 4 (R1)
ADDS	R3, R3, R5
LDR	R3, [R3, #0]
CMP	R4, R3
IT	LS
BLS	L_bigi_cmp23
;bigi.c,212 :: 		*res = CMP_GREATER;
MOVS	R3, #3
STRB	R3, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,213 :: 		return OK;
IT	AL
BAL	L_end_bigi_cmp
;bigi.c,214 :: 		}
L_bigi_cmp23:
;bigi.c,217 :: 		*res = CMP_LOWER;
; res start address is: 0 (R0)
MOVS	R3, #2
STRB	R3, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,218 :: 		return OK;
IT	AL
BAL	L_end_bigi_cmp
;bigi.c,220 :: 		}
L_bigi_cmp20:
;bigi.c,206 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; i start address is: 24 (R6)
; A start address is: 8 (R2)
; B start address is: 4 (R1)
; res start address is: 0 (R0)
SUBS	R6, R6, #1
;bigi.c,220 :: 		}
; B end address is: 4 (R1)
; A end address is: 8 (R2)
; i end address is: 24 (R6)
IT	AL
BAL	L_bigi_cmp18
L_bigi_cmp19:
;bigi.c,221 :: 		*res = CMP_EQUAL;
MOVS	R3, #0
STRB	R3, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,223 :: 		return OK;
;bigi.c,224 :: 		}
L_end_bigi_cmp:
ADD	SP, SP, #4
BX	LR
; end of _bigi_cmp
_bigi_is_zero:
;bigi.c,227 :: 		cmp_t *const res)
; res start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; res end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; res start address is: 4 (R1)
;bigi.c,236 :: 		for (i = 0; i < A->wordlen; i++)
; i start address is: 16 (R4)
MOVS	R4, #0
; A end address is: 0 (R0)
; res end address is: 4 (R1)
; i end address is: 16 (R4)
STR	R1, [SP, #0]
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_is_zero25:
; i start address is: 16 (R4)
; res start address is: 0 (R0)
; A start address is: 4 (R1)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
CMP	R4, R2
IT	CS
BCS	L_bigi_is_zero26
;bigi.c,238 :: 		if (A->ary[i] != 0u)
LDR	R3, [R1, #0]
LSLS	R2, R4, #2
ADDS	R2, R3, R2
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_bigi_is_zero28
; A end address is: 4 (R1)
; i end address is: 16 (R4)
;bigi.c,240 :: 		*res = CMP_NEQ;
MOVS	R2, #1
STRB	R2, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,241 :: 		return OK;
IT	AL
BAL	L_end_bigi_is_zero
;bigi.c,242 :: 		}
L_bigi_is_zero28:
;bigi.c,236 :: 		for (i = 0; i < A->wordlen; i++)
; res start address is: 0 (R0)
; i start address is: 16 (R4)
; A start address is: 4 (R1)
ADDS	R4, R4, #1
;bigi.c,243 :: 		}
; i end address is: 16 (R4)
IT	AL
BAL	L_bigi_is_zero25
L_bigi_is_zero26:
;bigi.c,244 :: 		if (A->domain != DOMAIN_NORMAL)
ADDW	R2, R1, #12
LDRB	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L__bigi_is_zero312
;bigi.c,246 :: 		for (i = A->wordlen; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R2, R1, #4
LDR	R4, [R2, #0]
; i start address is: 16 (R4)
; A end address is: 4 (R1)
; i end address is: 16 (R4)
; res end address is: 0 (R0)
L_bigi_is_zero30:
; i start address is: 16 (R4)
; A start address is: 4 (R1)
; res start address is: 0 (R0)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R4, R2
IT	CS
BCS	L_bigi_is_zero31
;bigi.c,248 :: 		if (A->ary[i] != 0u)
LDR	R3, [R1, #0]
LSLS	R2, R4, #2
ADDS	R2, R3, R2
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_bigi_is_zero33
; A end address is: 4 (R1)
; i end address is: 16 (R4)
;bigi.c,250 :: 		*res = CMP_NEQ;
MOVS	R2, #1
STRB	R2, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,251 :: 		return OK;
IT	AL
BAL	L_end_bigi_is_zero
;bigi.c,252 :: 		}
L_bigi_is_zero33:
;bigi.c,246 :: 		for (i = A->wordlen; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
; res start address is: 0 (R0)
; i start address is: 16 (R4)
; A start address is: 4 (R1)
ADDS	R4, R4, #1
;bigi.c,253 :: 		}
; A end address is: 4 (R1)
; i end address is: 16 (R4)
IT	AL
BAL	L_bigi_is_zero30
L_bigi_is_zero31:
;bigi.c,254 :: 		}
IT	AL
BAL	L_bigi_is_zero29
; res end address is: 0 (R0)
L__bigi_is_zero312:
;bigi.c,244 :: 		if (A->domain != DOMAIN_NORMAL)
;bigi.c,254 :: 		}
L_bigi_is_zero29:
;bigi.c,255 :: 		*res = CMP_EQUAL;
; res start address is: 0 (R0)
MOVS	R2, #0
STRB	R2, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,257 :: 		return OK;
;bigi.c,258 :: 		}
L_end_bigi_is_zero:
ADD	SP, SP, #4
BX	LR
; end of _bigi_is_zero
_bigi_is_one:
;bigi.c,261 :: 		cmp_t *const res)
; res start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; res end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; res start address is: 4 (R1)
;bigi.c,272 :: 		if (A->ary[0] != 1u)
LDR	R2, [R0, #0]
LDR	R2, [R2, #0]
CMP	R2, #1
IT	EQ
BEQ	L_bigi_is_one34
; A end address is: 0 (R0)
;bigi.c,274 :: 		*res = CMP_NEQ;
MOVS	R2, #1
STRB	R2, [R1, #0]
; res end address is: 4 (R1)
;bigi.c,275 :: 		return OK;
IT	AL
BAL	L_end_bigi_is_one
;bigi.c,276 :: 		}
L_bigi_is_one34:
;bigi.c,278 :: 		for (i = 1; i < A->wordlen; i++)
; i start address is: 16 (R4)
; res start address is: 4 (R1)
; A start address is: 0 (R0)
MOVS	R4, #1
; A end address is: 0 (R0)
; res end address is: 4 (R1)
; i end address is: 16 (R4)
STR	R1, [SP, #0]
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_is_one35:
; i start address is: 16 (R4)
; res start address is: 0 (R0)
; A start address is: 4 (R1)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
CMP	R4, R2
IT	CS
BCS	L_bigi_is_one36
;bigi.c,280 :: 		if (A->ary[i] != 0)
LDR	R3, [R1, #0]
LSLS	R2, R4, #2
ADDS	R2, R3, R2
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_bigi_is_one38
; A end address is: 4 (R1)
; i end address is: 16 (R4)
;bigi.c,282 :: 		*res = CMP_NEQ;
MOVS	R2, #1
STRB	R2, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,283 :: 		return OK;
IT	AL
BAL	L_end_bigi_is_one
;bigi.c,284 :: 		}
L_bigi_is_one38:
;bigi.c,278 :: 		for (i = 1; i < A->wordlen; i++)
; i start address is: 16 (R4)
; A start address is: 4 (R1)
; res start address is: 0 (R0)
ADDS	R4, R4, #1
;bigi.c,285 :: 		}
; A end address is: 4 (R1)
; i end address is: 16 (R4)
IT	AL
BAL	L_bigi_is_one35
L_bigi_is_one36:
;bigi.c,286 :: 		*res = CMP_EQUAL;
MOVS	R2, #0
STRB	R2, [R0, #0]
; res end address is: 0 (R0)
;bigi.c,288 :: 		return OK;
;bigi.c,289 :: 		}
L_end_bigi_is_one:
ADD	SP, SP, #4
BX	LR
; end of _bigi_is_one
_bigi_tmp_safe_shorten:
;bigi.c,292 :: 		const index_t new_wordlen)
; new_wordlen start address is: 4 (R1)
; TMP start address is: 0 (R0)
SUB	SP, SP, #4
MOV	R2, R0
; new_wordlen end address is: 4 (R1)
; TMP end address is: 0 (R0)
; TMP start address is: 8 (R2)
; new_wordlen start address is: 4 (R1)
;bigi.c,304 :: 		for (i = new_wordlen; i < TMP->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 0 (R0)
MOV	R0, R1
; TMP end address is: 8 (R2)
; new_wordlen end address is: 4 (R1)
; i end address is: 0 (R0)
MOV	R4, R2
L_bigi_tmp_safe_shorten39:
; i start address is: 0 (R0)
; new_wordlen start address is: 4 (R1)
; TMP start address is: 16 (R4)
ADDS	R2, R4, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R0, R2
IT	CS
BCS	L_bigi_tmp_safe_shorten40
;bigi.c,306 :: 		if (TMP->ary[i] != 0u)
LDR	R3, [R4, #0]
LSLS	R2, R0, #2
ADDS	R2, R3, R2
LDR	R2, [R2, #0]
CMP	R2, #0
IT	EQ
BEQ	L_bigi_tmp_safe_shorten42
; new_wordlen end address is: 4 (R1)
; TMP end address is: 16 (R4)
; i end address is: 0 (R0)
;bigi.c,307 :: 		return BIGI_OVERFLOW;
IT	AL
BAL	L_end_bigi_tmp_safe_shorten
L_bigi_tmp_safe_shorten42:
;bigi.c,304 :: 		for (i = new_wordlen; i < TMP->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 0 (R0)
; TMP start address is: 16 (R4)
; new_wordlen start address is: 4 (R1)
ADDS	R0, R0, #1
;bigi.c,308 :: 		}
; i end address is: 0 (R0)
IT	AL
BAL	L_bigi_tmp_safe_shorten39
L_bigi_tmp_safe_shorten40:
;bigi.c,310 :: 		TMP->wordlen = new_wordlen;
ADDS	R2, R4, #4
; TMP end address is: 16 (R4)
STR	R1, [R2, #0]
; new_wordlen end address is: 4 (R1)
;bigi.c,312 :: 		return OK;
;bigi.c,313 :: 		}
L_end_bigi_tmp_safe_shorten:
ADD	SP, SP, #4
BX	LR
; end of _bigi_tmp_safe_shorten
_bigi_shift_left:
;bigi.c,321 :: 		const index_t count)
; count start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R3, R0
MOV	R4, R1
; count end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 12 (R3)
; count start address is: 16 (R4)
;bigi.c,332 :: 		if (count == 0)
CMP	R4, #0
IT	NE
BNE	L_bigi_shift_left43
; A end address is: 12 (R3)
; count end address is: 16 (R4)
;bigi.c,333 :: 		return OK;
IT	AL
BAL	L_end_bigi_shift_left
L_bigi_shift_left43:
;bigi.c,336 :: 		if (count >= (A->wordlen + MULT_BUFFER_WORDLEN) * MACHINE_WORD_BITLEN)
; count start address is: 16 (R4)
; A start address is: 12 (R3)
ADDS	R2, R3, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
LSLS	R2, R2, #5
CMP	R4, R2
IT	CC
BCC	L_bigi_shift_left44
; count end address is: 16 (R4)
;bigi.c,338 :: 		BIGI_RETURN(bigi_set_zero(A))
MOV	R0, R3
; A end address is: 12 (R3)
BL	_bigi_set_zero+0
IT	AL
BAL	L_end_bigi_shift_left
;bigi.c,339 :: 		}
L_bigi_shift_left44:
;bigi.c,342 :: 		bigs = count / MACHINE_WORD_BITLEN;
; count start address is: 16 (R4)
; A start address is: 12 (R3)
LSRS	R2, R4, #5
; bigs start address is: 0 (R0)
MOV	R0, R2
;bigi.c,343 :: 		if (bigs > 0)
CMP	R2, #0
IT	LS
BLS	L__bigi_shift_left313
;bigi.c,346 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= bigs; i--)
ADDS	R2, R3, #4
LDR	R2, [R2, #0]
ADDS	R1, R2, #3
SUBS	R1, R1, #1
; i start address is: 4 (R1)
; A end address is: 12 (R3)
; count end address is: 16 (R4)
; bigs end address is: 0 (R0)
; i end address is: 4 (R1)
MOV	R6, R3
MOV	R5, R4
L_bigi_shift_left46:
; i start address is: 4 (R1)
; bigs start address is: 0 (R0)
; count start address is: 20 (R5)
; A start address is: 24 (R6)
CMP	R1, R0
IT	CC
BCC	L_bigi_shift_left47
;bigi.c,348 :: 		A->ary[i] = A->ary[i-bigs];
LDR	R4, [R6, #0]
LSLS	R2, R1, #2
ADDS	R3, R4, R2
SUB	R2, R1, R0
LSLS	R2, R2, #2
ADDS	R2, R4, R2
LDR	R2, [R2, #0]
STR	R2, [R3, #0]
;bigi.c,346 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= bigs; i--)
SUBS	R1, R1, #1
;bigi.c,349 :: 		}
; i end address is: 4 (R1)
IT	AL
BAL	L_bigi_shift_left46
L_bigi_shift_left47:
;bigi.c,350 :: 		for (i = 0; i < bigs; i++)
; i start address is: 4 (R1)
MOVS	R1, #0
; bigs end address is: 0 (R0)
; count end address is: 20 (R5)
; A end address is: 24 (R6)
; i end address is: 4 (R1)
MOV	R4, R0
MOV	R0, R5
MOV	R5, R1
MOV	R1, R6
L_bigi_shift_left49:
; i start address is: 20 (R5)
; A start address is: 4 (R1)
; count start address is: 0 (R0)
; bigs start address is: 16 (R4)
CMP	R5, R4
IT	CS
BCS	L_bigi_shift_left50
;bigi.c,352 :: 		A->ary[i] = 0u;
LDR	R3, [R1, #0]
LSLS	R2, R5, #2
ADDS	R3, R3, R2
MOVS	R2, #0
STR	R2, [R3, #0]
;bigi.c,350 :: 		for (i = 0; i < bigs; i++)
ADDS	R5, R5, #1
;bigi.c,353 :: 		}
; i end address is: 20 (R5)
IT	AL
BAL	L_bigi_shift_left49
L_bigi_shift_left50:
;bigi.c,354 :: 		}
; count end address is: 0 (R0)
; A end address is: 4 (R1)
MOV	R6, R4
IT	AL
BAL	L_bigi_shift_left45
; bigs end address is: 16 (R4)
L__bigi_shift_left313:
;bigi.c,343 :: 		if (bigs > 0)
MOV	R6, R0
MOV	R0, R4
MOV	R1, R3
;bigi.c,354 :: 		}
L_bigi_shift_left45:
;bigi.c,357 :: 		smalls = count % MACHINE_WORD_BITLEN;
; bigs start address is: 24 (R6)
; count start address is: 0 (R0)
; A start address is: 4 (R1)
AND	R2, R0, #31
; count end address is: 0 (R0)
; smalls start address is: 0 (R0)
MOV	R0, R2
;bigi.c,358 :: 		if (smalls == 0)
CMP	R2, #0
IT	NE
BNE	L_bigi_shift_left52
; smalls end address is: 0 (R0)
; A end address is: 4 (R1)
; bigs end address is: 24 (R6)
;bigi.c,359 :: 		return OK;
IT	AL
BAL	L_end_bigi_shift_left
L_bigi_shift_left52:
;bigi.c,361 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i > bigs ; i--)
; bigs start address is: 24 (R6)
; A start address is: 4 (R1)
; smalls start address is: 0 (R0)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R5, R2, #3
SUBS	R5, R5, #1
; i start address is: 20 (R5)
; smalls end address is: 0 (R0)
; A end address is: 4 (R1)
; bigs end address is: 24 (R6)
; i end address is: 20 (R5)
L_bigi_shift_left53:
; i start address is: 20 (R5)
; smalls start address is: 0 (R0)
; A start address is: 4 (R1)
; bigs start address is: 24 (R6)
CMP	R5, R6
IT	LS
BLS	L_bigi_shift_left54
;bigi.c,363 :: 		A->ary[i] <<= smalls;                                                   /* 1000        . 0100 */
LDR	R3, [R1, #0]
LSLS	R2, R5, #2
ADDS	R3, R3, R2
LDR	R2, [R3, #0]
LSLS	R2, R0
STR	R2, [R3, #0]
;bigi.c,364 :: 		A->ary[i] |= (A->ary[i-1] >> (MACHINE_WORD_BITLEN - smalls));           /* 1000 | 0010 . 0100 */
LDR	R3, [R1, #0]
LSLS	R2, R5, #2
ADDS	R4, R3, R2
SUBS	R2, R5, #1
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
RSB	R2, R0, #32
LSRS	R3, R2
LDR	R2, [R4, #0]
ORRS	R2, R3
STR	R2, [R4, #0]
;bigi.c,361 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i > bigs ; i--)
SUBS	R5, R5, #1
;bigi.c,365 :: 		}                                                                           /* 1010        . 0100 */
; i end address is: 20 (R5)
IT	AL
BAL	L_bigi_shift_left53
L_bigi_shift_left54:
;bigi.c,366 :: 		A->ary[bigs] <<= smalls;
LDR	R3, [R1, #0]
; A end address is: 4 (R1)
LSLS	R2, R6, #2
; bigs end address is: 24 (R6)
ADDS	R3, R3, R2
LDR	R2, [R3, #0]
LSLS	R2, R0
; smalls end address is: 0 (R0)
STR	R2, [R3, #0]
;bigi.c,368 :: 		return OK;
;bigi.c,369 :: 		}
L_end_bigi_shift_left:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _bigi_shift_left
_bigi_shift_right:
;bigi.c,372 :: 		const index_t count)
; count start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
MOV	R3, R0
MOV	R4, R1
; count end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 12 (R3)
; count start address is: 16 (R4)
;bigi.c,382 :: 		if (count == 0)
CMP	R4, #0
IT	NE
BNE	L_bigi_shift_right56
; A end address is: 12 (R3)
; count end address is: 16 (R4)
;bigi.c,383 :: 		return OK;
IT	AL
BAL	L_end_bigi_shift_right
L_bigi_shift_right56:
;bigi.c,386 :: 		if (count >= (A->wordlen + MULT_BUFFER_WORDLEN) * MACHINE_WORD_BITLEN)
; count start address is: 16 (R4)
; A start address is: 12 (R3)
ADDS	R2, R3, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
LSLS	R2, R2, #5
CMP	R4, R2
IT	CC
BCC	L_bigi_shift_right57
; count end address is: 16 (R4)
;bigi.c,387 :: 		BIGI_RETURN(bigi_set_zero(A))
MOV	R0, R3
; A end address is: 12 (R3)
BL	_bigi_set_zero+0
IT	AL
BAL	L_end_bigi_shift_right
L_bigi_shift_right57:
;bigi.c,390 :: 		bigs = count / MACHINE_WORD_BITLEN;
; count start address is: 16 (R4)
; A start address is: 12 (R3)
LSRS	R2, R4, #5
; bigs start address is: 0 (R0)
MOV	R0, R2
;bigi.c,391 :: 		if (bigs > 0)
CMP	R2, #0
IT	LS
BLS	L__bigi_shift_right314
;bigi.c,394 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN - bigs; i++)
; i start address is: 4 (R1)
MOVS	R1, #0
; A end address is: 12 (R3)
; count end address is: 16 (R4)
; bigs end address is: 0 (R0)
; i end address is: 4 (R1)
MOV	R6, R3
MOV	R5, R4
L_bigi_shift_right59:
; i start address is: 4 (R1)
; bigs start address is: 0 (R0)
; count start address is: 20 (R5)
; A start address is: 24 (R6)
ADDS	R2, R6, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
SUB	R2, R2, R0
CMP	R1, R2
IT	CS
BCS	L_bigi_shift_right60
;bigi.c,396 :: 		A->ary[i] = A->ary[i+bigs];
LDR	R4, [R6, #0]
LSLS	R2, R1, #2
ADDS	R3, R4, R2
ADDS	R2, R1, R0
LSLS	R2, R2, #2
ADDS	R2, R4, R2
LDR	R2, [R2, #0]
STR	R2, [R3, #0]
;bigi.c,394 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN - bigs; i++)
ADDS	R1, R1, #1
;bigi.c,397 :: 		}
; i end address is: 4 (R1)
IT	AL
BAL	L_bigi_shift_right59
L_bigi_shift_right60:
;bigi.c,398 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - bigs; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R2, R6, #4
LDR	R2, [R2, #0]
ADDS	R1, R2, #3
SUB	R1, R1, R0
; i start address is: 4 (R1)
; A end address is: 24 (R6)
; count end address is: 20 (R5)
; bigs end address is: 0 (R0)
; i end address is: 4 (R1)
MOV	R4, R6
L_bigi_shift_right62:
; i start address is: 4 (R1)
; A start address is: 16 (R4)
; count start address is: 20 (R5)
; bigs start address is: 0 (R0)
ADDS	R2, R4, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R1, R2
IT	CS
BCS	L_bigi_shift_right63
;bigi.c,400 :: 		A->ary[i] = 0u;
LDR	R3, [R4, #0]
LSLS	R2, R1, #2
ADDS	R3, R3, R2
MOVS	R2, #0
STR	R2, [R3, #0]
;bigi.c,398 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - bigs; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R1, R1, #1
;bigi.c,401 :: 		}
; i end address is: 4 (R1)
IT	AL
BAL	L_bigi_shift_right62
L_bigi_shift_right63:
;bigi.c,402 :: 		}
STR	R0, [SP, #4]
; count end address is: 20 (R5)
; bigs end address is: 0 (R0)
MOV	R1, R4
MOV	R0, R5
LDR	R5, [SP, #4]
IT	AL
BAL	L_bigi_shift_right58
; A end address is: 16 (R4)
L__bigi_shift_right314:
;bigi.c,391 :: 		if (bigs > 0)
MOV	R5, R0
MOV	R0, R4
MOV	R1, R3
;bigi.c,402 :: 		}
L_bigi_shift_right58:
;bigi.c,405 :: 		smalls = count % MACHINE_WORD_BITLEN;
; bigs start address is: 20 (R5)
; count start address is: 0 (R0)
; A start address is: 4 (R1)
AND	R2, R0, #31
; count end address is: 0 (R0)
; smalls start address is: 0 (R0)
MOV	R0, R2
;bigi.c,406 :: 		if (smalls == 0)
CMP	R2, #0
IT	NE
BNE	L_bigi_shift_right65
; smalls end address is: 0 (R0)
; A end address is: 4 (R1)
; bigs end address is: 20 (R5)
;bigi.c,407 :: 		return OK;
IT	AL
BAL	L_end_bigi_shift_right
L_bigi_shift_right65:
;bigi.c,409 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN - bigs - 1; i++)
; i start address is: 24 (R6)
; bigs start address is: 20 (R5)
; A start address is: 4 (R1)
; smalls start address is: 0 (R0)
MOVS	R6, #0
; smalls end address is: 0 (R0)
; A end address is: 4 (R1)
; bigs end address is: 20 (R5)
; i end address is: 24 (R6)
L_bigi_shift_right66:
; i start address is: 24 (R6)
; smalls start address is: 0 (R0)
; A start address is: 4 (R1)
; bigs start address is: 20 (R5)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
SUB	R2, R2, R5
SUBS	R2, R2, #1
CMP	R6, R2
IT	CS
BCS	L_bigi_shift_right67
;bigi.c,411 :: 		A->ary[i] >>= smalls;
LDR	R3, [R1, #0]
LSLS	R2, R6, #2
ADDS	R3, R3, R2
LDR	R2, [R3, #0]
LSRS	R2, R0
STR	R2, [R3, #0]
;bigi.c,412 :: 		A->ary[i] |= (A->ary[i+1] << (MACHINE_WORD_BITLEN - smalls));
LDR	R3, [R1, #0]
LSLS	R2, R6, #2
ADDS	R4, R3, R2
ADDS	R2, R6, #1
LSLS	R2, R2, #2
ADDS	R2, R3, R2
LDR	R3, [R2, #0]
RSB	R2, R0, #32
LSLS	R3, R2
LDR	R2, [R4, #0]
ORRS	R2, R3
STR	R2, [R4, #0]
;bigi.c,409 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN - bigs - 1; i++)
ADDS	R6, R6, #1
;bigi.c,413 :: 		}
; i end address is: 24 (R6)
IT	AL
BAL	L_bigi_shift_right66
L_bigi_shift_right67:
;bigi.c,414 :: 		A->ary[A->wordlen + MULT_BUFFER_WORDLEN - bigs - 1] >>= smalls;
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
SUB	R2, R2, R5
; bigs end address is: 20 (R5)
SUBS	R2, R2, #1
LDR	R3, [R1, #0]
; A end address is: 4 (R1)
LSLS	R2, R2, #2
ADDS	R3, R3, R2
LDR	R2, [R3, #0]
LSRS	R2, R0
; smalls end address is: 0 (R0)
STR	R2, [R3, #0]
;bigi.c,416 :: 		return OK;
;bigi.c,417 :: 		}
L_end_bigi_shift_right:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _bigi_shift_right
_bigi_and:
;bigi.c,421 :: 		bigi_t *const RES)
; RES start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; RES end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; B start address is: 4 (R1)
; RES start address is: 8 (R2)
;bigi.c,432 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 28 (R7)
MOVS	R7, #0
; A end address is: 0 (R0)
; RES end address is: 8 (R2)
; i end address is: 28 (R7)
STR	R2, [SP, #0]
MOV	R2, R0
LDR	R0, [SP, #0]
L_bigi_and69:
; i start address is: 28 (R7)
; RES start address is: 0 (R0)
; B start address is: 4 (R1)
; B end address is: 4 (R1)
; A start address is: 8 (R2)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R7, R3
IT	CS
BCS	L_bigi_and70
; B end address is: 4 (R1)
;bigi.c,433 :: 		RES->ary[i] = A->ary[i] & B->ary[i];
; B start address is: 4 (R1)
LDR	R3, [R0, #0]
LSLS	R6, R7, #2
ADDS	R5, R3, R6
LDR	R3, [R2, #0]
ADDS	R3, R3, R6
LDR	R4, [R3, #0]
LDR	R3, [R1, #0]
ADDS	R3, R3, R6
LDR	R3, [R3, #0]
AND	R3, R4, R3, LSL #0
STR	R3, [R5, #0]
;bigi.c,432 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R7, R7, #1
;bigi.c,433 :: 		RES->ary[i] = A->ary[i] & B->ary[i];
; B end address is: 4 (R1)
; i end address is: 28 (R7)
IT	AL
BAL	L_bigi_and69
L_bigi_and70:
;bigi.c,434 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R3, R2, #4
; A end address is: 8 (R2)
LDR	R3, [R3, #0]
ADDS	R1, R3, #3
; i start address is: 4 (R1)
; RES end address is: 0 (R0)
; i end address is: 4 (R1)
L_bigi_and72:
; i start address is: 4 (R1)
; RES start address is: 0 (R0)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R1, R3
IT	CS
BCS	L_bigi_and73
;bigi.c,435 :: 		RES->ary[i] = 0u;
LDR	R4, [R0, #0]
LSLS	R3, R1, #2
ADDS	R4, R4, R3
MOVS	R3, #0
STR	R3, [R4, #0]
;bigi.c,434 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R1, R1, #1
;bigi.c,435 :: 		RES->ary[i] = 0u;
; RES end address is: 0 (R0)
; i end address is: 4 (R1)
IT	AL
BAL	L_bigi_and72
L_bigi_and73:
;bigi.c,437 :: 		return OK;
;bigi.c,438 :: 		}
L_end_bigi_and:
ADD	SP, SP, #4
BX	LR
; end of _bigi_and
_bigi_xor:
;bigi.c,442 :: 		bigi_t *const RES)
; RES start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; RES end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; B start address is: 4 (R1)
; RES start address is: 8 (R2)
;bigi.c,453 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 28 (R7)
MOVS	R7, #0
; A end address is: 0 (R0)
; RES end address is: 8 (R2)
; i end address is: 28 (R7)
STR	R2, [SP, #0]
MOV	R2, R0
LDR	R0, [SP, #0]
L_bigi_xor75:
; i start address is: 28 (R7)
; RES start address is: 0 (R0)
; B start address is: 4 (R1)
; B end address is: 4 (R1)
; A start address is: 8 (R2)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R7, R3
IT	CS
BCS	L_bigi_xor76
; B end address is: 4 (R1)
;bigi.c,454 :: 		RES->ary[i] = A->ary[i] ^ B->ary[i];
; B start address is: 4 (R1)
LDR	R3, [R0, #0]
LSLS	R6, R7, #2
ADDS	R5, R3, R6
LDR	R3, [R2, #0]
ADDS	R3, R3, R6
LDR	R4, [R3, #0]
LDR	R3, [R1, #0]
ADDS	R3, R3, R6
LDR	R3, [R3, #0]
EOR	R3, R4, R3, LSL #0
STR	R3, [R5, #0]
;bigi.c,453 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R7, R7, #1
;bigi.c,454 :: 		RES->ary[i] = A->ary[i] ^ B->ary[i];
; B end address is: 4 (R1)
; i end address is: 28 (R7)
IT	AL
BAL	L_bigi_xor75
L_bigi_xor76:
;bigi.c,455 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R3, R2, #4
; A end address is: 8 (R2)
LDR	R3, [R3, #0]
ADDS	R1, R3, #3
; i start address is: 4 (R1)
; RES end address is: 0 (R0)
; i end address is: 4 (R1)
L_bigi_xor78:
; i start address is: 4 (R1)
; RES start address is: 0 (R0)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R1, R3
IT	CS
BCS	L_bigi_xor79
;bigi.c,456 :: 		RES->ary[i] = 0u;
LDR	R4, [R0, #0]
LSLS	R3, R1, #2
ADDS	R4, R4, R3
MOVS	R3, #0
STR	R3, [R4, #0]
;bigi.c,455 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R1, R1, #1
;bigi.c,456 :: 		RES->ary[i] = 0u;
; RES end address is: 0 (R0)
; i end address is: 4 (R1)
IT	AL
BAL	L_bigi_xor78
L_bigi_xor79:
;bigi.c,458 :: 		return OK;
;bigi.c,459 :: 		}
L_end_bigi_xor:
ADD	SP, SP, #4
BX	LR
; end of _bigi_xor
_bigi_add:
;bigi.c,463 :: 		bigi_t *const RES)
; RES start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
MOV	R5, R1
MOV	R1, R0
; RES end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 4 (R1)
; B start address is: 20 (R5)
; RES start address is: 8 (R2)
;bigi.c,466 :: 		byte_t carry = 0u;
; carry start address is: 0 (R0)
MOVS	R0, #0
;bigi.c,481 :: 		shorter = (A->wordlen < B->wordlen) ? A : B;
ADDS	R3, R1, #4
LDR	R4, [R3, #0]
ADDS	R3, R5, #4
LDR	R3, [R3, #0]
CMP	R4, R3
IT	CS
BCS	L_bigi_add81
; ?FLOC___bigi_add?T665 start address is: 16 (R4)
MOV	R4, R1
; ?FLOC___bigi_add?T665 end address is: 16 (R4)
IT	AL
BAL	L_bigi_add82
L_bigi_add81:
; ?FLOC___bigi_add?T665 start address is: 16 (R4)
MOV	R4, R5
; ?FLOC___bigi_add?T665 end address is: 16 (R4)
L_bigi_add82:
; ?FLOC___bigi_add?T665 start address is: 16 (R4)
; shorter start address is: 12 (R3)
MOV	R3, R4
;bigi.c,482 :: 		longer = (shorter == A) ? B : A;
CMP	R4, R1
IT	NE
BNE	L_bigi_add83
; A end address is: 4 (R1)
; ?FLOC___bigi_add?T665 end address is: 16 (R4)
; ?FLOC___bigi_add?T667 start address is: 4 (R1)
MOV	R1, R5
; B end address is: 20 (R5)
MOV	R4, R1
; ?FLOC___bigi_add?T667 end address is: 4 (R1)
IT	AL
BAL	L_bigi_add84
L_bigi_add83:
; ?FLOC___bigi_add?T667 start address is: 16 (R4)
; A start address is: 4 (R1)
MOV	R4, R1
; A end address is: 4 (R1)
; ?FLOC___bigi_add?T667 end address is: 16 (R4)
L_bigi_add84:
; ?FLOC___bigi_add?T667 start address is: 16 (R4)
; longer start address is: 4 (R1)
MOV	R1, R4
; ?FLOC___bigi_add?T667 end address is: 16 (R4)
;bigi.c,484 :: 		for (i = 0; i < shorter->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 44 (R11)
MOVW	R11, #0
; carry end address is: 0 (R0)
; shorter end address is: 12 (R3)
; i end address is: 44 (R11)
; longer end address is: 4 (R1)
; RES end address is: 8 (R2)
UXTB	R7, R0
MOV	R0, R1
MOV	R1, R3
L_bigi_add85:
; i start address is: 44 (R11)
; longer start address is: 0 (R0)
; shorter start address is: 4 (R1)
; carry start address is: 28 (R7)
; RES start address is: 8 (R2)
ADDS	R3, R1, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R11, R3
IT	CS
BCS	L_bigi_add86
;bigi.c,486 :: 		tmp = (dword_t)(longer->ary[i]) + shorter->ary[i] + carry;
LDR	R3, [R0, #0]
LSL	R10, R11, #2
ADD	R3, R3, R10, LSL #0
LDR	R3, [R3, #0]
MOV	R5, R3
MOVS	R6, #0
LDR	R3, [R1, #0]
ADD	R3, R3, R10, LSL #0
LDR	R3, [R3, #0]
MOVS	R4, #0
ADDS	R8, R5, R3, LSL #0
ADC	R9, R6, R4, LSL #0
UXTB	R3, R7
MOVS	R4, #0
; carry end address is: 28 (R7)
ADDS	R6, R8, R3, LSL #0
ADC	R7, R9, R4, LSL #0
;bigi.c,487 :: 		carry = (tmp > MAX_VAL); /* TODO: check if this is MISRA-OK, or use ternary: (...) ? 1u : 0u; */
MOV	R3, #-1
MOVS	R4, #0
SUBS	R3, R3, R6
SBCS	R3, R4, R7, LSL #0
MOVW	R3, #0
MOVW	R4, #0
BCS	L__bigi_add379
MOVS	R3, #1
L__bigi_add379:
; carry start address is: 32 (R8)
UXTB	R8, R3
;bigi.c,490 :: 		tmp = tmp & MAX_VAL;
AND	R4, R6, #-1
AND	R5, R7, #0
;bigi.c,491 :: 		RES->ary[i] = (word_t)(tmp);
LDR	R3, [R2, #0]
ADD	R3, R3, R10, LSL #0
STR	R4, [R3, #0]
;bigi.c,484 :: 		for (i = 0; i < shorter->wordlen + MULT_BUFFER_WORDLEN; i++)
ADD	R11, R11, #1
;bigi.c,492 :: 		}
; shorter end address is: 4 (R1)
; carry end address is: 32 (R8)
UXTB	R7, R8
IT	AL
BAL	L_bigi_add85
L_bigi_add86:
;bigi.c,493 :: 		for (/* i = shorter->wordlen + MULT_BUFFER_WORDLEN */; i < longer->wordlen + MULT_BUFFER_WORDLEN; i++)
; carry start address is: 28 (R7)
MOV	R1, R0
; i end address is: 44 (R11)
; longer end address is: 0 (R0)
; RES end address is: 8 (R2)
MOV	R0, R2
MOV	R2, R11
UXTB	R5, R7
L_bigi_add88:
; carry end address is: 28 (R7)
; RES start address is: 0 (R0)
; carry start address is: 20 (R5)
; longer start address is: 4 (R1)
; i start address is: 8 (R2)
ADDS	R3, R1, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R2, R3
IT	CS
BCS	L_bigi_add89
;bigi.c,495 :: 		tmp = (dword_t)(longer->ary[i]) + carry;
LDR	R3, [R1, #0]
LSL	R10, R2, #2
ADD	R3, R3, R10, LSL #0
LDR	R3, [R3, #0]
MOV	R8, R3
MOVW	R9, #0
UXTB	R3, R5
MOVS	R4, #0
; carry end address is: 20 (R5)
ADDS	R6, R8, R3, LSL #0
ADC	R7, R9, R4, LSL #0
;bigi.c,496 :: 		carry = (tmp > MAX_VAL); /* TODO: check if this is MISRA-OK, or use ternary: (...) ? 1u : 0u; */
MOV	R3, #-1
MOVS	R4, #0
SUBS	R3, R3, R6
SBCS	R3, R4, R7, LSL #0
MOVW	R3, #0
MOVW	R4, #0
BCS	L__bigi_add380
MOVS	R3, #1
L__bigi_add380:
; carry start address is: 32 (R8)
UXTB	R8, R3
;bigi.c,499 :: 		tmp = tmp & MAX_VAL;
AND	R4, R6, #-1
AND	R5, R7, #0
;bigi.c,500 :: 		RES->ary[i] = (word_t)(tmp);
LDR	R3, [R0, #0]
ADD	R3, R3, R10, LSL #0
STR	R4, [R3, #0]
;bigi.c,493 :: 		for (/* i = shorter->wordlen + MULT_BUFFER_WORDLEN */; i < longer->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R2, R2, #1
;bigi.c,501 :: 		}
; carry end address is: 32 (R8)
UXTB	R5, R8
IT	AL
BAL	L_bigi_add88
L_bigi_add89:
;bigi.c,503 :: 		for (/* i = longer->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
; carry start address is: 20 (R5)
STR	R1, [SP, #0]
; carry end address is: 20 (R5)
; longer end address is: 4 (R1)
; i end address is: 8 (R2)
UXTB	R1, R5
MOV	R5, R2
MOV	R2, R0
LDR	R0, [SP, #0]
L_bigi_add91:
; RES end address is: 0 (R0)
; i start address is: 20 (R5)
; longer start address is: 0 (R0)
; carry start address is: 4 (R1)
; RES start address is: 8 (R2)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R5, R3
IT	CS
BCS	L_bigi_add92
;bigi.c,504 :: 		RES->ary[i] = 0u;
LDR	R4, [R2, #0]
LSLS	R3, R5, #2
ADDS	R4, R4, R3
MOVS	R3, #0
STR	R3, [R4, #0]
;bigi.c,503 :: 		for (/* i = longer->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R5, R5, #1
;bigi.c,504 :: 		RES->ary[i] = 0u;
; i end address is: 20 (R5)
IT	AL
BAL	L_bigi_add91
L_bigi_add92:
;bigi.c,506 :: 		if (carry)
CMP	R1, #0
IT	EQ
BEQ	L_bigi_add94
;bigi.c,508 :: 		if (RES->wordlen == longer->wordlen)
ADDS	R3, R2, #4
LDR	R4, [R3, #0]
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
CMP	R4, R3
IT	NE
BNE	L_bigi_add95
; longer end address is: 0 (R0)
; carry end address is: 4 (R1)
; RES end address is: 8 (R2)
;bigi.c,509 :: 		return BIGI_OVERFLOW;
IT	AL
BAL	L_end_bigi_add
L_bigi_add95:
;bigi.c,511 :: 		RES->ary[longer->wordlen + MULT_BUFFER_WORDLEN] = carry;
; RES start address is: 8 (R2)
; carry start address is: 4 (R1)
; longer start address is: 0 (R0)
ADDS	R3, R0, #4
; longer end address is: 0 (R0)
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
LDR	R4, [R2, #0]
; RES end address is: 8 (R2)
LSLS	R3, R3, #2
ADDS	R3, R4, R3
STR	R1, [R3, #0]
; carry end address is: 4 (R1)
;bigi.c,512 :: 		}
L_bigi_add94:
;bigi.c,514 :: 		return OK;
;bigi.c,515 :: 		}
L_end_bigi_add:
ADD	SP, SP, #4
BX	LR
; end of _bigi_add
_bigi_add_one:
;bigi.c,518 :: 		bigi_t *const RES)
; RES start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; RES end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; RES start address is: 4 (R1)
;bigi.c,521 :: 		byte_t carry = 1u;
; carry start address is: 16 (R4)
MOVS	R4, #1
;bigi.c,532 :: 		for (i = 0; (carry > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
; i start address is: 40 (R10)
MOVW	R10, #0
; carry end address is: 16 (R4)
; i end address is: 40 (R10)
; A end address is: 0 (R0)
; RES end address is: 4 (R1)
STR	R1, [SP, #0]
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_add_one97:
; i start address is: 40 (R10)
; carry start address is: 16 (R4)
; RES start address is: 0 (R0)
; A start address is: 4 (R1)
CMP	R4, #0
IT	LS
BLS	L__bigi_add_one317
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R10, R2
IT	CS
BCS	L__bigi_add_one316
L__bigi_add_one315:
;bigi.c,534 :: 		tmp = (dword_t)(A->ary[i]) + carry;
LDR	R2, [R1, #0]
LSL	R9, R10, #2
ADD	R2, R2, R9, LSL #0
LDR	R2, [R2, #0]
MOV	R7, R2
MOVW	R8, #0
UXTB	R2, R4
MOVS	R3, #0
; carry end address is: 16 (R4)
ADDS	R5, R7, R2, LSL #0
ADC	R6, R8, R3, LSL #0
;bigi.c,535 :: 		carry = (tmp > MAX_VAL);
MOV	R2, #-1
MOVS	R3, #0
SUBS	R2, R2, R5
SBCS	R2, R3, R6, LSL #0
MOVW	R2, #0
MOVW	R3, #0
BCS	L__bigi_add_one382
MOVS	R2, #1
L__bigi_add_one382:
; carry start address is: 28 (R7)
UXTB	R7, R2
;bigi.c,538 :: 		tmp = tmp & MAX_VAL;
AND	R3, R5, #-1
AND	R4, R6, #0
;bigi.c,539 :: 		RES->ary[i] = (word_t)(tmp);
LDR	R2, [R0, #0]
ADD	R2, R2, R9, LSL #0
STR	R3, [R2, #0]
;bigi.c,532 :: 		for (i = 0; (carry > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
ADD	R10, R10, #1
;bigi.c,540 :: 		}
; carry end address is: 28 (R7)
UXTB	R4, R7
IT	AL
BAL	L_bigi_add_one97
;bigi.c,532 :: 		for (i = 0; (carry > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
L__bigi_add_one317:
; carry start address is: 16 (R4)
L__bigi_add_one316:
;bigi.c,541 :: 		for (; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
STR	R1, [SP, #0]
; RES end address is: 0 (R0)
; A end address is: 4 (R1)
; i end address is: 40 (R10)
MOV	R6, R10
UXTB	R5, R4
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_add_one102:
; carry end address is: 16 (R4)
; A start address is: 0 (R0)
; RES start address is: 4 (R1)
; carry start address is: 20 (R5)
; i start address is: 24 (R6)
ADDS	R2, R0, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R6, R2
IT	CS
BCS	L_bigi_add_one103
;bigi.c,542 :: 		RES->ary[i] = A->ary[i];
LDR	R2, [R1, #0]
LSLS	R4, R6, #2
ADDS	R3, R2, R4
LDR	R2, [R0, #0]
ADDS	R2, R2, R4
LDR	R2, [R2, #0]
STR	R2, [R3, #0]
;bigi.c,541 :: 		for (; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R6, R6, #1
;bigi.c,542 :: 		RES->ary[i] = A->ary[i];
IT	AL
BAL	L_bigi_add_one102
L_bigi_add_one103:
;bigi.c,544 :: 		for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
MOV	R4, R0
; carry end address is: 20 (R5)
; i end address is: 24 (R6)
; RES end address is: 4 (R1)
UXTB	R0, R5
MOV	R5, R6
L_bigi_add_one105:
; A end address is: 0 (R0)
; i start address is: 20 (R5)
; carry start address is: 0 (R0)
; RES start address is: 4 (R1)
; A start address is: 16 (R4)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R5, R2
IT	CS
BCS	L_bigi_add_one106
;bigi.c,545 :: 		RES->ary[i] = 0u;
LDR	R3, [R1, #0]
LSLS	R2, R5, #2
ADDS	R3, R3, R2
MOVS	R2, #0
STR	R2, [R3, #0]
;bigi.c,544 :: 		for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R5, R5, #1
;bigi.c,545 :: 		RES->ary[i] = 0u;
; i end address is: 20 (R5)
IT	AL
BAL	L_bigi_add_one105
L_bigi_add_one106:
;bigi.c,547 :: 		if (carry)
CMP	R0, #0
IT	EQ
BEQ	L_bigi_add_one108
;bigi.c,549 :: 		if (RES->wordlen == A->wordlen)
ADDS	R2, R1, #4
LDR	R3, [R2, #0]
ADDS	R2, R4, #4
LDR	R2, [R2, #0]
CMP	R3, R2
IT	NE
BNE	L_bigi_add_one109
; carry end address is: 0 (R0)
; RES end address is: 4 (R1)
; A end address is: 16 (R4)
;bigi.c,550 :: 		return BIGI_OVERFLOW;
IT	AL
BAL	L_end_bigi_add_one
L_bigi_add_one109:
;bigi.c,552 :: 		RES->ary[A->wordlen + MULT_BUFFER_WORDLEN] = carry;
; A start address is: 16 (R4)
; RES start address is: 4 (R1)
; carry start address is: 0 (R0)
ADDS	R2, R4, #4
; A end address is: 16 (R4)
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
LDR	R3, [R1, #0]
; RES end address is: 4 (R1)
LSLS	R2, R2, #2
ADDS	R2, R3, R2
STR	R0, [R2, #0]
; carry end address is: 0 (R0)
;bigi.c,553 :: 		}
L_bigi_add_one108:
;bigi.c,555 :: 		return OK;
;bigi.c,556 :: 		}
L_end_bigi_add_one:
ADD	SP, SP, #4
BX	LR
; end of _bigi_add_one
_bigi_sub:
;bigi.c,560 :: 		bigi_t *const RES)
; RES start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; RES end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; B start address is: 4 (R1)
; RES start address is: 8 (R2)
;bigi.c,563 :: 		word_t borrow = 0u;
; borrow start address is: 28 (R7)
MOV	R7, #0
;bigi.c,573 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
; i start address is: 44 (R11)
MOVW	R11, #0
; A end address is: 0 (R0)
; RES end address is: 8 (R2)
; borrow end address is: 28 (R7)
; i end address is: 44 (R11)
STR	R2, [SP, #0]
MOV	R2, R0
LDR	R0, [SP, #0]
L_bigi_sub111:
; i start address is: 44 (R11)
; borrow start address is: 28 (R7)
; RES start address is: 0 (R0)
; B start address is: 4 (R1)
; B end address is: 4 (R1)
; A start address is: 8 (R2)
ADDS	R3, R2, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R11, R3
IT	CS
BCS	L_bigi_sub112
; B end address is: 4 (R1)
;bigi.c,575 :: 		r = (dword_t)(A->ary[i]) + MAX_VAL + 1 - B->ary[i] - borrow;
; B start address is: 4 (R1)
LDR	R3, [R2, #0]
LSL	R10, R11, #2
ADD	R3, R3, R10, LSL #0
LDR	R3, [R3, #0]
MOVS	R4, #0
ADDS	R3, R3, #-1
ADC	R4, R4, #0
ADDS	R5, R3, #1
ADC	R6, R4, #0
LDR	R3, [R1, #0]
ADD	R3, R3, R10, LSL #0
LDR	R3, [R3, #0]
MOVS	R4, #0
SUBS	R8, R5, R3, LSL #0
SBC	R9, R6, R4, LSL #0
MOV	R3, R7
MOVS	R4, #0
; borrow end address is: 28 (R7)
SUBS	R6, R8, R3, LSL #0
SBC	R7, R9, R4, LSL #0
;bigi.c,576 :: 		borrow = (r <= MAX_VAL);
MOV	R3, #-1
MOVS	R4, #0
SUBS	R3, R3, R6
SBCS	R3, R4, R7, LSL #0
MOVW	R4, #0
MOVW	R3, #0
BCC	L__bigi_sub384
MOVS	R3, #1
L__bigi_sub384:
; borrow start address is: 32 (R8)
MOV	R8, R3
;bigi.c,579 :: 		r = r & MAX_VAL;
AND	R4, R6, #-1
AND	R5, R7, #0
;bigi.c,580 :: 		RES->ary[i] = (word_t)(r);
LDR	R3, [R0, #0]
ADD	R3, R3, R10, LSL #0
STR	R4, [R3, #0]
;bigi.c,573 :: 		for (i = 0; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADD	R11, R11, #1
;bigi.c,581 :: 		}
; B end address is: 4 (R1)
; A end address is: 8 (R2)
; borrow end address is: 32 (R8)
MOV	R7, R8
IT	AL
BAL	L_bigi_sub111
L_bigi_sub112:
;bigi.c,583 :: 		for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
; borrow start address is: 28 (R7)
MOV	R2, R11
; i end address is: 44 (R11)
; RES end address is: 0 (R0)
MOV	R1, R7
L_bigi_sub114:
; borrow end address is: 28 (R7)
; RES start address is: 0 (R0)
; borrow start address is: 4 (R1)
; i start address is: 8 (R2)
ADDS	R3, R0, #4
LDR	R3, [R3, #0]
ADDS	R3, R3, #3
CMP	R2, R3
IT	CS
BCS	L_bigi_sub115
;bigi.c,584 :: 		RES->ary[i] = 0u;
LDR	R4, [R0, #0]
LSLS	R3, R2, #2
ADDS	R4, R4, R3
MOVS	R3, #0
STR	R3, [R4, #0]
;bigi.c,583 :: 		for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R2, R2, #1
;bigi.c,584 :: 		RES->ary[i] = 0u;
; RES end address is: 0 (R0)
; i end address is: 8 (R2)
IT	AL
BAL	L_bigi_sub114
L_bigi_sub115:
;bigi.c,586 :: 		if (borrow)
CMP	R1, #0
IT	EQ
BEQ	L_bigi_sub117
; borrow end address is: 4 (R1)
;bigi.c,587 :: 		return BIGI_OVERFLOW;
IT	AL
BAL	L_end_bigi_sub
L_bigi_sub117:
;bigi.c,589 :: 		return OK;
;bigi.c,590 :: 		}
L_end_bigi_sub:
ADD	SP, SP, #4
BX	LR
; end of _bigi_sub
_bigi_sub_one:
;bigi.c,593 :: 		bigi_t *const RES)
; RES start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
; RES end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; RES start address is: 4 (R1)
;bigi.c,596 :: 		word_t borrow = 1u;
; borrow start address is: 16 (R4)
MOV	R4, #1
;bigi.c,607 :: 		for (i = 0; (borrow > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
; i start address is: 40 (R10)
MOVW	R10, #0
; borrow end address is: 16 (R4)
; i end address is: 40 (R10)
; A end address is: 0 (R0)
; RES end address is: 4 (R1)
STR	R1, [SP, #0]
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_sub_one118:
; i start address is: 40 (R10)
; borrow start address is: 16 (R4)
; RES start address is: 0 (R0)
; A start address is: 4 (R1)
CMP	R4, #0
IT	LS
BLS	L__bigi_sub_one320
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R10, R2
IT	CS
BCS	L__bigi_sub_one319
L__bigi_sub_one318:
;bigi.c,609 :: 		r = (dword_t)(A->ary[i]) + MAX_VAL + 1 - borrow;
LDR	R2, [R1, #0]
LSL	R9, R10, #2
ADD	R2, R2, R9, LSL #0
LDR	R2, [R2, #0]
MOVS	R3, #0
ADDS	R2, R2, #-1
ADC	R3, R3, #0
ADDS	R7, R2, #1
ADC	R8, R3, #0
MOV	R2, R4
MOVS	R3, #0
; borrow end address is: 16 (R4)
SUBS	R5, R7, R2, LSL #0
SBC	R6, R8, R3, LSL #0
;bigi.c,610 :: 		borrow = (r <= MAX_VAL);
MOV	R2, #-1
MOVS	R3, #0
SUBS	R2, R2, R5
SBCS	R2, R3, R6, LSL #0
MOVW	R3, #0
MOVW	R2, #0
BCC	L__bigi_sub_one386
MOVS	R2, #1
L__bigi_sub_one386:
; borrow start address is: 28 (R7)
MOV	R7, R2
;bigi.c,613 :: 		r = r & MAX_VAL;
AND	R3, R5, #-1
AND	R4, R6, #0
;bigi.c,614 :: 		RES->ary[i] = (word_t)(r);
LDR	R2, [R0, #0]
ADD	R2, R2, R9, LSL #0
STR	R3, [R2, #0]
;bigi.c,607 :: 		for (i = 0; (borrow > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
ADD	R10, R10, #1
;bigi.c,615 :: 		}
; borrow end address is: 28 (R7)
MOV	R4, R7
IT	AL
BAL	L_bigi_sub_one118
;bigi.c,607 :: 		for (i = 0; (borrow > 0) && (i < A->wordlen + MULT_BUFFER_WORDLEN); i++)
L__bigi_sub_one320:
; borrow start address is: 16 (R4)
L__bigi_sub_one319:
;bigi.c,616 :: 		for (; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
STR	R1, [SP, #0]
; RES end address is: 0 (R0)
; A end address is: 4 (R1)
; i end address is: 40 (R10)
MOV	R6, R10
MOV	R5, R4
MOV	R1, R0
LDR	R0, [SP, #0]
L_bigi_sub_one123:
; borrow end address is: 16 (R4)
; A start address is: 0 (R0)
; RES start address is: 4 (R1)
; borrow start address is: 20 (R5)
; i start address is: 24 (R6)
ADDS	R2, R0, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R6, R2
IT	CS
BCS	L_bigi_sub_one124
;bigi.c,617 :: 		RES->ary[i] = A->ary[i];
LDR	R2, [R1, #0]
LSLS	R4, R6, #2
ADDS	R3, R2, R4
LDR	R2, [R0, #0]
ADDS	R2, R2, R4
LDR	R2, [R2, #0]
STR	R2, [R3, #0]
;bigi.c,616 :: 		for (; i < A->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R6, R6, #1
;bigi.c,617 :: 		RES->ary[i] = A->ary[i];
; A end address is: 0 (R0)
IT	AL
BAL	L_bigi_sub_one123
L_bigi_sub_one124:
;bigi.c,619 :: 		for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
; i end address is: 24 (R6)
; RES end address is: 4 (R1)
MOV	R0, R5
MOV	R4, R6
L_bigi_sub_one126:
; borrow end address is: 20 (R5)
; i start address is: 16 (R4)
; borrow start address is: 0 (R0)
; RES start address is: 4 (R1)
ADDS	R2, R1, #4
LDR	R2, [R2, #0]
ADDS	R2, R2, #3
CMP	R4, R2
IT	CS
BCS	L_bigi_sub_one127
;bigi.c,620 :: 		RES->ary[i] = 0u;
LDR	R3, [R1, #0]
LSLS	R2, R4, #2
ADDS	R3, R3, R2
MOVS	R2, #0
STR	R2, [R3, #0]
;bigi.c,619 :: 		for (/* i = A->wordlen + MULT_BUFFER_WORDLEN */; i < RES->wordlen + MULT_BUFFER_WORDLEN; i++)
ADDS	R4, R4, #1
;bigi.c,620 :: 		RES->ary[i] = 0u;
; RES end address is: 4 (R1)
; i end address is: 16 (R4)
IT	AL
BAL	L_bigi_sub_one126
L_bigi_sub_one127:
;bigi.c,622 :: 		if (borrow)
CMP	R0, #0
IT	EQ
BEQ	L_bigi_sub_one129
; borrow end address is: 0 (R0)
;bigi.c,623 :: 		return BIGI_OVERFLOW;
IT	AL
BAL	L_end_bigi_sub_one
L_bigi_sub_one129:
;bigi.c,625 :: 		return OK;
;bigi.c,626 :: 		}
L_end_bigi_sub_one:
ADD	SP, SP, #4
BX	LR
; end of _bigi_sub_one
_bigi_mod_red:
;bigi.c,632 :: 		bigi_t *const RES)
; tmparylen start address is: 12 (R3)
; TMPARY start address is: 8 (R2)
; MOD start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; tmparylen end address is: 12 (R3)
; TMPARY end address is: 8 (R2)
; MOD end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; MOD start address is: 4 (R1)
; TMPARY start address is: 8 (R2)
; tmparylen start address is: 12 (R3)
; RES start address is: 28 (R7)
LDR	R7, [SP, #4]
;bigi.c,644 :: 		TMPARY[0].domain = DOMAIN_NORMAL;
ADDW	R5, R2, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,645 :: 		TMPARY[0].wordlen = A->wordlen;
ADDS	R5, R2, #4
ADDS	R4, R0, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,647 :: 		BIGI_RETURN(bigi_div(A, MOD, &TMPARY[1], tmparylen - 1, &TMPARY[0], RES))
MOV	R6, R7
; RES end address is: 28 (R7)
SUBS	R5, R3, #1
; tmparylen end address is: 12 (R3)
ADDW	R4, R2, #16
PUSH	(R6)
PUSH	(R2)
MOV	R3, R5
MOV	R2, R4
; MOD end address is: 4 (R1)
; A end address is: 0 (R0)
; TMPARY end address is: 8 (R2)
BL	_bigi_div+0
ADD	SP, SP, #8
;bigi.c,648 :: 		}
L_end_bigi_mod_red:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _bigi_mod_red
_bigi_mod_red_barrett:
;bigi.c,655 :: 		bigi_t *const RES)
SUB	SP, SP, #48
STR	LR, [SP, #0]
STR	R0, [SP, #20]
STR	R1, [SP, #24]
STR	R2, [SP, #28]
STR	R3, [SP, #32]
LDR	R4, [SP, #48]
STR	R4, [SP, #48]
LDR	R4, [SP, #52]
STR	R4, [SP, #52]
;bigi.c,657 :: 		bigi_t * Q1 = NULL;
;bigi.c,658 :: 		bigi_t * Q2 = NULL;
;bigi.c,659 :: 		bigi_t * Q3 = NULL;
;bigi.c,660 :: 		bigi_t * Bk = NULL;
;bigi.c,661 :: 		bigi_t * R1 = NULL;
;bigi.c,691 :: 		for (i = 0; i < BARRETT__MIN_TMPARY_LEN; i++)
MOVS	R4, #0
STR	R4, [SP, #8]
L_bigi_mod_red_barrett130:
LDR	R4, [SP, #8]
CMP	R4, #8
IT	GE
BGE	L_bigi_mod_red_barrett131
;bigi.c,693 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LDR	R4, [SP, #8]
LSLS	R5, R4, #4
LDR	R4, [SP, #32]
ADDS	R4, R4, R5
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,694 :: 		TMPARY[i].wordlen = A->wordlen;
LDR	R4, [SP, #8]
LSLS	R5, R4, #4
LDR	R4, [SP, #32]
ADDS	R4, R4, R5
ADDS	R5, R4, #4
LDR	R4, [SP, #20]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,691 :: 		for (i = 0; i < BARRETT__MIN_TMPARY_LEN; i++)
LDR	R4, [SP, #8]
ADDS	R4, R4, #1
STR	R4, [SP, #8]
;bigi.c,695 :: 		}
IT	AL
BAL	L_bigi_mod_red_barrett130
L_bigi_mod_red_barrett131:
;bigi.c,696 :: 		RES->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #52]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,698 :: 		Q1 = &TMPARY[0];
; Q1 start address is: 48 (R12)
LDR	R12, [SP, #32]
;bigi.c,699 :: 		Q2 = &TMPARY[1];
LDR	R4, [SP, #32]
ADDS	R4, #16
STR	R4, [SP, #36]
;bigi.c,700 :: 		Q3 = &TMPARY[2];
LDR	R4, [SP, #32]
ADDS	R4, #32
STR	R4, [SP, #40]
;bigi.c,701 :: 		Bk = &TMPARY[3];
LDR	R4, [SP, #32]
ADD	R9, R4, #48
; Bk start address is: 36 (R9)
;bigi.c,702 :: 		R1 = &TMPARY[4];
LDR	R4, [SP, #32]
ADDS	R4, #64
STR	R4, [SP, #44]
;bigi.c,703 :: 		R2 = &TMPARY[5];
LDR	R4, [SP, #32]
ADDS	R4, #80
STR	R4, [SP, #4]
;bigi.c,705 :: 		BIGI_CALL(bigi_set_zero(Q1))
LDR	R0, [SP, #32]
BL	_bigi_set_zero+0
;bigi.c,706 :: 		BIGI_CALL(bigi_set_zero(Q2))
LDR	R0, [SP, #36]
BL	_bigi_set_zero+0
;bigi.c,707 :: 		BIGI_CALL(bigi_set_zero(Q3))
LDR	R0, [SP, #40]
BL	_bigi_set_zero+0
;bigi.c,708 :: 		BIGI_CALL(bigi_set_zero(Bk))
MOV	R0, R9
BL	_bigi_set_zero+0
;bigi.c,711 :: 		k = 0;
MOVS	R4, #0
STR	R4, [SP, #12]
;bigi.c,712 :: 		for (i = MOD->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #24]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #8]
; Bk end address is: 36 (R9)
; Q1 end address is: 48 (R12)
L_bigi_mod_red_barrett133:
; Bk start address is: 36 (R9)
; Q1 start address is: 48 (R12)
LDR	R4, [SP, #8]
CMP	R4, #0
IT	LT
BLT	L_bigi_mod_red_barrett134
;bigi.c,714 :: 		if (MOD->ary[i] != 0u)
LDR	R4, [SP, #24]
LDR	R5, [R4, #0]
LDR	R4, [SP, #8]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mod_red_barrett136
;bigi.c,716 :: 		k = i + 1;
LDR	R4, [SP, #8]
ADDS	R4, R4, #1
STR	R4, [SP, #12]
;bigi.c,717 :: 		break;
IT	AL
BAL	L_bigi_mod_red_barrett134
;bigi.c,718 :: 		}
L_bigi_mod_red_barrett136:
;bigi.c,712 :: 		for (i = MOD->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #8]
SUBS	R4, R4, #1
STR	R4, [SP, #8]
;bigi.c,719 :: 		}
IT	AL
BAL	L_bigi_mod_red_barrett133
L_bigi_mod_red_barrett134:
;bigi.c,720 :: 		Bk->ary[MOD->wordlen + MULT_BUFFER_WORDLEN - 1 - k] = 1u;
LDR	R4, [SP, #24]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R5, R4, #1
LDR	R4, [SP, #12]
SUB	R4, R5, R4
LDR	R5, [R9, #0]
; Bk end address is: 36 (R9)
LSLS	R4, R4, #2
ADDS	R5, R5, R4
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,722 :: 		BIGI_CALL(bigi_copy(Q1, A))
LDR	R1, [SP, #20]
MOV	R0, R12
BL	_bigi_copy+0
;bigi.c,723 :: 		BIGI_CALL(bigi_shift_left(Q1, MACHINE_WORD_BITLEN * (k-1)))
LDR	R4, [SP, #12]
SUBS	R4, R4, #1
LSLS	R4, R4, #5
MOV	R1, R4
MOV	R0, R12
BL	_bigi_shift_left+0
;bigi.c,725 :: 		BIGI_CALL(bigi_mult_fit(Q1, MU, &TMPARY[6], tmparylen - 6, Q2))
LDR	R6, [SP, #36]
LDR	R4, [SP, #48]
SUBS	R5, R4, #6
LDR	R4, [SP, #32]
ADDS	R4, #96
MOV	R3, R5
MOV	R2, R4
LDR	R1, [SP, #28]
MOV	R0, R12
; Q1 end address is: 48 (R12)
PUSH	(R6)
BL	_bigi_mult_fit+0
ADD	SP, SP, #4
;bigi.c,726 :: 		BIGI_CALL(bigi_copy(Q3, Q2))
LDR	R1, [SP, #36]
LDR	R0, [SP, #40]
BL	_bigi_copy+0
;bigi.c,727 :: 		BIGI_CALL(bigi_shift_left(Q2, MACHINE_WORD_BITLEN * (k+1)))
LDR	R4, [SP, #12]
ADDS	R4, R4, #1
LSLS	R4, R4, #5
MOV	R1, R4
LDR	R0, [SP, #36]
BL	_bigi_shift_left+0
;bigi.c,730 :: 		BIGI_CALL(bigi_copy(R1, A))
LDR	R1, [SP, #20]
LDR	R0, [SP, #44]
BL	_bigi_copy+0
;bigi.c,731 :: 		for (i = R1->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #44]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #8]
L_bigi_mod_red_barrett137:
LDR	R4, [SP, #8]
CMP	R4, #0
IT	LT
BLT	L_bigi_mod_red_barrett138
;bigi.c,734 :: 		if (i > k)
LDR	R5, [SP, #12]
LDR	R4, [SP, #8]
CMP	R4, R5
IT	LS
BLS	L_bigi_mod_red_barrett140
;bigi.c,735 :: 		R1->ary[i] = 0u;
LDR	R4, [SP, #44]
LDR	R5, [R4, #0]
LDR	R4, [SP, #8]
LSLS	R4, R4, #2
ADDS	R5, R5, R4
MOVS	R4, #0
STR	R4, [R5, #0]
L_bigi_mod_red_barrett140:
;bigi.c,731 :: 		for (i = R1->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #8]
SUBS	R4, R4, #1
STR	R4, [SP, #8]
;bigi.c,736 :: 		}
IT	AL
BAL	L_bigi_mod_red_barrett137
L_bigi_mod_red_barrett138:
;bigi.c,738 :: 		BIGI_CALL(bigi_mult_fit(Q3, MOD, &TMPARY[6], tmparylen - 6, R2))
LDR	R6, [SP, #4]
LDR	R4, [SP, #48]
SUBS	R5, R4, #6
LDR	R4, [SP, #32]
ADDS	R4, #96
MOV	R3, R5
MOV	R2, R4
LDR	R1, [SP, #24]
LDR	R0, [SP, #40]
PUSH	(R6)
BL	_bigi_mult_fit+0
ADD	SP, SP, #4
;bigi.c,739 :: 		for (i = R2->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #4]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #8]
L_bigi_mod_red_barrett141:
LDR	R4, [SP, #8]
CMP	R4, #0
IT	LT
BLT	L_bigi_mod_red_barrett142
;bigi.c,742 :: 		if (i > k)
LDR	R5, [SP, #12]
LDR	R4, [SP, #8]
CMP	R4, R5
IT	LS
BLS	L_bigi_mod_red_barrett144
;bigi.c,743 :: 		R2->ary[i] = 0u;
LDR	R4, [SP, #4]
LDR	R5, [R4, #0]
LDR	R4, [SP, #8]
LSLS	R4, R4, #2
ADDS	R5, R5, R4
MOVS	R4, #0
STR	R4, [R5, #0]
L_bigi_mod_red_barrett144:
;bigi.c,739 :: 		for (i = R2->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #8]
SUBS	R4, R4, #1
STR	R4, [SP, #8]
;bigi.c,744 :: 		}
IT	AL
BAL	L_bigi_mod_red_barrett141
L_bigi_mod_red_barrett142:
;bigi.c,746 :: 		BIGI_CALL(bigi_cmp(R2, R1, &cmp))
ADD	R4, SP, #16
MOV	R2, R4
LDR	R1, [SP, #44]
LDR	R0, [SP, #4]
BL	_bigi_cmp+0
;bigi.c,747 :: 		if (cmp == CMP_GREATER)
LDRB	R4, [SP, #16]
CMP	R4, #3
IT	NE
BNE	L_bigi_mod_red_barrett145
;bigi.c,749 :: 		R1->ary[R1->wordlen + MULT_BUFFER_WORDLEN - 1 - (k+2)] = 1u;
LDR	R4, [SP, #44]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R5, R4, #1
LDR	R4, [SP, #12]
ADDS	R4, R4, #2
SUB	R6, R5, R4
LDR	R4, [SP, #44]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R5, R5, R4
MOVS	R4, #1
STR	R4, [R5, #0]
L_bigi_mod_red_barrett145:
;bigi.c,751 :: 		BIGI_CALL(bigi_sub(R1, R2, RES))
LDR	R2, [SP, #52]
LDR	R1, [SP, #4]
LDR	R0, [SP, #44]
BL	_bigi_sub+0
;bigi.c,753 :: 		return OK;
;bigi.c,754 :: 		}
L_end_bigi_mod_red_barrett:
LDR	LR, [SP, #0]
ADD	SP, SP, #48
BX	LR
; end of _bigi_mod_red_barrett
_bigi_mult_word:
;bigi.c,759 :: 		bigi_t *const RES)
; RES start address is: 12 (R3)
; TMP start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
MOV	R7, R2
MOV	R2, R1
MOV	R6, R3
; RES end address is: 12 (R3)
; TMP end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; B start address is: 8 (R2)
; TMP start address is: 28 (R7)
; RES start address is: 24 (R6)
;bigi.c,762 :: 		int32_t i, m = 0;
; m start address is: 12 (R3)
MOV	R3, #0
;bigi.c,772 :: 		TMP->domain = A->domain;
ADDW	R5, R7, #12
ADDW	R4, R0, #12
LDRB	R4, [R4, #0]
STRB	R4, [R5, #0]
;bigi.c,773 :: 		RES->domain = A->domain;
ADDW	R5, R6, #12
ADDW	R4, R0, #12
LDRB	R4, [R4, #0]
STRB	R4, [R5, #0]
;bigi.c,775 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
ADDS	R4, R0, #4
LDR	R4, [R4, #0]
ADDS	R1, R4, #3
SUBS	R1, R1, #1
; i start address is: 4 (R1)
; A end address is: 0 (R0)
; i end address is: 4 (R1)
; m end address is: 12 (R3)
; RES end address is: 24 (R6)
; TMP end address is: 28 (R7)
; B end address is: 8 (R2)
MOV	R9, R0
MOV	R8, R2
L_bigi_mult_word146:
; i start address is: 4 (R1)
; m start address is: 12 (R3)
; RES start address is: 24 (R6)
; TMP start address is: 28 (R7)
; B start address is: 32 (R8)
; A start address is: 36 (R9)
CMP	R1, #0
IT	LT
BLT	L__bigi_mult_word334
;bigi.c,777 :: 		if (A->ary[i] != 0u)
LDR	R5, [R9, #0]
LSLS	R4, R1, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mult_word149
; m end address is: 12 (R3)
;bigi.c,779 :: 		m = i + 1;
ADD	R10, R1, #1
; i end address is: 4 (R1)
; m start address is: 40 (R10)
;bigi.c,780 :: 		break;
; m end address is: 40 (R10)
IT	AL
BAL	L_bigi_mult_word147
;bigi.c,781 :: 		}
L_bigi_mult_word149:
;bigi.c,775 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; m start address is: 12 (R3)
; i start address is: 4 (R1)
SUBS	R4, R1, #1
; i end address is: 4 (R1)
; i start address is: 16 (R4)
;bigi.c,782 :: 		}
; m end address is: 12 (R3)
; i end address is: 16 (R4)
MOV	R1, R4
IT	AL
BAL	L_bigi_mult_word146
L__bigi_mult_word334:
;bigi.c,775 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
MOV	R10, R3
;bigi.c,782 :: 		}
L_bigi_mult_word147:
;bigi.c,789 :: 		BIGI_CALL(bigi_copy(TMP, A))
; m start address is: 40 (R10)
MOV	R1, R9
; A end address is: 36 (R9)
MOV	R0, R7
BL	_bigi_copy+0
;bigi.c,790 :: 		BIGI_CALL(bigi_set_zero(RES))
MOV	R0, R6
BL	_bigi_set_zero+0
;bigi.c,792 :: 		for (k = 0, i = 0; i < m; i++)
MOVS	R4, #0
MOVS	R5, #0
STRD	R4, R5, [SP, #4]
; i start address is: 8 (R2)
MOVS	R2, #0
; RES end address is: 24 (R6)
; TMP end address is: 28 (R7)
; B end address is: 32 (R8)
; m end address is: 40 (R10)
; i end address is: 8 (R2)
MOV	R11, R6
MOV	R3, R7
MOV	R0, R2
MOV	R2, R8
MOV	R1, R10
L_bigi_mult_word150:
; TMP start address is: 12 (R3)
; B start address is: 8 (R2)
; i start address is: 0 (R0)
; m start address is: 4 (R1)
; B start address is: 8 (R2)
; B end address is: 8 (R2)
; TMP start address is: 12 (R3)
; TMP end address is: 12 (R3)
; RES start address is: 44 (R11)
CMP	R0, R1
IT	GE
BGE	L_bigi_mult_word151
; B end address is: 8 (R2)
; TMP end address is: 12 (R3)
;bigi.c,794 :: 		t  = (dword_t)(TMP->ary[i]);
; TMP start address is: 12 (R3)
; B start address is: 8 (R2)
LDR	R4, [R3, #0]
LSL	R10, R0, #2
ADD	R4, R4, R10, LSL #0
LDR	R4, [R4, #0]
MOV	R8, R4
MOVW	R9, #0
;bigi.c,795 :: 		t *= (dword_t)(B);
MOV	R4, R2
MOVS	R5, #0
UMULL	R6, R7, R8, R4
MLA	R7, R9, R4, R7
MLA	R7, R8, R5, R7
;bigi.c,796 :: 		t += (dword_t)(RES->ary[i]);
LDR	R4, [R11, #0]
ADD	R8, R4, R10, LSL #0
LDR	R4, [R8, #0]
MOVS	R5, #0
ADDS	R6, R6, R4
ADCS	R7, R5
;bigi.c,797 :: 		t += k;
LDRD	R4, R5, [SP, #4]
ADDS	R4, R6, R4
ADC	R5, R7, R5, LSL #0
; t start address is: 24 (R6)
MOV	R6, R4
MOV	R7, R5
;bigi.c,800 :: 		tmp_loc = t & MAX_VAL;   /* (I.e., t & 0xFFFF). */
AND	R4, R4, #-1
AND	R5, R5, #0
;bigi.c,801 :: 		RES->ary[i] = (word_t)(tmp_loc);
STR	R4, [R8, #0]
;bigi.c,802 :: 		k = t >> MACHINE_WORD_BITLEN;
MOV	R4, R7
MOVS	R5, #0
; t end address is: 24 (R6)
STRD	R4, R5, [SP, #4]
;bigi.c,792 :: 		for (k = 0, i = 0; i < m; i++)
ADDS	R4, R0, #1
; i end address is: 0 (R0)
; i start address is: 16 (R4)
;bigi.c,803 :: 		}
; B end address is: 8 (R2)
; TMP end address is: 12 (R3)
; i end address is: 16 (R4)
MOV	R0, R4
IT	AL
BAL	L_bigi_mult_word150
L_bigi_mult_word151:
;bigi.c,804 :: 		RES->ary[m] = (word_t)(k);
LDR	R5, [R11, #0]
; RES end address is: 44 (R11)
LSLS	R4, R1, #2
; m end address is: 4 (R1)
ADDS	R5, R5, R4
LDR	R4, [SP, #4]
STR	R4, [R5, #0]
;bigi.c,806 :: 		return OK;
;bigi.c,807 :: 		}
L_end_bigi_mult_word:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _bigi_mult_word
_bigi_mult_fit:
;bigi.c,813 :: 		bigi_t *const RES)
; TMPARY start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #16
STR	LR, [SP, #0]
MOV	R6, R1
MOV	R7, R2
; TMPARY end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; B start address is: 24 (R6)
; TMPARY start address is: 28 (R7)
; RES start address is: 4 (R1)
LDR	R1, [SP, #16]
;bigi.c,817 :: 		index_t j, m = 0, n = 0;
; m start address is: 12 (R3)
MOV	R3, #0
; n start address is: 8 (R2)
MOV	R2, #0
;bigi.c,818 :: 		bigi_t * TMPA = NULL;
;bigi.c,819 :: 		bigi_t * TMPB = NULL;
;bigi.c,840 :: 		for (i = 0; i < MULT_FIT__MIN_TMPARY_LEN; i++)
MOVS	R4, #0
STR	R4, [SP, #12]
; n end address is: 8 (R2)
; m end address is: 12 (R3)
; TMPARY end address is: 28 (R7)
; B end address is: 24 (R6)
; A end address is: 0 (R0)
; RES end address is: 4 (R1)
L_bigi_mult_fit153:
; n start address is: 8 (R2)
; m start address is: 12 (R3)
; RES start address is: 4 (R1)
; TMPARY start address is: 28 (R7)
; B start address is: 24 (R6)
; A start address is: 0 (R0)
LDR	R4, [SP, #12]
CMP	R4, #2
IT	GE
BGE	L_bigi_mult_fit154
;bigi.c,842 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LDR	R4, [SP, #12]
LSLS	R4, R4, #4
ADDS	R4, R7, R4
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,843 :: 		TMPARY[i].wordlen = A->wordlen;
LDR	R4, [SP, #12]
LSLS	R4, R4, #4
ADDS	R4, R7, R4
ADDS	R5, R4, #4
ADDS	R4, R0, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,840 :: 		for (i = 0; i < MULT_FIT__MIN_TMPARY_LEN; i++)
LDR	R4, [SP, #12]
ADDS	R4, R4, #1
STR	R4, [SP, #12]
;bigi.c,844 :: 		}
IT	AL
BAL	L_bigi_mult_fit153
L_bigi_mult_fit154:
;bigi.c,845 :: 		RES->domain = DOMAIN_NORMAL;
ADDW	R5, R1, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,847 :: 		TMPA = &TMPARY[0];
; TMPA start address is: 20 (R5)
MOV	R5, R7
;bigi.c,848 :: 		TMPB = &TMPARY[1];
ADD	R8, R7, #16
; TMPARY end address is: 28 (R7)
; TMPB start address is: 32 (R8)
;bigi.c,852 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
ADDS	R4, R0, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #12]
; n end address is: 8 (R2)
; m end address is: 12 (R3)
; B end address is: 24 (R6)
; TMPA end address is: 20 (R5)
; A end address is: 0 (R0)
; TMPB end address is: 32 (R8)
; RES end address is: 4 (R1)
MOV	R10, R6
MOV	R6, R1
MOV	R1, R2
MOV	R9, R3
MOV	R7, R5
L_bigi_mult_fit156:
; TMPB start address is: 32 (R8)
; TMPA start address is: 28 (R7)
; A start address is: 0 (R0)
; B start address is: 40 (R10)
; RES start address is: 24 (R6)
; m start address is: 36 (R9)
; n start address is: 4 (R1)
LDR	R4, [SP, #12]
CMP	R4, #0
IT	LT
BLT	L__bigi_mult_fit332
;bigi.c,854 :: 		if (A->ary[i] != 0u)
LDR	R5, [R0, #0]
LDR	R4, [SP, #12]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mult_fit159
; m end address is: 36 (R9)
;bigi.c,856 :: 		m = i + 1;
LDR	R4, [SP, #12]
ADD	R9, R4, #1
; m start address is: 36 (R9)
;bigi.c,857 :: 		break;
IT	AL
BAL	L_bigi_mult_fit157
;bigi.c,858 :: 		}
L_bigi_mult_fit159:
;bigi.c,852 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #12]
SUBS	R4, R4, #1
STR	R4, [SP, #12]
;bigi.c,859 :: 		}
; m end address is: 36 (R9)
IT	AL
BAL	L_bigi_mult_fit156
L__bigi_mult_fit332:
;bigi.c,852 :: 		for (i = A->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
;bigi.c,859 :: 		}
L_bigi_mult_fit157:
;bigi.c,861 :: 		for (i = B->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; m start address is: 36 (R9)
ADD	R4, R10, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #12]
; n end address is: 4 (R1)
; B end address is: 40 (R10)
; A end address is: 0 (R0)
; TMPB end address is: 32 (R8)
; TMPA end address is: 28 (R7)
; RES end address is: 24 (R6)
; m end address is: 36 (R9)
L_bigi_mult_fit160:
; m start address is: 36 (R9)
; n start address is: 4 (R1)
; RES start address is: 24 (R6)
; B start address is: 40 (R10)
; A start address is: 0 (R0)
; TMPA start address is: 28 (R7)
; TMPB start address is: 32 (R8)
LDR	R4, [SP, #12]
CMP	R4, #0
IT	LT
BLT	L__bigi_mult_fit333
;bigi.c,863 :: 		if (B->ary[i] != 0u)
LDR	R5, [R10, #0]
LDR	R4, [SP, #12]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mult_fit163
; n end address is: 4 (R1)
;bigi.c,865 :: 		n = i + 1;
LDR	R4, [SP, #12]
ADD	R11, R4, #1
; n start address is: 44 (R11)
;bigi.c,866 :: 		break;
; n end address is: 44 (R11)
IT	AL
BAL	L_bigi_mult_fit161
;bigi.c,867 :: 		}
L_bigi_mult_fit163:
;bigi.c,861 :: 		for (i = B->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; n start address is: 4 (R1)
LDR	R4, [SP, #12]
SUBS	R4, R4, #1
STR	R4, [SP, #12]
;bigi.c,868 :: 		}
; n end address is: 4 (R1)
IT	AL
BAL	L_bigi_mult_fit160
L__bigi_mult_fit333:
;bigi.c,861 :: 		for (i = B->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
MOV	R11, R1
;bigi.c,868 :: 		}
L_bigi_mult_fit161:
;bigi.c,875 :: 		BIGI_CALL(bigi_copy(TMPA, A))
; n start address is: 44 (R11)
MOV	R1, R0
; A end address is: 0 (R0)
MOV	R0, R7
BL	_bigi_copy+0
;bigi.c,876 :: 		BIGI_CALL(bigi_copy(TMPB, B))
MOV	R1, R10
; B end address is: 40 (R10)
MOV	R0, R8
BL	_bigi_copy+0
;bigi.c,878 :: 		BIGI_CALL(bigi_set_zero(RES))
MOV	R0, R6
BL	_bigi_set_zero+0
;bigi.c,880 :: 		for (j = 0; j < n; j++)
; j start address is: 48 (R12)
MOVW	R12, #0
; TMPB end address is: 32 (R8)
; TMPA end address is: 28 (R7)
; RES end address is: 24 (R6)
; m end address is: 36 (R9)
; n end address is: 44 (R11)
; j end address is: 48 (R12)
MOV	R10, R11
L_bigi_mult_fit164:
; j start address is: 48 (R12)
; n start address is: 40 (R10)
; TMPB start address is: 32 (R8)
; TMPA start address is: 28 (R7)
; RES start address is: 24 (R6)
; m start address is: 36 (R9)
CMP	R12, R10
IT	CS
BCS	L_bigi_mult_fit165
;bigi.c,882 :: 		for (k = 0, i = 0; i < m; i++)
MOVS	R4, #0
MOVS	R5, #0
STRD	R4, R5, [SP, #4]
MOVS	R4, #0
STR	R4, [SP, #12]
; n end address is: 40 (R10)
; TMPB end address is: 32 (R8)
; TMPA end address is: 28 (R7)
; RES end address is: 24 (R6)
; m end address is: 36 (R9)
; j end address is: 48 (R12)
MOV	R11, R10
MOV	R3, R8
MOV	R2, R7
MOV	R1, R6
MOV	R0, R9
L_bigi_mult_fit167:
; m start address is: 0 (R0)
; RES start address is: 4 (R1)
; TMPA start address is: 8 (R2)
; TMPB start address is: 12 (R3)
; n start address is: 44 (R11)
; j start address is: 48 (R12)
LDR	R4, [SP, #12]
CMP	R4, R0
IT	CS
BCS	L_bigi_mult_fit168
;bigi.c,884 :: 		t  = (dword_t)(TMPA->ary[i]);
LDR	R5, [R2, #0]
LDR	R4, [SP, #12]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
;bigi.c,885 :: 		t *= (dword_t)(TMPB->ary[j]);
LDR	R5, [R3, #0]
LSL	R4, R12, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOVS	R5, #0
UMULL	R9, R10, R6, R4
MLA	R10, R7, R4, R10
MLA	R10, R6, R5, R10
;bigi.c,886 :: 		t += (dword_t)(RES->ary[i + j]);
LDR	R4, [SP, #12]
ADD	R5, R4, R12, LSL #0
LDR	R4, [R1, #0]
LSL	R8, R5, #2
ADD	R4, R4, R8, LSL #0
LDR	R4, [R4, #0]
MOVS	R5, #0
ADDS	R6, R9, R4, LSL #0
ADC	R7, R10, R5, LSL #0
;bigi.c,887 :: 		t += k;
LDRD	R4, R5, [SP, #4]
ADDS	R4, R6, R4
ADC	R5, R7, R5, LSL #0
; t start address is: 36 (R9)
MOV	R9, R4
MOV	R10, R5
;bigi.c,890 :: 		tmp = t & MAX_VAL;   /* (I.e., t & 0xFFFF). */
AND	R6, R4, #-1
AND	R7, R5, #0
;bigi.c,891 :: 		RES->ary[i + j] = (word_t)(tmp);
LDR	R4, [R1, #0]
ADD	R4, R4, R8, LSL #0
STR	R6, [R4, #0]
;bigi.c,892 :: 		k = t >> MACHINE_WORD_BITLEN;
MOV	R4, R10
MOVS	R5, #0
; t end address is: 36 (R9)
STRD	R4, R5, [SP, #4]
;bigi.c,882 :: 		for (k = 0, i = 0; i < m; i++)
LDR	R4, [SP, #12]
ADDS	R4, R4, #1
STR	R4, [SP, #12]
;bigi.c,893 :: 		}
IT	AL
BAL	L_bigi_mult_fit167
L_bigi_mult_fit168:
;bigi.c,894 :: 		RES->ary[j + m] = (word_t)(k);
ADD	R4, R12, R0, LSL #0
LDR	R5, [R1, #0]
LSLS	R4, R4, #2
ADDS	R5, R5, R4
LDR	R4, [SP, #4]
STR	R4, [R5, #0]
;bigi.c,880 :: 		for (j = 0; j < n; j++)
ADD	R12, R12, #1
;bigi.c,895 :: 		}
MOV	R9, R0
; m end address is: 0 (R0)
; RES end address is: 4 (R1)
; TMPA end address is: 8 (R2)
; TMPB end address is: 12 (R3)
; n end address is: 44 (R11)
; j end address is: 48 (R12)
MOV	R6, R1
MOV	R7, R2
MOV	R8, R3
MOV	R10, R11
IT	AL
BAL	L_bigi_mult_fit164
L_bigi_mult_fit165:
;bigi.c,897 :: 		return OK;
;bigi.c,898 :: 		}
L_end_bigi_mult_fit:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _bigi_mult_fit
_bigi_mod_mult:
;bigi.c,903 :: 		bigi_t *const RES)
SUB	SP, SP, #28
STR	LR, [SP, #0]
STR	R0, [SP, #12]
STR	R1, [SP, #16]
STR	R2, [SP, #20]
STR	R3, [SP, #24]
;bigi.c,906 :: 		cmp_t cmp = CMP_UNDEFINED;
ADD	R11, SP, #4
ADD	R10, R11, #8
MOVW	R12, #lo_addr(?ICSbigi_mod_mult_cmp_L0+0)
MOVT	R12, #hi_addr(?ICSbigi_mod_mult_cmp_L0+0)
BL	___CC2DW+0
;bigi.c,907 :: 		word_t readbit = 0u;
;bigi.c,926 :: 		RES->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #24]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,927 :: 		BIGI_CALL(bigi_set_zero(RES))
LDR	R0, [SP, #24]
BL	_bigi_set_zero+0
;bigi.c,929 :: 		for (i = B->wordlen * MACHINE_WORD_BITLEN - 1; i >= 0; i--)
LDR	R4, [SP, #16]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
LSL	R12, R4, #5
SUB	R12, R12, #1
; i start address is: 48 (R12)
; i end address is: 48 (R12)
L_bigi_mod_mult170:
; i start address is: 48 (R12)
CMP	R12, #0
IT	LT
BLT	L_bigi_mod_mult171
;bigi.c,932 :: 		BIGI_CALL(bigi_shift_left(RES, 1))
MOVS	R1, #1
LDR	R0, [SP, #24]
BL	_bigi_shift_left+0
;bigi.c,935 :: 		BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
ADD	R4, SP, #4
MOV	R2, R4
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_cmp+0
;bigi.c,936 :: 		if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
LDRB	R4, [SP, #4]
CMP	R4, #3
IT	EQ
BEQ	L__bigi_mod_mult340
LDRB	R4, [SP, #4]
CMP	R4, #0
IT	EQ
BEQ	L__bigi_mod_mult339
IT	AL
BAL	L_bigi_mod_mult175
L__bigi_mod_mult340:
L__bigi_mod_mult339:
;bigi.c,938 :: 		BIGI_CALL(bigi_sub(RES, MOD, RES))
LDR	R2, [SP, #24]
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_sub+0
;bigi.c,939 :: 		}
L_bigi_mod_mult175:
;bigi.c,941 :: 		BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
ADD	R4, SP, #4
MOV	R2, R4
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_cmp+0
;bigi.c,942 :: 		if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
LDRB	R4, [SP, #4]
CMP	R4, #3
IT	EQ
BEQ	L__bigi_mod_mult342
LDRB	R4, [SP, #4]
CMP	R4, #0
IT	EQ
BEQ	L__bigi_mod_mult341
IT	AL
BAL	L_bigi_mod_mult178
L__bigi_mod_mult342:
L__bigi_mod_mult341:
;bigi.c,944 :: 		BIGI_CALL(bigi_sub(RES, MOD, RES))
LDR	R2, [SP, #24]
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_sub+0
;bigi.c,945 :: 		}
L_bigi_mod_mult178:
;bigi.c,948 :: 		BIGI_CALL(bigi_get_bit(B, i, &readbit))
ADD	R4, SP, #8
MOV	R2, R4
MOV	R1, R12
LDR	R0, [SP, #16]
BL	_bigi_get_bit+0
;bigi.c,949 :: 		if (readbit)
LDR	R4, [SP, #8]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mod_mult179
;bigi.c,951 :: 		BIGI_CALL(bigi_add(A, RES, RES))
LDR	R2, [SP, #24]
LDR	R1, [SP, #24]
LDR	R0, [SP, #12]
BL	_bigi_add+0
;bigi.c,952 :: 		}
L_bigi_mod_mult179:
;bigi.c,955 :: 		BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
ADD	R4, SP, #4
MOV	R2, R4
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_cmp+0
;bigi.c,956 :: 		if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
LDRB	R4, [SP, #4]
CMP	R4, #3
IT	EQ
BEQ	L__bigi_mod_mult344
LDRB	R4, [SP, #4]
CMP	R4, #0
IT	EQ
BEQ	L__bigi_mod_mult343
IT	AL
BAL	L_bigi_mod_mult182
L__bigi_mod_mult344:
L__bigi_mod_mult343:
;bigi.c,958 :: 		BIGI_CALL(bigi_sub(RES, MOD, RES))
LDR	R2, [SP, #24]
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_sub+0
;bigi.c,959 :: 		}
L_bigi_mod_mult182:
;bigi.c,961 :: 		BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
ADD	R4, SP, #4
MOV	R2, R4
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_cmp+0
;bigi.c,962 :: 		if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
LDRB	R4, [SP, #4]
CMP	R4, #3
IT	EQ
BEQ	L__bigi_mod_mult346
LDRB	R4, [SP, #4]
CMP	R4, #0
IT	EQ
BEQ	L__bigi_mod_mult345
IT	AL
BAL	L_bigi_mod_mult185
L__bigi_mod_mult346:
L__bigi_mod_mult345:
;bigi.c,964 :: 		BIGI_CALL(bigi_sub(RES, MOD, RES))
LDR	R2, [SP, #24]
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_sub+0
;bigi.c,965 :: 		}
L_bigi_mod_mult185:
;bigi.c,929 :: 		for (i = B->wordlen * MACHINE_WORD_BITLEN - 1; i >= 0; i--)
SUB	R4, R12, #1
; i end address is: 48 (R12)
; i start address is: 0 (R0)
MOV	R0, R4
;bigi.c,966 :: 		}
MOV	R12, R0
; i end address is: 0 (R0)
IT	AL
BAL	L_bigi_mod_mult170
L_bigi_mod_mult171:
;bigi.c,968 :: 		return OK;
;bigi.c,969 :: 		}
L_end_bigi_mod_mult:
LDR	LR, [SP, #0]
ADD	SP, SP, #28
BX	LR
; end of _bigi_mod_mult
_bigi_mod_exp:
;bigi.c,975 :: 		bigi_t *const RES)
SUB	SP, SP, #28
STR	LR, [SP, #0]
STR	R0, [SP, #12]
STR	R1, [SP, #16]
STR	R2, [SP, #20]
STR	R3, [SP, #24]
LDR	R4, [SP, #28]
STR	R4, [SP, #28]
;bigi.c,978 :: 		word_t readbit = 0u;
MOV	R4, #0
STR	R4, [SP, #8]
;bigi.c,998 :: 		TMP->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #24]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,999 :: 		RES->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #28]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1001 :: 		BIGI_CALL(bigi_set_zero(RES))
LDR	R0, [SP, #28]
BL	_bigi_set_zero+0
;bigi.c,1002 :: 		RES->ary[0] = 1u;
LDR	R4, [SP, #28]
LDR	R5, [R4, #0]
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,1004 :: 		for (i = B->wordlen * MACHINE_WORD_BITLEN - 1; i >= 0; i--)
LDR	R4, [SP, #16]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
LSLS	R4, R4, #5
SUBS	R4, R4, #1
STR	R4, [SP, #4]
L_bigi_mod_exp186:
LDR	R4, [SP, #4]
CMP	R4, #0
IT	LT
BLT	L_bigi_mod_exp187
;bigi.c,1007 :: 		BIGI_CALL(bigi_mod_mult(RES, RES, MOD, TMP))
LDR	R3, [SP, #24]
LDR	R2, [SP, #20]
LDR	R1, [SP, #28]
LDR	R0, [SP, #28]
BL	_bigi_mod_mult+0
;bigi.c,1010 :: 		BIGI_CALL(bigi_get_bit(B, i, &readbit))
ADD	R4, SP, #8
MOV	R2, R4
LDR	R1, [SP, #4]
LDR	R0, [SP, #16]
BL	_bigi_get_bit+0
;bigi.c,1011 :: 		if (readbit)
LDR	R4, [SP, #8]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mod_exp189
;bigi.c,1013 :: 		BIGI_CALL(bigi_mod_mult(A, TMP, MOD, RES))
LDR	R3, [SP, #28]
LDR	R2, [SP, #20]
LDR	R1, [SP, #24]
LDR	R0, [SP, #12]
BL	_bigi_mod_mult+0
;bigi.c,1014 :: 		}
IT	AL
BAL	L_bigi_mod_exp190
L_bigi_mod_exp189:
;bigi.c,1017 :: 		BIGI_CALL(bigi_copy(RES, TMP))
LDR	R1, [SP, #24]
LDR	R0, [SP, #28]
BL	_bigi_copy+0
;bigi.c,1018 :: 		}
L_bigi_mod_exp190:
;bigi.c,1004 :: 		for (i = B->wordlen * MACHINE_WORD_BITLEN - 1; i >= 0; i--)
LDR	R4, [SP, #4]
SUBS	R4, R4, #1
STR	R4, [SP, #4]
;bigi.c,1019 :: 		}
IT	AL
BAL	L_bigi_mod_exp186
L_bigi_mod_exp187:
;bigi.c,1020 :: 		return OK;
;bigi.c,1021 :: 		}
L_end_bigi_mod_exp:
LDR	LR, [SP, #0]
ADD	SP, SP, #28
BX	LR
; end of _bigi_mod_exp
_bigi_mod_inv:
;bigi.c,1027 :: 		bigi_t *const RES)
; A start address is: 0 (R0)
SUB	SP, SP, #52
STR	LR, [SP, #0]
STR	R1, [SP, #8]
MOV	R1, R0
STR	R2, [SP, #12]
STR	R3, [SP, #16]
; A end address is: 0 (R0)
; A start address is: 4 (R1)
LDR	R4, [SP, #52]
STR	R4, [SP, #52]
;bigi.c,1031 :: 		bigi_t * T  = NULL;
;bigi.c,1032 :: 		bigi_t * NT = NULL;
;bigi.c,1033 :: 		bigi_t * R  = NULL;
;bigi.c,1034 :: 		bigi_t * NR = NULL;
;bigi.c,1035 :: 		bigi_t * Q  = NULL;
;bigi.c,1036 :: 		bigi_t * TMP  = NULL;
;bigi.c,1037 :: 		bigi_t * TMP1 = NULL;
;bigi.c,1038 :: 		bigi_t * TMP2 = NULL;
;bigi.c,1061 :: 		for (i = 0; i < MOD_INV__MIN_TMPARY_LEN; i++)
; i start address is: 8 (R2)
MOVS	R2, #0
; A end address is: 4 (R1)
; i end address is: 8 (R2)
MOV	R7, R1
MOV	R0, R2
L_bigi_mod_inv191:
; i start address is: 0 (R0)
; A start address is: 28 (R7)
CMP	R0, #12
IT	GE
BGE	L_bigi_mod_inv192
;bigi.c,1063 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LSLS	R5, R0, #4
LDR	R4, [SP, #12]
ADDS	R4, R4, R5
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1064 :: 		TMPARY[i].wordlen = A->wordlen;
LSLS	R5, R0, #4
LDR	R4, [SP, #12]
ADDS	R4, R4, R5
ADDS	R5, R4, #4
ADDS	R4, R7, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1061 :: 		for (i = 0; i < MOD_INV__MIN_TMPARY_LEN; i++)
ADDS	R4, R0, #1
; i end address is: 0 (R0)
; i start address is: 8 (R2)
MOV	R2, R4
;bigi.c,1065 :: 		}
; i end address is: 8 (R2)
MOV	R0, R2
IT	AL
BAL	L_bigi_mod_inv191
L_bigi_mod_inv192:
;bigi.c,1066 :: 		RES->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #52]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1068 :: 		T =     &TMPARY[0];
LDR	R4, [SP, #12]
STR	R4, [SP, #20]
;bigi.c,1069 :: 		NT =    &TMPARY[1];
LDR	R4, [SP, #12]
ADDS	R4, #16
STR	R4, [SP, #24]
;bigi.c,1070 :: 		R =     &TMPARY[2];
LDR	R4, [SP, #12]
ADDS	R4, #32
STR	R4, [SP, #28]
;bigi.c,1071 :: 		NR =    &TMPARY[3];
LDR	R4, [SP, #12]
ADDS	R4, #48
STR	R4, [SP, #32]
;bigi.c,1072 :: 		Q =     &TMPARY[4];
LDR	R4, [SP, #12]
ADDS	R4, #64
STR	R4, [SP, #36]
;bigi.c,1073 :: 		TMP =   &TMPARY[5];
LDR	R4, [SP, #12]
ADDS	R4, #80
STR	R4, [SP, #40]
;bigi.c,1074 :: 		TMP1 =  &TMPARY[6];
LDR	R4, [SP, #12]
ADDS	R4, #96
STR	R4, [SP, #44]
;bigi.c,1075 :: 		TMP2 =  &TMPARY[7];
LDR	R4, [SP, #12]
ADDS	R4, #112
STR	R4, [SP, #48]
;bigi.c,1077 :: 		BIGI_CALL(bigi_set_zero(T))
LDR	R0, [SP, #12]
BL	_bigi_set_zero+0
;bigi.c,1078 :: 		BIGI_CALL(bigi_set_zero(NT))
LDR	R0, [SP, #24]
BL	_bigi_set_zero+0
;bigi.c,1079 :: 		BIGI_CALL(bigi_set_zero(Q))
LDR	R0, [SP, #36]
BL	_bigi_set_zero+0
;bigi.c,1080 :: 		BIGI_CALL(bigi_set_zero(TMP1))
LDR	R0, [SP, #44]
BL	_bigi_set_zero+0
;bigi.c,1081 :: 		BIGI_CALL(bigi_copy(R, MOD))
LDR	R1, [SP, #8]
LDR	R0, [SP, #28]
BL	_bigi_copy+0
;bigi.c,1085 :: 		BIGI_CALL(bigi_copy(NR,A))
MOV	R1, R7
; A end address is: 28 (R7)
LDR	R0, [SP, #32]
BL	_bigi_copy+0
;bigi.c,1087 :: 		NT->ary[0] = 1u;
LDR	R4, [SP, #24]
LDR	R5, [R4, #0]
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,1089 :: 		BIGI_CALL(bigi_is_zero(NR, &cmpx))
ADD	R4, SP, #4
MOV	R1, R4
LDR	R0, [SP, #32]
BL	_bigi_is_zero+0
;bigi.c,1090 :: 		BIGI_CALL(bigi_is_one(R, &cmpy))
ADD	R4, SP, #5
MOV	R1, R4
LDR	R0, [SP, #28]
BL	_bigi_is_one+0
;bigi.c,1092 :: 		while ((cmpx == CMP_NEQ) && (cmpy != CMP_EQUAL))
L_bigi_mod_inv194:
LDRB	R4, [SP, #4]
CMP	R4, #1
IT	NE
BNE	L__bigi_mod_inv349
LDRB	R4, [SP, #5]
CMP	R4, #0
IT	EQ
BEQ	L__bigi_mod_inv348
L__bigi_mod_inv347:
;bigi.c,1094 :: 		BIGI_CALL(bigi_div(R, NR, &TMPARY[8], tmparylen - 8, Q, TMP1))
LDR	R7, [SP, #44]
LDR	R6, [SP, #36]
LDR	R4, [SP, #16]
SUBW	R5, R4, #8
LDR	R4, [SP, #12]
ADDS	R4, #128
MOV	R3, R5
MOV	R2, R4
LDR	R1, [SP, #32]
LDR	R0, [SP, #28]
PUSH	(R7)
PUSH	(R6)
BL	_bigi_div+0
ADD	SP, SP, #8
;bigi.c,1095 :: 		BIGI_CALL(bigi_copy(TMP, NT))
LDR	R1, [SP, #24]
LDR	R0, [SP, #40]
BL	_bigi_copy+0
;bigi.c,1096 :: 		BIGI_CALL(bigi_mod_mult(Q, NT, MOD, TMP2))
LDR	R3, [SP, #48]
LDR	R2, [SP, #8]
LDR	R1, [SP, #24]
LDR	R0, [SP, #36]
BL	_bigi_mod_mult+0
;bigi.c,1098 :: 		BIGI_CALL(bigi_cmp(T, TMP2, &cmpx))
ADD	R4, SP, #4
MOV	R2, R4
LDR	R1, [SP, #48]
LDR	R0, [SP, #20]
BL	_bigi_cmp+0
;bigi.c,1099 :: 		if (cmpx == CMP_GREATER)
LDRB	R4, [SP, #4]
CMP	R4, #3
IT	NE
BNE	L_bigi_mod_inv198
;bigi.c,1101 :: 		BIGI_CALL(bigi_sub(T, TMP2, NT))
LDR	R2, [SP, #24]
LDR	R1, [SP, #48]
LDR	R0, [SP, #20]
BL	_bigi_sub+0
;bigi.c,1102 :: 		}
IT	AL
BAL	L_bigi_mod_inv199
L_bigi_mod_inv198:
;bigi.c,1105 :: 		BIGI_CALL(bigi_sub(TMP2, T, NT))
LDR	R2, [SP, #24]
LDR	R1, [SP, #20]
LDR	R0, [SP, #48]
BL	_bigi_sub+0
;bigi.c,1106 :: 		BIGI_CALL(bigi_sub(MOD, NT, NT))
LDR	R2, [SP, #24]
LDR	R1, [SP, #24]
LDR	R0, [SP, #8]
BL	_bigi_sub+0
;bigi.c,1107 :: 		}
L_bigi_mod_inv199:
;bigi.c,1109 :: 		BIGI_CALL(bigi_copy(T, TMP))
LDR	R1, [SP, #40]
LDR	R0, [SP, #20]
BL	_bigi_copy+0
;bigi.c,1110 :: 		BIGI_CALL(bigi_copy(TMP, NR))
LDR	R1, [SP, #32]
LDR	R0, [SP, #40]
BL	_bigi_copy+0
;bigi.c,1111 :: 		BIGI_CALL(bigi_mod_mult(Q, NR, MOD, TMP2))
LDR	R3, [SP, #48]
LDR	R2, [SP, #8]
LDR	R1, [SP, #32]
LDR	R0, [SP, #36]
BL	_bigi_mod_mult+0
;bigi.c,1112 :: 		BIGI_CALL(bigi_copy(NR, TMP1))
LDR	R1, [SP, #44]
LDR	R0, [SP, #32]
BL	_bigi_copy+0
;bigi.c,1113 :: 		BIGI_CALL(bigi_copy(R, TMP))
LDR	R1, [SP, #40]
LDR	R0, [SP, #28]
BL	_bigi_copy+0
;bigi.c,1115 :: 		BIGI_CALL(bigi_is_zero(NR, &cmpx))
ADD	R4, SP, #4
MOV	R1, R4
LDR	R0, [SP, #32]
BL	_bigi_is_zero+0
;bigi.c,1116 :: 		BIGI_CALL(bigi_is_one(R, &cmpy))
ADD	R4, SP, #5
MOV	R1, R4
LDR	R0, [SP, #28]
BL	_bigi_is_one+0
;bigi.c,1117 :: 		}
IT	AL
BAL	L_bigi_mod_inv194
;bigi.c,1092 :: 		while ((cmpx == CMP_NEQ) && (cmpy != CMP_EQUAL))
L__bigi_mod_inv349:
L__bigi_mod_inv348:
;bigi.c,1119 :: 		BIGI_CALL(bigi_is_one(R, &cmpy))
ADD	R4, SP, #5
MOV	R1, R4
LDR	R0, [SP, #28]
BL	_bigi_is_one+0
;bigi.c,1120 :: 		if (cmpy == CMP_NEQ) return BIGI_NOT_COPRIME;
LDRB	R4, [SP, #5]
CMP	R4, #1
IT	NE
BNE	L_bigi_mod_inv200
IT	AL
BAL	L_end_bigi_mod_inv
L_bigi_mod_inv200:
;bigi.c,1122 :: 		BIGI_CALL(bigi_copy(RES, T))
LDR	R1, [SP, #20]
LDR	R0, [SP, #52]
BL	_bigi_copy+0
;bigi.c,1124 :: 		return OK;
;bigi.c,1125 :: 		}
L_end_bigi_mod_inv:
LDR	LR, [SP, #0]
ADD	SP, SP, #52
BX	LR
; end of _bigi_mod_inv
_bigi_div:
;bigi.c,1132 :: 		bigi_t *const REM)
; TMPARY start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #80
STR	LR, [SP, #0]
MOV	R7, R2
; TMPARY end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 0 (R0)
; B start address is: 4 (R1)
; TMPARY start address is: 28 (R7)
LDR	R4, [SP, #80]
STR	R4, [SP, #80]
LDR	R4, [SP, #84]
STR	R4, [SP, #84]
;bigi.c,1135 :: 		bigi_t * U = NULL;
;bigi.c,1136 :: 		bigi_t * V = NULL;
;bigi.c,1137 :: 		bigi_t * UN = NULL;
;bigi.c,1138 :: 		bigi_t * VN = NULL;
;bigi.c,1145 :: 		int32_t m = 0, n = 0;
; m start address is: 32 (R8)
MOV	R8, #0
; n start address is: 24 (R6)
MOV	R6, #0
;bigi.c,1172 :: 		for (i = 0; i < DIV__MIN_TMPARY_LEN; i++)
MOVS	R4, #0
STR	R4, [SP, #36]
; B end address is: 4 (R1)
; n end address is: 24 (R6)
; m end address is: 32 (R8)
; TMPARY end address is: 28 (R7)
; A end address is: 0 (R0)
MOV	R11, R0
MOV	R12, R1
L_bigi_div201:
; n start address is: 24 (R6)
; m start address is: 32 (R8)
; TMPARY start address is: 28 (R7)
; B start address is: 48 (R12)
; A start address is: 44 (R11)
LDR	R4, [SP, #36]
CMP	R4, #4
IT	GE
BGE	L_bigi_div202
;bigi.c,1174 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LDR	R4, [SP, #36]
LSLS	R4, R4, #4
ADDS	R4, R7, R4
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1175 :: 		TMPARY[i].wordlen = A->wordlen;
LDR	R4, [SP, #36]
LSLS	R4, R4, #4
ADDS	R4, R7, R4
ADDS	R5, R4, #4
ADD	R4, R11, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1172 :: 		for (i = 0; i < DIV__MIN_TMPARY_LEN; i++)
LDR	R4, [SP, #36]
ADDS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1176 :: 		}
IT	AL
BAL	L_bigi_div201
L_bigi_div202:
;bigi.c,1177 :: 		if (REM != NULL)
LDR	R4, [SP, #84]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div204
;bigi.c,1178 :: 		REM->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #84]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
L_bigi_div204:
;bigi.c,1179 :: 		QUO->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #80]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1181 :: 		U   = &TMPARY[0];
STR	R7, [SP, #40]
;bigi.c,1182 :: 		UN  = &TMPARY[1];
ADDW	R4, R7, #16
STR	R4, [SP, #48]
;bigi.c,1183 :: 		V   = &TMPARY[2];
ADDW	R5, R7, #32
STR	R5, [SP, #44]
;bigi.c,1184 :: 		VN  = &TMPARY[3];
ADDW	R4, R7, #48
; TMPARY end address is: 28 (R7)
STR	R4, [SP, #52]
;bigi.c,1185 :: 		V->wordlen  = B->wordlen;
ADDS	R5, R5, #4
ADD	R4, R12, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1186 :: 		VN->wordlen = B->wordlen;
LDR	R4, [SP, #52]
ADDS	R5, R4, #4
ADD	R4, R12, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1188 :: 		BIGI_CALL(bigi_copy(U, A))
MOV	R1, R11
LDR	R0, [SP, #40]
BL	_bigi_copy+0
;bigi.c,1189 :: 		BIGI_CALL(bigi_copy(V, B))
MOV	R1, R12
; B end address is: 48 (R12)
LDR	R0, [SP, #44]
BL	_bigi_copy+0
;bigi.c,1193 :: 		BIGI_CALL(bigi_set_zero(UN))
LDR	R0, [SP, #48]
BL	_bigi_set_zero+0
;bigi.c,1194 :: 		BIGI_CALL(bigi_set_zero(VN))
LDR	R0, [SP, #52]
BL	_bigi_set_zero+0
;bigi.c,1195 :: 		BIGI_CALL(bigi_set_zero(QUO))
LDR	R0, [SP, #80]
BL	_bigi_set_zero+0
;bigi.c,1196 :: 		if (REM != NULL)
LDR	R4, [SP, #84]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div205
;bigi.c,1198 :: 		BIGI_CALL(bigi_set_zero(REM))
LDR	R0, [SP, #84]
BL	_bigi_set_zero+0
;bigi.c,1199 :: 		}
L_bigi_div205:
;bigi.c,1201 :: 		for (i = U->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
LDR	R4, [SP, #40]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #36]
; n end address is: 24 (R6)
; m end address is: 32 (R8)
; A end address is: 44 (R11)
MOV	R2, R6
MOV	R1, R11
MOV	R11, R8
L_bigi_div206:
; A start address is: 4 (R1)
; m start address is: 44 (R11)
; n start address is: 8 (R2)
LDR	R4, [SP, #36]
CMP	R4, #0
IT	LT
BLT	L__bigi_div329
;bigi.c,1203 :: 		if (U->ary[i] != 0u)
LDR	R4, [SP, #40]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div209
; m end address is: 44 (R11)
;bigi.c,1205 :: 		m = i + 1;
LDR	R4, [SP, #36]
ADD	R9, R4, #1
; m start address is: 36 (R9)
;bigi.c,1206 :: 		break;
; m end address is: 36 (R9)
IT	AL
BAL	L_bigi_div207
;bigi.c,1207 :: 		}
L_bigi_div209:
;bigi.c,1201 :: 		for (i = U->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; m start address is: 44 (R11)
LDR	R4, [SP, #36]
SUBS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1208 :: 		}
; m end address is: 44 (R11)
IT	AL
BAL	L_bigi_div206
L__bigi_div329:
;bigi.c,1201 :: 		for (i = U->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
MOV	R9, R11
;bigi.c,1208 :: 		}
L_bigi_div207:
;bigi.c,1209 :: 		for (i = V->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; m start address is: 36 (R9)
LDR	R4, [SP, #44]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
SUBS	R4, R4, #1
STR	R4, [SP, #36]
; A end address is: 4 (R1)
; m end address is: 36 (R9)
; n end address is: 8 (R2)
MOV	R0, R1
L_bigi_div210:
; m start address is: 36 (R9)
; n start address is: 8 (R2)
; A start address is: 0 (R0)
LDR	R4, [SP, #36]
CMP	R4, #0
IT	LT
BLT	L__bigi_div330
;bigi.c,1211 :: 		if (V->ary[i] != 0u)
LDR	R4, [SP, #44]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div213
; n end address is: 8 (R2)
;bigi.c,1213 :: 		n = i + 1;
LDR	R4, [SP, #36]
ADD	R12, R4, #1
; n start address is: 48 (R12)
;bigi.c,1214 :: 		break;
MOV	R11, R12
; n end address is: 48 (R12)
IT	AL
BAL	L_bigi_div211
;bigi.c,1215 :: 		}
L_bigi_div213:
;bigi.c,1209 :: 		for (i = V->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
; n start address is: 8 (R2)
LDR	R4, [SP, #36]
SUBS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1216 :: 		}
; n end address is: 8 (R2)
IT	AL
BAL	L_bigi_div210
L__bigi_div330:
;bigi.c,1209 :: 		for (i = V->wordlen + MULT_BUFFER_WORDLEN - 1; i >= 0; i--)
MOV	R11, R2
;bigi.c,1216 :: 		}
L_bigi_div211:
;bigi.c,1219 :: 		if (m < n)
; n start address is: 44 (R11)
CMP	R9, R11
IT	GE
BGE	L_bigi_div214
; m end address is: 36 (R9)
; n end address is: 44 (R11)
;bigi.c,1221 :: 		if (REM != NULL)
LDR	R4, [SP, #84]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div215
;bigi.c,1223 :: 		BIGI_CALL(bigi_copy(REM, A))
MOV	R1, R0
; A end address is: 0 (R0)
LDR	R0, [SP, #84]
BL	_bigi_copy+0
;bigi.c,1224 :: 		}
L_bigi_div215:
;bigi.c,1225 :: 		return OK;
IT	AL
BAL	L_end_bigi_div
;bigi.c,1226 :: 		}
L_bigi_div214:
;bigi.c,1228 :: 		if (n == 0)
; n start address is: 44 (R11)
; m start address is: 36 (R9)
CMP	R11, #0
IT	NE
BNE	L_bigi_div216
; m end address is: 36 (R9)
; n end address is: 44 (R11)
;bigi.c,1229 :: 		return BIGI_DIV_BY_ZERO;
IT	AL
BAL	L_end_bigi_div
L_bigi_div216:
;bigi.c,1232 :: 		if (n == 1)
; n start address is: 44 (R11)
; m start address is: 36 (R9)
CMP	R11, #1
IT	NE
BNE	L_bigi_div217
; n end address is: 44 (R11)
;bigi.c,1234 :: 		for (k = 0, j = m - 1; j >= 0; j--)
MOVS	R4, #0
MOVS	R5, #0
STRD	R4, R5, [SP, #28]
SUB	R11, R9, #1
; m end address is: 36 (R9)
; j start address is: 44 (R11)
; j end address is: 44 (R11)
L_bigi_div218:
; j start address is: 44 (R11)
CMP	R11, #0
IT	LT
BLT	L_bigi_div219
;bigi.c,1236 :: 		tmp =  ((k << MACHINE_WORD_BITLEN) + U->ary[j])/ V->ary[0];
LDRD	R4, R5, [SP, #28]
MOV	R7, R4
MOVS	R6, #0
LDR	R4, [SP, #40]
LDR	R5, [R4, #0]
LSL	R4, R11, #2
STR	R4, [SP, #68]
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOVS	R5, #0
ADDS	R0, R6, R4
ADC	R1, R7, R5, LSL #0
LDR	R4, [SP, #44]
LDR	R4, [R4, #0]
LDR	R4, [R4, #0]
MOV	R2, R4
MOVS	R3, #0
BL	__Div_64x64_S+0
;bigi.c,1237 :: 		QUO->ary[j] = (word_t)(tmp);
LDR	R4, [SP, #80]
LDR	R5, [R4, #0]
LDR	R4, [SP, #68]
ADDS	R4, R5, R4
STR	R0, [R4, #0]
;bigi.c,1238 :: 		k = ((k << MACHINE_WORD_BITLEN) + U->ary[j])- QUO->ary[j] * V->ary[0];
LDRD	R4, R5, [SP, #28]
MOV	R8, R4
MOVS	R7, #0
LDR	R4, [SP, #40]
LDR	R4, [R4, #0]
LSL	R6, R11, #2
ADDS	R4, R4, R6
LDR	R4, [R4, #0]
MOVS	R5, #0
ADDS	R9, R7, R4, LSL #0
ADC	R10, R8, R5, LSL #0
LDR	R4, [SP, #80]
LDR	R4, [R4, #0]
ADDS	R7, R4, R6
LDR	R4, [SP, #44]
LDR	R4, [R4, #0]
LDR	R5, [R4, #0]
LDR	R4, [R7, #0]
MULS	R4, R5, R4
MOVS	R5, #0
SUBS	R4, R9, R4, LSL #0
SBC	R5, R10, R5, LSL #0
STRD	R4, R5, [SP, #28]
;bigi.c,1234 :: 		for (k = 0, j = m - 1; j >= 0; j--)
SUB	R0, R11, #1
; j end address is: 44 (R11)
; j start address is: 0 (R0)
;bigi.c,1239 :: 		}
MOV	R11, R0
; j end address is: 0 (R0)
IT	AL
BAL	L_bigi_div218
L_bigi_div219:
;bigi.c,1241 :: 		if (REM != NULL)
LDR	R4, [SP, #84]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div221
;bigi.c,1242 :: 		REM->ary[0] = (word_t)(k);
LDR	R4, [SP, #84]
LDR	R5, [R4, #0]
LDR	R4, [SP, #28]
STR	R4, [R5, #0]
L_bigi_div221:
;bigi.c,1244 :: 		return OK;
IT	AL
BAL	L_end_bigi_div
;bigi.c,1245 :: 		}
L_bigi_div217:
;bigi.c,1250 :: 		s = nlz(V->ary[n-1]); /* 0 <= s <= 31. */
; n start address is: 44 (R11)
; m start address is: 36 (R9)
SUB	R6, R11, #1
LDR	R4, [SP, #44]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R0, R4
BL	_nlz+0
; s start address is: 0 (R0)
;bigi.c,1252 :: 		for (i = n - 1; i > 0; i--)
SUB	R4, R11, #1
STR	R4, [SP, #36]
; m end address is: 36 (R9)
; n end address is: 44 (R11)
; s end address is: 0 (R0)
MOV	R12, R9
L_bigi_div222:
; s start address is: 0 (R0)
; n start address is: 44 (R11)
; m start address is: 48 (R12)
LDR	R4, [SP, #36]
CMP	R4, #0
IT	LE
BLE	L_bigi_div223
;bigi.c,1255 :: 		VN->ary[i] = V->ary[i] << s;
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R6, R4, #2
ADDS	R5, R5, R6
LDR	R4, [SP, #44]
LDR	R4, [R4, #0]
ADDS	R4, R4, R6
LDR	R4, [R4, #0]
LSLS	R4, R0
STR	R4, [R5, #0]
;bigi.c,1256 :: 		tmp2 = (dword_t)(V->ary[i-1]) >> (MACHINE_WORD_BITLEN - s);
LDR	R4, [SP, #36]
SUBS	R6, R4, #1
LDR	R4, [SP, #44]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
RSB	R4, R0, #32
SXTH	R4, R4
ASRS	R5, R4, #31
SUBS	R9, R4, #32
BPL	L__bigi_div395
RSB	R9, R4, #32
LSL	R9, R7, R9
LSR	R8, R6, R4
ORR	R8, R8, R9
LSR	R9, R7, R4
B	L__bigi_div396
L__bigi_div395:
LSR	R8, R7, R9
MOVW	R9, #0
L__bigi_div396:
;bigi.c,1257 :: 		VN->ary[i] |= tmp2;
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R5, R5, R4
LDR	R4, [R5, #0]
ORR	R4, R4, R8, LSL #0
STR	R4, [R5, #0]
;bigi.c,1252 :: 		for (i = n - 1; i > 0; i--)
LDR	R4, [SP, #36]
SUBS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1258 :: 		}
IT	AL
BAL	L_bigi_div222
L_bigi_div223:
;bigi.c,1259 :: 		VN->ary[0] = V->ary[0] << s;
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LDR	R4, [SP, #44]
LDR	R4, [R4, #0]
LDR	R4, [R4, #0]
LSLS	R4, R0
STR	R4, [R5, #0]
;bigi.c,1261 :: 		UN->ary[m] = (dword_t)(U->ary[m-1]) >> (MACHINE_WORD_BITLEN - s);
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSL	R4, R12, #2
ADD	R10, R5, R4, LSL #0
SUB	R6, R12, #1
LDR	R4, [SP, #40]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R8, R4
MOVW	R9, #0
RSB	R4, R0, #32
SXTH	R4, R4
SXTH	R6, R4
ASRS	R7, R6, #31
SUBS	R5, R6, #32
BPL	L__bigi_div397
RSB	R5, R6, #32
LSL	R5, R9, R5
LSR	R4, R8, R6
ORRS	R4, R5
LSR	R5, R9, R6
B	L__bigi_div398
L__bigi_div397:
LSR	R4, R9, R5
MOVS	R5, #0
L__bigi_div398:
STR	R4, [R10, #0]
;bigi.c,1262 :: 		for (i = m - 1; i > 0; i--)
SUB	R4, R12, #1
STR	R4, [SP, #36]
; n end address is: 44 (R11)
; m end address is: 48 (R12)
; s end address is: 0 (R0)
MOV	R1, R11
L_bigi_div225:
; m start address is: 48 (R12)
; n start address is: 4 (R1)
; s start address is: 0 (R0)
LDR	R4, [SP, #36]
CMP	R4, #0
IT	LE
BLE	L_bigi_div226
;bigi.c,1265 :: 		UN->ary[i] = U->ary[i] << s;
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R6, R4, #2
ADDS	R5, R5, R6
LDR	R4, [SP, #40]
LDR	R4, [R4, #0]
ADDS	R4, R4, R6
LDR	R4, [R4, #0]
LSLS	R4, R0
STR	R4, [R5, #0]
;bigi.c,1266 :: 		tmp = (dword_t)(U->ary[i-1]) >> (MACHINE_WORD_BITLEN - s);
LDR	R4, [SP, #36]
SUBS	R6, R4, #1
LDR	R4, [SP, #40]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
RSB	R4, R0, #32
SXTH	R4, R4
ASRS	R5, R4, #31
SUBS	R9, R4, #32
BPL	L__bigi_div399
RSB	R9, R4, #32
LSL	R9, R7, R9
LSR	R8, R6, R4
ORR	R8, R8, R9
LSR	R9, R7, R4
B	L__bigi_div400
L__bigi_div399:
LSR	R8, R7, R9
MOVW	R9, #0
L__bigi_div400:
;bigi.c,1267 :: 		UN->ary[i] |= (word_t)(tmp);
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R5, R5, R4
LDR	R4, [R5, #0]
ORR	R4, R4, R8, LSL #0
STR	R4, [R5, #0]
;bigi.c,1262 :: 		for (i = m - 1; i > 0; i--)
LDR	R4, [SP, #36]
SUBS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1268 :: 		}
IT	AL
BAL	L_bigi_div225
L_bigi_div226:
;bigi.c,1269 :: 		UN->ary[0] = U->ary[0] << s;
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LDR	R4, [SP, #40]
LDR	R4, [R4, #0]
LDR	R4, [R4, #0]
LSLS	R4, R0
STR	R4, [R5, #0]
;bigi.c,1271 :: 		for (j = m - n; j >= 0; j--)
SUB	R11, R12, R1, LSL #0
; m end address is: 48 (R12)
; j start address is: 44 (R11)
; s end address is: 0 (R0)
; n end address is: 4 (R1)
; j end address is: 44 (R11)
MOV	R12, R1
UXTB	R10, R0
L_bigi_div228:
; j start address is: 44 (R11)
; s start address is: 40 (R10)
; n start address is: 48 (R12)
CMP	R11, #0
IT	LT
BLT	L_bigi_div229
;bigi.c,1274 :: 		qhat  = (dword_t)(UN->ary[j+n]);
ADD	R9, R11, R12, LSL #0
LDR	R4, [SP, #48]
LDR	R8, [R4, #0]
LSL	R4, R9, #2
ADD	R4, R8, R4, LSL #0
LDR	R4, [R4, #0]
STR	R4, [SP, #76]
MOVS	R5, #0
;bigi.c,1275 :: 		qhat <<= MACHINE_WORD_BITLEN;
MOV	R7, R4
MOVS	R6, #0
;bigi.c,1276 :: 		qhat += (dword_t)(UN->ary[j+n-1]);
SUB	R4, R9, #1
LSLS	R4, R4, #2
ADD	R4, R8, R4, LSL #0
LDR	R4, [R4, #0]
STR	R4, [SP, #72]
MOVS	R5, #0
ADDS	R0, R6, R4
ADC	R1, R7, R5, LSL #0
;bigi.c,1277 :: 		qhat /= VN->ary[n-1];
SUB	R6, R12, #1
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
STR	R4, [SP, #68]
MOV	R2, R4
MOVS	R3, #0
BL	__Div_64x64_U+0
STRD	R0, R1, [SP, #4]
;bigi.c,1280 :: 		rhat  = (dword_t)(UN->ary[j+n]);
LDR	R4, [SP, #76]
MOVS	R5, #0
;bigi.c,1281 :: 		rhat <<= MACHINE_WORD_BITLEN;
MOV	R7, R4
MOVS	R6, #0
;bigi.c,1282 :: 		rhat += (dword_t)(UN->ary[j+n-1]);
LDR	R4, [SP, #72]
MOVS	R5, #0
ADDS	R8, R6, R4, LSL #0
ADC	R9, R7, R5, LSL #0
;bigi.c,1283 :: 		rhat -= qhat * (dword_t)(VN->ary[n-1]);
LDR	R4, [SP, #68]
MOV	R6, R4
MOVS	R7, #0
UMULL	R4, R5, R0, R6
MLA	R5, R1, R6, R5
MLA	R5, R0, R7, R5
SUBS	R4, R8, R4, LSL #0
SBC	R5, R9, R5, LSL #0
STRD	R4, R5, [SP, #12]
; n end address is: 48 (R12)
; s end address is: 40 (R10)
; j end address is: 44 (R11)
MOV	R0, R11
UXTB	R2, R10
MOV	R1, R12
;bigi.c,1286 :: 		again:
___bigi_div_again:
;bigi.c,1287 :: 		tmp1_big = qhat;
; s start address is: 8 (R2)
; n start address is: 4 (R1)
; s start address is: 8 (R2)
; s end address is: 8 (R2)
; j start address is: 0 (R0)
;bigi.c,1288 :: 		tmp1_big *= (dword_t)(VN->ary[n-2]);
SUBS	R6, R1, #2
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R8, R4
MOVW	R9, #0
LDRD	R6, R7, [SP, #4]
UMULL	R4, R5, R6, R8
MLA	R5, R7, R8, R5
MLA	R5, R6, R9, R5
; tmp1_big start address is: 36 (R9)
MOV	R9, R4
MOV	R10, R5
;bigi.c,1289 :: 		tmp2_big = (rhat << MACHINE_WORD_BITLEN);
LDRD	R4, R5, [SP, #12]
MOV	R8, R4
MOVS	R7, #0
;bigi.c,1290 :: 		tmp2_big += UN->ary[j+n-2];
ADDS	R4, R0, R1
SUBS	R6, R4, #2
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOVS	R5, #0
ADDS	R4, R7, R4, LSL #0
ADC	R5, R8, R5, LSL #0
; tmp2_big start address is: 24 (R6)
MOV	R6, R4
MOV	R7, R5
;bigi.c,1293 :: 		if ((qhat >= b1) || (tmp1_big > tmp2_big))
LDRD	R4, R5, [SP, #4]
SUBS	R4, R4, #0
SBCS	R4, R5, #1
IT	CS
BCS	L__bigi_div328
; s end address is: 8 (R2)
; s start address is: 8 (R2)
SUBS	R4, R6, R9, LSL #0
SBCS	R4, R7, R10, LSL #0
IT	CC
BCC	L__bigi_div327
; tmp1_big end address is: 36 (R9)
; tmp2_big end address is: 24 (R6)
IT	AL
BAL	L_bigi_div233
L__bigi_div328:
L__bigi_div327:
;bigi.c,1295 :: 		qhat--;
LDRD	R4, R5, [SP, #4]
SUBS	R4, R4, #1
SBC	R5, R5, #0
STRD	R4, R5, [SP, #4]
;bigi.c,1296 :: 		rhat += (dword_t)(VN->ary[n-1]);
SUBS	R6, R1, #1
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
LDRD	R4, R5, [SP, #12]
ADDS	R4, R4, R6
ADCS	R5, R7
STRD	R4, R5, [SP, #12]
;bigi.c,1297 :: 		if (rhat < b1)
SUBS	R4, R4, #0
SBCS	R4, R5, #1
IT	CS
BCS	L_bigi_div234
;bigi.c,1298 :: 		goto again;
IT	AL
BAL	___bigi_div_again
L_bigi_div234:
;bigi.c,1299 :: 		}
L_bigi_div233:
;bigi.c,1301 :: 		for (k = 0, i = 0; i < n; i++)
MOVS	R4, #0
MOVS	R5, #0
STRD	R4, R5, [SP, #28]
MOVS	R4, #0
STR	R4, [SP, #36]
; s end address is: 8 (R2)
; n end address is: 4 (R1)
; j end address is: 0 (R0)
UXTB	R3, R2
L_bigi_div235:
; j start address is: 0 (R0)
; s start address is: 12 (R3)
; n start address is: 4 (R1)
LDR	R4, [SP, #36]
CMP	R4, R1
IT	GE
BGE	L_bigi_div236
;bigi.c,1303 :: 		p = qhat * VN->ary[i];
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
LDRD	R4, R5, [SP, #4]
UMULL	R9, R10, R4, R6
MLA	R10, R5, R6, R10
MLA	R10, R4, R7, R10
STRD	R9, R10, [SP, #20]
;bigi.c,1304 :: 		t = (dword_t)(UN->ary[i+j]) - k - (p & MAX_VAL);
LDR	R4, [SP, #36]
ADDS	R6, R4, R0
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADD	R8, R5, R4, LSL #0
LDR	R4, [R8, #0]
MOV	R6, R4
MOVS	R7, #0
LDRD	R4, R5, [SP, #28]
SUBS	R6, R6, R4
SBCS	R7, R5
AND	R4, R9, #-1
AND	R5, R10, #0
SUBS	R4, R6, R4
SBC	R5, R7, R5, LSL #0
; t start address is: 36 (R9)
MOV	R9, R4
MOV	R10, R5
;bigi.c,1305 :: 		UN->ary[i+j] = (word_t)(t);
STR	R4, [R8, #0]
;bigi.c,1306 :: 		k = (p >> MACHINE_WORD_BITLEN) - (t >> MACHINE_WORD_BITLEN);
LDRD	R4, R5, [SP, #20]
MOV	R6, R5
MOVS	R7, #0
MOV	R4, R10
ASR	R5, R10, #31
; t end address is: 36 (R9)
SUBS	R4, R6, R4
SBC	R5, R7, R5, LSL #0
STRD	R4, R5, [SP, #28]
;bigi.c,1301 :: 		for (k = 0, i = 0; i < n; i++)
LDR	R4, [SP, #36]
ADDS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1307 :: 		}
IT	AL
BAL	L_bigi_div235
L_bigi_div236:
;bigi.c,1308 :: 		t = (dword_t)(UN->ary[j+n]) - k;
ADDS	R6, R0, R1
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADD	R8, R5, R4, LSL #0
LDR	R4, [R8, #0]
MOV	R6, R4
MOVS	R7, #0
LDRD	R4, R5, [SP, #28]
SUBS	R4, R6, R4
SBC	R5, R7, R5, LSL #0
; t start address is: 24 (R6)
MOV	R6, R4
MOV	R7, R5
;bigi.c,1309 :: 		UN->ary[j+n] = (word_t)(t);
STR	R4, [R8, #0]
;bigi.c,1310 :: 		QUO->ary[j] = (word_t)(qhat);
LDR	R4, [SP, #80]
LDR	R5, [R4, #0]
LSLS	R4, R0, #2
ADDS	R5, R5, R4
LDR	R4, [SP, #4]
STR	R4, [R5, #0]
;bigi.c,1312 :: 		if (t < 0)
SUBS	R4, R6, #0
SBCS	R4, R7, #0
IT	GE
BGE	L__bigi_div331
; t end address is: 24 (R6)
;bigi.c,1314 :: 		QUO->ary[j]--;
LDR	R4, [SP, #80]
LDR	R5, [R4, #0]
LSLS	R4, R0, #2
ADDS	R5, R5, R4
LDR	R4, [R5, #0]
SUBS	R4, R4, #1
STR	R4, [R5, #0]
;bigi.c,1315 :: 		for (k = 0, i = 0; i < n; i++)
MOVS	R4, #0
MOVS	R5, #0
STRD	R4, R5, [SP, #28]
MOVS	R4, #0
STR	R4, [SP, #36]
; n end address is: 4 (R1)
; j end address is: 0 (R0)
; s end address is: 12 (R3)
MOV	R2, R0
UXTB	R9, R3
MOV	R10, R1
L_bigi_div239:
; n start address is: 40 (R10)
; s start address is: 36 (R9)
; j start address is: 8 (R2)
LDR	R4, [SP, #36]
CMP	R4, R10
IT	GE
BGE	L_bigi_div240
;bigi.c,1317 :: 		t = (dword_t)(UN->ary[i+j]);
LDR	R4, [SP, #36]
ADDS	R6, R4, R2
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADD	R8, R5, R4, LSL #0
LDR	R4, [R8, #0]
MOV	R6, R4
MOVS	R7, #0
;bigi.c,1318 :: 		t += (dword_t)(VN->ary[i]);
LDR	R4, [SP, #52]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOVS	R5, #0
ADDS	R6, R6, R4
ADCS	R7, R5
;bigi.c,1319 :: 		t += k;
LDRD	R4, R5, [SP, #28]
ADDS	R4, R6, R4
ADC	R5, R7, R5, LSL #0
; t start address is: 0 (R0)
MOV	R0, R4
MOV	R1, R5
;bigi.c,1320 :: 		UN->ary[i+j] = t;
STR	R4, [R8, #0]
;bigi.c,1321 :: 		k = t >> MACHINE_WORD_BITLEN;
MOV	R4, R1
ASRS	R5, R1, #31
; t end address is: 0 (R0)
STRD	R4, R5, [SP, #28]
;bigi.c,1315 :: 		for (k = 0, i = 0; i < n; i++)
LDR	R4, [SP, #36]
ADDS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1322 :: 		}
IT	AL
BAL	L_bigi_div239
L_bigi_div240:
;bigi.c,1323 :: 		UN->ary[j+n] = UN->ary[j+n] + k;
ADD	R6, R2, R10, LSL #0
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R6, R5, R4
LDR	R5, [R6, #0]
LDRD	R4, R5, [SP, #28]
ADDS	R4, R5, R4
ADC	R5, R6, R5, LSL #0
STR	R4, [R6, #0]
; n end address is: 40 (R10)
; s end address is: 36 (R9)
; j end address is: 8 (R2)
MOV	R0, R2
MOV	R12, R10
UXTB	R10, R9
;bigi.c,1324 :: 		}
IT	AL
BAL	L_bigi_div238
L__bigi_div331:
;bigi.c,1312 :: 		if (t < 0)
MOV	R12, R1
UXTB	R10, R3
;bigi.c,1324 :: 		}
L_bigi_div238:
;bigi.c,1271 :: 		for (j = m - n; j >= 0; j--)
; n start address is: 48 (R12)
; s start address is: 40 (R10)
; j start address is: 0 (R0)
SUBS	R4, R0, #1
; j end address is: 0 (R0)
; j start address is: 44 (R11)
MOV	R11, R4
;bigi.c,1325 :: 		} /* End j. */
; j end address is: 44 (R11)
IT	AL
BAL	L_bigi_div228
L_bigi_div229:
;bigi.c,1327 :: 		if (REM != NULL)
LDR	R4, [SP, #84]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_div242
;bigi.c,1329 :: 		for (i = 0; i < n-1; i++)
MOVS	R4, #0
STR	R4, [SP, #36]
; n end address is: 48 (R12)
; s end address is: 40 (R10)
UXTB	R3, R10
MOV	R2, R12
L_bigi_div243:
; n start address is: 8 (R2)
; s start address is: 12 (R3)
SUBS	R5, R2, #1
LDR	R4, [SP, #36]
CMP	R4, R5
IT	GE
BGE	L_bigi_div244
;bigi.c,1331 :: 		tmp = UN->ary[i] >> s;
LDR	R4, [SP, #48]
LDR	R6, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R5, R4, #2
ADDS	R4, R6, R5
LDR	R4, [R4, #0]
LSRS	R4, R3
; tmp start address is: 0 (R0)
MOV	R0, R4
MOVS	R1, #0
;bigi.c,1332 :: 		REM->ary[i] = (word_t)(tmp);
LDR	R4, [SP, #84]
LDR	R4, [R4, #0]
ADDS	R4, R4, R5
STR	R0, [R4, #0]
; tmp end address is: 0 (R0)
;bigi.c,1333 :: 		tmp = (dword_t)(UN->ary[i+1]) << (MACHINE_WORD_BITLEN - s);
LDR	R4, [SP, #36]
ADDS	R6, R4, #1
LDR	R4, [SP, #48]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
RSB	R4, R3, #32
SXTH	R4, R4
ASRS	R5, R4, #31
SUBS	R8, R4, #32
BPL	L__bigi_div401
RSB	R8, R4, #32
LSR	R8, R6, R8
LSL	R9, R7, R4
ORR	R9, R9, R8
LSL	R8, R6, R4
B	L__bigi_div402
L__bigi_div401:
LSL	R9, R6, R8
MOVW	R8, #0
L__bigi_div402:
;bigi.c,1334 :: 		REM->ary[i] |= (word_t)(tmp);
LDR	R4, [SP, #84]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
LSLS	R4, R4, #2
ADDS	R5, R5, R4
LDR	R4, [R5, #0]
ORR	R4, R4, R8, LSL #0
STR	R4, [R5, #0]
;bigi.c,1329 :: 		for (i = 0; i < n-1; i++)
LDR	R4, [SP, #36]
ADDS	R4, R4, #1
STR	R4, [SP, #36]
;bigi.c,1335 :: 		}
IT	AL
BAL	L_bigi_div243
L_bigi_div244:
;bigi.c,1336 :: 		REM->ary[n-1] = UN->ary[n-1] >> s;
SUBS	R5, R2, #1
; n end address is: 8 (R2)
LDR	R4, [SP, #84]
LDR	R4, [R4, #0]
LSLS	R6, R5, #2
ADDS	R5, R4, R6
LDR	R4, [SP, #48]
LDR	R4, [R4, #0]
ADDS	R4, R4, R6
LDR	R4, [R4, #0]
LSRS	R4, R3
; s end address is: 12 (R3)
STR	R4, [R5, #0]
;bigi.c,1337 :: 		}
L_bigi_div242:
;bigi.c,1339 :: 		return OK;
;bigi.c,1340 :: 		}
L_end_bigi_div:
LDR	LR, [SP, #0]
ADD	SP, SP, #80
BX	LR
; end of _bigi_div
_bigi_gcd:
;bigi.c,1346 :: 		bigi_t *const RES)
; tmparylen start address is: 12 (R3)
; TMPARY start address is: 8 (R2)
; B start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #24
STR	LR, [SP, #0]
STR	R3, [SP, #4]
MOV	R3, R2
MOV	R2, R1
MOV	R1, R0
LDR	R0, [SP, #4]
; tmparylen end address is: 12 (R3)
; TMPARY end address is: 8 (R2)
; B end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 4 (R1)
; B start address is: 8 (R2)
; TMPARY start address is: 12 (R3)
; tmparylen start address is: 0 (R0)
LDR	R4, [SP, #24]
STR	R4, [SP, #24]
;bigi.c,1348 :: 		bigi_t * A_big = NULL;
;bigi.c,1349 :: 		bigi_t * B_big = NULL;
;bigi.c,1352 :: 		cmp_t cmp = CMP_UNDEFINED;
MOVS	R4, #11
STRB	R4, [SP, #16]
;bigi.c,1373 :: 		for (i = 0; i < tmparylen; i++)
MOVS	R4, #0
STR	R4, [SP, #12]
; A end address is: 4 (R1)
; B end address is: 8 (R2)
; TMPARY end address is: 12 (R3)
; tmparylen end address is: 0 (R0)
MOV	R7, R1
MOV	R6, R2
MOV	R8, R3
L_bigi_gcd246:
; tmparylen start address is: 0 (R0)
; TMPARY start address is: 32 (R8)
; B start address is: 24 (R6)
; A start address is: 28 (R7)
LDR	R4, [SP, #12]
CMP	R4, R0
IT	CS
BCS	L_bigi_gcd247
;bigi.c,1375 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LDR	R4, [SP, #12]
LSLS	R4, R4, #4
ADD	R4, R8, R4, LSL #0
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1376 :: 		TMPARY[i].wordlen = A->wordlen;
LDR	R4, [SP, #12]
LSLS	R4, R4, #4
ADD	R4, R8, R4, LSL #0
ADDS	R5, R4, #4
ADDS	R4, R7, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1373 :: 		for (i = 0; i < tmparylen; i++)
LDR	R4, [SP, #12]
ADDS	R4, R4, #1
STR	R4, [SP, #12]
;bigi.c,1377 :: 		}
; tmparylen end address is: 0 (R0)
IT	AL
BAL	L_bigi_gcd246
L_bigi_gcd247:
;bigi.c,1378 :: 		RES->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #24]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1380 :: 		A_big = &TMPARY[0];
STR	R8, [SP, #20]
;bigi.c,1381 :: 		B_big = &TMPARY[1];
ADD	R8, R8, #16
; TMPARY end address is: 32 (R8)
; B_big start address is: 32 (R8)
;bigi.c,1384 :: 		BIGI_CALL(bigi_is_zero(A, &cmp))
ADD	R4, SP, #16
MOV	R1, R4
MOV	R0, R7
BL	_bigi_is_zero+0
;bigi.c,1385 :: 		if (cmp == CMP_EQUAL)
LDRB	R4, [SP, #16]
CMP	R4, #0
IT	NE
BNE	L_bigi_gcd249
; A end address is: 28 (R7)
; B_big end address is: 32 (R8)
;bigi.c,1387 :: 		BIGI_CALL(bigi_copy(RES, B))
MOV	R1, R6
; B end address is: 24 (R6)
LDR	R0, [SP, #24]
BL	_bigi_copy+0
;bigi.c,1388 :: 		return OK;
IT	AL
BAL	L_end_bigi_gcd
;bigi.c,1389 :: 		}
L_bigi_gcd249:
;bigi.c,1390 :: 		BIGI_CALL(bigi_is_zero(B, &cmp))
; B_big start address is: 32 (R8)
; A start address is: 28 (R7)
; B start address is: 24 (R6)
ADD	R4, SP, #16
MOV	R1, R4
MOV	R0, R6
BL	_bigi_is_zero+0
;bigi.c,1391 :: 		if (cmp == CMP_EQUAL)
LDRB	R4, [SP, #16]
CMP	R4, #0
IT	NE
BNE	L_bigi_gcd250
; B end address is: 24 (R6)
; B_big end address is: 32 (R8)
;bigi.c,1393 :: 		BIGI_CALL(bigi_copy(RES, A))
MOV	R1, R7
; A end address is: 28 (R7)
LDR	R0, [SP, #24]
BL	_bigi_copy+0
;bigi.c,1394 :: 		return OK;
IT	AL
BAL	L_end_bigi_gcd
;bigi.c,1395 :: 		}
L_bigi_gcd250:
;bigi.c,1397 :: 		BIGI_CALL(bigi_copy(A_big, A))
; B_big start address is: 32 (R8)
; A start address is: 28 (R7)
; B start address is: 24 (R6)
MOV	R1, R7
; A end address is: 28 (R7)
LDR	R0, [SP, #20]
BL	_bigi_copy+0
;bigi.c,1398 :: 		BIGI_CALL(bigi_copy(B_big, B))
MOV	R1, R6
; B end address is: 24 (R6)
MOV	R0, R8
BL	_bigi_copy+0
;bigi.c,1401 :: 		for (shift = 0; shift < A_big->wordlen + MULT_BUFFER_WORDLEN; shift++)
; shift start address is: 44 (R11)
MOVW	R11, #0
; shift end address is: 44 (R11)
; B_big end address is: 32 (R8)
MOV	R9, R11
L_bigi_gcd251:
; shift start address is: 36 (R9)
; B_big start address is: 32 (R8)
LDR	R4, [SP, #20]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
ADDS	R4, R4, #3
CMP	R9, R4
IT	CS
BCS	L__bigi_gcd353
;bigi.c,1403 :: 		if ((A_big->ary[shift] != 0u) || (B_big->ary[shift] != 0u))
LDR	R4, [SP, #20]
LDR	R5, [R4, #0]
LSL	R4, R9, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	NE
BNE	L__bigi_gcd352
LDR	R5, [R8, #0]
LSL	R4, R9, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	NE
BNE	L__bigi_gcd351
IT	AL
BAL	L_bigi_gcd256
L__bigi_gcd352:
L__bigi_gcd351:
;bigi.c,1408 :: 		i = 0;
MOVS	R4, #0
STR	R4, [SP, #12]
;bigi.c,1409 :: 		a1 = A_big->ary[shift];
LDR	R4, [SP, #20]
LDR	R4, [R4, #0]
LSL	R5, R9, #2
ADDS	R4, R4, R5
LDR	R7, [R4, #0]
;bigi.c,1410 :: 		b1 = B_big->ary[shift];
LDR	R4, [R8, #0]
ADDS	R4, R4, R5
LDR	R6, [R4, #0]
;bigi.c,1414 :: 		index_a = MultiplyDeBruijnBitPosition[((word_t)((a1 & -a1) * 0x077CB531U)) >> 27];
RSBS	R4, R7, #0
AND	R5, R7, R4, LSL #0
MOVW	R4, #46385
MOVT	R4, #1916
MULS	R4, R5, R4
LSRS	R4, R4, #27
LSLS	R5, R4, #2
MOVW	R4, #lo_addr(bigi_MultiplyDeBruijnBitPosition+0)
MOVT	R4, #hi_addr(bigi_MultiplyDeBruijnBitPosition+0)
ADDS	R4, R4, R5
LDR	R4, [R4, #0]
; index_a start address is: 0 (R0)
MOV	R0, R4
;bigi.c,1415 :: 		index_b = MultiplyDeBruijnBitPosition[((word_t)((b1 & -b1) * 0x077CB531U)) >> 27];
RSBS	R4, R6, #0
AND	R5, R6, R4, LSL #0
MOVW	R4, #46385
MOVT	R4, #1916
MULS	R4, R5, R4
LSRS	R4, R4, #27
LSLS	R5, R4, #2
MOVW	R4, #lo_addr(bigi_MultiplyDeBruijnBitPosition+0)
MOVT	R4, #hi_addr(bigi_MultiplyDeBruijnBitPosition+0)
ADDS	R4, R4, R5
LDR	R4, [R4, #0]
; index_b start address is: 4 (R1)
MOV	R1, R4
;bigi.c,1417 :: 		if (A_big->ary[shift] == 0)
CMP	R7, #0
IT	NE
BNE	L_bigi_gcd257
; index_a end address is: 0 (R0)
;bigi.c,1418 :: 		i = index_b;
STR	R1, [SP, #12]
; index_b end address is: 4 (R1)
IT	AL
BAL	L_bigi_gcd258
L_bigi_gcd257:
;bigi.c,1419 :: 		else if (B_big->ary[shift] == 0)
; index_b start address is: 4 (R1)
; index_a start address is: 0 (R0)
LDR	R5, [R8, #0]
LSL	R4, R9, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	NE
BNE	L_bigi_gcd259
; index_b end address is: 4 (R1)
;bigi.c,1420 :: 		i = index_a;
STR	R0, [SP, #12]
; index_a end address is: 0 (R0)
IT	AL
BAL	L_bigi_gcd260
L_bigi_gcd259:
;bigi.c,1421 :: 		else if (index_a < index_b)
; index_b start address is: 4 (R1)
; index_a start address is: 0 (R0)
CMP	R0, R1
IT	CS
BCS	L_bigi_gcd261
; index_b end address is: 4 (R1)
;bigi.c,1422 :: 		i = index_a;
STR	R0, [SP, #12]
; index_a end address is: 0 (R0)
IT	AL
BAL	L_bigi_gcd262
L_bigi_gcd261:
;bigi.c,1424 :: 		i = index_b;
; index_b start address is: 4 (R1)
STR	R1, [SP, #12]
; index_b end address is: 4 (R1)
L_bigi_gcd262:
L_bigi_gcd260:
L_bigi_gcd258:
;bigi.c,1426 :: 		shift = shift * MACHINE_WORD_BITLEN + i;
LSL	R7, R9, #5
; shift end address is: 36 (R9)
LDR	R4, [SP, #12]
ADDS	R7, R7, R4
; shift start address is: 28 (R7)
;bigi.c,1427 :: 		break;
; shift end address is: 28 (R7)
IT	AL
BAL	L_bigi_gcd252
;bigi.c,1428 :: 		}
L_bigi_gcd256:
;bigi.c,1401 :: 		for (shift = 0; shift < A_big->wordlen + MULT_BUFFER_WORDLEN; shift++)
; shift start address is: 36 (R9)
ADD	R4, R9, #1
; shift end address is: 36 (R9)
; shift start address is: 44 (R11)
MOV	R11, R4
;bigi.c,1429 :: 		}
; shift end address is: 44 (R11)
MOV	R9, R11
IT	AL
BAL	L_bigi_gcd251
L__bigi_gcd353:
;bigi.c,1401 :: 		for (shift = 0; shift < A_big->wordlen + MULT_BUFFER_WORDLEN; shift++)
MOV	R7, R9
;bigi.c,1429 :: 		}
L_bigi_gcd252:
;bigi.c,1431 :: 		BIGI_CALL(bigi_shift_right(A_big, shift))
; shift start address is: 28 (R7)
MOV	R1, R7
LDR	R0, [SP, #20]
BL	_bigi_shift_right+0
;bigi.c,1432 :: 		BIGI_CALL(bigi_shift_right(B_big, shift))
MOV	R1, R7
MOV	R0, R8
BL	_bigi_shift_right+0
;bigi.c,1434 :: 		BIGI_CALL(bigi_get_bit(A_big, 0, &i))
ADD	R4, SP, #12
MOV	R2, R4
MOVS	R1, #0
LDR	R0, [SP, #20]
BL	_bigi_get_bit+0
; shift end address is: 28 (R7)
; B_big end address is: 32 (R8)
;bigi.c,1436 :: 		while (i == 0) /* TODO: this could be more effective for long sequences of zeroes */
L_bigi_gcd263:
; shift start address is: 28 (R7)
; B_big start address is: 32 (R8)
LDR	R4, [SP, #12]
CMP	R4, #0
IT	NE
BNE	L_bigi_gcd264
;bigi.c,1438 :: 		BIGI_CALL(bigi_shift_right(A_big, 1))
MOVS	R1, #1
LDR	R0, [SP, #20]
BL	_bigi_shift_right+0
;bigi.c,1439 :: 		BIGI_CALL(bigi_get_bit(A_big, 0, &i))
ADD	R4, SP, #12
MOV	R2, R4
MOVS	R1, #0
LDR	R0, [SP, #20]
BL	_bigi_get_bit+0
;bigi.c,1440 :: 		}
IT	AL
BAL	L_bigi_gcd263
L_bigi_gcd264:
;bigi.c,1442 :: 		do
MOV	R0, R7
; B_big end address is: 32 (R8)
MOV	R1, R8
IT	AL
BAL	L_bigi_gcd265
; shift end address is: 28 (R7)
L__bigi_gcd354:
;bigi.c,1464 :: 		} while (cmp == CMP_NEQ);
MOV	R1, R0
MOV	R0, R12
;bigi.c,1442 :: 		do
L_bigi_gcd265:
;bigi.c,1444 :: 		BIGI_CALL(bigi_get_bit(B_big, 0, &i))
; B_big start address is: 4 (R1)
; shift start address is: 0 (R0)
ADD	R4, SP, #12
STR	R0, [SP, #4]
STR	R1, [SP, #8]
MOV	R2, R4
MOV	R0, R1
MOVS	R1, #0
BL	_bigi_get_bit+0
; shift end address is: 0 (R0)
; B_big end address is: 4 (R1)
LDR	R1, [SP, #8]
LDR	R0, [SP, #4]
MOV	R12, R0
MOV	R0, R1
;bigi.c,1446 :: 		while (i == 0) /* TODO: same applies here */
L_bigi_gcd268:
; shift start address is: 48 (R12)
; B_big start address is: 0 (R0)
LDR	R4, [SP, #12]
CMP	R4, #0
IT	NE
BNE	L_bigi_gcd269
;bigi.c,1448 :: 		BIGI_CALL(bigi_shift_right(B_big, 1))
STR	R0, [SP, #4]
MOVS	R1, #1
BL	_bigi_shift_right+0
LDR	R0, [SP, #4]
;bigi.c,1449 :: 		BIGI_CALL(bigi_get_bit(B_big, 0, &i))
ADD	R4, SP, #12
STR	R0, [SP, #4]
MOV	R2, R4
MOVS	R1, #0
BL	_bigi_get_bit+0
LDR	R0, [SP, #4]
;bigi.c,1450 :: 		}
IT	AL
BAL	L_bigi_gcd268
L_bigi_gcd269:
;bigi.c,1452 :: 		BIGI_CALL(bigi_cmp(A_big, B_big, &cmp))
ADD	R4, SP, #16
STR	R0, [SP, #4]
MOV	R2, R4
MOV	R1, R0
LDR	R0, [SP, #20]
BL	_bigi_cmp+0
LDR	R0, [SP, #4]
;bigi.c,1454 :: 		if (cmp == CMP_GREATER)
LDRB	R4, [SP, #16]
CMP	R4, #3
IT	NE
BNE	L__bigi_gcd355
;bigi.c,1457 :: 		A_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
LDR	R4, [SP, #20]
EOR	R5, R4, R0, LSL #0
;bigi.c,1458 :: 		B_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
EOR	R4, R5, R0, LSL #0
; B_big end address is: 0 (R0)
; B_big start address is: 4 (R1)
MOV	R1, R4
;bigi.c,1459 :: 		A_big = (bigi_t *)((uintptr_t)A_big ^ (uintptr_t)B_big);
EOR	R4, R5, R4, LSL #0
STR	R4, [SP, #20]
; B_big end address is: 4 (R1)
MOV	R0, R1
;bigi.c,1460 :: 		}
IT	AL
BAL	L_bigi_gcd270
L__bigi_gcd355:
;bigi.c,1454 :: 		if (cmp == CMP_GREATER)
;bigi.c,1460 :: 		}
L_bigi_gcd270:
;bigi.c,1461 :: 		BIGI_CALL(bigi_sub(B_big, A_big, B_big)) /* TODO: check if this works properly */
; B_big start address is: 0 (R0)
STR	R0, [SP, #4]
MOV	R2, R0
LDR	R1, [SP, #20]
BL	_bigi_sub+0
LDR	R0, [SP, #4]
;bigi.c,1463 :: 		BIGI_CALL(bigi_is_zero(B_big, &cmp))
ADD	R4, SP, #16
STR	R0, [SP, #4]
MOV	R1, R4
BL	_bigi_is_zero+0
LDR	R0, [SP, #4]
;bigi.c,1464 :: 		} while (cmp == CMP_NEQ);
LDRB	R4, [SP, #16]
CMP	R4, #1
IT	EQ
BEQ	L__bigi_gcd354
; B_big end address is: 0 (R0)
;bigi.c,1466 :: 		BIGI_CALL(bigi_shift_left(A_big, shift))
MOV	R1, R12
; shift end address is: 48 (R12)
LDR	R0, [SP, #20]
BL	_bigi_shift_left+0
;bigi.c,1467 :: 		BIGI_CALL(bigi_copy(RES, A_big))
LDR	R1, [SP, #20]
LDR	R0, [SP, #24]
BL	_bigi_copy+0
;bigi.c,1469 :: 		return OK;
;bigi.c,1470 :: 		}
L_end_bigi_gcd:
LDR	LR, [SP, #0]
ADD	SP, SP, #24
BX	LR
; end of _bigi_gcd
_bigi_get_mont_params:
;bigi.c,1482 :: 		word_t *const mu)
; TMPARY start address is: 4 (R1)
; MOD start address is: 0 (R0)
SUB	SP, SP, #36
STR	LR, [SP, #0]
MOV	R6, R1
MOV	R1, R0
STR	R2, [SP, #12]
STR	R3, [SP, #16]
; TMPARY end address is: 4 (R1)
; MOD end address is: 0 (R0)
; MOD start address is: 4 (R1)
; TMPARY start address is: 24 (R6)
LDR	R4, [SP, #36]
STR	R4, [SP, #36]
;bigi.c,1488 :: 		bigi_t * POW2 = NULL;
;bigi.c,1489 :: 		bigi_t * X = NULL;
;bigi.c,1490 :: 		bigi_t * Y = NULL;
;bigi.c,1491 :: 		bigi_t * Z = NULL;
;bigi.c,1509 :: 		for (i = 0; i < MONT_PARAMS__MIN_TMPARY_LEN; i++)
; i start address is: 0 (R0)
MOVS	R0, #0
; TMPARY end address is: 24 (R6)
; i end address is: 0 (R0)
; MOD end address is: 4 (R1)
MOV	R7, R1
L_bigi_get_mont_params271:
; i start address is: 0 (R0)
; TMPARY start address is: 24 (R6)
; MOD start address is: 28 (R7)
CMP	R0, #16
IT	GE
BGE	L_bigi_get_mont_params272
;bigi.c,1511 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LSLS	R4, R0, #4
ADDS	R4, R6, R4
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1512 :: 		TMPARY[i].wordlen = MOD->wordlen;
LSLS	R4, R0, #4
ADDS	R4, R6, R4
ADDS	R5, R4, #4
ADDS	R4, R7, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1509 :: 		for (i = 0; i < MONT_PARAMS__MIN_TMPARY_LEN; i++)
ADDS	R4, R0, #1
; i end address is: 0 (R0)
; i start address is: 8 (R2)
MOV	R2, R4
;bigi.c,1513 :: 		}
; i end address is: 8 (R2)
MOV	R0, R2
IT	AL
BAL	L_bigi_get_mont_params271
L_bigi_get_mont_params272:
;bigi.c,1515 :: 		POW2    = &TMPARY[0];
STR	R6, [SP, #20]
;bigi.c,1516 :: 		X       = &TMPARY[1];
ADDW	R4, R6, #16
STR	R4, [SP, #24]
;bigi.c,1517 :: 		Y       = &TMPARY[2];
ADDW	R4, R6, #32
STR	R4, [SP, #28]
;bigi.c,1518 :: 		Z       = &TMPARY[3];
ADDW	R4, R6, #48
STR	R4, [SP, #32]
;bigi.c,1521 :: 		BIGI_CALL(bigi_set_zero(POW2))
MOV	R0, R6
BL	_bigi_set_zero+0
;bigi.c,1522 :: 		i = MOD->wordlen - 1;
ADDS	R4, R7, #4
LDR	R4, [R4, #0]
SUBS	R0, R4, #1
; i start address is: 0 (R0)
; TMPARY end address is: 24 (R6)
; MOD end address is: 28 (R7)
; i end address is: 0 (R0)
MOV	R2, R6
MOV	R1, R7
;bigi.c,1523 :: 		while ((i >= 0) && (MOD->ary[i] == 0u))
L_bigi_get_mont_params274:
; i start address is: 0 (R0)
; MOD start address is: 4 (R1)
; TMPARY start address is: 8 (R2)
CMP	R0, #0
IT	LT
BLT	L__bigi_get_mont_params358
LDR	R5, [R1, #0]
LSLS	R4, R0, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	NE
BNE	L__bigi_get_mont_params357
L__bigi_get_mont_params356:
;bigi.c,1525 :: 		i--;
SUBS	R4, R0, #1
; i end address is: 0 (R0)
; i start address is: 32 (R8)
MOV	R8, R4
;bigi.c,1526 :: 		}
; i end address is: 32 (R8)
MOV	R0, R8
IT	AL
BAL	L_bigi_get_mont_params274
;bigi.c,1523 :: 		while ((i >= 0) && (MOD->ary[i] == 0u))
L__bigi_get_mont_params358:
; i start address is: 0 (R0)
L__bigi_get_mont_params357:
;bigi.c,1527 :: 		if (i >= 0)
CMP	R0, #0
IT	LT
BLT	L_bigi_get_mont_params278
;bigi.c,1529 :: 		POW2->ary[i+1] = 1u;
ADDS	R6, R0, #1
; i end address is: 0 (R0)
LDR	R4, [SP, #20]
LDR	R5, [R4, #0]
LSLS	R4, R6, #2
ADDS	R5, R5, R4
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,1530 :: 		}
IT	AL
BAL	L_bigi_get_mont_params279
; MOD end address is: 4 (R1)
; TMPARY end address is: 8 (R2)
L_bigi_get_mont_params278:
;bigi.c,1533 :: 		return ERR_INVALID_PARAMS; /* MOD == 0 */
IT	AL
BAL	L_end_bigi_get_mont_params
;bigi.c,1534 :: 		}
L_bigi_get_mont_params279:
;bigi.c,1535 :: 		BIGI_CALL(bigi_mod_red(POW2, MOD, &TMPARY[4], tmparylen - 4, R))
; TMPARY start address is: 8 (R2)
; MOD start address is: 4 (R1)
LDR	R4, [SP, #12]
SUBS	R6, R4, #4
ADDW	R5, R2, #64
LDR	R4, [SP, #16]
STR	R2, [SP, #4]
STR	R1, [SP, #8]
MOV	R3, R6
MOV	R2, R5
LDR	R0, [SP, #20]
PUSH	(R4)
BL	_bigi_mod_red+0
ADD	SP, SP, #4
;bigi.c,1538 :: 		BIGI_CALL(bigi_set_zero(X))
LDR	R0, [SP, #24]
BL	_bigi_set_zero+0
;bigi.c,1539 :: 		X->ary[1] = 1;
LDR	R4, [SP, #24]
LDR	R4, [R4, #0]
ADDS	R5, R4, #4
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,1540 :: 		BIGI_CALL(bigi_set_zero(Y))
LDR	R0, [SP, #28]
BL	_bigi_set_zero+0
LDR	R1, [SP, #8]
LDR	R2, [SP, #4]
;bigi.c,1541 :: 		Y->ary[0] = MOD->ary[0];
LDR	R4, [SP, #28]
LDR	R5, [R4, #0]
LDR	R4, [R1, #0]
; MOD end address is: 4 (R1)
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1543 :: 		BIGI_CALL(bigi_mod_inv(Y, X, &TMPARY[4], tmparylen - 4, POW2))
LDR	R6, [SP, #20]
LDR	R4, [SP, #12]
SUBS	R5, R4, #4
ADDW	R4, R2, #64
; TMPARY end address is: 8 (R2)
MOV	R3, R5
MOV	R2, R4
LDR	R1, [SP, #24]
LDR	R0, [SP, #28]
PUSH	(R6)
BL	_bigi_mod_inv+0
ADD	SP, SP, #4
;bigi.c,1545 :: 		BIGI_CALL(bigi_set_zero(Y))
LDR	R0, [SP, #28]
BL	_bigi_set_zero+0
;bigi.c,1546 :: 		Y->ary[0] = (word_t)(MAX_VAL);
LDR	R4, [SP, #28]
LDR	R5, [R4, #0]
MOV	R4, #-1
STR	R4, [R5, #0]
;bigi.c,1548 :: 		BIGI_CALL(bigi_mod_mult(Y, POW2, X, Z))
LDR	R3, [SP, #32]
LDR	R2, [SP, #24]
LDR	R1, [SP, #20]
LDR	R0, [SP, #28]
BL	_bigi_mod_mult+0
;bigi.c,1549 :: 		*mu = Z->ary[0];
LDR	R4, [SP, #32]
LDR	R4, [R4, #0]
LDR	R5, [R4, #0]
LDR	R4, [SP, #36]
STR	R5, [R4, #0]
;bigi.c,1551 :: 		return OK;
;bigi.c,1552 :: 		}
L_end_bigi_get_mont_params:
LDR	LR, [SP, #0]
ADD	SP, SP, #36
BX	LR
; end of _bigi_get_mont_params
_bigi_to_mont_domain:
;bigi.c,1557 :: 		bigi_t *const AR)
; AR start address is: 12 (R3)
; R start address is: 8 (R2)
; MOD start address is: 4 (R1)
; A start address is: 0 (R0)
SUB	SP, SP, #12
STR	LR, [SP, #0]
STR	R3, [SP, #4]
MOV	R3, R2
MOV	R2, R1
MOV	R1, R0
LDR	R0, [SP, #4]
; AR end address is: 12 (R3)
; R end address is: 8 (R2)
; MOD end address is: 4 (R1)
; A end address is: 0 (R0)
; A start address is: 4 (R1)
; MOD start address is: 8 (R2)
; R start address is: 12 (R3)
; AR start address is: 0 (R0)
;bigi.c,1575 :: 		BIGI_CALL(bigi_mod_mult(A, R, MOD, AR))
STR	R0, [SP, #4]
STR	R1, [SP, #8]
MOV	R1, R3
; MOD end address is: 8 (R2)
MOV	R3, R0
; R end address is: 12 (R3)
LDR	R0, [SP, #8]
; A end address is: 4 (R1)
BL	_bigi_mod_mult+0
LDR	R0, [SP, #4]
;bigi.c,1577 :: 		AR->domain = DOMAIN_MONTGOMERY;
ADDW	R5, R0, #12
; AR end address is: 0 (R0)
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1579 :: 		return OK;
;bigi.c,1580 :: 		}
L_end_bigi_to_mont_domain:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _bigi_to_mont_domain
_bigi_from_mont_domain:
;bigi.c,1588 :: 		bigi_t *const A)
; mu start address is: 12 (R3)
; MOD start address is: 4 (R1)
SUB	SP, SP, #12
STR	LR, [SP, #0]
STR	R0, [SP, #8]
MOV	R2, R1
; mu end address is: 12 (R3)
; MOD end address is: 4 (R1)
; MOD start address is: 8 (R2)
; mu start address is: 12 (R3)
; TMPARY start address is: 4 (R1)
LDR	R1, [SP, #12]
; tmparylen start address is: 28 (R7)
LDR	R7, [SP, #16]
LDR	R4, [SP, #20]
STR	R4, [SP, #20]
;bigi.c,1594 :: 		bigi_t * TMP = NULL;
;bigi.c,1615 :: 		for (i = 0; i < FROM_MONT__MIN_TMPARY_LEN; i++)
; i start address is: 0 (R0)
MOVS	R0, #0
; MOD end address is: 8 (R2)
; mu end address is: 12 (R3)
; i end address is: 0 (R0)
; tmparylen end address is: 28 (R7)
; TMPARY end address is: 4 (R1)
MOV	R9, R2
MOV	R8, R3
L_bigi_from_mont_domain280:
; i start address is: 0 (R0)
; tmparylen start address is: 28 (R7)
; TMPARY start address is: 4 (R1)
; mu start address is: 32 (R8)
; MOD start address is: 36 (R9)
CMP	R0, #3
IT	CS
BCS	L_bigi_from_mont_domain281
;bigi.c,1617 :: 		TMPARY[i].domain = DOMAIN_MONTGOMERY;
LSLS	R4, R0, #4
ADDS	R4, R1, R4
ADDW	R5, R4, #12
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1618 :: 		TMPARY[i].wordlen = AR->wordlen;
LSLS	R4, R0, #4
ADDS	R4, R1, R4
ADDS	R5, R4, #4
LDR	R4, [SP, #8]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1615 :: 		for (i = 0; i < FROM_MONT__MIN_TMPARY_LEN; i++)
ADDS	R4, R0, #1
; i end address is: 0 (R0)
; i start address is: 16 (R4)
;bigi.c,1619 :: 		}
; i end address is: 16 (R4)
MOV	R0, R4
IT	AL
BAL	L_bigi_from_mont_domain280
L_bigi_from_mont_domain281:
;bigi.c,1620 :: 		TMP = &TMPARY[0];
; TMP start address is: 40 (R10)
MOV	R10, R1
;bigi.c,1622 :: 		BIGI_CALL(bigi_set_zero(TMP))
STR	R1, [SP, #4]
MOV	R0, R1
BL	_bigi_set_zero+0
LDR	R1, [SP, #4]
;bigi.c,1623 :: 		TMP->ary[0] = 1u;
LDR	R5, [R10, #0]
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,1625 :: 		BIGI_CALL(bigi_mod_mult_mont(TMP, AR, MOD, mu, &TMPARY[1], tmparylen - 1, A))
LDR	R6, [SP, #20]
SUBS	R5, R7, #1
; tmparylen end address is: 28 (R7)
ADDW	R4, R1, #16
; TMPARY end address is: 4 (R1)
MOV	R3, R8
; mu end address is: 32 (R8)
MOV	R2, R9
; MOD end address is: 36 (R9)
LDR	R1, [SP, #8]
MOV	R0, R10
; TMP end address is: 40 (R10)
PUSH	(R6)
PUSH	(R5)
PUSH	(R4)
BL	_bigi_mod_mult_mont+0
ADD	SP, SP, #12
;bigi.c,1627 :: 		A->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #20]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1629 :: 		return OK;
;bigi.c,1630 :: 		}
L_end_bigi_from_mont_domain:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _bigi_from_mont_domain
_bigi_mod_mult_mont:
;bigi.c,1638 :: 		bigi_t *const RES)
SUB	SP, SP, #40
STR	LR, [SP, #0]
STR	R0, [SP, #16]
STR	R1, [SP, #20]
STR	R2, [SP, #24]
STR	R3, [SP, #28]
; TMPARY start address is: 44 (R11)
LDR	R11, [SP, #40]
; tmparylen start address is: 16 (R4)
LDR	R4, [SP, #44]
; tmparylen end address is: 16 (R4)
; RES start address is: 24 (R6)
LDR	R6, [SP, #48]
;bigi.c,1646 :: 		bigi_t * TMPM = NULL;
;bigi.c,1647 :: 		bigi_t * TMPW = NULL;
;bigi.c,1648 :: 		cmp_t cmp = CMP_UNDEFINED;
MOVS	R4, #11
STRB	R4, [SP, #12]
;bigi.c,1670 :: 		for (i = 0; i < MOD_MULT_MONT__MIN_TMPARY_LEN; i++)
MOVS	R4, #0
STR	R4, [SP, #4]
; RES end address is: 24 (R6)
; TMPARY end address is: 44 (R11)
L_bigi_mod_mult_mont283:
; RES start address is: 24 (R6)
; TMPARY start address is: 44 (R11)
LDR	R4, [SP, #4]
CMP	R4, #2
IT	CS
BCS	L_bigi_mod_mult_mont284
;bigi.c,1672 :: 		TMPARY[i].domain = DOMAIN_MONTGOMERY;
LDR	R4, [SP, #4]
LSLS	R4, R4, #4
ADD	R4, R11, R4, LSL #0
ADDW	R5, R4, #12
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1673 :: 		TMPARY[i].wordlen = AR->wordlen;
LDR	R4, [SP, #4]
LSLS	R4, R4, #4
ADD	R4, R11, R4, LSL #0
ADDS	R5, R4, #4
LDR	R4, [SP, #16]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1670 :: 		for (i = 0; i < MOD_MULT_MONT__MIN_TMPARY_LEN; i++)
LDR	R4, [SP, #4]
ADDS	R4, R4, #1
STR	R4, [SP, #4]
;bigi.c,1674 :: 		}
IT	AL
BAL	L_bigi_mod_mult_mont283
L_bigi_mod_mult_mont284:
;bigi.c,1676 :: 		TMPM = &TMPARY[0];
STR	R11, [SP, #32]
;bigi.c,1677 :: 		TMPW = &TMPARY[1];
ADD	R4, R11, #16
; TMPARY end address is: 44 (R11)
STR	R4, [SP, #36]
;bigi.c,1679 :: 		BIGI_CALL(bigi_set_zero(RES))
MOV	R0, R6
BL	_bigi_set_zero+0
;bigi.c,1685 :: 		for (mlen = MOD->wordlen - 1; mlen >= 0 ; mlen--)
LDR	R4, [SP, #24]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
SUBS	R4, R4, #1
STR	R4, [SP, #8]
; RES end address is: 24 (R6)
MOV	R12, R6
L_bigi_mod_mult_mont286:
; RES start address is: 48 (R12)
LDR	R4, [SP, #8]
CMP	R4, #0
IT	LT
BLT	L_bigi_mod_mult_mont287
;bigi.c,1686 :: 		if (MOD->ary[mlen]) break;
LDR	R4, [SP, #24]
LDR	R5, [R4, #0]
LDR	R4, [SP, #8]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mod_mult_mont289
IT	AL
BAL	L_bigi_mod_mult_mont287
L_bigi_mod_mult_mont289:
;bigi.c,1685 :: 		for (mlen = MOD->wordlen - 1; mlen >= 0 ; mlen--)
LDR	R4, [SP, #8]
SUBS	R4, R4, #1
STR	R4, [SP, #8]
;bigi.c,1686 :: 		if (MOD->ary[mlen]) break;
IT	AL
BAL	L_bigi_mod_mult_mont286
L_bigi_mod_mult_mont287:
;bigi.c,1688 :: 		if ((mlen == 0) && (MOD->ary[0] == 0u))
LDR	R4, [SP, #8]
CMP	R4, #0
IT	NE
BNE	L__bigi_mod_mult_mont362
LDR	R4, [SP, #24]
LDR	R4, [R4, #0]
LDR	R4, [R4, #0]
CMP	R4, #0
IT	NE
BNE	L__bigi_mod_mult_mont361
; RES end address is: 48 (R12)
L__bigi_mod_mult_mont360:
;bigi.c,1689 :: 		return BIGI_DIV_BY_ZERO;
IT	AL
BAL	L_end_bigi_mod_mult_mont
;bigi.c,1688 :: 		if ((mlen == 0) && (MOD->ary[0] == 0u))
L__bigi_mod_mult_mont362:
; RES start address is: 48 (R12)
L__bigi_mod_mult_mont361:
;bigi.c,1691 :: 		for (i = 0; i <= mlen; i++)
MOVS	R4, #0
STR	R4, [SP, #4]
; RES end address is: 48 (R12)
L_bigi_mod_mult_mont293:
; RES start address is: 48 (R12)
LDR	R5, [SP, #8]
LDR	R4, [SP, #4]
CMP	R4, R5
IT	HI
BHI	L_bigi_mod_mult_mont294
;bigi.c,1693 :: 		BIGI_CALL(bigi_mult_word(BR, AR->ary[i], TMPW, TMPM))
LDR	R4, [SP, #16]
LDR	R5, [R4, #0]
LDR	R4, [SP, #4]
LSLS	R4, R4, #2
ADDS	R4, R5, R4
LDR	R4, [R4, #0]
LDR	R3, [SP, #32]
LDR	R2, [SP, #36]
MOV	R1, R4
LDR	R0, [SP, #20]
BL	_bigi_mult_word+0
;bigi.c,1694 :: 		BIGI_CALL(bigi_add(TMPM, RES, RES))
MOV	R2, R12
MOV	R1, R12
LDR	R0, [SP, #32]
BL	_bigi_add+0
;bigi.c,1696 :: 		q  = (dword_t)(mu) * RES->ary[0];
LDR	R8, [SP, #28]
MOVW	R9, #0
LDR	R4, [R12, #0]
LDR	R4, [R4, #0]
MOV	R6, R4
MOVS	R7, #0
UMULL	R4, R5, R8, R6
MLA	R5, R9, R6, R5
MLA	R5, R8, R7, R5
;bigi.c,1697 :: 		q &= MAX_VAL;
AND	R4, R4, #-1
AND	R5, R5, #0
;bigi.c,1699 :: 		BIGI_CALL(bigi_mult_word(MOD, (word_t)(q), TMPW, TMPM))
LDR	R3, [SP, #32]
LDR	R2, [SP, #36]
MOV	R1, R4
LDR	R0, [SP, #24]
BL	_bigi_mult_word+0
;bigi.c,1700 :: 		BIGI_CALL(bigi_add(TMPM, RES, RES))
MOV	R2, R12
MOV	R1, R12
LDR	R0, [SP, #32]
BL	_bigi_add+0
;bigi.c,1701 :: 		BIGI_CALL(bigi_shift_right(RES, MACHINE_WORD_BITLEN))   /* TODO: use circular buffer instead & omit this operation */
MOVS	R1, #32
MOV	R0, R12
BL	_bigi_shift_right+0
;bigi.c,1691 :: 		for (i = 0; i <= mlen; i++)
LDR	R4, [SP, #4]
ADDS	R4, R4, #1
STR	R4, [SP, #4]
;bigi.c,1702 :: 		}
IT	AL
BAL	L_bigi_mod_mult_mont293
L_bigi_mod_mult_mont294:
;bigi.c,1704 :: 		BIGI_CALL(bigi_cmp(RES, MOD, &cmp))
ADD	R4, SP, #12
MOV	R2, R4
LDR	R1, [SP, #24]
MOV	R0, R12
BL	_bigi_cmp+0
;bigi.c,1706 :: 		if (cmp == CMP_GREATER || cmp == CMP_EQUAL)
LDRB	R4, [SP, #12]
CMP	R4, #3
IT	EQ
BEQ	L__bigi_mod_mult_mont364
LDRB	R4, [SP, #12]
CMP	R4, #0
IT	EQ
BEQ	L__bigi_mod_mult_mont363
IT	AL
BAL	L_bigi_mod_mult_mont298
L__bigi_mod_mult_mont364:
L__bigi_mod_mult_mont363:
;bigi.c,1708 :: 		BIGI_CALL(bigi_sub(RES, MOD, RES))
MOV	R2, R12
LDR	R1, [SP, #24]
MOV	R0, R12
BL	_bigi_sub+0
;bigi.c,1709 :: 		}
L_bigi_mod_mult_mont298:
;bigi.c,1711 :: 		RES->domain = DOMAIN_MONTGOMERY;
ADD	R5, R12, #12
; RES end address is: 48 (R12)
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1713 :: 		return OK;
;bigi.c,1714 :: 		}
L_end_bigi_mod_mult_mont:
LDR	LR, [SP, #0]
ADD	SP, SP, #40
BX	LR
; end of _bigi_mod_mult_mont
_bigi_mod_exp_mont:
;bigi.c,1723 :: 		bigi_t *const RES)
; A start address is: 0 (R0)
SUB	SP, SP, #36
STR	LR, [SP, #0]
STR	R1, [SP, #12]
STR	R2, [SP, #16]
STR	R3, [SP, #20]
; A end address is: 0 (R0)
; A start address is: 0 (R0)
LDR	R4, [SP, #36]
STR	R4, [SP, #36]
LDR	R4, [SP, #40]
STR	R4, [SP, #40]
LDR	R4, [SP, #44]
STR	R4, [SP, #44]
LDR	R4, [SP, #48]
STR	R4, [SP, #48]
;bigi.c,1726 :: 		word_t readbit = 0u;
MOV	R4, #0
STR	R4, [SP, #8]
;bigi.c,1727 :: 		bigi_t * A_R = NULL;
;bigi.c,1728 :: 		bigi_t * B_R = NULL;
;bigi.c,1729 :: 		bigi_t * TMP = NULL;
;bigi.c,1757 :: 		for (i = 0; i < MODEXP_MONT__MIN_TMPARY_LEN; i++)
MOVS	R4, #0
STR	R4, [SP, #4]
; A end address is: 0 (R0)
MOV	R11, R0
L_bigi_mod_exp_mont299:
; A start address is: 44 (R11)
LDR	R4, [SP, #4]
CMP	R4, #5
IT	CS
BCS	L_bigi_mod_exp_mont300
;bigi.c,1759 :: 		TMPARY[i].domain = DOMAIN_NORMAL;
LDR	R4, [SP, #4]
LSLS	R5, R4, #4
LDR	R4, [SP, #40]
ADDS	R4, R4, R5
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1760 :: 		TMPARY[i].wordlen = MOD->wordlen;
LDR	R4, [SP, #4]
LSLS	R5, R4, #4
LDR	R4, [SP, #40]
ADDS	R4, R4, R5
ADDS	R5, R4, #4
LDR	R4, [SP, #16]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
STR	R4, [R5, #0]
;bigi.c,1757 :: 		for (i = 0; i < MODEXP_MONT__MIN_TMPARY_LEN; i++)
LDR	R4, [SP, #4]
ADDS	R4, R4, #1
STR	R4, [SP, #4]
;bigi.c,1761 :: 		}
IT	AL
BAL	L_bigi_mod_exp_mont299
L_bigi_mod_exp_mont300:
;bigi.c,1763 :: 		A_R  = &TMPARY[0];
LDR	R4, [SP, #40]
STR	R4, [SP, #24]
;bigi.c,1764 :: 		B_R  = &TMPARY[1];
LDR	R4, [SP, #40]
ADDS	R4, #16
STR	R4, [SP, #28]
;bigi.c,1765 :: 		TMP  = &TMPARY[2];
LDR	R4, [SP, #40]
ADDS	R4, #32
STR	R4, [SP, #32]
;bigi.c,1767 :: 		BIGI_CALL(bigi_set_zero(RES))
LDR	R0, [SP, #48]
BL	_bigi_set_zero+0
;bigi.c,1768 :: 		BIGI_CALL(bigi_set_zero(TMP))
LDR	R0, [SP, #32]
BL	_bigi_set_zero+0
;bigi.c,1771 :: 		BIGI_CALL(bigi_to_mont_domain(A, MOD, R, A_R))
LDR	R3, [SP, #24]
LDR	R2, [SP, #20]
LDR	R1, [SP, #16]
MOV	R0, R11
; A end address is: 44 (R11)
BL	_bigi_to_mont_domain+0
;bigi.c,1772 :: 		BIGI_CALL(bigi_copy(B_R, R))
LDR	R1, [SP, #20]
LDR	R0, [SP, #28]
BL	_bigi_copy+0
;bigi.c,1774 :: 		A_R->domain = DOMAIN_MONTGOMERY;
LDR	R4, [SP, #24]
ADDW	R5, R4, #12
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1775 :: 		B_R->domain = DOMAIN_MONTGOMERY;
LDR	R4, [SP, #28]
ADDW	R5, R4, #12
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1776 :: 		RES->domain = DOMAIN_MONTGOMERY;
LDR	R4, [SP, #48]
ADDW	R5, R4, #12
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1777 :: 		TMP->domain = DOMAIN_MONTGOMERY; /* TODO: needed? */
LDR	R4, [SP, #32]
ADDW	R5, R4, #12
MOVS	R4, #1
STRB	R4, [R5, #0]
;bigi.c,1779 :: 		for (i = 0; i < B->wordlen * MACHINE_WORD_BITLEN; i++)
MOVS	R4, #0
STR	R4, [SP, #4]
L_bigi_mod_exp_mont302:
LDR	R4, [SP, #12]
ADDS	R4, R4, #4
LDR	R4, [R4, #0]
LSLS	R5, R4, #5
LDR	R4, [SP, #4]
CMP	R4, R5
IT	CS
BCS	L_bigi_mod_exp_mont303
;bigi.c,1781 :: 		BIGI_CALL(bigi_get_bit(B, i, &readbit))
ADD	R4, SP, #8
MOV	R2, R4
LDR	R1, [SP, #4]
LDR	R0, [SP, #12]
BL	_bigi_get_bit+0
;bigi.c,1782 :: 		if (readbit)
LDR	R4, [SP, #8]
CMP	R4, #0
IT	EQ
BEQ	L_bigi_mod_exp_mont305
;bigi.c,1784 :: 		BIGI_CALL(bigi_mod_mult_mont(A_R, B_R, MOD, mu, &TMPARY[3], tmparylen - 3, TMP))
LDR	R6, [SP, #32]
LDR	R4, [SP, #44]
SUBS	R5, R4, #3
LDR	R4, [SP, #40]
ADDS	R4, #48
LDR	R3, [SP, #36]
LDR	R2, [SP, #16]
LDR	R1, [SP, #28]
LDR	R0, [SP, #24]
PUSH	(R6)
PUSH	(R5)
PUSH	(R4)
BL	_bigi_mod_mult_mont+0
ADD	SP, SP, #12
;bigi.c,1785 :: 		BIGI_CALL(bigi_copy(B_R, TMP))
LDR	R1, [SP, #32]
LDR	R0, [SP, #28]
BL	_bigi_copy+0
;bigi.c,1786 :: 		}
L_bigi_mod_exp_mont305:
;bigi.c,1788 :: 		BIGI_CALL(bigi_mod_mult_mont(A_R, A_R, MOD, mu, &TMPARY[3], tmparylen - 3, TMP))
LDR	R6, [SP, #32]
LDR	R4, [SP, #44]
SUBS	R5, R4, #3
LDR	R4, [SP, #40]
ADDS	R4, #48
LDR	R3, [SP, #36]
LDR	R2, [SP, #16]
LDR	R1, [SP, #24]
LDR	R0, [SP, #24]
PUSH	(R6)
PUSH	(R5)
PUSH	(R4)
BL	_bigi_mod_mult_mont+0
ADD	SP, SP, #12
;bigi.c,1789 :: 		BIGI_CALL(bigi_copy(A_R, TMP))
LDR	R1, [SP, #32]
LDR	R0, [SP, #24]
BL	_bigi_copy+0
;bigi.c,1779 :: 		for (i = 0; i < B->wordlen * MACHINE_WORD_BITLEN; i++)
LDR	R4, [SP, #4]
ADDS	R4, R4, #1
STR	R4, [SP, #4]
;bigi.c,1790 :: 		}
IT	AL
BAL	L_bigi_mod_exp_mont302
L_bigi_mod_exp_mont303:
;bigi.c,1792 :: 		BIGI_CALL(bigi_set_zero(A_R))
LDR	R0, [SP, #24]
BL	_bigi_set_zero+0
;bigi.c,1793 :: 		A_R->ary[0] = 1u;
LDR	R4, [SP, #24]
LDR	R5, [R4, #0]
MOVS	R4, #1
STR	R4, [R5, #0]
;bigi.c,1795 :: 		BIGI_CALL(bigi_mod_mult_mont(A_R, B_R, MOD, mu, &TMPARY[3], tmparylen - 3, RES))
LDR	R6, [SP, #48]
LDR	R4, [SP, #44]
SUBS	R5, R4, #3
LDR	R4, [SP, #40]
ADDS	R4, #48
LDR	R3, [SP, #36]
LDR	R2, [SP, #16]
LDR	R1, [SP, #28]
LDR	R0, [SP, #24]
PUSH	(R6)
PUSH	(R5)
PUSH	(R4)
BL	_bigi_mod_mult_mont+0
ADD	SP, SP, #12
;bigi.c,1797 :: 		RES->domain = DOMAIN_NORMAL;
LDR	R4, [SP, #48]
ADDW	R5, R4, #12
MOVS	R4, #0
STRB	R4, [R5, #0]
;bigi.c,1799 :: 		return OK;
;bigi.c,1800 :: 		}
L_end_bigi_mod_exp_mont:
LDR	LR, [SP, #0]
ADD	SP, SP, #36
BX	LR
; end of _bigi_mod_exp_mont
_nlz:
;bigi.c,1808 :: 		byte_t nlz(word_t x)
; x start address is: 0 (R0)
SUB	SP, SP, #4
MOV	R2, R0
; x end address is: 0 (R0)
; x start address is: 8 (R2)
;bigi.c,1812 :: 		if (x == 0) return(MACHINE_WORD_BITLEN);
CMP	R2, #0
IT	NE
BNE	L_nlz306
; x end address is: 8 (R2)
MOVS	R0, #32
IT	AL
BAL	L_end_nlz
L_nlz306:
;bigi.c,1814 :: 		n = 0;
; n start address is: 0 (R0)
; x start address is: 8 (R2)
MOVS	R0, #0
;bigi.c,1825 :: 		if (x <= 0x0000FFFF)    {n = n +16; x = x <<16;}
MOVW	R1, #65535
CMP	R2, R1
IT	HI
BHI	L__nlz321
ADDW	R1, R0, #16
UXTB	R0, R1
LSLS	R2, R2, #16
; x end address is: 8 (R2)
; n end address is: 0 (R0)
IT	AL
BAL	L_nlz307
L__nlz321:
L_nlz307:
;bigi.c,1826 :: 		if (x <= 0x00FFFFFF)    {n = n + 8; x = x << 8;}
; x start address is: 8 (R2)
; n start address is: 0 (R0)
MVN	R1, #-16777216
CMP	R2, R1
IT	HI
BHI	L__nlz322
ADDW	R1, R0, #8
UXTB	R0, R1
LSLS	R2, R2, #8
; x end address is: 8 (R2)
; n end address is: 0 (R0)
IT	AL
BAL	L_nlz308
L__nlz322:
L_nlz308:
;bigi.c,1827 :: 		if (x <= 0x0FFFFFFF)    {n = n + 4; x = x << 4;}
; x start address is: 8 (R2)
; n start address is: 0 (R0)
MVN	R1, #-268435456
CMP	R2, R1
IT	HI
BHI	L__nlz323
ADDS	R1, R0, #4
UXTB	R0, R1
LSLS	R2, R2, #4
; x end address is: 8 (R2)
; n end address is: 0 (R0)
IT	AL
BAL	L_nlz309
L__nlz323:
L_nlz309:
;bigi.c,1828 :: 		if (x <= 0x3FFFFFFF)    {n = n + 2; x = x << 2;}
; x start address is: 8 (R2)
; n start address is: 0 (R0)
MVN	R1, #-1073741824
CMP	R2, R1
IT	HI
BHI	L__nlz324
ADDS	R1, R0, #2
UXTB	R0, R1
LSLS	R1, R2, #2
MOV	R2, R1
; x end address is: 8 (R2)
; n end address is: 0 (R0)
STR	R2, [SP, #0]
UXTB	R2, R0
LDR	R0, [SP, #0]
IT	AL
BAL	L_nlz310
L__nlz324:
STRB	R0, [SP, #0]
MOV	R0, R2
LDRB	R2, [SP, #0]
L_nlz310:
;bigi.c,1829 :: 		if (x <= 0x7FFFFFFF)    {n = n + 1;}
; x start address is: 0 (R0)
; n start address is: 8 (R2)
MVN	R1, #-2147483648
CMP	R0, R1
IT	HI
BHI	L__nlz325
; x end address is: 0 (R0)
ADDS	R1, R2, #1
; n end address is: 8 (R2)
; n start address is: 0 (R0)
UXTB	R0, R1
; n end address is: 0 (R0)
IT	AL
BAL	L_nlz311
L__nlz325:
UXTB	R0, R2
L_nlz311:
;bigi.c,1834 :: 		return n;
; n start address is: 0 (R0)
; n end address is: 0 (R0)
;bigi.c,1835 :: 		}
L_end_nlz:
ADD	SP, SP, #4
BX	LR
; end of _nlz
