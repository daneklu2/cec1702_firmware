#include <stdio.h>
#include <stdint.h>

#include "rsa_sw_implementation.h"
#include "../debug.h"
#include "../simpleserial/simpleserial.h"
#include "../device_setup/cec1702_setup.h"
#include "BIGI/bigi.h"
#include "BIGI/bigi_io.h"

/************************************/
/*SWITCH FOR USING  MONGOMERY DOMAIN*/
#define MONGOMERY
/************************************/
#define RSA_4096_BYTES 512
#define RSA_4096_WORDS 128

#define ENC 0
#define DEC 1


extern uint8_t  actual_opertion;
extern uint8_t  actual_implementation;
extern uint16_t message_parts;
extern uint16_t message_parts_cnt;
extern uint16_t key_parts;
extern uint16_t key_parts_cnt;
extern uint16_t operation_length_in_bites;
extern uint16_t block_length_in_bytes;

static uint8_t key_byte_array[RSA_4096_BYTES << 1];
static uint8_t plaintext_byte_array[RSA_4096_BYTES];
static uint8_t ciphertext_byte_array[RSA_4096_BYTES];



/*BIGI PART - START*/
#define TMPARY_LEN 20
#define BUFFLEN ((RSA_4096_WORDS + MULT_BUFFER_WORDLEN + 3) * HEXCHARS_PER_MW)

uint16_t actual_bigi_size = RSA_4096_WORDS;
uint16_t rsa_block_size = RSA_4096_BYTES;
uint8_t  encrytion_decryption_state = 0;

word_t tmp_arys[TMPARY_LEN * (RSA_4096_WORDS + MULT_BUFFER_WORDLEN)];
word_t tmp0_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t tmp1_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t tmp2_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t tmp3_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t tmp00_array[RSA_4096_WORDS + MULT_BUFFER_WORDLEN];
word_t tmp11_array[RSA_4096_WORDS + MULT_BUFFER_WORDLEN];
word_t tmp22_array[RSA_4096_WORDS + MULT_BUFFER_WORDLEN];

bigi_t tmpary_bigi[TMPARY_LEN];
bigi_t tmp0_bigi, tmp1_bigi, tmp2_bigi, tmp3_bigi, tmp00_bigi, tmp11_bigi, tmp22_bigi;

word_t  plaintext_array [RSA_4096_WORDS + MULT_BUFFER_WORDLEN];
word_t  ciphertext_array[RSA_4096_WORDS + MULT_BUFFER_WORDLEN];

word_t  p_array  [(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t  q_array  [(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t  n_array  [RSA_4096_WORDS + MULT_BUFFER_WORDLEN];
word_t  e_array  [RSA_4096_WORDS + MULT_BUFFER_WORDLEN];
word_t  d_array  [RSA_4096_WORDS + MULT_BUFFER_WORDLEN];

word_t  dp_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t  dq_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];
word_t  qi_array[(RSA_4096_WORDS >> 1) + MULT_BUFFER_WORDLEN];


bigi_t plaintext_bigi;
bigi_t ciphertext_bigi;
bigi_t p_bigi, q_bigi, n_bigi, e_bigi, d_bigi;
bigi_t dp_bigi, dq_bigi, qi_bigi;

word_t mu_mult;
cmp_t compare_result;
uint8_t is_add_p_neccessary;
word_t index_value;


/*BIGI PART - END  */

void sw_rsa_init (void) {
        uint16_t i;
    plaintext_bigi.ary = plaintext_array;
    plaintext_bigi.wordlen = RSA_4096_WORDS;
    plaintext_bigi.domain = DOMAIN_NORMAL;

    ciphertext_bigi.ary = ciphertext_array;
    ciphertext_bigi.wordlen = RSA_4096_WORDS;
    ciphertext_bigi.domain = DOMAIN_NORMAL;

    p_bigi.ary = p_array;
    p_bigi.wordlen = (RSA_4096_WORDS >> 1);
    p_bigi.domain = DOMAIN_NORMAL;

    q_bigi.ary = q_array;
    q_bigi.wordlen = (RSA_4096_WORDS >> 1);
    q_bigi.domain = DOMAIN_NORMAL;

    n_bigi.ary = n_array;
    n_bigi.wordlen = RSA_4096_WORDS;
    n_bigi.domain = DOMAIN_NORMAL;

    e_bigi.ary = e_array;
    e_bigi.wordlen = RSA_4096_WORDS;
    e_bigi.domain = DOMAIN_NORMAL;

    d_bigi.ary = d_array;
    d_bigi.wordlen = RSA_4096_WORDS;
    d_bigi.domain = DOMAIN_NORMAL;

    dp_bigi.ary = dp_array;
    dp_bigi.wordlen = (RSA_4096_WORDS >> 1);
    dp_bigi.domain = DOMAIN_NORMAL;

    dq_bigi.ary = dq_array;
    dq_bigi.wordlen = (RSA_4096_WORDS >> 1);
    dq_bigi.domain = DOMAIN_NORMAL;

    qi_bigi.ary = qi_array;
    qi_bigi.wordlen = (RSA_4096_WORDS >> 1);
    qi_bigi.domain = DOMAIN_NORMAL;

    for (i = 0; i < TMPARY_LEN; i++)
    {
        tmpary_bigi[i].ary      = &tmp_arys[i * (RSA_4096_WORDS + MULT_BUFFER_WORDLEN)];
        tmpary_bigi[i].wordlen  = RSA_4096_WORDS;
        tmpary_bigi[i].alloclen = RSA_4096_WORDS;
        tmpary_bigi[i].domain   = DOMAIN_NORMAL;
    }
    tmp0_bigi.ary      = tmp0_array;
    tmp0_bigi.wordlen  = RSA_4096_WORDS >> 1;
    tmp0_bigi.alloclen = RSA_4096_WORDS >> 1;
    tmp0_bigi.domain   = DOMAIN_NORMAL;

    tmp1_bigi.ary      = tmp1_array;
    tmp1_bigi.wordlen  = RSA_4096_WORDS >> 1;
    tmp1_bigi.alloclen = RSA_4096_WORDS >> 1;
    tmp1_bigi.domain   = DOMAIN_NORMAL;

    tmp2_bigi.ary      = tmp2_array;
    tmp2_bigi.wordlen  = RSA_4096_WORDS >> 1;
    tmp2_bigi.alloclen = RSA_4096_WORDS >> 1;
    tmp2_bigi.domain   = DOMAIN_NORMAL;

    tmp3_bigi.ary      = tmp3_array;
    tmp3_bigi.wordlen  = RSA_4096_WORDS >> 1;
    tmp3_bigi.alloclen = RSA_4096_WORDS >> 1;
    tmp3_bigi.domain   = DOMAIN_NORMAL;

    tmp00_bigi.ary      = tmp00_array;
    tmp00_bigi.wordlen  = RSA_4096_WORDS;
    tmp00_bigi.alloclen = RSA_4096_WORDS;
    tmp00_bigi.domain   = DOMAIN_NORMAL;

    tmp11_bigi.ary      = tmp11_array;
    tmp11_bigi.wordlen  = RSA_4096_WORDS;
    tmp11_bigi.alloclen = RSA_4096_WORDS;
    tmp11_bigi.domain   = DOMAIN_NORMAL;

    tmp22_bigi.ary      = tmp22_array;
    tmp22_bigi.wordlen  = RSA_4096_WORDS;
    tmp22_bigi.alloclen = RSA_4096_WORDS;
    tmp22_bigi.domain   = DOMAIN_NORMAL;
}

void sw_rsa_adjust_length (index_t new_length) {
     uint16_t i;
    dbg_puts_labeled ("IN ADJUST BIGI LENGTH");
    plaintext_bigi.wordlen = new_length;
    ciphertext_bigi.wordlen = new_length;

    p_bigi.wordlen   = (new_length >> 1);
    q_bigi.wordlen   = (new_length >> 1);
    n_bigi.wordlen   = new_length;
    e_bigi.wordlen   = new_length;
    d_bigi.wordlen   = new_length;

    dp_bigi.wordlen = (new_length >> 1);
    dq_bigi.wordlen = (new_length >> 1);
    qi_bigi.wordlen = (new_length >> 1);

    for (i = 0; i < TMPARY_LEN; i++)
    {
        tmpary_bigi[i].wordlen  = new_length;
    }
    tmp0_bigi.wordlen  = (new_length >> 1);
    tmp1_bigi.wordlen  = (new_length >> 1);
    tmp2_bigi.wordlen  = (new_length >> 1);
    tmp3_bigi.wordlen  = (new_length >> 1);

    tmp00_bigi.wordlen  = new_length;
    tmp11_bigi.wordlen  = new_length;
    tmp22_bigi.wordlen  = new_length;

    actual_bigi_size = new_length;

}

uint8_t sw_rsa_key_set (uint8_t* k) {
        /*loading part*/
        if (key_parts_cnt >= key_parts) {
                return 0x01;
        }

        memcpy(key_byte_array + (key_parts_cnt * block_length_in_bytes), k, block_length_in_bytes);
        ++key_parts_cnt;

        if (key_parts_cnt < key_parts) {
                return 0x00;
        }

        /*parsing part*/
        //adjust bigi structs operation length
        if (actual_bigi_size != (operation_length_in_bites / 32)) {
                sw_rsa_adjust_length(operation_length_in_bites / 32);
        }
        rsa_block_size = operation_length_in_bites >> 3; //  for RSA4096 -> 4096 / 8 = 512B

        //load p, q, e from key byte array
        bigi_from_bytes(&p_bigi, key_byte_array, rsa_block_size >> 1); // for RSA 4096 > 512B / 2 = 256B
        bigi_from_bytes(&q_bigi, key_byte_array + (rsa_block_size >> 1), rsa_block_size >> 1);
        bigi_from_bytes(&e_bigi, key_byte_array + rsa_block_size, rsa_block_size);

    dbg_puts_labeled ("P:"); bigi_print(&p_bigi); dbg_putch('\n');
    dbg_puts_labeled ("Q:"); bigi_print(&q_bigi); dbg_putch('\n');
    dbg_puts_labeled ("E:"); bigi_print(&e_bigi); dbg_putch('\n');

    //calculate and save p -1 and q - 1
    bigi_sub_one(&p_bigi,&tmp0_bigi);
    bigi_sub_one(&q_bigi,&tmp1_bigi); 
    dbg_puts_labeled ("P - 1:"); bigi_print(&tmp0_bigi); dbg_putch('\n');
    dbg_puts_labeled ("Q - 1:"); bigi_print(&tmp1_bigi); dbg_putch('\n');

        //calculate and save n
        bigi_mult_fit(&p_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &n_bigi);
        dbg_puts_labeled ("N:"); bigi_print(&n_bigi); dbg_putch('\n');

        //calculate and save Phi
        bigi_mult_fit(&tmp0_bigi, &tmp1_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp00_bigi);
        dbg_puts_labeled ("Phi:"); bigi_print(&tmp00_bigi); dbg_putch('\n');

        //calculate and save D
        bigi_mod_inv(&e_bigi, &tmp00_bigi, &tmpary_bigi[0], TMPARY_LEN, &d_bigi);
        dbg_puts_labeled ("D:"); bigi_print(&d_bigi); dbg_putch('\n');

        //calculate and save dp
        bigi_mod_red(&d_bigi, &tmp0_bigi, &tmpary_bigi[0], TMPARY_LEN, &dp_bigi);
        dbg_puts_labeled ("Dp:"); bigi_print(&dp_bigi); dbg_putch('\n');

        //calculate and save dq
        bigi_mod_red(&d_bigi, &tmp1_bigi, &tmpary_bigi[0], TMPARY_LEN, &dq_bigi);
        dbg_puts_labeled ("Dq:"); bigi_print(&dq_bigi); dbg_putch('\n');

        //calculate and save qi
        bigi_mod_inv(&q_bigi, &p_bigi, &tmpary_bigi[0], TMPARY_LEN, &qi_bigi);
        dbg_puts_labeled ("Qinv:"); bigi_print(&qi_bigi); dbg_putch('\n');

        return 0x00;

}

uint8_t sw_rsa_key_get (uint8_t* k) {
}

uint8_t sw_rsa_encrypt (uint8_t* plaintext) {
        /*loading part*/
    if (encrytion_decryption_state != ENC) {
        dbg_puts_labeled ("CHANGED TO ENCRIPTION.");
        encrytion_decryption_state = ENC;
        message_parts_cnt = 0;
    }

    if (message_parts_cnt >= message_parts) {
        dbg_puts_labeled ("MSG CNT HIGHER.");
        return 0x01;
    }

    if (message_parts_cnt < message_parts) {
        memcpy(plaintext_byte_array + (message_parts_cnt * block_length_in_bytes), plaintext, block_length_in_bytes);
        ++message_parts_cnt;
    }

    if (message_parts_cnt < message_parts) {
        return 0x00;
    }

    if (actual_bigi_size != (operation_length_in_bites / 32)) {
        key_parts_cnt = 0;
        dbg_puts_labeled ("ADJUST LENGHT");
        sw_rsa_adjust_length(operation_length_in_bites / 32);
    }

    if (key_parts_cnt != key_parts) {
        dbg_puts_labeled ("KEY PARTS NOT MATCH");
        return 0x01;
    }

/*parsing part*/
    //load plaintext to bigi_t
    bigi_from_bytes(&plaintext_bigi, plaintext_byte_array, rsa_block_size);
    dbg_puts_labeled("PLAINTEXT :"); bigi_print(&plaintext_bigi); dbg_putch('\n');

    dbg_puts_labeled("N:"); bigi_print(&n_bigi); dbg_putch('\n');
    dbg_puts_labeled("E:"); bigi_print(&e_bigi); dbg_putch('\n');
#ifdef MONGOMERY
    bigi_get_mont_params(&n_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp00_bigi, &mu_mult);
    dbg_puts_labeled("R:"); bigi_print(&tmp00_bigi); dbg_putch('\n');
    bigi_mod_exp_mont(&plaintext_bigi, &e_bigi, &n_bigi, &tmp00_bigi, mu_mult, &tmpary_bigi[0], TMPARY_LEN, &ciphertext_bigi);
#else
    bigi_set_zero(&tmp00_bigi);
    bigi_mod_exp(&plaintext_bigi, &e_bigi, &n_bigi, &tmp00_bigi, &ciphertext_bigi);
#endif
    dbg_puts_labeled ("CIPHERTEXT:"); bigi_print(&ciphertext_bigi); dbg_putch('\n');
    /*sending part*/
    bigi_to_bytes(ciphertext_byte_array, rsa_block_size, &ciphertext_bigi);

    message_parts_cnt = 0;
    while (message_parts_cnt < message_parts) {
        simpleserial_put('r', block_length_in_bytes, ciphertext_byte_array + (message_parts_cnt * block_length_in_bytes));
        ++message_parts_cnt;
    }

    return 0x00;
}

uint8_t sw_rsa_decrypt (uint8_t* ciphertext) {
        /*loading part*/
    if (encrytion_decryption_state != DEC) {
        dbg_puts_labeled ("CHANGED TO DECRIPTION.");
        encrytion_decryption_state = DEC;
        message_parts_cnt = 0;
    }

    if (message_parts_cnt >= message_parts) {
        dbg_puts_labeled ("MSG CNT HIGHER.");
        return 0x01;
    }

    if (message_parts_cnt < message_parts) {
        memcpy(ciphertext_byte_array + (message_parts_cnt * block_length_in_bytes), ciphertext, block_length_in_bytes);
        ++message_parts_cnt;
    }

    if (message_parts_cnt < message_parts) {
        return 0x00;
    }

    if (actual_bigi_size != (operation_length_in_bites / 32)) {
        key_parts_cnt = 0;
        dbg_puts_labeled ("ADJUST LENGHT");
        sw_rsa_adjust_length(operation_length_in_bites / 32);
    }

    if (key_parts_cnt != key_parts) {
        dbg_puts_labeled ("KEY PARTS NOT MATCH");
        return 0x01;
    }
        /*parsing part*/
    bigi_from_bytes(&ciphertext_bigi, ciphertext_byte_array, rsa_block_size);
    dbg_puts_labeled ("CIPHERTEXT :"); bigi_print(&ciphertext_bigi); dbg_putch('\n');
        /*decryption part*/
    is_add_p_neccessary = 1;
    //m1 = C^Dp mod P
#ifdef MONGOMERY
    //tmp0 = C mod P
    bigi_mod_red(&ciphertext_bigi, &p_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp1_bigi);
    dbg_puts_labeled("Cp :"); bigi_print(&tmp1_bigi); dbg_putch('\n');
    //m1 = tmp0^Dp mod P
    bigi_get_mont_params(&p_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp2_bigi, &mu_mult);
    dbg_puts_labeled ("R-M1:"); bigi_print(&tmp2_bigi); dbg_putch('\n');
    bigi_mod_exp_mont(&tmp1_bigi, &dp_bigi, &p_bigi, &tmp2_bigi, mu_mult, &tmpary_bigi[0], TMPARY_LEN, &tmp0_bigi);
    dbg_puts_labeled ("M1 :"); bigi_print(&tmp0_bigi); dbg_putch('\n');

    //tmp0 = C mod Q
    bigi_mod_red(&ciphertext_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp2_bigi);
    dbg_puts_labeled("Cq :"); bigi_print(&tmp2_bigi); dbg_putch('\n');
    //m2 = tmp0^Dq mod Q
    bigi_get_mont_params(&q_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp3_bigi, &mu_mult);
    dbg_puts_labeled ("R-M2:"); bigi_print(&tmp3_bigi); dbg_putch('\n');
    bigi_mod_exp_mont(&tmp2_bigi, &dq_bigi, &q_bigi, &tmp3_bigi, mu_mult, &tmpary_bigi[0], TMPARY_LEN, &tmp1_bigi);
    dbg_puts_labeled ("M2 :"); bigi_print(&tmp1_bigi); dbg_putch('\n');
#else
    //tmp0 = C mod P
    bigi_mod_red(&ciphertext_bigi, &p_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp1_bigi);
    dbg_puts_labeled("Cp :"); bigi_print(&tmp1_bigi); dbg_putch('\n');
    //m1 = tmp0^Dp mod P
    bigi_mod_exp(&tmp1_bigi, &dp_bigi, &p_bigi, &tmp2_bigi, &tmp0_bigi);
    dbg_puts_labeled("M1 :"); bigi_print(&tmp0_bigi); dbg_putch('\n');

    //tmp0 = C mod Q
    bigi_mod_red(&ciphertext_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp2_bigi);
    dbg_puts_labeled("Cq :"); bigi_print(&tmp2_bigi); dbg_putch('\n');
    //m2 = tmp0^Dq mod Q
    bigi_mod_exp(&tmp2_bigi, &dq_bigi, &q_bigi, &tmp3_bigi, &tmp1_bigi);
    dbg_puts_labeled("M2 :"); bigi_print(&tmp1_bigi); dbg_putch('\n');
 #endif
    //h  = (m1 - m2) * qi mod P
        // tmp2 = m1 - m2
    bigi_sub(&tmp0_bigi, &tmp1_bigi, &tmp2_bigi);
    dbg_puts_labeled("M1 - M2:"); bigi_print(&tmp2_bigi); dbg_putch('\n');

    /*First way to check negative value*/
    //bigi_cmp(&tmp0_bigi, &tmp1_bigi, &compare_result);
    //if (compare_result == CMP_LOWER) {

    /*Second way to check negative value*/
    //if (tmp2_bigi.ary[(operation_length_in_bites >> 6) + 1] > 0) {

    /* Check if result is negative number, then add P */
    index_value = 0;
    bigi_get_bit(&tmp2_bigi, ((operation_length_in_bites >> 1) + 1), &index_value);
    if (index_value == 1) {
        dbg_puts_labeled("A LOWER than B");
        bigi_add(&tmp2_bigi, &p_bigi, &tmp0_bigi);
        dbg_puts_labeled("M1 - M2 + p:"); bigi_print(&tmp0_bigi); dbg_putch('\n');
    }
    else {
        dbg_puts_labeled("A GREATER than B");
        bigi_copy(&tmp0_bigi, &tmp2_bigi);
        dbg_puts_labeled("M1 - M2:"); bigi_print(&tmp0_bigi); dbg_putch('\n');
    }

    // tmp0 * qi mod P
    bigi_mod_mult(&tmp0_bigi, &qi_bigi, &p_bigi, &tmp2_bigi);
    dbg_puts_labeled("H :"); bigi_print(&tmp2_bigi); dbg_putch('\n');

    // m = m2 + h * Q
        // tmp00 = h * Q
    bigi_mult_fit(&tmp2_bigi, &q_bigi, &tmpary_bigi[0], TMPARY_LEN, &tmp00_bigi);
    dbg_puts_labeled("H * Q :"); bigi_print(&tmp00_bigi); dbg_putch('\n');
    // m2 + tmp00
    bigi_add(&tmp1_bigi, &tmp00_bigi, &plaintext_bigi);
    dbg_puts_labeled("PLAINTEXT :"); bigi_print(&plaintext_bigi); dbg_putch('\n');

        /*sending part*/
    bigi_to_bytes(plaintext_byte_array, rsa_block_size, &plaintext_bigi);

    message_parts_cnt = 0;
    while (message_parts_cnt < message_parts) {
        simpleserial_put('r', block_length_in_bytes, plaintext_byte_array + (message_parts_cnt * block_length_in_bytes));
        ++message_parts_cnt;
    }

    return 0x00;
}