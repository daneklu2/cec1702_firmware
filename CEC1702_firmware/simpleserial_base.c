#include <stdint.h>
#include <stdlib.h>

#include "simpleserial/simpleserial.h"
#include "device_setup/cec1702_setup.h"
#include "RSA_implementation/rsa_sw_implementation.h"
#include "debug.h"
#define DBG
#ifdef DBG

#include "simpleserial/functions.h"
uint8_t AES_byte_array[16] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f};
uint8_t AES_key_array[16]  = {0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};
uint8_t AES_in_array[16]   = {0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef, 0x01, 0x23, 0x45, 0x67, 0x89};
//values for sw rsa 512bit
const char* key_part_1_ascii = "EF4AD246C2CEB9C68FA96AF1B5C33B99CD107551E86625A7FA593B821FC472A7CA5772354D642E058B2783B640CC93E44116907D94305519078068918E616BEF";
const char* key_part_2_ascii = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000010000001";
const char* plaintext_ascii  = "11144444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444444123456";
const char* ciphertext_ascii = "174ca6579fa508a2bea31621764d302e22a3cefe1c8e5e05ef0cdb8788031a3bdbf4e685f43c70020cc0e8f09b937436adfd19fc14a23f86e1e048a34f925188";
uint8_t key_part_1[64];
uint8_t key_part_2[64];
uint8_t plaintext[64];
uint8_t ciphertext[64];
#endif
int main(void)
{
    platform_init();
    init_uart();
    trigger_setup();

    /*default "k", "p", and "c" cmd is set to echo */
    /*adjustements are made by calling the "s" cmd*/
    dbg_puts_labeled("SimpleSerial Init");
    simpleserial_init();
    sw_rsa_init();
 #ifdef DBG
    dbg_puts_labeled( "RSA init.\n");
    sw_rsa_init();
    dbg_puts_labeled("RSA key set:\n");
    dbg_puts_labeled("part1:");
    dbg_puts_labeled(key_part_1_ascii);
    hex_decode(64, key_part_1_ascii, key_part_1);
    dbg_puts_labeled("part2:");
    dbg_puts_labeled(key_part_2_ascii);
    hex_decode(64, key_part_2_ascii, key_part_2);
    sw_rsa_key_set(key_part_1);
    sw_rsa_key_set(key_part_2);

    dbg_puts("\n\n\n\n\n");


    dbg_puts_labeled("RSA encrypt.\n");
    dbg_puts_labeled("plaintext:");
    dbg_puts_labeled(plaintext_ascii);
    hex_decode(64, plaintext_ascii, plaintext);
    sw_rsa_encrypt(plaintext);


    dbg_puts("\n\n\n\n\n");


    dbg_puts_labeled("RSA decrypt.\n");
    dbg_puts_labeled("ciphertext:");
    dbg_puts_labeled(ciphertext_ascii);
    hex_decode(64, ciphertext_ascii, ciphertext);
    sw_rsa_decrypt(ciphertext);
 #endif
    while(1)
            simpleserial_get();
            
    return 0;
}