#ifndef DEBUG_H
#define DEBUG_H

#define DBG
void dbg_putch (char c);

void dbg_puts (char * str);

void dbg_putch_labeled (char c);

void dbg_puts_labeled (char * str);

#endif //DEBUG_H