#include "debug.h"
#include "device_setup/cec1702_setup.h"

#ifdef DBG

void put_label () {
        putch ('D');
        putch ('B');
        putch ('G');
        putch (':');
}

void dbg_putch (char c) {
        putch(c);
}

void dbg_puts (char * str) {
        putch('\n');
        UART0_Write_Text(str);
        putch('\n');
}

void dbg_putch_labeled (char c) {
        put_label();
        putch(c);
        putch('\n');
}

void dbg_puts_labeled (char * str) {
        put_label();
        UART0_Write_Text(str);
        putch('\n');
}

#else

void put_label () {}

void dbg_putch (char c) {}

void dbg_puts (char * str) {}

void dbg_putch_labeled (char c) {}

void dbg_puts_labeled (char * str) {}


#endif