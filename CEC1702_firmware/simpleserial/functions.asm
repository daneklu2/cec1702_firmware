_echo:
;functions.c,30 :: 		uint8_t echo(uint8_t* pt) {
; pt start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; pt end address is: 0 (R0)
; pt start address is: 0 (R0)
;functions.c,31 :: 		if (!pt || !outbuf) {
CMP	R0, #0
IT	EQ
BEQ	L__echo64
MOVW	R1, #lo_addr(functions_outbuf+0)
MOVT	R1, #hi_addr(functions_outbuf+0)
CMP	R1, #0
IT	EQ
BEQ	L__echo63
IT	AL
BAL	L_echo2
; pt end address is: 0 (R0)
L__echo64:
L__echo63:
;functions.c,32 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_echo
;functions.c,33 :: 		}
L_echo2:
;functions.c,34 :: 		trigger_high();
; pt start address is: 0 (R0)
STR	R0, [SP, #4]
BL	_trigger_high+0
LDR	R0, [SP, #4]
;functions.c,35 :: 		memcpy(outbuf, pt, 16);
MOVS	R2, #16
SXTH	R2, R2
MOV	R1, R0
; pt end address is: 0 (R0)
MOVW	R0, #lo_addr(functions_outbuf+0)
MOVT	R0, #hi_addr(functions_outbuf+0)
BL	_memcpy+0
;functions.c,36 :: 		Delay_ms(100);
MOVW	R7, #27134
MOVT	R7, #24
NOP
NOP
L_echo3:
SUBS	R7, R7, #1
BNE	L_echo3
NOP
NOP
NOP
;functions.c,37 :: 		trigger_low();
BL	_trigger_low+0
;functions.c,38 :: 		simpleserial_put('r', 16, outbuf);
MOVW	R2, #lo_addr(functions_outbuf+0)
MOVT	R2, #hi_addr(functions_outbuf+0)
MOVS	R1, #16
SXTH	R1, R1
MOVS	R0, #114
BL	_simpleserial_put+0
;functions.c,39 :: 		return 0x00;
MOVS	R0, #0
;functions.c,40 :: 		}
L_end_echo:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _echo
_only_return:
;functions.c,42 :: 		uint8_t only_return (uint8_t* pt) {
SUB	SP, SP, #4
;functions.c,43 :: 		return 0x00;
MOVS	R0, #0
;functions.c,44 :: 		}
L_end_only_return:
ADD	SP, SP, #4
BX	LR
; end of _only_return
_reset:
;functions.c,46 :: 		uint8_t reset(uint8_t* params) {
SUB	SP, SP, #4
;functions.c,55 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,56 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,57 :: 		return 0x00;
MOVS	R0, #0
;functions.c,58 :: 		}
L_end_reset:
ADD	SP, SP, #4
BX	LR
; end of _reset
_check_version:
;functions.c,61 :: 		uint8_t check_version(uint8_t* v) {
SUB	SP, SP, #4
;functions.c,62 :: 		return 0x00;
MOVS	R0, #0
;functions.c,63 :: 		}
L_end_check_version:
ADD	SP, SP, #4
BX	LR
; end of _check_version
_set_operation:
;functions.c,65 :: 		uint8_t set_operation (uint8_t* operation) {
; operation start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; operation end address is: 0 (R0)
; operation start address is: 0 (R0)
;functions.c,66 :: 		dbg_puts_labeled ("In set operation");
MOVW	R1, #lo_addr(?lstr1_functions+0)
MOVT	R1, #hi_addr(?lstr1_functions+0)
STR	R0, [SP, #4]
MOV	R0, R1
BL	_dbg_puts_labeled+0
LDR	R0, [SP, #4]
;functions.c,67 :: 		if ( *operation == 'a' ) {
LDRB	R1, [R0, #0]
CMP	R1, #97
IT	NE
BNE	L_set_operation5
; operation end address is: 0 (R0)
;functions.c,68 :: 		dbg_puts_labeled ("Operation = a");
MOVW	R1, #lo_addr(?lstr2_functions+0)
MOVT	R1, #hi_addr(?lstr2_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,69 :: 		simpleserial_addcmd ('k', 16, hw_aes_key_set);
MOVW	R1, #lo_addr(_hw_aes_key_set+0)
MOVT	R1, #hi_addr(_hw_aes_key_set+0)
MOV	R2, R1
MOVS	R1, #16
MOVS	R0, #107
BL	_simpleserial_addcmd+0
;functions.c,70 :: 		simpleserial_addcmd ('K',  0, only_return);
MOVW	R1, #lo_addr(_only_return+0)
MOVT	R1, #hi_addr(_only_return+0)
MOV	R2, R1
MOVS	R1, #0
MOVS	R0, #75
BL	_simpleserial_addcmd+0
;functions.c,71 :: 		simpleserial_addcmd ('p', 16, hw_aes_encrypt);
MOVW	R1, #lo_addr(_hw_aes_encrypt+0)
MOVT	R1, #hi_addr(_hw_aes_encrypt+0)
MOV	R2, R1
MOVS	R1, #16
MOVS	R0, #112
BL	_simpleserial_addcmd+0
;functions.c,72 :: 		simpleserial_addcmd ('c', 16, hw_aes_decrypt);
MOVW	R1, #lo_addr(_hw_aes_decrypt+0)
MOVT	R1, #hi_addr(_hw_aes_decrypt+0)
MOV	R2, R1
MOVS	R1, #16
MOVS	R0, #99
BL	_simpleserial_addcmd+0
;functions.c,75 :: 		message_parts = 1;
MOVS	R2, #1
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
STRH	R2, [R1, #0]
;functions.c,76 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,77 :: 		key_parts = 1;
MOVS	R2, #1
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
STRH	R2, [R1, #0]
;functions.c,78 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,79 :: 		operation_length_in_bites = 128;
MOVS	R2, #128
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
STRH	R2, [R1, #0]
;functions.c,80 :: 		block_length_in_bytes = 16;
MOVS	R2, #16
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
STRH	R2, [R1, #0]
;functions.c,81 :: 		actual_opertion = 'a';
MOVS	R2, #97
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
STRB	R2, [R1, #0]
;functions.c,82 :: 		actual_implementation = 'h';
MOVS	R2, #104
MOVW	R1, #lo_addr(_actual_implementation+0)
MOVT	R1, #hi_addr(_actual_implementation+0)
STRB	R2, [R1, #0]
;functions.c,83 :: 		}
IT	AL
BAL	L_set_operation6
L_set_operation5:
;functions.c,84 :: 		else if ( *operation == 'r' ) {
; operation start address is: 0 (R0)
LDRB	R1, [R0, #0]
CMP	R1, #114
IT	NE
BNE	L_set_operation7
; operation end address is: 0 (R0)
;functions.c,85 :: 		dbg_puts_labeled ("Operation = r");
MOVW	R1, #lo_addr(?lstr3_functions+0)
MOVT	R1, #hi_addr(?lstr3_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,86 :: 		simpleserial_addcmd ('k', 64, sw_rsa_key_set);
MOVW	R1, #lo_addr(_sw_rsa_key_set+0)
MOVT	R1, #hi_addr(_sw_rsa_key_set+0)
MOV	R2, R1
MOVS	R1, #64
MOVS	R0, #107
BL	_simpleserial_addcmd+0
;functions.c,87 :: 		simpleserial_addcmd ('K',  0, sw_rsa_key_get);
MOVW	R1, #lo_addr(_sw_rsa_key_get+0)
MOVT	R1, #hi_addr(_sw_rsa_key_get+0)
MOV	R2, R1
MOVS	R1, #0
MOVS	R0, #75
BL	_simpleserial_addcmd+0
;functions.c,88 :: 		simpleserial_addcmd ('p', 64, sw_rsa_encrypt);
MOVW	R1, #lo_addr(_sw_rsa_encrypt+0)
MOVT	R1, #hi_addr(_sw_rsa_encrypt+0)
MOV	R2, R1
MOVS	R1, #64
MOVS	R0, #112
BL	_simpleserial_addcmd+0
;functions.c,89 :: 		simpleserial_addcmd ('c', 64, sw_rsa_decrypt);
MOVW	R1, #lo_addr(_sw_rsa_decrypt+0)
MOVT	R1, #hi_addr(_sw_rsa_decrypt+0)
MOV	R2, R1
MOVS	R1, #64
MOVS	R0, #99
BL	_simpleserial_addcmd+0
;functions.c,92 :: 		message_parts = 1;
MOVS	R2, #1
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
STRH	R2, [R1, #0]
;functions.c,93 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,94 :: 		key_parts = 2; // 1. p[256] | q[256], 2. e[512]
MOVS	R2, #2
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
STRH	R2, [R1, #0]
;functions.c,95 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,96 :: 		operation_length_in_bites = 512;
MOVW	R2, #512
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
STRH	R2, [R1, #0]
;functions.c,97 :: 		block_length_in_bytes = 64;
MOVS	R2, #64
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
STRH	R2, [R1, #0]
;functions.c,98 :: 		actual_opertion = 'r';
MOVS	R2, #114
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
STRB	R2, [R1, #0]
;functions.c,99 :: 		actual_implementation = 's';
MOVS	R2, #115
MOVW	R1, #lo_addr(_actual_implementation+0)
MOVT	R1, #hi_addr(_actual_implementation+0)
STRB	R2, [R1, #0]
;functions.c,100 :: 		}
IT	AL
BAL	L_set_operation8
L_set_operation7:
;functions.c,101 :: 		else if ( *operation == 'b' ) {
; operation start address is: 0 (R0)
LDRB	R1, [R0, #0]
CMP	R1, #98
IT	NE
BNE	L_set_operation9
; operation end address is: 0 (R0)
;functions.c,103 :: 		}
IT	AL
BAL	L_set_operation10
L_set_operation9:
;functions.c,104 :: 		else if ( *operation == 'p' ) {
; operation start address is: 0 (R0)
LDRB	R1, [R0, #0]
; operation end address is: 0 (R0)
CMP	R1, #112
IT	NE
BNE	L_set_operation11
;functions.c,106 :: 		}
IT	AL
BAL	L_set_operation12
L_set_operation11:
;functions.c,108 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_operation
;functions.c,109 :: 		}
L_set_operation12:
L_set_operation10:
L_set_operation8:
L_set_operation6:
;functions.c,110 :: 		dbg_puts_labeled ("SUCCESS operation set");
MOVW	R1, #lo_addr(?lstr4_functions+0)
MOVT	R1, #hi_addr(?lstr4_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,111 :: 		return 0x00;
MOVS	R0, #0
;functions.c,113 :: 		}
L_end_set_operation:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _set_operation
_change_function:
;functions.c,115 :: 		void change_function (char cmd, uint8_t (*fp)(uint8_t*)) {
; i start address is: 4 (R1)
; cmd start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
;functions.c,117 :: 		for (i = 0; i < MAX_SS_CMDS; ++i) {
;functions.c,115 :: 		void change_function (char cmd, uint8_t (*fp)(uint8_t*)) {
;functions.c,117 :: 		for (i = 0; i < MAX_SS_CMDS; ++i) {
;functions.c,115 :: 		void change_function (char cmd, uint8_t (*fp)(uint8_t*)) {
; i end address is: 4 (R1)
; cmd end address is: 0 (R0)
; cmd start address is: 0 (R0)
;functions.c,116 :: 		uint8_t i = 0;
;functions.c,117 :: 		for (i = 0; i < MAX_SS_CMDS; ++i) {
; i start address is: 4 (R1)
MOVS	R1, #0
; i end address is: 4 (R1)
L_change_function13:
; i start address is: 4 (R1)
; cmd start address is: 0 (R0)
; cmd end address is: 0 (R0)
CMP	R1, #20
IT	CS
BCS	L_change_function14
; cmd end address is: 0 (R0)
;functions.c,118 :: 		dbg_putch_labeled(commands[i].c);
; cmd start address is: 0 (R0)
LSLS	R3, R1, #3
MOVW	R2, #lo_addr(_commands+0)
MOVT	R2, #hi_addr(_commands+0)
ADDS	R2, R2, R3
LDRB	R2, [R2, #0]
STRB	R0, [SP, #4]
STRB	R1, [SP, #5]
UXTB	R0, R2
BL	_dbg_putch_labeled+0
LDRB	R1, [SP, #5]
LDRB	R0, [SP, #4]
;functions.c,119 :: 		if (commands[i].c == cmd) {
LSLS	R3, R1, #3
MOVW	R2, #lo_addr(_commands+0)
MOVT	R2, #hi_addr(_commands+0)
ADDS	R2, R2, R3
LDRB	R2, [R2, #0]
CMP	R2, R0
IT	NE
BNE	L_change_function16
;functions.c,120 :: 		commands[i].c   = cmd;
LSLS	R3, R1, #3
; i end address is: 4 (R1)
MOVW	R2, #lo_addr(_commands+0)
MOVT	R2, #hi_addr(_commands+0)
ADDS	R2, R2, R3
STRB	R0, [R2, #0]
; cmd end address is: 0 (R0)
;functions.c,121 :: 		return;
IT	AL
BAL	L_end_change_function
;functions.c,122 :: 		}
L_change_function16:
;functions.c,117 :: 		for (i = 0; i < MAX_SS_CMDS; ++i) {
; i start address is: 4 (R1)
; cmd start address is: 0 (R0)
ADDS	R1, R1, #1
UXTB	R1, R1
;functions.c,123 :: 		}
; cmd end address is: 0 (R0)
; i end address is: 4 (R1)
IT	AL
BAL	L_change_function13
L_change_function14:
;functions.c,124 :: 		}
L_end_change_function:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _change_function
_set_implementation:
;functions.c,126 :: 		uint8_t set_implementation (uint8_t* implementation) {
SUB	SP, SP, #8
STR	LR, [SP, #0]
STR	R0, [SP, #4]
;functions.c,127 :: 		uint8_t i = 0;
;functions.c,128 :: 		dbg_puts_labeled ("In set implementation");
MOVW	R1, #lo_addr(?lstr5_functions+0)
MOVT	R1, #hi_addr(?lstr5_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,129 :: 		if ( actual_opertion == 'a' ) {
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #97
IT	NE
BNE	L_set_implementation17
;functions.c,130 :: 		if (*implementation == 's') {
LDR	R1, [SP, #4]
LDRB	R1, [R1, #0]
CMP	R1, #115
IT	NE
BNE	L_set_implementation18
;functions.c,131 :: 		dbg_puts_labeled ("Implementation = SA");
MOVW	R1, #lo_addr(?lstr6_functions+0)
MOVT	R1, #hi_addr(?lstr6_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,132 :: 		change_function ('k', sw_aes_key_set);
MOVW	R1, #lo_addr(_sw_aes_key_set+0)
MOVT	R1, #hi_addr(_sw_aes_key_set+0)
MOVS	R0, #107
BL	_change_function+0
;functions.c,133 :: 		change_function ('K', only_return);
MOVW	R1, #lo_addr(_only_return+0)
MOVT	R1, #hi_addr(_only_return+0)
MOVS	R0, #75
BL	_change_function+0
;functions.c,134 :: 		change_function ('p', sw_aes_encrypt);
MOVW	R1, #lo_addr(_sw_aes_encrypt+0)
MOVT	R1, #hi_addr(_sw_aes_encrypt+0)
MOVS	R0, #112
BL	_change_function+0
;functions.c,135 :: 		change_function ('c', echo);
MOVW	R1, #lo_addr(_echo+0)
MOVT	R1, #hi_addr(_echo+0)
MOVS	R0, #99
BL	_change_function+0
;functions.c,136 :: 		}
IT	AL
BAL	L_set_implementation19
L_set_implementation18:
;functions.c,137 :: 		else if (*implementation == 'h') {
LDR	R1, [SP, #4]
LDRB	R1, [R1, #0]
CMP	R1, #104
IT	NE
BNE	L_set_implementation20
;functions.c,138 :: 		dbg_puts_labeled ("Implementation = HA");
MOVW	R1, #lo_addr(?lstr7_functions+0)
MOVT	R1, #hi_addr(?lstr7_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,139 :: 		change_function ('k', hw_aes_key_set);
MOVW	R1, #lo_addr(_hw_aes_key_set+0)
MOVT	R1, #hi_addr(_hw_aes_key_set+0)
MOVS	R0, #107
BL	_change_function+0
;functions.c,140 :: 		change_function ('K', only_return);
MOVW	R1, #lo_addr(_only_return+0)
MOVT	R1, #hi_addr(_only_return+0)
MOVS	R0, #75
BL	_change_function+0
;functions.c,141 :: 		change_function ('p', hw_aes_encrypt);
MOVW	R1, #lo_addr(_hw_aes_encrypt+0)
MOVT	R1, #hi_addr(_hw_aes_encrypt+0)
MOVS	R0, #112
BL	_change_function+0
;functions.c,142 :: 		change_function ('c', hw_aes_decrypt);
MOVW	R1, #lo_addr(_hw_aes_decrypt+0)
MOVT	R1, #hi_addr(_hw_aes_decrypt+0)
MOVS	R0, #99
BL	_change_function+0
;functions.c,143 :: 		}
IT	AL
BAL	L_set_implementation21
L_set_implementation20:
;functions.c,145 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_implementation
;functions.c,146 :: 		}
L_set_implementation21:
L_set_implementation19:
;functions.c,147 :: 		}
IT	AL
BAL	L_set_implementation22
L_set_implementation17:
;functions.c,148 :: 		else if ( actual_opertion == 'r' ) {
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #114
IT	NE
BNE	L_set_implementation23
;functions.c,149 :: 		if (*implementation == 's') {
LDR	R1, [SP, #4]
LDRB	R1, [R1, #0]
CMP	R1, #115
IT	NE
BNE	L_set_implementation24
;functions.c,150 :: 		dbg_puts_labeled ("Implementation = SR");
MOVW	R1, #lo_addr(?lstr8_functions+0)
MOVT	R1, #hi_addr(?lstr8_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,151 :: 		change_function ('k', sw_rsa_key_set);
MOVW	R1, #lo_addr(_sw_rsa_key_set+0)
MOVT	R1, #hi_addr(_sw_rsa_key_set+0)
MOVS	R0, #107
BL	_change_function+0
;functions.c,152 :: 		change_function ('K', sw_rsa_key_get);
MOVW	R1, #lo_addr(_sw_rsa_key_get+0)
MOVT	R1, #hi_addr(_sw_rsa_key_get+0)
MOVS	R0, #75
BL	_change_function+0
;functions.c,153 :: 		change_function ('p', sw_rsa_encrypt);
MOVW	R1, #lo_addr(_sw_rsa_encrypt+0)
MOVT	R1, #hi_addr(_sw_rsa_encrypt+0)
MOVS	R0, #112
BL	_change_function+0
;functions.c,154 :: 		change_function ('c', sw_rsa_decrypt);
MOVW	R1, #lo_addr(_sw_rsa_decrypt+0)
MOVT	R1, #hi_addr(_sw_rsa_decrypt+0)
MOVS	R0, #99
BL	_change_function+0
;functions.c,155 :: 		}
IT	AL
BAL	L_set_implementation25
L_set_implementation24:
;functions.c,156 :: 		else if (*implementation == 'h') {
LDR	R1, [SP, #4]
LDRB	R1, [R1, #0]
CMP	R1, #104
IT	NE
BNE	L_set_implementation26
;functions.c,157 :: 		dbg_puts_labeled ("Implementation = HR");
MOVW	R1, #lo_addr(?lstr9_functions+0)
MOVT	R1, #hi_addr(?lstr9_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,158 :: 		if (operation_length_in_bites < 1024) {
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
LDRH	R1, [R1, #0]
CMP	R1, #1024
IT	CS
BCS	L_set_implementation27
;functions.c,159 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_implementation
;functions.c,160 :: 		}
L_set_implementation27:
;functions.c,165 :: 		}
IT	AL
BAL	L_set_implementation28
L_set_implementation26:
;functions.c,167 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_implementation
;functions.c,168 :: 		}
L_set_implementation28:
L_set_implementation25:
;functions.c,169 :: 		}
IT	AL
BAL	L_set_implementation29
L_set_implementation23:
;functions.c,170 :: 		else if ( actual_opertion == 'b' ) {
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #98
IT	NE
BNE	L_set_implementation30
;functions.c,172 :: 		}
IT	AL
BAL	L_set_implementation31
L_set_implementation30:
;functions.c,173 :: 		else if ( actual_opertion == 'p' ) {
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #112
IT	NE
BNE	L_set_implementation32
;functions.c,175 :: 		}
IT	AL
BAL	L_set_implementation33
L_set_implementation32:
;functions.c,177 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_implementation
;functions.c,178 :: 		}
L_set_implementation33:
L_set_implementation31:
L_set_implementation29:
L_set_implementation22:
;functions.c,180 :: 		actual_implementation = *implementation;
LDR	R1, [SP, #4]
LDRB	R2, [R1, #0]
MOVW	R1, #lo_addr(_actual_implementation+0)
MOVT	R1, #hi_addr(_actual_implementation+0)
STRB	R2, [R1, #0]
;functions.c,181 :: 		dbg_puts_labeled ("SUCCESS implementation set");
MOVW	R1, #lo_addr(?lstr10_functions+0)
MOVT	R1, #hi_addr(?lstr10_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,182 :: 		return 0x00;
MOVS	R0, #0
;functions.c,183 :: 		}
L_end_set_implementation:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _set_implementation
_check_digits:
;functions.c,185 :: 		uint8_t check_digits (uint8_t * length) {
; length start address is: 0 (R0)
SUB	SP, SP, #4
MOV	R1, R0
; length end address is: 0 (R0)
; length start address is: 4 (R1)
;functions.c,186 :: 		uint8_t i = 0;
; i start address is: 0 (R0)
MOVS	R0, #0
; length end address is: 4 (R1)
; i end address is: 0 (R0)
UXTB	R2, R0
MOV	R0, R1
;functions.c,187 :: 		for (i; i < 4; ++i) {
L_check_digits34:
; i start address is: 8 (R2)
; length start address is: 0 (R0)
; length start address is: 0 (R0)
; length end address is: 0 (R0)
CMP	R2, #4
IT	CS
BCS	L_check_digits35
; length end address is: 0 (R0)
;functions.c,188 :: 		if (length[i] > '9' || length[i] < '0') {
; length start address is: 0 (R0)
ADDS	R1, R0, R2
LDRB	R1, [R1, #0]
CMP	R1, #57
IT	HI
BHI	L__check_digits67
ADDS	R1, R0, R2
LDRB	R1, [R1, #0]
CMP	R1, #48
IT	CC
BCC	L__check_digits66
IT	AL
BAL	L_check_digits39
; length end address is: 0 (R0)
; i end address is: 8 (R2)
L__check_digits67:
L__check_digits66:
;functions.c,189 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_check_digits
;functions.c,190 :: 		}
L_check_digits39:
;functions.c,187 :: 		for (i; i < 4; ++i) {
; i start address is: 8 (R2)
; length start address is: 0 (R0)
ADDS	R2, R2, #1
UXTB	R2, R2
;functions.c,191 :: 		}
; length end address is: 0 (R0)
; i end address is: 8 (R2)
IT	AL
BAL	L_check_digits34
L_check_digits35:
;functions.c,192 :: 		return 0x00;
MOVS	R0, #0
;functions.c,193 :: 		}
L_end_check_digits:
ADD	SP, SP, #4
BX	LR
; end of _check_digits
_check_allowed_lengths:
;functions.c,196 :: 		uint8_t check_allowed_lengths (uint16_t number) {
; number start address is: 0 (R0)
SUB	SP, SP, #4
; number end address is: 0 (R0)
; number start address is: 0 (R0)
;functions.c,197 :: 		uint8_t i = 0;
; i start address is: 12 (R3)
MOVS	R3, #0
; i end address is: 12 (R3)
;functions.c,198 :: 		for (i; i < ALLOWED_LENGHTS_CNT; ++i) {
L_check_allowed_lengths40:
; i start address is: 12 (R3)
; number start address is: 0 (R0)
; number end address is: 0 (R0)
CMP	R3, #8
IT	CS
BCS	L_check_allowed_lengths41
; number end address is: 0 (R0)
;functions.c,199 :: 		if (allowed_lengths[i] == number) {
; number start address is: 0 (R0)
LSLS	R2, R3, #1
MOVW	R1, #lo_addr(functions_allowed_lengths+0)
MOVT	R1, #hi_addr(functions_allowed_lengths+0)
ADDS	R1, R1, R2
LDRH	R1, [R1, #0]
CMP	R1, R0
IT	NE
BNE	L_check_allowed_lengths43
; number end address is: 0 (R0)
; i end address is: 12 (R3)
;functions.c,200 :: 		return 0x00;
MOVS	R0, #0
IT	AL
BAL	L_end_check_allowed_lengths
;functions.c,201 :: 		}
L_check_allowed_lengths43:
;functions.c,198 :: 		for (i; i < ALLOWED_LENGHTS_CNT; ++i) {
; i start address is: 12 (R3)
; number start address is: 0 (R0)
ADDS	R3, R3, #1
UXTB	R3, R3
;functions.c,202 :: 		}
; number end address is: 0 (R0)
; i end address is: 12 (R3)
IT	AL
BAL	L_check_allowed_lengths40
L_check_allowed_lengths41:
;functions.c,203 :: 		return 0x01;
MOVS	R0, #1
;functions.c,204 :: 		}
L_end_check_allowed_lengths:
ADD	SP, SP, #4
BX	LR
; end of _check_allowed_lengths
_transform_to_uint:
;functions.c,206 :: 		uint16_t transform_to_uint (uint8_t * length) {
; length start address is: 0 (R0)
SUB	SP, SP, #4
; length end address is: 0 (R0)
; length start address is: 0 (R0)
;functions.c,207 :: 		uint16_t sum = 0;
; sum start address is: 16 (R4)
MOVW	R4, #0
;functions.c,208 :: 		sum += (length[0] - '0') * 1000;
LDRB	R1, [R0, #0]
SUBW	R2, R1, #48
SXTH	R2, R2
MOVW	R1, #1000
SXTH	R1, R1
MULS	R1, R2, R1
SXTH	R1, R1
ADDS	R3, R4, R1
UXTH	R3, R3
; sum end address is: 16 (R4)
;functions.c,209 :: 		sum += (length[1] - '0') *  100;
ADDS	R1, R0, #1
LDRB	R1, [R1, #0]
SUBW	R2, R1, #48
SXTH	R2, R2
MOVS	R1, #100
SXTH	R1, R1
MULS	R1, R2, R1
SXTH	R1, R1
ADDS	R3, R3, R1
UXTH	R3, R3
;functions.c,210 :: 		sum += (length[2] - '0') *   10;
ADDS	R1, R0, #2
LDRB	R1, [R1, #0]
SUBW	R2, R1, #48
SXTH	R2, R2
MOVS	R1, #10
SXTH	R1, R1
MULS	R1, R2, R1
SXTH	R1, R1
ADDS	R2, R3, R1
UXTH	R2, R2
;functions.c,211 :: 		sum += (length[3] - '0');
ADDS	R1, R0, #3
; length end address is: 0 (R0)
LDRB	R1, [R1, #0]
SUBS	R1, #48
SXTH	R1, R1
ADDS	R1, R2, R1
;functions.c,213 :: 		return sum;
UXTH	R0, R1
;functions.c,214 :: 		}
L_end_transform_to_uint:
ADD	SP, SP, #4
BX	LR
; end of _transform_to_uint
_change_length:
;functions.c,216 :: 		void change_length (char cmd, unsigned int length) {
; length start address is: 4 (R1)
; cmd start address is: 0 (R0)
SUB	SP, SP, #4
; length end address is: 4 (R1)
; cmd end address is: 0 (R0)
; cmd start address is: 0 (R0)
; length start address is: 4 (R1)
;functions.c,217 :: 		uint8_t i = 0;
; i start address is: 16 (R4)
MOVS	R4, #0
; cmd end address is: 0 (R0)
; length end address is: 4 (R1)
; i end address is: 16 (R4)
STRH	R1, [SP, #0]
UXTB	R1, R0
LDRH	R0, [SP, #0]
;functions.c,218 :: 		while (commands[i].c != cmd) {
L_change_length44:
; i start address is: 16 (R4)
; length start address is: 0 (R0)
; cmd start address is: 4 (R1)
LSLS	R3, R4, #3
MOVW	R2, #lo_addr(_commands+0)
MOVT	R2, #hi_addr(_commands+0)
ADDS	R2, R2, R3
LDRB	R2, [R2, #0]
CMP	R2, R1
IT	EQ
BEQ	L_change_length45
;functions.c,219 :: 		++i;
ADDS	R4, R4, #1
UXTB	R4, R4
;functions.c,220 :: 		}
; cmd end address is: 4 (R1)
IT	AL
BAL	L_change_length44
L_change_length45:
;functions.c,221 :: 		commands[i].len = length;
LSLS	R3, R4, #3
; i end address is: 16 (R4)
MOVW	R2, #lo_addr(_commands+0)
MOVT	R2, #hi_addr(_commands+0)
ADDS	R2, R2, R3
ADDS	R2, R2, #2
STRH	R0, [R2, #0]
; length end address is: 0 (R0)
;functions.c,222 :: 		}
L_end_change_length:
ADD	SP, SP, #4
BX	LR
; end of _change_length
_set_length:
;functions.c,224 :: 		uint8_t set_length (uint8_t* length) {
; length start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
MOV	R2, R0
; length end address is: 0 (R0)
; length start address is: 8 (R2)
;functions.c,225 :: 		uint16_t length_number = 0;
;functions.c,226 :: 		uint8_t i = 0;
;functions.c,227 :: 		dbg_puts_labeled ("In set length");
MOVW	R1, #lo_addr(?lstr11_functions+0)
MOVT	R1, #hi_addr(?lstr11_functions+0)
STR	R2, [SP, #4]
MOV	R0, R1
BL	_dbg_puts_labeled+0
LDR	R2, [SP, #4]
;functions.c,229 :: 		if (0x01 == check_digits(length)) {
STR	R2, [SP, #4]
MOV	R0, R2
BL	_check_digits+0
LDR	R2, [SP, #4]
CMP	R0, #1
IT	NE
BNE	L_set_length46
; length end address is: 8 (R2)
;functions.c,230 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_length
;functions.c,231 :: 		}
L_set_length46:
;functions.c,232 :: 		length_number = transform_to_uint(length);
; length start address is: 8 (R2)
MOV	R0, R2
; length end address is: 8 (R2)
BL	_transform_to_uint+0
; length_number start address is: 20 (R5)
UXTH	R5, R0
;functions.c,234 :: 		if (0x01 == check_allowed_lengths(length_number)) {
BL	_check_allowed_lengths+0
CMP	R0, #1
IT	NE
BNE	L_set_length47
; length_number end address is: 20 (R5)
;functions.c,235 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_length
;functions.c,236 :: 		}
L_set_length47:
;functions.c,239 :: 		if ( actual_opertion == 'a' ) {
; length_number start address is: 20 (R5)
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #97
IT	NE
BNE	L_set_length48
;functions.c,240 :: 		if (length_number != 128) {
CMP	R5, #128
IT	EQ
BEQ	L_set_length49
; length_number end address is: 20 (R5)
;functions.c,241 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_length
;functions.c,242 :: 		}
L_set_length49:
;functions.c,243 :: 		change_length ('k', 16);
; length_number start address is: 20 (R5)
MOVS	R1, #16
MOVS	R0, #107
BL	_change_length+0
;functions.c,244 :: 		operation_length_in_bites = length_number;
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
STRH	R5, [R1, #0]
; length_number end address is: 20 (R5)
;functions.c,245 :: 		block_length_in_bytes = 16;
MOVS	R2, #16
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
STRH	R2, [R1, #0]
;functions.c,255 :: 		}
IT	AL
BAL	L_set_length50
L_set_length48:
;functions.c,256 :: 		else if ( actual_opertion == 'r' ) {
; length_number start address is: 20 (R5)
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #114
IT	NE
BNE	L_set_length51
;functions.c,257 :: 		if (actual_implementation == 's') {
MOVW	R1, #lo_addr(_actual_implementation+0)
MOVT	R1, #hi_addr(_actual_implementation+0)
LDRB	R1, [R1, #0]
CMP	R1, #115
IT	NE
BNE	L_set_length52
;functions.c,258 :: 		if (length_number >= 512) {
CMP	R5, #512
IT	CC
BCC	L_set_length53
;functions.c,259 :: 		operation_length_in_bites = length_number;
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
STRH	R5, [R1, #0]
;functions.c,260 :: 		block_length_in_bytes = 64;
MOVS	R2, #64
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
STRH	R2, [R1, #0]
;functions.c,261 :: 		message_parts = length_number >> 9; // (lenght / 512)
LSRS	R2, R5, #9
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
STRH	R2, [R1, #0]
;functions.c,262 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,263 :: 		key_parts = length_number >> 8; // (lenght / 512) * 2
LSRS	R2, R5, #8
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
STRH	R2, [R1, #0]
;functions.c,264 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,265 :: 		change_length ('k', 64);
MOVS	R1, #64
MOVS	R0, #107
BL	_change_length+0
;functions.c,266 :: 		change_length ('p', 64);
MOVS	R1, #64
MOVS	R0, #112
BL	_change_length+0
;functions.c,267 :: 		change_length ('c', 64);
MOVS	R1, #64
MOVS	R0, #99
BL	_change_length+0
;functions.c,268 :: 		} else {
IT	AL
BAL	L_set_length54
L_set_length53:
;functions.c,269 :: 		operation_length_in_bites = length_number;
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
STRH	R5, [R1, #0]
;functions.c,270 :: 		block_length_in_bytes = length_number >> 3;
LSRS	R3, R5, #3
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
STRH	R3, [R1, #0]
;functions.c,271 :: 		message_parts = 1;
MOVS	R2, #1
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
STRH	R2, [R1, #0]
;functions.c,272 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,273 :: 		key_parts = 2; // 1. p | q, 2. e
MOVS	R2, #2
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
STRH	R2, [R1, #0]
;functions.c,274 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,275 :: 		change_length ('k', block_length_in_bytes);
UXTH	R1, R3
MOVS	R0, #107
BL	_change_length+0
;functions.c,276 :: 		change_length ('p', block_length_in_bytes);
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
LDRH	R1, [R1, #0]
MOVS	R0, #112
BL	_change_length+0
;functions.c,277 :: 		change_length ('c', block_length_in_bytes);
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
LDRH	R1, [R1, #0]
MOVS	R0, #99
BL	_change_length+0
;functions.c,279 :: 		}
L_set_length54:
;functions.c,280 :: 		}
L_set_length52:
;functions.c,282 :: 		if (actual_implementation == 'h') {
MOVW	R1, #lo_addr(_actual_implementation+0)
MOVT	R1, #hi_addr(_actual_implementation+0)
LDRB	R1, [R1, #0]
CMP	R1, #104
IT	NE
BNE	L_set_length55
;functions.c,283 :: 		if (length_number < 1024) {
CMP	R5, #1024
IT	CS
BCS	L_set_length56
; length_number end address is: 20 (R5)
;functions.c,284 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_length
;functions.c,285 :: 		}
L_set_length56:
;functions.c,286 :: 		operation_length_in_bites = length_number;
; length_number start address is: 20 (R5)
MOVW	R1, #lo_addr(_operation_length_in_bites+0)
MOVT	R1, #hi_addr(_operation_length_in_bites+0)
STRH	R5, [R1, #0]
;functions.c,287 :: 		block_length_in_bytes = 64;
MOVS	R2, #64
MOVW	R1, #lo_addr(_block_length_in_bytes+0)
MOVT	R1, #hi_addr(_block_length_in_bytes+0)
STRH	R2, [R1, #0]
;functions.c,288 :: 		message_parts = length_number >> 9; // (lenght / 512)
LSRS	R2, R5, #9
MOVW	R1, #lo_addr(_message_parts+0)
MOVT	R1, #hi_addr(_message_parts+0)
STRH	R2, [R1, #0]
;functions.c,289 :: 		message_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_message_parts_cnt+0)
MOVT	R1, #hi_addr(_message_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,290 :: 		key_parts = length_number >> 8; // (lenght / 512) * 2
LSRS	R2, R5, #8
; length_number end address is: 20 (R5)
MOVW	R1, #lo_addr(_key_parts+0)
MOVT	R1, #hi_addr(_key_parts+0)
STRH	R2, [R1, #0]
;functions.c,291 :: 		key_parts_cnt = 0;
MOVS	R2, #0
MOVW	R1, #lo_addr(_key_parts_cnt+0)
MOVT	R1, #hi_addr(_key_parts_cnt+0)
STRH	R2, [R1, #0]
;functions.c,292 :: 		change_length ('k', 64);
MOVS	R1, #64
MOVS	R0, #107
BL	_change_length+0
;functions.c,293 :: 		change_length ('p', 64);
MOVS	R1, #64
MOVS	R0, #112
BL	_change_length+0
;functions.c,294 :: 		change_length ('c', 64);
MOVS	R1, #64
MOVS	R0, #99
BL	_change_length+0
;functions.c,295 :: 		}
L_set_length55:
;functions.c,297 :: 		}
IT	AL
BAL	L_set_length57
L_set_length51:
;functions.c,298 :: 		else if ( actual_opertion == 'b' ) {
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #98
IT	NE
BNE	L_set_length58
;functions.c,300 :: 		}
IT	AL
BAL	L_set_length59
L_set_length58:
;functions.c,301 :: 		else if ( actual_opertion == 'p' ) {
MOVW	R1, #lo_addr(_actual_opertion+0)
MOVT	R1, #hi_addr(_actual_opertion+0)
LDRB	R1, [R1, #0]
CMP	R1, #112
IT	NE
BNE	L_set_length60
;functions.c,303 :: 		}
IT	AL
BAL	L_set_length61
L_set_length60:
;functions.c,305 :: 		return 0x01;
MOVS	R0, #1
IT	AL
BAL	L_end_set_length
;functions.c,306 :: 		}
L_set_length61:
L_set_length59:
L_set_length57:
L_set_length50:
;functions.c,307 :: 		dbg_puts_labeled ("SUCCESS length set");
MOVW	R1, #lo_addr(?lstr12_functions+0)
MOVT	R1, #hi_addr(?lstr12_functions+0)
MOV	R0, R1
BL	_dbg_puts_labeled+0
;functions.c,308 :: 		return 0x00;
MOVS	R0, #0
;functions.c,309 :: 		}
L_end_set_length:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _set_length
