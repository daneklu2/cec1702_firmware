#ifndef FUNCTIONS_H
#define  FUNCTIONS_H

//needed bacause of uintX_t types
#include <stdint.h>
#include <stdlib.h>

uint8_t echo (uint8_t* pt);

uint8_t only_return (uint8_t* pt);

uint8_t reset (uint8_t* params);

// TODO: not implemented right 
uint8_t check_version(uint8_t* v);

uint8_t set_operation (uint8_t* operation);

uint8_t set_implementation (uint8_t* implementation);

uint8_t set_length (uint8_t* length);

#endif // FUNCTIONS_H