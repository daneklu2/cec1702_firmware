#include "functions.h"
#include "../debug.h"
#include "../simpleserial/simpleserial.h"
#include "../device_setup/cec1702_setup.h"
#include "../AES_implementation/aes_sw_implementation.h"
#include "../AES_implementation/aes_hw_accelerated.h"
#include "../RSA_implementation/BIGI/bigi_communication.h"
#include "../RSA_implementation/rsa_sw_implementation.h"
#include "../RSA_implementation/rsa_hw_accelerated.h"

/*global variables for function get_pt*/
#define ALLOWED_LENGHTS_CNT 8
static uint32_t outbuf[4] aligned(16);
static const uint8_t default_key_16B[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
static const uint16_t allowed_lengths[ALLOWED_LENGHTS_CNT] = {32, 128, 192, 256, 512, 1024, 2048, 4096};


extern ss_cmd commands[MAX_SS_CMDS];
extern int num_commands;
#ifdef DBG
uint8_t  actual_opertion = 'r';
uint8_t  actual_implementation = 's';
uint16_t message_parts = 1;
uint16_t message_parts_cnt = 0;
uint16_t key_parts = 2;
uint16_t key_parts_cnt = 0;
uint16_t operation_length_in_bites = 512;
uint16_t block_length_in_bytes = 64;

#else
uint8_t  actual_opertion = 0;
uint8_t  actual_implementation = 0;
uint16_t message_parts = 1;
uint16_t message_parts_cnt = 0;
uint16_t key_parts = 1;
uint16_t key_parts_cnt = 0;
uint16_t operation_length_in_bites = 128; 
uint16_t block_length_in_bytes = 16; 
#endif
uint8_t echo(uint8_t* pt) {
        if (!pt || !outbuf) {
           return 0x01;
        }
        trigger_high();
        memcpy(outbuf, pt, 16);
        Delay_ms(100);
        trigger_low();
        simpleserial_put('r', 16, outbuf);
        return 0x00;
}

uint8_t only_return (uint8_t* pt) {
        return 0x00;
}

uint8_t reset(uint8_t* params) {
        /*
        if (0x00 != sw_aes_key_set(default_key_16B)) {
                return 0x01;
        }
        if (0x00 != hw_aes_key_set(default_key_16B)) {
                return 0x01;
        }
        */
        key_parts_cnt = 0;
        message_parts_cnt = 0;
        return 0x00;
}

// TODO: not implemented right 
uint8_t check_version(uint8_t* v) {
        return 0x00;
}

uint8_t set_operation (uint8_t* operation) {
        dbg_puts_labeled ("In set operation");
        if ( *operation == 'a' ) {
                dbg_puts_labeled ("Operation = a");
                simpleserial_addcmd ('k', 16, hw_aes_key_set);
                simpleserial_addcmd ('K',  0, only_return);
                simpleserial_addcmd ('p', 16, hw_aes_encrypt);
                simpleserial_addcmd ('c', 16, hw_aes_decrypt);
                //set global array lenght
                //set global cnt (and max cnt) of message parts to send
                message_parts = 1;
                message_parts_cnt = 0;
                key_parts = 1;
                key_parts_cnt = 0;
                operation_length_in_bites = 128;
                block_length_in_bytes = 16;
                actual_opertion = 'a';
                actual_implementation = 'h';
        }
        else if ( *operation == 'r' ) {
                dbg_puts_labeled ("Operation = r");
                simpleserial_addcmd ('k', 64, sw_rsa_key_set);
                simpleserial_addcmd ('K',  0, sw_rsa_key_get);
                simpleserial_addcmd ('p', 64, sw_rsa_encrypt);
                simpleserial_addcmd ('c', 64, sw_rsa_decrypt);
                //set global array lenght
                //set global cnt (and max cnt) of message parts to send
                message_parts = 1;
                message_parts_cnt = 0;
                key_parts = 2; // 1. p[256] | q[256], 2. e[512]
                key_parts_cnt = 0;
                operation_length_in_bites = 512;
                block_length_in_bytes = 64;
                actual_opertion = 'r';
                actual_implementation = 's';
        }
        else if ( *operation == 'b' ) {
                //TODO: not necessary now, implement later
        }
        else if ( *operation == 'p' ) {
                //TODO: after Paillier implementation, fill in here..
        }
        else {
                return 0x01;
        }
        dbg_puts_labeled ("SUCCESS operation set");
        return 0x00;

}

void change_function (char cmd, uint8_t (*fp)(uint8_t*)) {
        uint8_t i = 0;
        for (i = 0; i < MAX_SS_CMDS; ++i) {
                dbg_putch_labeled(commands[i].c);
                if (commands[i].c == cmd) {
                        commands[i].c   = cmd;
                        return;
                }
        }
}

uint8_t set_implementation (uint8_t* implementation) {
        uint8_t i = 0;
        dbg_puts_labeled ("In set implementation");
        if ( actual_opertion == 'a' ) {
                if (*implementation == 's') {
                        dbg_puts_labeled ("Implementation = SA");
                        change_function ('k', sw_aes_key_set);
                        change_function ('K', only_return);
                        change_function ('p', sw_aes_encrypt);
                        change_function ('c', echo);
                }
                else if (*implementation == 'h') {
                        dbg_puts_labeled ("Implementation = HA");
                        change_function ('k', hw_aes_key_set);
                        change_function ('K', only_return);
                        change_function ('p', hw_aes_encrypt);
                        change_function ('c', hw_aes_decrypt);
                }
                else {
                        return 0x01;
                }
        }
        else if ( actual_opertion == 'r' ) {
                if (*implementation == 's') {
                        dbg_puts_labeled ("Implementation = SR");
                        change_function ('k', sw_rsa_key_set);
                        change_function ('K', sw_rsa_key_get);
                        change_function ('p', sw_rsa_encrypt);
                        change_function ('c', sw_rsa_decrypt);
                }
                else if (*implementation == 'h') {
                        dbg_puts_labeled ("Implementation = HR");
                        if (operation_length_in_bites < 1024) {
                                return 0x01;
                        }
                        /*change_function ('k', hw_rsa_key_set);
                        change_function ('K', hw_rsa_key_get);
                        change_function ('p', hw_rsa_encrypt);
                        change_function ('c', hw_rsa_decrypt);      */
                }
                else {
                        return 0x01;
                }
        }
        else if ( actual_opertion == 'b' ) {
                //TODO: not necessary now, implement later
        }
        else if ( actual_opertion == 'p' ) {
                //TODO: after Paillier implementation, fill in here..
        }
        else {
                return 0x01;
        }

        actual_implementation = *implementation;
        dbg_puts_labeled ("SUCCESS implementation set");
        return 0x00;
}

uint8_t check_digits (uint8_t * length) {
        uint8_t i = 0;
        for (i; i < 4; ++i) {
                if (length[i] > '9' || length[i] < '0') {
                        return 0x01;
                }
        }
        return 0x00;
}


uint8_t check_allowed_lengths (uint16_t number) {
        uint8_t i = 0;
        for (i; i < ALLOWED_LENGHTS_CNT; ++i) {
                if (allowed_lengths[i] == number) {
                        return 0x00;
                }
        }
        return 0x01;
}

uint16_t transform_to_uint (uint8_t * length) {
        uint16_t sum = 0;
        sum += (length[0] - '0') * 1000;
        sum += (length[1] - '0') *  100;
        sum += (length[2] - '0') *   10;
        sum += (length[3] - '0');

        return sum;
}

void change_length (char cmd, unsigned int length) {
        uint8_t i = 0;
        while (commands[i].c != cmd) {
                ++i;
        }
        commands[i].len = length;
}

uint8_t set_length (uint8_t* length) {
        uint16_t length_number = 0;
        uint8_t i = 0;
        dbg_puts_labeled ("In set length");
        //check and transform number
        if (0x01 == check_digits(length)) {
                return 0x01;
        }
        length_number = transform_to_uint(length);

        if (0x01 == check_allowed_lengths(length_number)) {
                return 0x01;
        }


        if ( actual_opertion == 'a' ) {
                if (length_number != 128) {
                        return 0x01;
                }
                change_length ('k', 16);
                operation_length_in_bites = length_number;
                block_length_in_bytes = 16;
                /*
                TODO: add implemetation of AES 192, 256...
                if (length_number > 256 || length_number < 128) {
                        return 0x01;
                }
                change_length ('k', length_number >> 3);
                operation_length_in_bites = length_number;
                */

        }
        else if ( actual_opertion == 'r' ) {
                if (actual_implementation == 's') {
                        if (length_number >= 512) {
                                operation_length_in_bites = length_number;
                                block_length_in_bytes = 64;
                                message_parts = length_number >> 9; // (lenght / 512)
                                message_parts_cnt = 0;
                                key_parts = length_number >> 8; // (lenght / 512) * 2 
                                key_parts_cnt = 0;
                                change_length ('k', 64);
                                change_length ('p', 64);
                                change_length ('c', 64);
                        } else {
                                operation_length_in_bites = length_number;
                                block_length_in_bytes = length_number >> 3;
                                message_parts = 1;
                                message_parts_cnt = 0;
                                key_parts = 2; // 1. p | q, 2. e 
                                key_parts_cnt = 0;
                                change_length ('k', block_length_in_bytes);
                                change_length ('p', block_length_in_bytes);
                                change_length ('c', block_length_in_bytes);

                        }
                }

                if (actual_implementation == 'h') {
                        if (length_number < 1024) {
                                return 0x01;
                        }
                        operation_length_in_bites = length_number;
                        block_length_in_bytes = 64;
                        message_parts = length_number >> 9; // (lenght / 512)
                        message_parts_cnt = 0;
                        key_parts = length_number >> 8; // (lenght / 512) * 2 
                        key_parts_cnt = 0;
                        change_length ('k', 64);
                        change_length ('p', 64);
                        change_length ('c', 64);
                }

        }
        else if ( actual_opertion == 'b' ) {
                //TODO: not necessary now, implement later
        }
        else if ( actual_opertion == 'p' ) {
                //TODO: after Paillier implementation, fill in here..
        }
        else {
                return 0x01;
        }
        dbg_puts_labeled ("SUCCESS length set");
        return 0x00;
}