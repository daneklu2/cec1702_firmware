_hex_decode:
;simpleserial.c,32 :: 		int hex_decode(int len, char* ascii_buf, uint8_t* data_buf) {
; data_buf start address is: 8 (R2)
; ascii_buf start address is: 4 (R1)
; len start address is: 0 (R0)
SUB	SP, SP, #4
; data_buf end address is: 8 (R2)
; ascii_buf end address is: 4 (R1)
; len end address is: 0 (R0)
; len start address is: 0 (R0)
; ascii_buf start address is: 4 (R1)
; data_buf start address is: 8 (R2)
;simpleserial.c,33 :: 		int i = 0;
;simpleserial.c,34 :: 		for(i = 0; i < len; i++)
; i start address is: 24 (R6)
MOVS	R6, #0
SXTH	R6, R6
; len end address is: 0 (R0)
; data_buf end address is: 8 (R2)
; i end address is: 24 (R6)
STR	R2, [SP, #0]
SXTH	R2, R0
LDR	R0, [SP, #0]
L_hex_decode0:
; i start address is: 24 (R6)
; data_buf start address is: 0 (R0)
; data_buf start address is: 0 (R0)
; data_buf end address is: 0 (R0)
; ascii_buf start address is: 4 (R1)
; ascii_buf end address is: 4 (R1)
; len start address is: 8 (R2)
CMP	R6, R2
IT	GE
BGE	L_hex_decode1
; data_buf end address is: 0 (R0)
; ascii_buf end address is: 4 (R1)
;simpleserial.c,36 :: 		char n_hi = ascii_buf[2*i];
; ascii_buf start address is: 4 (R1)
; data_buf start address is: 0 (R0)
LSLS	R4, R6, #1
SXTH	R4, R4
ADDS	R3, R1, R4
LDRB	R3, [R3, #0]
; n_hi start address is: 28 (R7)
UXTB	R7, R3
;simpleserial.c,37 :: 		char n_lo = ascii_buf[2*i+1];
ADDS	R3, R4, #1
SXTH	R3, R3
ADDS	R3, R1, R3
LDRB	R3, [R3, #0]
; n_lo start address is: 20 (R5)
UXTB	R5, R3
;simpleserial.c,39 :: 		if(n_lo >= '0' && n_lo <= '9')
CMP	R3, #48
IT	CC
BCC	L__hex_decode71
CMP	R5, #57
IT	HI
BHI	L__hex_decode70
L__hex_decode69:
;simpleserial.c,40 :: 		data_buf[i] = n_lo - '0';
ADDS	R4, R0, R6
SUBW	R3, R5, #48
; n_lo end address is: 20 (R5)
STRB	R3, [R4, #0]
IT	AL
BAL	L_hex_decode6
;simpleserial.c,39 :: 		if(n_lo >= '0' && n_lo <= '9')
L__hex_decode71:
; n_lo start address is: 20 (R5)
L__hex_decode70:
;simpleserial.c,41 :: 		else if(n_lo >= 'A' && n_lo <= 'F')
CMP	R5, #65
IT	CC
BCC	L__hex_decode73
CMP	R5, #70
IT	HI
BHI	L__hex_decode72
L__hex_decode68:
;simpleserial.c,42 :: 		data_buf[i] = n_lo - 'A' + 10;
ADDS	R4, R0, R6
SUBW	R3, R5, #65
SXTH	R3, R3
; n_lo end address is: 20 (R5)
ADDS	R3, #10
STRB	R3, [R4, #0]
IT	AL
BAL	L_hex_decode10
;simpleserial.c,41 :: 		else if(n_lo >= 'A' && n_lo <= 'F')
L__hex_decode73:
; n_lo start address is: 20 (R5)
L__hex_decode72:
;simpleserial.c,43 :: 		else if(n_lo >= 'a' && n_lo <= 'f')
CMP	R5, #97
IT	CC
BCC	L__hex_decode75
CMP	R5, #102
IT	HI
BHI	L__hex_decode74
L__hex_decode67:
;simpleserial.c,44 :: 		data_buf[i] = n_lo - 'a' + 10;
ADDS	R4, R0, R6
SUBW	R3, R5, #97
SXTH	R3, R3
; n_lo end address is: 20 (R5)
ADDS	R3, #10
STRB	R3, [R4, #0]
IT	AL
BAL	L_hex_decode14
; data_buf end address is: 0 (R0)
; ascii_buf end address is: 4 (R1)
; len end address is: 8 (R2)
; n_hi end address is: 28 (R7)
; i end address is: 24 (R6)
;simpleserial.c,43 :: 		else if(n_lo >= 'a' && n_lo <= 'f')
L__hex_decode75:
L__hex_decode74:
;simpleserial.c,46 :: 		return 1;
MOVS	R0, #1
SXTH	R0, R0
IT	AL
BAL	L_end_hex_decode
L_hex_decode14:
; i start address is: 24 (R6)
; n_hi start address is: 28 (R7)
; len start address is: 8 (R2)
; ascii_buf start address is: 4 (R1)
; data_buf start address is: 0 (R0)
L_hex_decode10:
L_hex_decode6:
;simpleserial.c,48 :: 		if(n_hi >= '0' && n_hi <= '9')
CMP	R7, #48
IT	CC
BCC	L__hex_decode77
CMP	R7, #57
IT	HI
BHI	L__hex_decode76
L__hex_decode66:
;simpleserial.c,49 :: 		data_buf[i] |= (n_hi - '0') << 4;
ADDS	R5, R0, R6
SUBW	R3, R7, #48
SXTH	R3, R3
; n_hi end address is: 28 (R7)
LSLS	R4, R3, #4
SXTH	R4, R4
LDRB	R3, [R5, #0]
ORRS	R3, R4
STRB	R3, [R5, #0]
IT	AL
BAL	L_hex_decode18
;simpleserial.c,48 :: 		if(n_hi >= '0' && n_hi <= '9')
L__hex_decode77:
; n_hi start address is: 28 (R7)
L__hex_decode76:
;simpleserial.c,50 :: 		else if(n_hi >= 'A' && n_hi <= 'F')
CMP	R7, #65
IT	CC
BCC	L__hex_decode79
CMP	R7, #70
IT	HI
BHI	L__hex_decode78
L__hex_decode65:
;simpleserial.c,51 :: 		data_buf[i] |= (n_hi - 'A' + 10) << 4;
ADDS	R5, R0, R6
SUBW	R3, R7, #65
SXTH	R3, R3
; n_hi end address is: 28 (R7)
ADDS	R3, #10
SXTH	R3, R3
LSLS	R4, R3, #4
SXTH	R4, R4
LDRB	R3, [R5, #0]
ORRS	R3, R4
STRB	R3, [R5, #0]
IT	AL
BAL	L_hex_decode22
;simpleserial.c,50 :: 		else if(n_hi >= 'A' && n_hi <= 'F')
L__hex_decode79:
; n_hi start address is: 28 (R7)
L__hex_decode78:
;simpleserial.c,52 :: 		else if(n_hi >= 'a' && n_hi <= 'f')
CMP	R7, #97
IT	CC
BCC	L__hex_decode81
CMP	R7, #102
IT	HI
BHI	L__hex_decode80
L__hex_decode64:
;simpleserial.c,53 :: 		data_buf[i] |= (n_hi - 'a' + 10) << 4;
ADDS	R5, R0, R6
SUBW	R3, R7, #97
SXTH	R3, R3
; n_hi end address is: 28 (R7)
ADDS	R3, #10
SXTH	R3, R3
LSLS	R4, R3, #4
SXTH	R4, R4
LDRB	R3, [R5, #0]
ORRS	R3, R4
STRB	R3, [R5, #0]
IT	AL
BAL	L_hex_decode26
; data_buf end address is: 0 (R0)
; ascii_buf end address is: 4 (R1)
; len end address is: 8 (R2)
; i end address is: 24 (R6)
;simpleserial.c,52 :: 		else if(n_hi >= 'a' && n_hi <= 'f')
L__hex_decode81:
L__hex_decode80:
;simpleserial.c,55 :: 		return 1;
MOVS	R0, #1
SXTH	R0, R0
IT	AL
BAL	L_end_hex_decode
L_hex_decode26:
; i start address is: 24 (R6)
; len start address is: 8 (R2)
; ascii_buf start address is: 4 (R1)
; data_buf start address is: 0 (R0)
L_hex_decode22:
L_hex_decode18:
;simpleserial.c,34 :: 		for(i = 0; i < len; i++)
ADDS	R6, R6, #1
SXTH	R6, R6
;simpleserial.c,56 :: 		}
; data_buf end address is: 0 (R0)
; ascii_buf end address is: 4 (R1)
; len end address is: 8 (R2)
; i end address is: 24 (R6)
IT	AL
BAL	L_hex_decode0
L_hex_decode1:
;simpleserial.c,58 :: 		return 0;
MOVS	R0, #0
SXTH	R0, R0
;simpleserial.c,59 :: 		}
L_end_hex_decode:
ADD	SP, SP, #4
BX	LR
; end of _hex_decode
_simpleserial_init:
;simpleserial.c,61 :: 		void simpleserial_init() {
SUB	SP, SP, #4
STR	LR, [SP, #0]
;simpleserial.c,63 :: 		simpleserial_addcmd('v',  0, check_version);
MOVW	R0, #lo_addr(_check_version+0)
MOVT	R0, #hi_addr(_check_version+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #118
BL	_simpleserial_addcmd+0
;simpleserial.c,64 :: 		simpleserial_addcmd('x',  0, reset);
MOVW	R0, #lo_addr(_reset+0)
MOVT	R0, #hi_addr(_reset+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #120
BL	_simpleserial_addcmd+0
;simpleserial.c,65 :: 		simpleserial_addcmd('k', 16, only_return);
MOVW	R0, #lo_addr(_only_return+0)
MOVT	R0, #hi_addr(_only_return+0)
MOV	R2, R0
MOVS	R1, #16
MOVS	R0, #107
BL	_simpleserial_addcmd+0
;simpleserial.c,66 :: 		simpleserial_addcmd('K',  0, only_return);
MOVW	R0, #lo_addr(_only_return+0)
MOVT	R0, #hi_addr(_only_return+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #75
BL	_simpleserial_addcmd+0
;simpleserial.c,67 :: 		simpleserial_addcmd('p', 16, echo);
MOVW	R0, #lo_addr(_echo+0)
MOVT	R0, #hi_addr(_echo+0)
MOV	R2, R0
MOVS	R1, #16
MOVS	R0, #112
BL	_simpleserial_addcmd+0
;simpleserial.c,68 :: 		simpleserial_addcmd('c', 16, echo);
MOVW	R0, #lo_addr(_echo+0)
MOVT	R0, #hi_addr(_echo+0)
MOV	R2, R0
MOVS	R1, #16
MOVS	R0, #99
BL	_simpleserial_addcmd+0
;simpleserial.c,71 :: 		simpleserial_addcmd('o', 1, set_operation);
MOVW	R0, #lo_addr(_set_operation+0)
MOVT	R0, #hi_addr(_set_operation+0)
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #111
BL	_simpleserial_addcmd+0
;simpleserial.c,72 :: 		simpleserial_addcmd('i', 1, set_implementation);
MOVW	R0, #lo_addr(_set_implementation+0)
MOVT	R0, #hi_addr(_set_implementation+0)
MOV	R2, R0
MOVS	R1, #1
MOVS	R0, #105
BL	_simpleserial_addcmd+0
;simpleserial.c,73 :: 		simpleserial_addcmd('l', 4, set_length);
MOVW	R0, #lo_addr(_set_length+0)
MOVT	R0, #hi_addr(_set_length+0)
MOV	R2, R0
MOVS	R1, #4
MOVS	R0, #108
BL	_simpleserial_addcmd+0
;simpleserial.c,75 :: 		simpleserial_addcmd('a', 0, demo128_bigi_add);
MOVW	R0, #lo_addr(_demo128_bigi_add+0)
MOVT	R0, #hi_addr(_demo128_bigi_add+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #97
BL	_simpleserial_addcmd+0
;simpleserial.c,76 :: 		simpleserial_addcmd('g', 0, demo128_bigi_gcd);
MOVW	R0, #lo_addr(_demo128_bigi_gcd+0)
MOVT	R0, #hi_addr(_demo128_bigi_gcd+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #103
BL	_simpleserial_addcmd+0
;simpleserial.c,77 :: 		simpleserial_addcmd('e', 0, demo128_bigi_mod_exp);
MOVW	R0, #lo_addr(_demo128_bigi_mod_exp+0)
MOVT	R0, #hi_addr(_demo128_bigi_mod_exp+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #101
BL	_simpleserial_addcmd+0
;simpleserial.c,78 :: 		simpleserial_addcmd('m', 0, demo128_bigi_mod_exp_mont);
MOVW	R0, #lo_addr(_demo128_bigi_mod_exp_mont+0)
MOVT	R0, #hi_addr(_demo128_bigi_mod_exp_mont+0)
MOV	R2, R0
MOVS	R1, #0
MOVS	R0, #109
BL	_simpleserial_addcmd+0
;simpleserial.c,80 :: 		}
L_end_simpleserial_init:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _simpleserial_init
_simpleserial_addcmd:
;simpleserial.c,82 :: 		int simpleserial_addcmd(char c, unsigned int len, uint8_t (*fp)(uint8_t*))
; fp start address is: 8 (R2)
; len start address is: 4 (R1)
; c start address is: 0 (R0)
SUB	SP, SP, #4
; fp end address is: 8 (R2)
; len end address is: 4 (R1)
; c end address is: 0 (R0)
; c start address is: 0 (R0)
; len start address is: 4 (R1)
; fp start address is: 8 (R2)
;simpleserial.c,85 :: 		for (i = 0; i < MAX_SS_CMDS; ++i) {
; i start address is: 20 (R5)
MOVS	R5, #0
; c end address is: 0 (R0)
; fp end address is: 8 (R2)
; len end address is: 4 (R1)
; i end address is: 20 (R5)
STR	R2, [SP, #0]
UXTB	R2, R0
LDR	R0, [SP, #0]
L_simpleserial_addcmd27:
; i start address is: 20 (R5)
; fp start address is: 0 (R0)
; len start address is: 4 (R1)
; c start address is: 8 (R2)
CMP	R5, #20
IT	CS
BCS	L_simpleserial_addcmd28
;simpleserial.c,87 :: 		if (commands[i].c == c) {
LSLS	R4, R5, #3
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
LDRB	R3, [R3, #0]
CMP	R3, R2
IT	NE
BNE	L_simpleserial_addcmd30
;simpleserial.c,88 :: 		commands[i].c   = c;
LSLS	R4, R5, #3
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
STRB	R2, [R3, #0]
; c end address is: 8 (R2)
;simpleserial.c,89 :: 		commands[i].len = len;
LSLS	R4, R5, #3
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
ADDS	R3, R3, #2
STRH	R1, [R3, #0]
; len end address is: 4 (R1)
;simpleserial.c,90 :: 		commands[i].fp  = fp;
LSLS	R4, R5, #3
; i end address is: 20 (R5)
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
ADDS	R3, R3, #4
STR	R0, [R3, #0]
; fp end address is: 0 (R0)
;simpleserial.c,91 :: 		return 0;
MOVS	R0, #0
SXTH	R0, R0
IT	AL
BAL	L_end_simpleserial_addcmd
;simpleserial.c,92 :: 		}
L_simpleserial_addcmd30:
;simpleserial.c,85 :: 		for (i = 0; i < MAX_SS_CMDS; ++i) {
; i start address is: 20 (R5)
; c start address is: 8 (R2)
; len start address is: 4 (R1)
; fp start address is: 0 (R0)
ADDS	R5, R5, #1
UXTB	R5, R5
;simpleserial.c,93 :: 		}
; i end address is: 20 (R5)
IT	AL
BAL	L_simpleserial_addcmd27
L_simpleserial_addcmd28:
;simpleserial.c,95 :: 		if(num_commands >= MAX_SS_CMDS)
MOVW	R3, #lo_addr(_num_commands+0)
MOVT	R3, #hi_addr(_num_commands+0)
LDRSH	R3, [R3, #0]
CMP	R3, #20
IT	LT
BLT	L_simpleserial_addcmd31
; fp end address is: 0 (R0)
; len end address is: 4 (R1)
; c end address is: 8 (R2)
;simpleserial.c,96 :: 		return 1;
MOVS	R0, #1
SXTH	R0, R0
IT	AL
BAL	L_end_simpleserial_addcmd
L_simpleserial_addcmd31:
;simpleserial.c,98 :: 		if(len >= MAX_SS_LEN)
; c start address is: 8 (R2)
; len start address is: 4 (R1)
; fp start address is: 0 (R0)
CMP	R1, #64
IT	CC
BCC	L_simpleserial_addcmd32
; fp end address is: 0 (R0)
; len end address is: 4 (R1)
; c end address is: 8 (R2)
;simpleserial.c,99 :: 		return 1;
MOVS	R0, #1
SXTH	R0, R0
IT	AL
BAL	L_end_simpleserial_addcmd
L_simpleserial_addcmd32:
;simpleserial.c,101 :: 		commands[num_commands].c   = c;
; c start address is: 8 (R2)
; len start address is: 4 (R1)
; fp start address is: 0 (R0)
MOVW	R5, #lo_addr(_num_commands+0)
MOVT	R5, #hi_addr(_num_commands+0)
LDRSH	R3, [R5, #0]
LSLS	R4, R3, #3
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
STRB	R2, [R3, #0]
; c end address is: 8 (R2)
;simpleserial.c,102 :: 		commands[num_commands].len = len;
MOV	R3, R5
LDRSH	R3, [R3, #0]
LSLS	R4, R3, #3
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
ADDS	R3, R3, #2
STRH	R1, [R3, #0]
; len end address is: 4 (R1)
;simpleserial.c,103 :: 		commands[num_commands].fp  = fp;
MOV	R3, R5
LDRSH	R3, [R3, #0]
LSLS	R4, R3, #3
MOVW	R3, #lo_addr(_commands+0)
MOVT	R3, #hi_addr(_commands+0)
ADDS	R3, R3, R4
ADDS	R3, R3, #4
STR	R0, [R3, #0]
; fp end address is: 0 (R0)
;simpleserial.c,104 :: 		num_commands++;
MOV	R3, R5
LDRSH	R3, [R3, #0]
ADDS	R3, R3, #1
STRH	R3, [R5, #0]
;simpleserial.c,106 :: 		return 0;
MOVS	R0, #0
SXTH	R0, R0
;simpleserial.c,107 :: 		}
L_end_simpleserial_addcmd:
ADD	SP, SP, #4
BX	LR
; end of _simpleserial_addcmd
_simpleserial_get:
;simpleserial.c,109 :: 		void simpleserial_get(void)
SUB	SP, SP, #8
STR	LR, [SP, #0]
;simpleserial.c,111 :: 		dbg_puts_labeled("IN GET");
MOVW	R0, #lo_addr(?lstr1_simpleserial+0)
MOVT	R0, #hi_addr(?lstr1_simpleserial+0)
BL	_dbg_puts_labeled+0
;simpleserial.c,112 :: 		received_char = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(simpleserial_received_char+0)
MOVT	R0, #hi_addr(simpleserial_received_char+0)
STRB	R1, [R0, #0]
;simpleserial.c,113 :: 		received_command = -1;
MOVW	R1, #65535
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
STRH	R1, [R0, #0]
;simpleserial.c,114 :: 		index = 0;
MOVS	R0, #0
SXTH	R0, R0
MOVW	R2, #lo_addr(simpleserial_index+0)
MOVT	R2, #hi_addr(simpleserial_index+0)
STRH	R0, [R2, #0]
;simpleserial.c,115 :: 		ack_value[0] = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(simpleserial_ack_value+0)
MOVT	R0, #hi_addr(simpleserial_ack_value+0)
STRB	R1, [R0, #0]
;simpleserial.c,116 :: 		full_buffer_flag = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(simpleserial_full_buffer_flag+0)
MOVT	R0, #hi_addr(simpleserial_full_buffer_flag+0)
STRB	R1, [R0, #0]
;simpleserial.c,118 :: 		for (index = 0; index < 2*MAX_SS_LEN + 1; ++index) {
MOVS	R0, #0
SXTH	R0, R0
STRH	R0, [R2, #0]
L_simpleserial_get33:
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R0, #0]
CMP	R0, #129
IT	GE
BGE	L_simpleserial_get34
;simpleserial.c,119 :: 		receive_buffer[index] = getch();
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
STR	R0, [SP, #4]
BL	_getch+0
LDR	R1, [SP, #4]
STRB	R0, [R1, #0]
;simpleserial.c,120 :: 		putch(receive_buffer[index]);
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
BL	_putch+0
;simpleserial.c,121 :: 		if (receive_buffer[index] == '\n' || receive_buffer[index] == '\r') {
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
CMP	R0, #10
IT	EQ
BEQ	L__simpleserial_get86
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
CMP	R0, #13
IT	EQ
BEQ	L__simpleserial_get85
IT	AL
BAL	L_simpleserial_get38
L__simpleserial_get86:
L__simpleserial_get85:
;simpleserial.c,122 :: 		full_buffer_flag = 0;
MOVS	R1, #0
MOVW	R0, #lo_addr(simpleserial_full_buffer_flag+0)
MOVT	R0, #hi_addr(simpleserial_full_buffer_flag+0)
STRB	R1, [R0, #0]
;simpleserial.c,123 :: 		break;
IT	AL
BAL	L_simpleserial_get34
;simpleserial.c,124 :: 		}
L_simpleserial_get38:
;simpleserial.c,118 :: 		for (index = 0; index < 2*MAX_SS_LEN + 1; ++index) {
MOVW	R1, #lo_addr(simpleserial_index+0)
MOVT	R1, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;simpleserial.c,125 :: 		}
IT	AL
BAL	L_simpleserial_get33
L_simpleserial_get34:
;simpleserial.c,127 :: 		if (full_buffer_flag) {
MOVW	R0, #lo_addr(simpleserial_full_buffer_flag+0)
MOVT	R0, #hi_addr(simpleserial_full_buffer_flag+0)
LDRB	R0, [R0, #0]
CMP	R0, #0
IT	EQ
BEQ	L_simpleserial_get39
;simpleserial.c,128 :: 		received_char = getch();
BL	_getch+0
MOVW	R1, #lo_addr(simpleserial_received_char+0)
MOVT	R1, #hi_addr(simpleserial_received_char+0)
STRB	R0, [R1, #0]
;simpleserial.c,129 :: 		if (received_char != '\n' && received_char != '\r' ) {
CMP	R0, #10
IT	EQ
BEQ	L__simpleserial_get88
MOVW	R0, #lo_addr(simpleserial_received_char+0)
MOVT	R0, #hi_addr(simpleserial_received_char+0)
LDRB	R0, [R0, #0]
CMP	R0, #13
IT	EQ
BEQ	L__simpleserial_get87
L__simpleserial_get83:
;simpleserial.c,130 :: 		dbg_puts("No ENDL");
MOVW	R0, #lo_addr(?lstr2_simpleserial+0)
MOVT	R0, #hi_addr(?lstr2_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,131 :: 		dbg_putch('|');
MOVS	R0, #124
BL	_dbg_putch+0
;simpleserial.c,132 :: 		dbg_putch(received_char);
MOVW	R0, #lo_addr(simpleserial_received_char+0)
MOVT	R0, #hi_addr(simpleserial_received_char+0)
LDRB	R0, [R0, #0]
BL	_dbg_putch+0
;simpleserial.c,133 :: 		dbg_putch('|');
MOVS	R0, #124
BL	_dbg_putch+0
;simpleserial.c,134 :: 		return;
IT	AL
BAL	L_end_simpleserial_get
;simpleserial.c,129 :: 		if (received_char != '\n' && received_char != '\r' ) {
L__simpleserial_get88:
L__simpleserial_get87:
;simpleserial.c,136 :: 		}
L_simpleserial_get39:
;simpleserial.c,139 :: 		for(received_command = 0; received_command < num_commands; received_command++) {
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
STRH	R1, [R0, #0]
L_simpleserial_get43:
MOVW	R0, #lo_addr(_num_commands+0)
MOVT	R0, #hi_addr(_num_commands+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
CMP	R0, R1
IT	GE
BGE	L_simpleserial_get44
;simpleserial.c,141 :: 		if(commands[received_command].c == receive_buffer[0])
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
LDRB	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
LDRB	R0, [R0, #0]
CMP	R1, R0
IT	NE
BNE	L_simpleserial_get46
;simpleserial.c,142 :: 		break;
IT	AL
BAL	L_simpleserial_get44
L_simpleserial_get46:
;simpleserial.c,139 :: 		for(received_command = 0; received_command < num_commands; received_command++) {
MOVW	R1, #lo_addr(simpleserial_received_command+0)
MOVT	R1, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;simpleserial.c,143 :: 		}
IT	AL
BAL	L_simpleserial_get43
L_simpleserial_get44:
;simpleserial.c,144 :: 		dbg_puts("Cmd: ");
MOVW	R0, #lo_addr(?lstr3_simpleserial+0)
MOVT	R0, #hi_addr(?lstr3_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,145 :: 		dbg_putch(receive_buffer[0]);
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
LDRB	R0, [R0, #0]
BL	_dbg_putch+0
;simpleserial.c,148 :: 		if(received_command == num_commands){
MOVW	R0, #lo_addr(_num_commands+0)
MOVT	R0, #hi_addr(_num_commands+0)
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
CMP	R0, R1
IT	NE
BNE	L_simpleserial_get47
;simpleserial.c,149 :: 		dbg_puts("Not a match.");
MOVW	R0, #lo_addr(?lstr4_simpleserial+0)
MOVT	R0, #hi_addr(?lstr4_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,150 :: 		return;
IT	AL
BAL	L_end_simpleserial_get
;simpleserial.c,151 :: 		}
L_simpleserial_get47:
;simpleserial.c,153 :: 		if (receive_buffer[0] == 'o' || receive_buffer[0] == 'i' || receive_buffer[0] == 'l') {
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
LDRB	R0, [R0, #0]
CMP	R0, #111
IT	EQ
BEQ	L__simpleserial_get91
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
LDRB	R0, [R0, #0]
CMP	R0, #105
IT	EQ
BEQ	L__simpleserial_get90
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
LDRB	R0, [R0, #0]
CMP	R0, #108
IT	EQ
BEQ	L__simpleserial_get89
IT	AL
BAL	L_simpleserial_get50
L__simpleserial_get91:
L__simpleserial_get90:
L__simpleserial_get89:
;simpleserial.c,155 :: 		if ((index - 1) != commands[received_command].len) {
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R0, #0]
SUBS	R2, R0, #1
SXTH	R2, R2
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #2
LDRH	R0, [R0, #0]
CMP	R2, R0
IT	EQ
BEQ	L_simpleserial_get51
;simpleserial.c,156 :: 		dbg_puts("GET: SHORT MSG");
MOVW	R0, #lo_addr(?lstr5_simpleserial+0)
MOVT	R0, #hi_addr(?lstr5_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,157 :: 		return;
IT	AL
BAL	L_end_simpleserial_get
;simpleserial.c,158 :: 		}
L_simpleserial_get51:
;simpleserial.c,160 :: 		dbg_puts("BUF: ");
MOVW	R0, #lo_addr(?lstr6_simpleserial+0)
MOVT	R0, #hi_addr(?lstr6_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,161 :: 		for (index = 0; index < commands[received_command].len; ++index) {
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
STRH	R1, [R0, #0]
L_simpleserial_get52:
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #2
LDRH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R0, #0]
CMP	R0, R1
IT	CS
BCS	L_simpleserial_get53
;simpleserial.c,162 :: 		received_ascii_buffer[index] = receive_buffer[index + 1];
MOVW	R3, #lo_addr(simpleserial_index+0)
MOVT	R3, #hi_addr(simpleserial_index+0)
LDRSH	R1, [R3, #0]
MOVW	R0, #lo_addr(simpleserial_received_ascii_buffer+0)
MOVT	R0, #hi_addr(simpleserial_received_ascii_buffer+0)
ADDS	R2, R0, R1
MOV	R0, R3
LDRSH	R0, [R0, #0]
ADDS	R1, R0, #1
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
STRB	R0, [R2, #0]
;simpleserial.c,163 :: 		dbg_putch(receive_buffer[index + 1]);
MOV	R0, R3
LDRSH	R0, [R0, #0]
ADDS	R1, R0, #1
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
BL	_dbg_putch+0
;simpleserial.c,161 :: 		for (index = 0; index < commands[received_command].len; ++index) {
MOVW	R1, #lo_addr(simpleserial_index+0)
MOVT	R1, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;simpleserial.c,164 :: 		}
IT	AL
BAL	L_simpleserial_get52
L_simpleserial_get53:
;simpleserial.c,167 :: 		dbg_puts("CALLBACK");
MOVW	R0, #lo_addr(?lstr7_simpleserial+0)
MOVT	R0, #hi_addr(?lstr7_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,169 :: 		ack_value[0] = commands[received_command].fp(received_ascii_buffer);
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #4
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_received_ascii_buffer+0)
MOVT	R0, #hi_addr(simpleserial_received_ascii_buffer+0)
BLX	R1
MOVW	R1, #lo_addr(simpleserial_ack_value+0)
MOVT	R1, #hi_addr(simpleserial_ack_value+0)
STRB	R0, [R1, #0]
;simpleserial.c,170 :: 		}
IT	AL
BAL	L_simpleserial_get55
L_simpleserial_get50:
;simpleserial.c,173 :: 		if ((index - 1) != 2*commands[received_command].len) {
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R0, #0]
SUBS	R2, R0, #1
SXTH	R2, R2
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #2
LDRH	R0, [R0, #0]
LSLS	R0, R0, #1
UXTH	R0, R0
CMP	R2, R0
IT	EQ
BEQ	L_simpleserial_get56
;simpleserial.c,174 :: 		dbg_puts("Msg too short.");
MOVW	R0, #lo_addr(?lstr8_simpleserial+0)
MOVT	R0, #hi_addr(?lstr8_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,175 :: 		return;
IT	AL
BAL	L_end_simpleserial_get
;simpleserial.c,176 :: 		}
L_simpleserial_get56:
;simpleserial.c,178 :: 		dbg_puts("BUF: ");
MOVW	R0, #lo_addr(?lstr9_simpleserial+0)
MOVT	R0, #hi_addr(?lstr9_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,179 :: 		for (index = 0; index < 2*commands[received_command].len; ++index) {
MOVS	R1, #0
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
STRH	R1, [R0, #0]
L_simpleserial_get57:
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #2
LDRH	R0, [R0, #0]
LSLS	R1, R0, #1
UXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_index+0)
MOVT	R0, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R0, #0]
CMP	R0, R1
IT	CS
BCS	L_simpleserial_get58
;simpleserial.c,180 :: 		received_ascii_buffer[index] = receive_buffer[index + 1];
MOVW	R3, #lo_addr(simpleserial_index+0)
MOVT	R3, #hi_addr(simpleserial_index+0)
LDRSH	R1, [R3, #0]
MOVW	R0, #lo_addr(simpleserial_received_ascii_buffer+0)
MOVT	R0, #hi_addr(simpleserial_received_ascii_buffer+0)
ADDS	R2, R0, R1
MOV	R0, R3
LDRSH	R0, [R0, #0]
ADDS	R1, R0, #1
SXTH	R1, R1
MOVW	R0, #lo_addr(simpleserial_receive_buffer+0)
MOVT	R0, #hi_addr(simpleserial_receive_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
STRB	R0, [R2, #0]
;simpleserial.c,181 :: 		dbg_putch(received_ascii_buffer[index]);
MOV	R0, R3
LDRSH	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_received_ascii_buffer+0)
MOVT	R0, #hi_addr(simpleserial_received_ascii_buffer+0)
ADDS	R0, R0, R1
LDRB	R0, [R0, #0]
BL	_dbg_putch+0
;simpleserial.c,179 :: 		for (index = 0; index < 2*commands[received_command].len; ++index) {
MOVW	R1, #lo_addr(simpleserial_index+0)
MOVT	R1, #hi_addr(simpleserial_index+0)
LDRSH	R0, [R1, #0]
ADDS	R0, R0, #1
STRH	R0, [R1, #0]
;simpleserial.c,182 :: 		}
IT	AL
BAL	L_simpleserial_get57
L_simpleserial_get58:
;simpleserial.c,186 :: 		if(hex_decode(commands[received_command].len, received_ascii_buffer, converted_byte_buffer)) {
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #2
LDRH	R0, [R0, #0]
SXTH	R0, R0
MOVW	R2, #lo_addr(simpleserial_converted_byte_buffer+0)
MOVT	R2, #hi_addr(simpleserial_converted_byte_buffer+0)
MOVW	R1, #lo_addr(simpleserial_received_ascii_buffer+0)
MOVT	R1, #hi_addr(simpleserial_received_ascii_buffer+0)
BL	_hex_decode+0
CMP	R0, #0
IT	EQ
BEQ	L_simpleserial_get60
;simpleserial.c,187 :: 		dbg_puts("ILLEGAL.");
MOVW	R0, #lo_addr(?lstr10_simpleserial+0)
MOVT	R0, #hi_addr(?lstr10_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,188 :: 		return;
IT	AL
BAL	L_end_simpleserial_get
;simpleserial.c,189 :: 		}
L_simpleserial_get60:
;simpleserial.c,192 :: 		dbg_puts("CALLBACK");
MOVW	R0, #lo_addr(?lstr11_simpleserial+0)
MOVT	R0, #hi_addr(?lstr11_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,194 :: 		ack_value[0] = commands[received_command].fp(converted_byte_buffer);
MOVW	R0, #lo_addr(simpleserial_received_command+0)
MOVT	R0, #hi_addr(simpleserial_received_command+0)
LDRSH	R0, [R0, #0]
LSLS	R1, R0, #3
MOVW	R0, #lo_addr(_commands+0)
MOVT	R0, #hi_addr(_commands+0)
ADDS	R0, R0, R1
ADDS	R0, R0, #4
LDR	R1, [R0, #0]
MOVW	R0, #lo_addr(simpleserial_converted_byte_buffer+0)
MOVT	R0, #hi_addr(simpleserial_converted_byte_buffer+0)
BLX	R1
MOVW	R1, #lo_addr(simpleserial_ack_value+0)
MOVT	R1, #hi_addr(simpleserial_ack_value+0)
STRB	R0, [R1, #0]
;simpleserial.c,195 :: 		}
L_simpleserial_get55:
;simpleserial.c,197 :: 		dbg_puts("END GET");
MOVW	R0, #lo_addr(?lstr12_simpleserial+0)
MOVT	R0, #hi_addr(?lstr12_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,200 :: 		dbg_puts("SEND ACK");
MOVW	R0, #lo_addr(?lstr13_simpleserial+0)
MOVT	R0, #hi_addr(?lstr13_simpleserial+0)
BL	_dbg_puts+0
;simpleserial.c,201 :: 		simpleserial_put('z', 1, ack_value);
MOVW	R2, #lo_addr(simpleserial_ack_value+0)
MOVT	R2, #hi_addr(simpleserial_ack_value+0)
MOVS	R1, #1
SXTH	R1, R1
MOVS	R0, #122
BL	_simpleserial_put+0
;simpleserial.c,202 :: 		}
L_end_simpleserial_get:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _simpleserial_get
_simpleserial_put:
;simpleserial.c,204 :: 		void simpleserial_put(char c, int size, uint8_t* output)
; c start address is: 0 (R0)
SUB	SP, SP, #16
STR	LR, [SP, #0]
STRH	R1, [SP, #8]
STR	R2, [SP, #12]
; c end address is: 0 (R0)
; c start address is: 0 (R0)
;simpleserial.c,206 :: 		int i = 0;
;simpleserial.c,208 :: 		putch(c);
; c end address is: 0 (R0)
BL	_putch+0
;simpleserial.c,211 :: 		for(i = 0; i < size; i++)
; i start address is: 8 (R2)
MOVS	R2, #0
SXTH	R2, R2
; i end address is: 8 (R2)
SXTH	R0, R2
L_simpleserial_put61:
; i start address is: 0 (R0)
LDRSH	R3, [SP, #8]
CMP	R0, R3
IT	GE
BGE	L_simpleserial_put62
;simpleserial.c,213 :: 		putch(hex_lookup[output[i] >> 4 ]);
LDR	R3, [SP, #12]
ADDS	R3, R3, R0
LDRB	R3, [R3, #0]
LSRS	R4, R3, #4
UXTB	R4, R4
MOVW	R3, #lo_addr(simpleserial_hex_lookup+0)
MOVT	R3, #hi_addr(simpleserial_hex_lookup+0)
ADDS	R3, R3, R4
LDRB	R3, [R3, #0]
STRH	R0, [SP, #4]
UXTB	R0, R3
BL	_putch+0
LDRSH	R0, [SP, #4]
;simpleserial.c,214 :: 		putch(hex_lookup[output[i] & 0xF]);
LDR	R3, [SP, #12]
ADDS	R3, R3, R0
LDRB	R3, [R3, #0]
AND	R4, R3, #15
UXTB	R4, R4
MOVW	R3, #lo_addr(simpleserial_hex_lookup+0)
MOVT	R3, #hi_addr(simpleserial_hex_lookup+0)
ADDS	R3, R3, R4
LDRB	R3, [R3, #0]
STRH	R0, [SP, #4]
UXTB	R0, R3
BL	_putch+0
LDRSH	R0, [SP, #4]
;simpleserial.c,211 :: 		for(i = 0; i < size; i++)
ADDS	R3, R0, #1
; i end address is: 0 (R0)
; i start address is: 8 (R2)
SXTH	R2, R3
;simpleserial.c,215 :: 		}
SXTH	R0, R2
; i end address is: 8 (R2)
IT	AL
BAL	L_simpleserial_put61
L_simpleserial_put62:
;simpleserial.c,218 :: 		putch('\n');
MOVS	R0, #10
BL	_putch+0
;simpleserial.c,219 :: 		}
L_end_simpleserial_put:
LDR	LR, [SP, #0]
ADD	SP, SP, #16
BX	LR
; end of _simpleserial_put
