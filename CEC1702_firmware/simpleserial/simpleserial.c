// simpleserial.c

#include "simpleserial.h"
#include <stdint.h>
#include "../device_setup/hal.h"
#include "../debug.h"
#include "../simpleserial/functions.h"
#include "../RSA_implementation/BIGI/bigi_communication.h"


ss_cmd commands[MAX_SS_CMDS];
int num_commands = 0;


/*global static variables used in simpleserial_get function - START*/
static char receive_buffer[2*MAX_SS_LEN + 1]; // + 1 - command
static char received_ascii_buffer[2*MAX_SS_LEN];
static uint8_t converted_byte_buffer[MAX_SS_LEN];
static char received_char;
static int received_command;
static int index;
static uint8_t ack_value[1];
static char full_buffer_flag;
/*global static variables used in simpleserial_get function - END*/

static char hex_lookup[16] =
{
        '0', '1', '2', '3', '4', '5', '6', '7',
        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

int hex_decode(int len, char* ascii_buf, uint8_t* data_buf) {
        int i = 0;
        for(i = 0; i < len; i++)
        {
                char n_hi = ascii_buf[2*i];
                char n_lo = ascii_buf[2*i+1];

                if(n_lo >= '0' && n_lo <= '9')
                        data_buf[i] = n_lo - '0';
                else if(n_lo >= 'A' && n_lo <= 'F')
                        data_buf[i] = n_lo - 'A' + 10;
                else if(n_lo >= 'a' && n_lo <= 'f')
                        data_buf[i] = n_lo - 'a' + 10;
                else
                        return 1;

                if(n_hi >= '0' && n_hi <= '9')
                        data_buf[i] |= (n_hi - '0') << 4;
                else if(n_hi >= 'A' && n_hi <= 'F')
                        data_buf[i] |= (n_hi - 'A' + 10) << 4;
                else if(n_hi >= 'a' && n_hi <= 'f')
                        data_buf[i] |= (n_hi - 'a' + 10) << 4;
                else
                        return 1;
        }

        return 0;
}

void simpleserial_init() {
        //basic commands
        simpleserial_addcmd('v',  0, check_version);
        simpleserial_addcmd('x',  0, reset);
        simpleserial_addcmd('k', 16, only_return);
        simpleserial_addcmd('K',  0, only_return);
        simpleserial_addcmd('p', 16, echo);
        simpleserial_addcmd('c', 16, echo);

        //setup commands
        simpleserial_addcmd('o', 1, set_operation);
        simpleserial_addcmd('i', 1, set_implementation);
        simpleserial_addcmd('l', 4, set_length);
#ifdef DEMO
        simpleserial_addcmd('a', 0, demo128_bigi_add);
        simpleserial_addcmd('g', 0, demo128_bigi_gcd);
        simpleserial_addcmd('e', 0, demo128_bigi_mod_exp);
        simpleserial_addcmd('m', 0, demo128_bigi_mod_exp_mont);
#endif //DEMO
}

int simpleserial_addcmd(char c, unsigned int len, uint8_t (*fp)(uint8_t*))
{
        uint8_t i;
        for (i = 0; i < MAX_SS_CMDS; ++i) {
                // currently using command is changed
                if (commands[i].c == c) {
                        commands[i].c   = c;
                        commands[i].len = len;
                        commands[i].fp  = fp;
                        return 0;
                }
        }

        if(num_commands >= MAX_SS_CMDS)
                return 1;

        if(len >= MAX_SS_LEN)
                return 1;

        commands[num_commands].c   = c;
        commands[num_commands].len = len;
        commands[num_commands].fp  = fp;
        num_commands++;

        return 0;
}

void simpleserial_get(void)
{
        dbg_puts_labeled("IN GET");
        received_char = 0;
        received_command = -1;
        index = 0;
        ack_value[0] = 0;
        full_buffer_flag = 1;
        //get data from uart
        for (index = 0; index < 2*MAX_SS_LEN + 1; ++index) {
            receive_buffer[index] = getch();
            putch(receive_buffer[index]);
             if (receive_buffer[index] == '\n' || receive_buffer[index] == '\r') {
               full_buffer_flag = 0;
               break;
            }
        }
        //get last \n
        if (full_buffer_flag) {
           received_char = getch();
           if (received_char != '\n' && received_char != '\r' ) {
              dbg_puts("No ENDL");
              dbg_putch('|');
              dbg_putch(received_char);
              dbg_putch('|');
              return;
           }
        }


        for(received_command = 0; received_command < num_commands; received_command++) {

                if(commands[received_command].c == receive_buffer[0])
                        break;
        }
         dbg_puts("Cmd: ");
         dbg_putch(receive_buffer[0]);

        // If we didn't find a match, give up right away
        if(received_command == num_commands){
               dbg_puts("Not a match.");
               return;
        }

        if (receive_buffer[0] == 'o' || receive_buffer[0] == 'i' || receive_buffer[0] == 'l') {
                //check correct received data lenght
                if ((index - 1) != commands[received_command].len) {
                   dbg_puts("GET: SHORT MSG");
                   return;
                }

                dbg_puts("BUF: ");
                for (index = 0; index < commands[received_command].len; ++index) {
                    received_ascii_buffer[index] = receive_buffer[index + 1];
                    dbg_putch(receive_buffer[index + 1]);
                }

                // Callback
                dbg_puts("CALLBACK");

                ack_value[0] = commands[received_command].fp(received_ascii_buffer);
        }
        else {
                //check correct received data lenght
                if ((index - 1) != 2*commands[received_command].len) {
                   dbg_puts("Msg too short.");
                   return;
                }

                dbg_puts("BUF: ");
                for (index = 0; index < 2*commands[received_command].len; ++index) {
                    received_ascii_buffer[index] = receive_buffer[index + 1];
                    dbg_putch(received_ascii_buffer[index]);
                }

                // ASCII buffer is full: convert to bytes
                // Check for illegal characters here
                if(hex_decode(commands[received_command].len, received_ascii_buffer, converted_byte_buffer)) {
                     dbg_puts("ILLEGAL.");
                     return;
                }

                // Callback
                dbg_puts("CALLBACK");

                ack_value[0] = commands[received_command].fp(converted_byte_buffer);
        }
        
        dbg_puts("END GET");

        // Acknowledge (if version is 1.1)
        dbg_puts("SEND ACK");
        simpleserial_put('z', 1, ack_value);
}

void simpleserial_put(char c, int size, uint8_t* output)
{
        int i = 0;
        // Write first character
        putch(c);

        // Write each byte as two nibbles
        for(i = 0; i < size; i++)
        {
                putch(hex_lookup[output[i] >> 4 ]);
                putch(hex_lookup[output[i] & 0xF]);
        }

        // Write trailing '\n'
        putch('\n');
}