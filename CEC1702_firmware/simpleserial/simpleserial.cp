#line 1 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/simpleserial/simpleserial.c"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/simpleserial.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"





typedef signed char int8_t;
typedef signed int int16_t;
typedef signed long int int32_t;
typedef signed long long int64_t;


typedef unsigned char uint8_t;
typedef unsigned int uint16_t;
typedef unsigned long int uint32_t;
typedef unsigned long long uint64_t;


typedef signed char int_least8_t;
typedef signed int int_least16_t;
typedef signed long int int_least32_t;
typedef signed long long int_least64_t;


typedef unsigned char uint_least8_t;
typedef unsigned int uint_least16_t;
typedef unsigned long int uint_least32_t;
typedef unsigned long long uint_least64_t;



typedef signed long int int_fast8_t;
typedef signed long int int_fast16_t;
typedef signed long int int_fast32_t;
typedef signed long long int_fast64_t;


typedef unsigned long int uint_fast8_t;
typedef unsigned long int uint_fast16_t;
typedef unsigned long int uint_fast32_t;
typedef unsigned long long uint_fast64_t;


typedef signed long int intptr_t;
typedef unsigned long int uintptr_t;


typedef signed long long intmax_t;
typedef unsigned long long uintmax_t;
#line 14 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/simpleserial.h"
typedef struct ss_cmd
{
 char c;
 unsigned int len;
 uint8_t (*fp)(uint8_t*);
} ss_cmd;



void simpleserial_init(void);
#line 39 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/simpleserial.h"
int simpleserial_addcmd(char c, unsigned int len, uint8_t (*fp)(uint8_t*));







void simpleserial_get(void);




void simpleserial_put(char c, int size, uint8_t* output);
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/../device_setup/hal.h"



void putch(char c);

char getch(void);

void platform_init(void);

void init_uart(void);

void trigger_setup(void);

void trigger_low(void);

void trigger_high(void);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/../debug.h"




void dbg_putch (char c);

void dbg_puts (char * str);

void dbg_putch_labeled (char c);

void dbg_puts_labeled (char * str);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/../simpleserial/functions.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 1 "c:/mikroc pro for arm/include/stdlib.h"







 typedef struct divstruct {
 int quot;
 int rem;
 } div_t;

 typedef struct ldivstruct {
 long quot;
 long rem;
 } ldiv_t;

 typedef struct uldivstruct {
 unsigned long quot;
 unsigned long rem;
 } uldiv_t;

int abs(int a);
float atof(char * s);
int atoi(char * s);
long atol(char * s);
div_t div(int number, int denom);
ldiv_t ldiv(long number, long denom);
uldiv_t uldiv(unsigned long number, unsigned long denom);
long labs(long x);
long int max(long int a, long int b);
long int min(long int a, long int b);
void srand(unsigned x);
int rand();
int xtoi(char * s);
#line 8 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/../simpleserial/functions.h"
uint8_t echo (uint8_t* pt);

uint8_t only_return (uint8_t* pt);

uint8_t reset (uint8_t* params);


uint8_t check_version(uint8_t* v);

uint8_t set_operation (uint8_t* operation);

uint8_t set_implementation (uint8_t* implementation);

uint8_t set_length (uint8_t* length);
#line 1 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/../rsa_implementation/bigi/bigi_communication.h"
#line 1 "c:/mikroc pro for arm/include/stdint.h"
#line 9 "c:/users/ludan/onedrive/�kola/fit/bakalarka/cec1702_fw/cec1702_firmware/simpleserial/../rsa_implementation/bigi/bigi_communication.h"
uint8_t demo128_bigi_add (uint8_t* pt);

uint8_t demo128_bigi_mod_exp (uint8_t* pt);

uint8_t demo128_bigi_gcd (uint8_t* pt);

uint8_t demo128_bigi_mod_exp_mont (uint8_t* pt);
#line 11 "C:/Users/ludan/OneDrive/�kola/FIT/BAKALARKA/CEC1702_FW/CEC1702_firmware/simpleserial/simpleserial.c"
ss_cmd commands[ 20 ];
int num_commands = 0;



static char receive_buffer[2* 64  + 1];
static char received_ascii_buffer[2* 64 ];
static uint8_t converted_byte_buffer[ 64 ];
static char received_char;
static int received_command;
static int index;
static uint8_t ack_value[1];
static char full_buffer_flag;


static char hex_lookup[16] =
{
 '0', '1', '2', '3', '4', '5', '6', '7',
 '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
};

int hex_decode(int len, char* ascii_buf, uint8_t* data_buf) {
 int i = 0;
 for(i = 0; i < len; i++)
 {
 char n_hi = ascii_buf[2*i];
 char n_lo = ascii_buf[2*i+1];

 if(n_lo >= '0' && n_lo <= '9')
 data_buf[i] = n_lo - '0';
 else if(n_lo >= 'A' && n_lo <= 'F')
 data_buf[i] = n_lo - 'A' + 10;
 else if(n_lo >= 'a' && n_lo <= 'f')
 data_buf[i] = n_lo - 'a' + 10;
 else
 return 1;

 if(n_hi >= '0' && n_hi <= '9')
 data_buf[i] |= (n_hi - '0') << 4;
 else if(n_hi >= 'A' && n_hi <= 'F')
 data_buf[i] |= (n_hi - 'A' + 10) << 4;
 else if(n_hi >= 'a' && n_hi <= 'f')
 data_buf[i] |= (n_hi - 'a' + 10) << 4;
 else
 return 1;
 }

 return 0;
}

void simpleserial_init() {

 simpleserial_addcmd('v', 0, check_version);
 simpleserial_addcmd('x', 0, reset);
 simpleserial_addcmd('k', 16, only_return);
 simpleserial_addcmd('K', 0, only_return);
 simpleserial_addcmd('p', 16, echo);
 simpleserial_addcmd('c', 16, echo);


 simpleserial_addcmd('o', 1, set_operation);
 simpleserial_addcmd('i', 1, set_implementation);
 simpleserial_addcmd('l', 4, set_length);

 simpleserial_addcmd('a', 0, demo128_bigi_add);
 simpleserial_addcmd('g', 0, demo128_bigi_gcd);
 simpleserial_addcmd('e', 0, demo128_bigi_mod_exp);
 simpleserial_addcmd('m', 0, demo128_bigi_mod_exp_mont);

}

int simpleserial_addcmd(char c, unsigned int len, uint8_t (*fp)(uint8_t*))
{
 uint8_t i;
 for (i = 0; i <  20 ; ++i) {

 if (commands[i].c == c) {
 commands[i].c = c;
 commands[i].len = len;
 commands[i].fp = fp;
 return 0;
 }
 }

 if(num_commands >=  20 )
 return 1;

 if(len >=  64 )
 return 1;

 commands[num_commands].c = c;
 commands[num_commands].len = len;
 commands[num_commands].fp = fp;
 num_commands++;

 return 0;
}

void simpleserial_get(void)
{
 dbg_puts_labeled("IN GET");
 received_char = 0;
 received_command = -1;
 index = 0;
 ack_value[0] = 0;
 full_buffer_flag = 1;

 for (index = 0; index < 2* 64  + 1; ++index) {
 receive_buffer[index] = getch();
 putch(receive_buffer[index]);
 if (receive_buffer[index] == '\n' || receive_buffer[index] == '\r') {
 full_buffer_flag = 0;
 break;
 }
 }

 if (full_buffer_flag) {
 received_char = getch();
 if (received_char != '\n' && received_char != '\r' ) {
 dbg_puts("No ENDL");
 dbg_putch('|');
 dbg_putch(received_char);
 dbg_putch('|');
 return;
 }
 }


 for(received_command = 0; received_command < num_commands; received_command++) {

 if(commands[received_command].c == receive_buffer[0])
 break;
 }
 dbg_puts("Cmd: ");
 dbg_putch(receive_buffer[0]);


 if(received_command == num_commands){
 dbg_puts("Not a match.");
 return;
 }

 if (receive_buffer[0] == 'o' || receive_buffer[0] == 'i' || receive_buffer[0] == 'l') {

 if ((index - 1) != commands[received_command].len) {
 dbg_puts("GET: SHORT MSG");
 return;
 }

 dbg_puts("BUF: ");
 for (index = 0; index < commands[received_command].len; ++index) {
 received_ascii_buffer[index] = receive_buffer[index + 1];
 dbg_putch(receive_buffer[index + 1]);
 }


 dbg_puts("CALLBACK");

 ack_value[0] = commands[received_command].fp(received_ascii_buffer);
 }
 else {

 if ((index - 1) != 2*commands[received_command].len) {
 dbg_puts("Msg too short.");
 return;
 }

 dbg_puts("BUF: ");
 for (index = 0; index < 2*commands[received_command].len; ++index) {
 received_ascii_buffer[index] = receive_buffer[index + 1];
 dbg_putch(received_ascii_buffer[index]);
 }



 if(hex_decode(commands[received_command].len, received_ascii_buffer, converted_byte_buffer)) {
 dbg_puts("ILLEGAL.");
 return;
 }


 dbg_puts("CALLBACK");

 ack_value[0] = commands[received_command].fp(converted_byte_buffer);
 }

 dbg_puts("END GET");


 dbg_puts("SEND ACK");
 simpleserial_put('z', 1, ack_value);
}

void simpleserial_put(char c, int size, uint8_t* output)
{
 int i = 0;

 putch(c);


 for(i = 0; i < size; i++)
 {
 putch(hex_lookup[output[i] >> 4 ]);
 putch(hex_lookup[output[i] & 0xF]);
 }


 putch('\n');
}
