#ifndef CEC1702_SETUP_H
#define CEC1702_SETUP_H


void init_uart(void);

void putch(char c);

char getch(void);

void platform_init(void);

void trigger_setup(void);

void trigger_high(void);

void trigger_low(void);


#endif //CEC1702_SETUP_H