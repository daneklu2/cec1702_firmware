#include "cec1702_setup.h"

void init_uart(void)
{
    UART0_Init(38400);
    Delay_ms(100);
}

void putch(char c)
{
    UART0_Write(c);
}

char getch(void)
{
    return UART0_Read();
}


void platform_init(void)
{
   PWM5_INST_COUNTER_ON_TIME = 1;
   PWM5_INST_COUNTER_OFF_TIME = 1;
   PWM5_INST_CONFIG.B1 = 0;
   PWM5_INST_CONFIG.B2 = 0;
   PWM5_INST_CONFIG.B3 = 0;
   PWM5_INST_CONFIG.B4 = 0;
   PWM5_INST_CONFIG.B5 = 0;
   PWM5_INST_CONFIG.B6 = 0;
   PWM5_INST_CONFIG.B0 = 1;
   GPIO_PIN_CONTROL_002BITS.MUX = 1;
   aes_hash_power(1);
   aes_hash_reset();

}

void trigger_setup(void)
{
    GPIO_Digital_Output(&GPIO_PORT_010_017, _GPIO_PINMASK_7);
    GPIO_Digital_Output(&GPIO_PORT_150_157, _GPIO_PINMASK_6 | _GPIO_PINMASK_7);
    GPIO_OUTPUT_010_017.B7 = 0;
    GPIO_OUTPUT_150_157.B6 = 1;
}

void trigger_high(void)
{
    GPIO_OUTPUT_010_017.B7 = 1;
}

void trigger_low(void)
{
    GPIO_OUTPUT_010_017.B7 = 0;
}