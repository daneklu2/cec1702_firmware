_init_uart:
;cec1702_setup.c,3 :: 		void init_uart(void)
SUB	SP, SP, #4
STR	LR, [SP, #0]
;cec1702_setup.c,5 :: 		UART0_Init(38400);
MOVW	R0, #38400
BL	_UART0_Init+0
;cec1702_setup.c,6 :: 		Delay_ms(100);
MOVW	R7, #27134
MOVT	R7, #24
NOP
NOP
L_init_uart0:
SUBS	R7, R7, #1
BNE	L_init_uart0
NOP
NOP
NOP
;cec1702_setup.c,7 :: 		}
L_end_init_uart:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _init_uart
_putch:
;cec1702_setup.c,9 :: 		void putch(char c)
; c start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
; c end address is: 0 (R0)
; c start address is: 0 (R0)
;cec1702_setup.c,11 :: 		UART0_Write(c);
; c end address is: 0 (R0)
BL	_UART0_Write+0
;cec1702_setup.c,12 :: 		}
L_end_putch:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _putch
_getch:
;cec1702_setup.c,14 :: 		char getch(void)
SUB	SP, SP, #4
STR	LR, [SP, #0]
;cec1702_setup.c,16 :: 		return UART0_Read();
BL	_UART0_Read+0
UXTB	R0, R0
;cec1702_setup.c,17 :: 		}
L_end_getch:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _getch
_platform_init:
;cec1702_setup.c,20 :: 		void platform_init(void)
SUB	SP, SP, #4
STR	LR, [SP, #0]
;cec1702_setup.c,22 :: 		PWM5_INST_COUNTER_ON_TIME = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(PWM5_INST_COUNTER_ON_TIME+0)
MOVT	R0, #hi_addr(PWM5_INST_COUNTER_ON_TIME+0)
STR	R1, [R0, #0]
;cec1702_setup.c,23 :: 		PWM5_INST_COUNTER_OFF_TIME = 1;
MOVS	R1, #1
MOVW	R0, #lo_addr(PWM5_INST_COUNTER_OFF_TIME+0)
MOVT	R0, #hi_addr(PWM5_INST_COUNTER_OFF_TIME+0)
STR	R1, [R0, #0]
;cec1702_setup.c,24 :: 		PWM5_INST_CONFIG.B1 = 0;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
BFC	R0, #1, #1
STR	R0, [R1, #0]
;cec1702_setup.c,25 :: 		PWM5_INST_CONFIG.B2 = 0;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
BFC	R0, #2, #1
STR	R0, [R1, #0]
;cec1702_setup.c,26 :: 		PWM5_INST_CONFIG.B3 = 0;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
BFC	R0, #3, #1
STR	R0, [R1, #0]
;cec1702_setup.c,27 :: 		PWM5_INST_CONFIG.B4 = 0;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
BFC	R0, #4, #1
STR	R0, [R1, #0]
;cec1702_setup.c,28 :: 		PWM5_INST_CONFIG.B5 = 0;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
BFC	R0, #5, #1
STR	R0, [R1, #0]
;cec1702_setup.c,29 :: 		PWM5_INST_CONFIG.B6 = 0;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
BFC	R0, #6, #1
STR	R0, [R1, #0]
;cec1702_setup.c,30 :: 		PWM5_INST_CONFIG.B0 = 1;
MOVW	R1, #lo_addr(PWM5_INST_CONFIG+0)
MOVT	R1, #hi_addr(PWM5_INST_CONFIG+0)
LDR	R0, [R1, #0]
ORR	R0, R0, #1
STR	R0, [R1, #0]
;cec1702_setup.c,31 :: 		GPIO_PIN_CONTROL_002BITS.MUX = 1;
MOVS	R2, #1
MOVW	R1, #lo_addr(GPIO_PIN_CONTROL_002bits+0)
MOVT	R1, #hi_addr(GPIO_PIN_CONTROL_002bits+0)
LDRH	R0, [R1, #0]
BFI	R0, R2, #12, #2
STRH	R0, [R1, #0]
;cec1702_setup.c,32 :: 		aes_hash_power(1);
MOVS	R0, #1
BL	_aes_hash_power+0
;cec1702_setup.c,33 :: 		aes_hash_reset();
BL	_aes_hash_reset+0
;cec1702_setup.c,35 :: 		}
L_end_platform_init:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _platform_init
_trigger_setup:
;cec1702_setup.c,37 :: 		void trigger_setup(void)
SUB	SP, SP, #4
STR	LR, [SP, #0]
;cec1702_setup.c,39 :: 		GPIO_Digital_Output(&GPIO_PORT_010_017, _GPIO_PINMASK_7);
MOVW	R1, #128
MOVW	R0, #lo_addr(GPIO_PORT_010_017+0)
MOVT	R0, #hi_addr(GPIO_PORT_010_017+0)
BL	_GPIO_Digital_Output+0
;cec1702_setup.c,40 :: 		GPIO_Digital_Output(&GPIO_PORT_150_157, _GPIO_PINMASK_6 | _GPIO_PINMASK_7);
MOVW	R1, #192
MOVW	R0, #lo_addr(GPIO_PORT_150_157+0)
MOVT	R0, #hi_addr(GPIO_PORT_150_157+0)
BL	_GPIO_Digital_Output+0
;cec1702_setup.c,41 :: 		GPIO_OUTPUT_010_017.B7 = 0;
MOVW	R1, #lo_addr(GPIO_OUTPUT_010_017+0)
MOVT	R1, #hi_addr(GPIO_OUTPUT_010_017+0)
_LX	[R1, ByteOffset(GPIO_OUTPUT_010_017+0)]
BFC	R0, #7, #1
_SX	[R1, ByteOffset(GPIO_OUTPUT_010_017+0)]
;cec1702_setup.c,42 :: 		GPIO_OUTPUT_150_157.B6 = 1;
MOVW	R1, #lo_addr(GPIO_OUTPUT_150_157+0)
MOVT	R1, #hi_addr(GPIO_OUTPUT_150_157+0)
_LX	[R1, ByteOffset(GPIO_OUTPUT_150_157+0)]
ORR	R0, R0, #64
_SX	[R1, ByteOffset(GPIO_OUTPUT_150_157+0)]
;cec1702_setup.c,43 :: 		}
L_end_trigger_setup:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _trigger_setup
_trigger_high:
;cec1702_setup.c,45 :: 		void trigger_high(void)
SUB	SP, SP, #4
;cec1702_setup.c,47 :: 		GPIO_OUTPUT_010_017.B7 = 1;
MOVW	R1, #lo_addr(GPIO_OUTPUT_010_017+0)
MOVT	R1, #hi_addr(GPIO_OUTPUT_010_017+0)
_LX	[R1, ByteOffset(GPIO_OUTPUT_010_017+0)]
ORR	R0, R0, #128
_SX	[R1, ByteOffset(GPIO_OUTPUT_010_017+0)]
;cec1702_setup.c,48 :: 		}
L_end_trigger_high:
ADD	SP, SP, #4
BX	LR
; end of _trigger_high
_trigger_low:
;cec1702_setup.c,50 :: 		void trigger_low(void)
SUB	SP, SP, #4
;cec1702_setup.c,52 :: 		GPIO_OUTPUT_010_017.B7 = 0;
MOVW	R1, #lo_addr(GPIO_OUTPUT_010_017+0)
MOVT	R1, #hi_addr(GPIO_OUTPUT_010_017+0)
_LX	[R1, ByteOffset(GPIO_OUTPUT_010_017+0)]
BFC	R0, #7, #1
_SX	[R1, ByteOffset(GPIO_OUTPUT_010_017+0)]
;cec1702_setup.c,53 :: 		}
L_end_trigger_low:
ADD	SP, SP, #4
BX	LR
; end of _trigger_low
